--------------------------------------------------------
--  File created - Sunday-August-18-2013   
--------------------------------------------------------
DROP TABLE "DLM"."ARTIKEL" cascade constraints;
DROP TABLE "DLM"."BANTUAN_LAINNYA" cascade constraints;
DROP TABLE "DLM"."BANTUAN_MODAL" cascade constraints;
DROP TABLE "DLM"."BANTUAN_SOSIAL" cascade constraints;
DROP TABLE "DLM"."BIODATA" cascade constraints;
DROP TABLE "DLM"."DAYAH" cascade constraints;
DROP TABLE "DLM"."GAMPONG" cascade constraints;
DROP TABLE "DLM"."INSTANSI" cascade constraints;
DROP TABLE "DLM"."JENIS_PMKS" cascade constraints;
DROP TABLE "DLM"."KABUPATEN" cascade constraints;
DROP TABLE "DLM"."KECAMATAN" cascade constraints;
DROP TABLE "DLM"."KELAS_BANTUAN" cascade constraints;
DROP TABLE "DLM"."KELAS_BANTUAN_LAINNYA" cascade constraints;
DROP TABLE "DLM"."KELOMPOK" cascade constraints;
DROP TABLE "DLM"."KEPALA_KELUARGA" cascade constraints;
DROP TABLE "DLM"."KEPALA_KELUARGA_DETAILS" cascade constraints;
DROP TABLE "DLM"."KEPEMILIKAN_ASSET" cascade constraints;
DROP TABLE "DLM"."KOPERASI" cascade constraints;
DROP TABLE "DLM"."PANTI_ASUHAN" cascade constraints;
DROP TABLE "DLM"."PEKERJAAN" cascade constraints;
DROP TABLE "DLM"."PENYAKIT" cascade constraints;
DROP TABLE "DLM"."PMKS" cascade constraints;
DROP TABLE "DLM"."PUSKESMAS" cascade constraints;
DROP TABLE "DLM"."RIWAYAT_KESEHATAN" cascade constraints;
DROP TABLE "DLM"."RIWAYAT_PEKERJAAN" cascade constraints;
DROP TABLE "DLM"."RIWAYAT_PENDIDIKAN" cascade constraints;
DROP TABLE "DLM"."RUMAH" cascade constraints;
DROP TABLE "DLM"."SEKOLAH" cascade constraints;
DROP TABLE "DLM"."USER" cascade constraints;
DROP TABLE "DLM"."ZAKAT" cascade constraints;
DROP SEQUENCE "DLM"."SEC_ARTIKEL";
DROP SEQUENCE "DLM"."SEC_BANTUAN_LAINNYA";
DROP SEQUENCE "DLM"."SEC_BANTUAN_MODAL";
DROP SEQUENCE "DLM"."SEC_BANTUAN_SOSIAL";
DROP SEQUENCE "DLM"."SEC_DAYAH";
DROP SEQUENCE "DLM"."SEC_GAMPONG";
DROP SEQUENCE "DLM"."SEC_INSTANSI";
DROP SEQUENCE "DLM"."SEC_JENIS_PMKS";
DROP SEQUENCE "DLM"."SEC_KABUPATEN";
DROP SEQUENCE "DLM"."SEC_KECAMATAN";
DROP SEQUENCE "DLM"."SEC_KELAS_BANTUAN";
DROP SEQUENCE "DLM"."SEC_KELAS_BANTUAN_LAINNYA";
DROP SEQUENCE "DLM"."SEC_KELOMPOK";
DROP SEQUENCE "DLM"."SEC_KEPALA_KELUARGA_DETAILS";
DROP SEQUENCE "DLM"."SEC_KEPEMILIKAN_ASSET";
DROP SEQUENCE "DLM"."SEC_KOPERASI";
DROP SEQUENCE "DLM"."SEC_PANTI_ASUHAN";
DROP SEQUENCE "DLM"."SEC_PEKERJAAN";
DROP SEQUENCE "DLM"."SEC_PENYAKIT";
DROP SEQUENCE "DLM"."SEC_PMKS";
DROP SEQUENCE "DLM"."SEC_PUSKESMAS";
DROP SEQUENCE "DLM"."SEC_RIWAYAT_KESEHATAN";
DROP SEQUENCE "DLM"."SEC_RIWAYAT_PEKERJAAN";
DROP SEQUENCE "DLM"."SEC_RIWAYAT_PENDIDIKAN";
DROP SEQUENCE "DLM"."SEC_RUMAH";
DROP SEQUENCE "DLM"."SEC_SEKOLAH";
DROP SEQUENCE "DLM"."SEC_USER";
DROP SEQUENCE "DLM"."SEC_ZAKAT";
DROP VIEW "DLM"."VIEW_BIODATA";
DROP VIEW "DLM"."VIEW_KEPALA_KELUARGA";
DROP VIEW "DLM"."VIEW_LAPORAN_BIODATA";
DROP VIEW "DLM"."VIEW_LAPORAN_PENDIDIKAN";
DROP VIEW "DLM"."VIEW_LAPORAN_PMKS";
DROP VIEW "DLM"."VIEW_LAPORAN_RUMAH";
DROP FUNCTION "DLM"."GETHUBUNGANKELUARGATERAKHIR";
DROP FUNCTION "DLM"."GETKEPALAKELUARGABYNOKK";
DROP FUNCTION "DLM"."GETNIKNAMAAYAH";
DROP FUNCTION "DLM"."GETNIKNAMAIBU";
DROP FUNCTION "DLM"."GETNOKKTERAKHIR";
DROP FUNCTION "DLM"."GETPEKERJAANTERAKHIRKKBYNOKK";
DROP FUNCTION "DLM"."GETPENGHASILANTERAKHIRKKBYNOKK";
DROP FUNCTION "DLM"."GETRIWAYATPEKERJAANTERAKHIR";
DROP FUNCTION "DLM"."GETRIWAYATPENDIDIKANTERAKHIR";
DROP FUNCTION "DLM"."GETRIWAYATPENDIDIKANTERAKHIRID";
--------------------------------------------------------
--  DDL for Sequence SEC_ARTIKEL
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_ARTIKEL"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_BANTUAN_LAINNYA
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_BANTUAN_LAINNYA"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_BANTUAN_MODAL
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_BANTUAN_MODAL"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_BANTUAN_SOSIAL
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_BANTUAN_SOSIAL"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_DAYAH
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_DAYAH"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 3 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_GAMPONG
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_GAMPONG"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_INSTANSI
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_INSTANSI"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_JENIS_PMKS
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_JENIS_PMKS"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 27 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_KABUPATEN
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_KABUPATEN"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_KECAMATAN
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_KECAMATAN"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 3 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_KELAS_BANTUAN
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_KELAS_BANTUAN"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_KELAS_BANTUAN_LAINNYA
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_KELAS_BANTUAN_LAINNYA"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_KELOMPOK
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_KELOMPOK"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_KEPALA_KELUARGA_DETAILS
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_KEPALA_KELUARGA_DETAILS"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_KEPEMILIKAN_ASSET
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_KEPEMILIKAN_ASSET"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_KOPERASI
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_KOPERASI"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_PANTI_ASUHAN
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_PANTI_ASUHAN"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_PEKERJAAN
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_PEKERJAAN"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 8 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_PENYAKIT
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_PENYAKIT"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 7 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_PMKS
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_PMKS"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_PUSKESMAS
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_PUSKESMAS"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_RIWAYAT_KESEHATAN
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_RIWAYAT_KESEHATAN"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_RIWAYAT_PEKERJAAN
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_RIWAYAT_PEKERJAAN"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_RIWAYAT_PENDIDIKAN
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_RIWAYAT_PENDIDIKAN"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_RUMAH
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_RUMAH"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_SEKOLAH
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_SEKOLAH"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 11 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_USER
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_USER"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_ZAKAT
--------------------------------------------------------

   CREATE SEQUENCE  "DLM"."SEC_ZAKAT"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table ARTIKEL
--------------------------------------------------------

  CREATE TABLE "DLM"."ARTIKEL" 
   (	"id" NUMBER(38,0), 
	"text_intro" CLOB, 
	"text_full" CLOB, 
	"is_visible" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE, 
	"title" VARCHAR2(225 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" 
 LOB ("text_intro") STORE AS (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 PCTVERSION 10
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)) 
 LOB ("text_full") STORE AS (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 PCTVERSION 10
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)) ;
--------------------------------------------------------
--  DDL for Table BANTUAN_LAINNYA
--------------------------------------------------------

  CREATE TABLE "DLM"."BANTUAN_LAINNYA" 
   (	"id" NUMBER(38,0), 
	"nik" NVARCHAR2(50), 
	"no_kk" NVARCHAR2(50), 
	"status_bantuan_lainnya" NUMBER(38,0), 
	"jenis_bantuan_lainnya" NVARCHAR2(200), 
	"id_instansi" NUMBER(38,0), 
	"tahun_diberikan" NUMBER(38,0), 
	"kategory_bantuan_lainnya" NUMBER(38,0), 
	"id_kelompok" NUMBER(38,0), 
	"sumber_modal_bantuan_lainnya" NUMBER(38,0), 
	"id_koperasi" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table BANTUAN_MODAL
--------------------------------------------------------

  CREATE TABLE "DLM"."BANTUAN_MODAL" 
   (	"id" NUMBER(38,0), 
	"nik" NVARCHAR2(50), 
	"no_kk" NVARCHAR2(50), 
	"status_bantuan_modal" NUMBER(38,0), 
	"jenis_bantuan_modal" NVARCHAR2(200), 
	"kategory_bantuan_modal" NUMBER(38,0), 
	"id_kelompok" NUMBER(38,0), 
	"id_instansi" NUMBER(38,0), 
	"tahun_diberi" NUMBER(38,0), 
	"sumber_bantuan_modal" NUMBER(38,0), 
	"id_koperasi" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table BANTUAN_SOSIAL
--------------------------------------------------------

  CREATE TABLE "DLM"."BANTUAN_SOSIAL" 
   (	"id" NUMBER(38,0), 
	"nik" NVARCHAR2(50), 
	"no_kk" NVARCHAR2(50), 
	"status_bantuan" NUMBER(38,0), 
	"jenis_bantuan" NVARCHAR2(500), 
	"id_instansi" NUMBER(38,0), 
	"tahun_diberikan" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table BIODATA
--------------------------------------------------------

  CREATE TABLE "DLM"."BIODATA" 
   (	"nik" NVARCHAR2(50), 
	"nama_lgkp" NVARCHAR2(100), 
	"tmpt_lhr" NVARCHAR2(100), 
	"tgl_lhr" DATE, 
	"akta_lhr" NUMBER(38,0), 
	"no_akta_lhr" VARCHAR2(50 BYTE), 
	"gol_drh" NUMBER(38,0), 
	"jenis_kelamin" NUMBER(38,0), 
	"agama" NUMBER(38,0), 
	"stat_kwn" NUMBER(38,0), 
	"akta_kwn" NUMBER(38,0), 
	"no_akta_kwn" VARCHAR2(50 BYTE), 
	"tgl_kwn" DATE, 
	"akta_crai" NUMBER(38,0), 
	"no_akta_crai" NVARCHAR2(2000), 
	"tgl_crai" DATE, 
	"pnydng_cct" NUMBER(38,0), 
	"pddk_akh" NUMBER(38,0), 
	"kode_pkrj" NUMBER(38,0), 
	"stat_hidup" NUMBER(38,0), 
	"no_kab" NUMBER(38,0), 
	"no_kec" NUMBER(38,0), 
	"no_kel" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE, 
	"tgl_meninggal" DATE, 
	"no_akta_kematian" VARCHAR2(50 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table DAYAH
--------------------------------------------------------

  CREATE TABLE "DLM"."DAYAH" 
   (	"id" NUMBER(38,0), 
	"id_kab" NUMBER(38,0), 
	"nama_dayah" NVARCHAR2(100), 
	"nama_pimpinan" NVARCHAR2(100), 
	"type_dayah" NUMBER(38,0), 
	"jlh_pengasuh" NUMBER(38,0), 
	"jlh_santri" NUMBER(38,0), 
	"nama_tingkat" NUMBER(38,0), 
	"jlh_kelas" NUMBER(38,0), 
	"jlh_perpustakaan" NUMBER(38,0), 
	"jlh_lab_komputer" NUMBER(38,0), 
	"jlh_mushalla" NUMBER(38,0), 
	"jlh_kamar_mandi_wc" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table GAMPONG
--------------------------------------------------------

  CREATE TABLE "DLM"."GAMPONG" 
   (	"id" NUMBER(38,0), 
	"id_kab" NUMBER(38,0), 
	"id_kec" NUMBER(38,0), 
	"nama_gampong" NVARCHAR2(100), 
	"alamat" NVARCHAR2(500), 
	"keuchik" NVARCHAR2(100), 
	"visi" VARCHAR2(4000 BYTE), 
	"misi" VARCHAR2(4000 BYTE), 
	"koordinat_x" NVARCHAR2(50), 
	"koordinat_y" NVARCHAR2(50), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table INSTANSI
--------------------------------------------------------

  CREATE TABLE "DLM"."INSTANSI" 
   (	"id" NUMBER(38,0), 
	"id_kab" NUMBER(38,0), 
	"nama_instansi" NVARCHAR2(150), 
	"alamat_instansi" NVARCHAR2(500), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table JENIS_PMKS
--------------------------------------------------------

  CREATE TABLE "DLM"."JENIS_PMKS" 
   (	"id" NUMBER(38,0), 
	"nama_pmks" NVARCHAR2(200), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table KABUPATEN
--------------------------------------------------------

  CREATE TABLE "DLM"."KABUPATEN" 
   (	"id" NUMBER(38,0), 
	"nama_kabkot" NVARCHAR2(100), 
	"ibukota" NVARCHAR2(100), 
	"alamat" NVARCHAR2(500), 
	"bupati" NVARCHAR2(100), 
	"wakil" NVARCHAR2(100), 
	"luas_wilayah" NUMBER(38,0), 
	"visi" VARCHAR2(4000 BYTE), 
	"misi" VARCHAR2(4000 BYTE), 
	"koordinat_x" VARCHAR2(50 BYTE), 
	"koordinat_y" VARCHAR2(50 BYTE), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE, 
	"lambang" NVARCHAR2(100)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table KECAMATAN
--------------------------------------------------------

  CREATE TABLE "DLM"."KECAMATAN" 
   (	"id" NUMBER(38,0), 
	"id_kab" NUMBER(38,0), 
	"nama_kec" NVARCHAR2(100), 
	"ibukota" NVARCHAR2(100), 
	"alamat" NVARCHAR2(500), 
	"camat" NVARCHAR2(100), 
	"visi" VARCHAR2(4000 BYTE), 
	"misi" VARCHAR2(4000 BYTE), 
	"koordinat_x" NVARCHAR2(50), 
	"koordinat_y" NVARCHAR2(500), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table KELAS_BANTUAN
--------------------------------------------------------

  CREATE TABLE "DLM"."KELAS_BANTUAN" 
   (	"id" NUMBER(38,0), 
	"nama_kelas_bantuan" NVARCHAR2(100), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table KELAS_BANTUAN_LAINNYA
--------------------------------------------------------

  CREATE TABLE "DLM"."KELAS_BANTUAN_LAINNYA" 
   (	"id" NUMBER(38,0), 
	"id_bantuan_lainnya" NUMBER(38,0), 
	"id_kelas_bantuan" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table KELOMPOK
--------------------------------------------------------

  CREATE TABLE "DLM"."KELOMPOK" 
   (	"id" NUMBER(38,0), 
	"id_kab" NUMBER(38,0), 
	"nama_kelompok" NVARCHAR2(150), 
	"ketua_kelompok" NVARCHAR2(100), 
	"bidang_usaha" NVARCHAR2(150), 
	"no_telp" NVARCHAR2(20), 
	"alamat" NVARCHAR2(500), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table KEPALA_KELUARGA
--------------------------------------------------------

  CREATE TABLE "DLM"."KEPALA_KELUARGA" 
   (	"no_kk" NVARCHAR2(50), 
	"nik" NVARCHAR2(50), 
	"hub_keluarga" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table KEPALA_KELUARGA_DETAILS
--------------------------------------------------------

  CREATE TABLE "DLM"."KEPALA_KELUARGA_DETAILS" 
   (	"id" NUMBER(38,0), 
	"no_kk" NVARCHAR2(50), 
	"nik" NVARCHAR2(50), 
	"hub_keluarga" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table KEPEMILIKAN_ASSET
--------------------------------------------------------

  CREATE TABLE "DLM"."KEPEMILIKAN_ASSET" 
   (	"id" NUMBER(38,0), 
	"nik" NVARCHAR2(50), 
	"no_kk" NVARCHAR2(50), 
	"punya_tanah_rumah" NUMBER(38,0), 
	"luas_tanah_rumah" NUMBER(38,0), 
	"punya_tanah_sawah" NUMBER(38,0), 
	"luas_tanah_sawah" NUMBER(38,0), 
	"punya_tanah_kebun" NUMBER(38,0), 
	"luast_tanah_kebun" NUMBER(38,0), 
	"punya_tambak" NUMBER(38,0), 
	"luas_tambak" NUMBER(38,0), 
	"punya_lembu" NUMBER(38,0), 
	"jlh_lembu" NUMBER(38,0), 
	"punya_kerbau" NUMBER(38,0), 
	"jlh_kerbau" NUMBER(38,0), 
	"punya_ternak_lain" NUMBER(38,0), 
	"jlh_ternak_lain" NUMBER(38,0), 
	"punya_mobil" NUMBER(38,0), 
	"jlh_mobil" NUMBER(38,0), 
	"punya_motor" NUMBER(38,0), 
	"jlh_motor" NUMBER(38,0), 
	"punya_tabungan" NUMBER(38,0), 
	"punya_asuransi" NUMBER(38,0), 
	"punya_motor_tempel" NUMBER(38,0), 
	"punya_kapal_motor" NUMBER(38,0), 
	"punya_perahu" NUMBER(38,0), 
	"punya_aset_lain" NVARCHAR2(2000), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table KOPERASI
--------------------------------------------------------

  CREATE TABLE "DLM"."KOPERASI" 
   (	"id" NUMBER(38,0), 
	"id_kab" NUMBER(38,0), 
	"nama_koperasi" NVARCHAR2(150), 
	"ketua_koperasi" NVARCHAR2(100), 
	"bidang_usaha" NVARCHAR2(150), 
	"no_telp" NVARCHAR2(20), 
	"alamat" NVARCHAR2(500), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table PANTI_ASUHAN
--------------------------------------------------------

  CREATE TABLE "DLM"."PANTI_ASUHAN" 
   (	"id" NUMBER(38,0), 
	"nama_pantiasuhan" NVARCHAR2(100), 
	"type_pantiasuhan" NUMBER(38,0), 
	"alamat_pantiasuhan" NVARCHAR2(500), 
	"jlh_kamar" NUMBER(38,0), 
	"jlh_perpustakaan" NUMBER(38,0), 
	"jlh_aula" NUMBER(38,0), 
	"jlh_mushalla" NUMBER(38,0), 
	"jlh_kamar_mandi_wc" NUMBER(38,0), 
	"koordinat_x" NVARCHAR2(50), 
	"koordinat_y" NVARCHAR2(50), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table PEKERJAAN
--------------------------------------------------------

  CREATE TABLE "DLM"."PEKERJAAN" 
   (	"id" NUMBER(38,0), 
	"nama_pekerjaan" NVARCHAR2(150), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table PENYAKIT
--------------------------------------------------------

  CREATE TABLE "DLM"."PENYAKIT" 
   (	"id" NUMBER(38,0), 
	"nama_penyakit" NVARCHAR2(100), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table PMKS
--------------------------------------------------------

  CREATE TABLE "DLM"."PMKS" 
   (	"id" NUMBER(38,0), 
	"id_jenis_pmks" NUMBER(38,0), 
	"nik" NVARCHAR2(50), 
	"no_kk" NVARCHAR2(50), 
	"jenis_cacat" NUMBER(38,0), 
	"dititip_di" NUMBER(38,0), 
	"id_pantiasuhan" NUMBER(38,0), 
	"bantuan_diterima" NVARCHAR2(100), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table PUSKESMAS
--------------------------------------------------------

  CREATE TABLE "DLM"."PUSKESMAS" 
   (	"id" NVARCHAR2(20), 
	"nama_puskesmas" NVARCHAR2(100), 
	"jenis_puskesmas" NUMBER(38,0), 
	"alamat_puskesmas" NVARCHAR2(500), 
	"jlh_dokter_umum" NUMBER(38,0), 
	"jlh_dokter_gigi" NUMBER(38,0), 
	"jlh_tenaga_lab" NUMBER(38,0), 
	"jlh_tenaga_farmasi" NUMBER(38,0), 
	"jlh_perawat" NUMBER(38,0), 
	"jlh_ruang_inap" NUMBER(38,0), 
	"jlh_ruang_layanan" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table RIWAYAT_KESEHATAN
--------------------------------------------------------

  CREATE TABLE "DLM"."RIWAYAT_KESEHATAN" 
   (	"id" NUMBER(38,0), 
	"nik" NVARCHAR2(50), 
	"no_kk" NVARCHAR2(50), 
	"no_jka" NVARCHAR2(50), 
	"no_jamkesmas" NVARCHAR2(50), 
	"asuransilain1" NVARCHAR2(50), 
	"no_asuransilain1" NVARCHAR2(50), 
	"asuransilain2" NVARCHAR2(50), 
	"no_asuransilain2" NVARCHAR2(50), 
	"id_penyakit" NUMBER(38,0), 
	"tgl_sakit" DATE, 
	"id_puskesmas" NVARCHAR2(20), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table RIWAYAT_PEKERJAAN
--------------------------------------------------------

  CREATE TABLE "DLM"."RIWAYAT_PEKERJAAN" 
   (	"id" NUMBER(38,0), 
	"nik" NVARCHAR2(50), 
	"id_pek" NUMBER(38,0), 
	"tempat_bekerja" NVARCHAR2(100), 
	"rata_penghasilan" NUMBER(38,0), 
	"tahun" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table RIWAYAT_PENDIDIKAN
--------------------------------------------------------

  CREATE TABLE "DLM"."RIWAYAT_PENDIDIKAN" 
   (	"id" NUMBER(38,0), 
	"nik" NVARCHAR2(50), 
	"no_kk" NVARCHAR2(50), 
	"jenjang_sekolah" NVARCHAR2(100), 
	"kode_sekolah" NUMBER(38,0), 
	"kelas" NVARCHAR2(50), 
	"tahun" NUMBER(38,0), 
	"dapat_beasiswa" NUMBER(38,0), 
	"jlh_beasiswa" FLOAT(38), 
	"pemberi_beasiswa" NVARCHAR2(100), 
	"status_pendidikan" NUMBER(38,0), 
	"mengapa_tdk_sekolah" NVARCHAR2(200), 
	"thn_putus_sekolah" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table RUMAH
--------------------------------------------------------

  CREATE TABLE "DLM"."RUMAH" 
   (	"id" NUMBER(38,0), 
	"nik" NVARCHAR2(50), 
	"no_kk" NVARCHAR2(50), 
	"status_rumah" NUMBER(38,0), 
	"jenis_lantai" NUMBER(38,0), 
	"jenis_dinding" NUMBER(38,0), 
	"fasilitas_wc" NUMBER(38,0), 
	"penerangan" NUMBER(38,0), 
	"air_minum" NUMBER(38,0), 
	"bahan_bakar_masak" NUMBER(38,0), 
	"rumah_bantuan" NUMBER(38,0), 
	"pemberi_rumah_bantuan" NVARCHAR2(100), 
	"tahun_rumah_bantuan" NUMBER(38,0), 
	"koordinat_x" NVARCHAR2(500), 
	"koordinat_y" NVARCHAR2(500), 
	"photo" NVARCHAR2(100), 
	"jlh_makan" NUMBER(38,0), 
	"lauk_pauk" NUMBER(38,0), 
	"daging" NUMBER(38,0), 
	"pakaian" NUMBER(38,0), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE, 
	"panjang_lantai" FLOAT(38), 
	"lebar_lantai" FLOAT(38)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table SEKOLAH
--------------------------------------------------------

  CREATE TABLE "DLM"."SEKOLAH" 
   (	"id" NUMBER(38,0), 
	"nama_sekolah" NVARCHAR2(100), 
	"jenjang_sekolah" NVARCHAR2(100), 
	"alamat_sekolah" NVARCHAR2(500), 
	"jlh_ruang_kelas" NUMBER(38,0), 
	"jlh_perpustakaan" NUMBER(38,0), 
	"jlh_lab_ipa" NUMBER(38,0), 
	"jlh_lab_ips" NUMBER(38,0), 
	"jlh_mushalla" NUMBER(38,0), 
	"jlh_kamar_mandi_wc" NUMBER(38,0), 
	"koordinat_x" NVARCHAR2(50), 
	"koordinat_y" NVARCHAR2(50), 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table USER
--------------------------------------------------------

  CREATE TABLE "DLM"."USER" 
   (	"id" NUMBER(38,0), 
	"username" NVARCHAR2(50), 
	"password" NVARCHAR2(100), 
	"nik" NVARCHAR2(50), 
	"role_id" NUMBER(38,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table ZAKAT
--------------------------------------------------------

  CREATE TABLE "DLM"."ZAKAT" 
   (	"id" NUMBER(38,0), 
	"id_jenis_zakat" NUMBER(38,0), 
	"nik" NVARCHAR2(50), 
	"id_instansi" NUMBER(38,0), 
	"jlh_zakat" FLOAT(38), 
	"tgl_diberikan" DATE, 
	"userid" NUMBER(38,0), 
	"tglinput" DATE, 
	"tglupdate" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for View VIEW_BIODATA
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "DLM"."VIEW_BIODATA" ("no_kk", "nik", "nama_lgkp", "hub_keluarga") AS 
  SELECT "VIEW_KEPALA_KELUARGA"."no_kk", "BIODATA"."nik", "BIODATA"."nama_lgkp", "VIEW_KEPALA_KELUARGA"."hub_keluarga" FROM BIODATA
JOIN VIEW_KEPALA_KELUARGA ON "BIODATA"."nik" = "VIEW_KEPALA_KELUARGA"."nik"
;
--------------------------------------------------------
--  DDL for View VIEW_KEPALA_KELUARGA
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "DLM"."VIEW_KEPALA_KELUARGA" ("no_kk", "nik", "hub_keluarga") AS 
  SELECT "no_kk", "nik", "hub_keluarga" FROM KEPALA_KELUARGA
UNION 
SELECT "no_kk", "nik", "hub_keluarga" FROM KEPALA_KELUARGA_DETAILS;
--------------------------------------------------------
--  DDL for View VIEW_LAPORAN_BIODATA
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "DLM"."VIEW_LAPORAN_BIODATA" ("nik", "nama_lgkp", "tmpt_lhr", "tgl_lhr", "akta_lhr", "no_akta_lhr", "gol_drh", "jenis_kelamin", "agama", "stat_kwn", "akta_kwn", "no_akta_kwn", "tgl_kwn", "akta_crai", "no_akta_crai", "tgl_crai", "pnydng_cct", "stat_hidup", "tgl_meninggal", "no_akta_kematian", "no_kab", "nama_kabkot", "no_kec", "nama_kec", "no_kel", "nama_gampong", "alamat", "nik_nama_ibu", "nik_nama_ayah", "hub_keluarga", "pendidikanid", "pendidikan", "pekerjaan") AS 
  SELECT "BIODATA"."nik" AS "nik",
    	"BIODATA"."nama_lgkp" AS "nama_lgkp",
    	"BIODATA"."tmpt_lhr" AS "tmpt_lhr",
    	"BIODATA"."tgl_lhr" AS "tgl_lhr",
    	"BIODATA"."akta_lhr" AS "akta_lhr",
    	"BIODATA"."no_akta_lhr" AS "no_akta_lhr",   	
    	"BIODATA"."gol_drh" AS "gol_drh",
		"BIODATA"."jenis_kelamin" AS "jenis_kelamin",
		"BIODATA"."agama" AS "agama",
		"BIODATA"."stat_kwn" AS "stat_kwn",
		"BIODATA"."akta_kwn" AS "akta_kwn",
		"BIODATA"."no_akta_kwn" AS "no_akta_kwn",
		"BIODATA"."tgl_kwn" AS "tgl_kwn",
		"BIODATA"."akta_crai" AS "akta_crai",
		"BIODATA"."no_akta_crai" AS "no_akta_crai",
		"BIODATA"."tgl_crai" AS "tgl_crai",
		"BIODATA"."pnydng_cct" AS "pnydng_cct",
		"BIODATA"."stat_hidup" AS "stat_hidup",
		"BIODATA"."tgl_meninggal" AS "tgl_meninggal",
		"BIODATA"."no_akta_kematian" AS "no_akta_kematian",
		"BIODATA"."no_kab",
		"KABUPATEN"."nama_kabkot",
		"BIODATA"."no_kec",
		"KECAMATAN"."nama_kec",
		"BIODATA"."no_kel",
		"GAMPONG"."nama_gampong",
		"GAMPONG"."nama_gampong" || '<br/>' 
		|| "KECAMATAN"."nama_kec" || '<br/>'
		|| "KABUPATEN"."nama_kabkot" AS "alamat",
		(SELECT "DLM".GetNikNamaIbu("BIODATA"."nik") FROM dual) AS "nik_nama_ibu",
		(SELECT "DLM".GetNikNamaAyah("BIODATA"."nik") FROM dual) AS "nik_nama_ayah",
		(SELECT "DLM".GetHubunganKeluargaTerakhir("BIODATA"."nik") FROM dual) AS "hub_keluarga",
		(SELECT "DLM".GetRiwayatPendidikanTerakhirId("BIODATA"."nik") FROM dual) AS "pendidikanid",
		(SELECT "DLM".GetRiwayatPendidikanTerakhir("BIODATA"."nik") FROM dual) AS "pendidikan",	
		(SELECT "DLM".GetRiwayatPekerjaanTerakhir("BIODATA"."nik") FROM dual) AS "pekerjaan"	
 FROM "DLM"."BIODATA"
 JOIN "DLM"."KABUPATEN"
 ON "KABUPATEN"."id" = "BIODATA"."no_kab"
 JOIN "DLM"."KECAMATAN"
 ON "KECAMATAN"."id" = "BIODATA"."no_kec"
 JOIN "DLM"."GAMPONG"
 ON "GAMPONG"."id" = "BIODATA"."no_kel"
 ORDER BY "BIODATA"."nama_lgkp"
;
--------------------------------------------------------
--  DDL for View VIEW_LAPORAN_PENDIDIKAN
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "DLM"."VIEW_LAPORAN_PENDIDIKAN" ("no_kk", "nik", "nama_lgkp", "tmpt_lhr", "tgl_lhr", "akta_lhr", "jenis_kelamin", "agama", "pnydng_cct", "stat_hidup", "no_kab", "nama_kabkot", "no_kec", "nama_kec", "no_kel", "nama_gampong", "alamat", "nik_nama_ibu", "nik_nama_ayah", "hub_keluarga", "pendidikanid", "pendidikan", "pekerjaan", "jenjang_sekolah", "kode_sekolah", "kelas", "tahun", "dapat_beasiswa", "jlh_beasiswa", "pemberi_beasiswa", "status_pendidikan", "mengapa_tdk_sekolah", "thn_putus_sekolah", "nama_sekolah") AS 
  SELECT  (SELECT "DLM".GetNoKkTerakhir("VIEW_LAPORAN_BIODATA"."nik") FROM dual) AS "no_kk",
		"VIEW_LAPORAN_BIODATA"."nik",
       	"VIEW_LAPORAN_BIODATA"."nama_lgkp",
    	"VIEW_LAPORAN_BIODATA"."tmpt_lhr",
    	"VIEW_LAPORAN_BIODATA"."tgl_lhr",
    	"VIEW_LAPORAN_BIODATA"."akta_lhr",
      	"VIEW_LAPORAN_BIODATA"."jenis_kelamin",
		"VIEW_LAPORAN_BIODATA"."agama",
      "VIEW_LAPORAN_BIODATA"."pnydng_cct",
      "VIEW_LAPORAN_BIODATA"."stat_hidup",
      "VIEW_LAPORAN_BIODATA"."no_kab", 
      "VIEW_LAPORAN_BIODATA"."nama_kabkot",
      "VIEW_LAPORAN_BIODATA"."no_kec", 
      "VIEW_LAPORAN_BIODATA"."nama_kec", 
      "VIEW_LAPORAN_BIODATA"."no_kel", 
      "VIEW_LAPORAN_BIODATA"."nama_gampong", 
      "VIEW_LAPORAN_BIODATA"."alamat", 
      "VIEW_LAPORAN_BIODATA"."nik_nama_ibu", 
      "VIEW_LAPORAN_BIODATA"."nik_nama_ayah", 
      "VIEW_LAPORAN_BIODATA"."hub_keluarga",  
      "VIEW_LAPORAN_BIODATA"."pendidikanid",
      "VIEW_LAPORAN_BIODATA"."pendidikan",
      "VIEW_LAPORAN_BIODATA"."pekerjaan",
      "RIWAYAT_PENDIDIKAN"."jenjang_sekolah",
      "RIWAYAT_PENDIDIKAN"."kode_sekolah",
      "RIWAYAT_PENDIDIKAN"."kelas",
      "RIWAYAT_PENDIDIKAN"."tahun",
      "RIWAYAT_PENDIDIKAN"."dapat_beasiswa",
      "RIWAYAT_PENDIDIKAN"."jlh_beasiswa",
      "RIWAYAT_PENDIDIKAN"."pemberi_beasiswa",
      "RIWAYAT_PENDIDIKAN"."status_pendidikan",
      "RIWAYAT_PENDIDIKAN"."mengapa_tdk_sekolah",
      "RIWAYAT_PENDIDIKAN"."thn_putus_sekolah",
      "SEKOLAH"."nama_sekolah"
FROM "DLM"."VIEW_LAPORAN_BIODATA"
JOIN "DLM"."RIWAYAT_PENDIDIKAN" 
ON "RIWAYAT_PENDIDIKAN"."id" = "VIEW_LAPORAN_BIODATA"."pendidikanid" 
JOIN "DLM"."SEKOLAH" 
ON "RIWAYAT_PENDIDIKAN"."kode_sekolah" = "SEKOLAH"."id"
WHERE "VIEW_LAPORAN_BIODATA"."pendidikan" IS NOT NULL
;
--------------------------------------------------------
--  DDL for View VIEW_LAPORAN_PMKS
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "DLM"."VIEW_LAPORAN_PMKS" ("nama", "hub_keluarga", "tgl_lhr", "nik_nama_ibu", "nik_nama_ayah", "nama_pmks", "id_jenis_pmks", "agama", "no_kab", "nama_kabkot", "no_kec", "nama_kec", "no_kel", "nama_gampong", "alamat", "dititip_di", "id_pantiasuhan", "nama_pantiasuhan") AS 
  SELECT 	"BIODATA"."nama_lgkp" || '<br/>' 
		|| "VIEW_KEPALA_KELUARGA"."no_kk" || '<br/>' 
		|| "PMKS"."nik"  AS "nama",
		"VIEW_KEPALA_KELUARGA"."hub_keluarga",
		"BIODATA"."tgl_lhr",
		(SELECT "DLM".GetNikNamaIbu("PMKS"."nik") FROM dual) AS "nik_nama_ibu",
		(SELECT "DLM".GetNikNamaAyah("PMKS"."nik") FROM dual) AS "nik_nama_ayah",
		"JENIS_PMKS"."nama_pmks",
		"PMKS"."id_jenis_pmks",
		"BIODATA"."agama",
		"BIODATA"."no_kab",
		"KABUPATEN"."nama_kabkot",
		"BIODATA"."no_kec",
		"KECAMATAN"."nama_kec",
		"BIODATA"."no_kel",
		"GAMPONG"."nama_gampong",
		"GAMPONG"."nama_gampong" || '<br/>' 
		|| "KECAMATAN"."nama_kec" || '<br/>'
		|| "KABUPATEN"."nama_kabkot" AS "alamat",
		"PMKS"."dititip_di",
		"PMKS"."id_pantiasuhan",
		"PANTI_ASUHAN"."nama_pantiasuhan" || '<br/>' 
		|| "PMKS"."bantuan_diterima" AS "nama_pantiasuhan"
FROM "DLM"."PMKS" 
JOIN "DLM"."JENIS_PMKS"
ON "PMKS"."id_jenis_pmks" = "JENIS_PMKS"."id"
JOIN "DLM"."BIODATA" 
ON "PMKS"."nik" = "BIODATA"."nik"
JOIN "DLM"."VIEW_KEPALA_KELUARGA"
ON "VIEW_KEPALA_KELUARGA"."nik" = "BIODATA"."nik"
JOIN "DLM"."KABUPATEN"
ON "KABUPATEN"."id" = "BIODATA"."no_kab"
JOIN "DLM"."KECAMATAN"
ON "KECAMATAN"."id" = "BIODATA"."no_kec"
JOIN "DLM"."GAMPONG"
ON "GAMPONG"."id" = "BIODATA"."no_kel"
LEFT JOIN "DLM"."PANTI_ASUHAN"
ON "PANTI_ASUHAN"."id" = "PMKS"."id_pantiasuhan"
;
--------------------------------------------------------
--  DDL for View VIEW_LAPORAN_RUMAH
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "DLM"."VIEW_LAPORAN_RUMAH" ("id", "nik", "no_kk", "status_rumah", "jenis_lantai", "jenis_dinding", "fasilitas_wc", "penerangan", "air_minum", "bahan_bakar_masak", "rumah_bantuan", "pemberi_rumah_bantuan", "tahun_rumah_bantuan", "koordinat_x", "koordinat_y", "photo", "jlh_makan", "lauk_pauk", "daging", "pakaian", "panjang_lantai", "lebar_lantai", "nama", "pekerjaan", "penghasilan", "no_kab", "no_kec", "no_kel") AS 
  SELECT  "RUMAH"."id"
		,"RUMAH"."nik"
		,"RUMAH"."no_kk"
		,"RUMAH"."status_rumah"
		,"RUMAH"."jenis_lantai"
		,"RUMAH"."jenis_dinding"
		,"RUMAH"."fasilitas_wc"
		,"RUMAH"."penerangan"
		,"RUMAH"."air_minum"
		,"RUMAH"."bahan_bakar_masak"
		,"RUMAH"."rumah_bantuan"
		,"RUMAH"."pemberi_rumah_bantuan"
		,"RUMAH"."tahun_rumah_bantuan"
		,"RUMAH"."koordinat_x"
		,"RUMAH"."koordinat_y"
		,"RUMAH"."photo"
		,"RUMAH"."jlh_makan"
		,"RUMAH"."lauk_pauk"
		,"RUMAH"."daging"
		,"RUMAH"."pakaian"
		,"RUMAH"."panjang_lantai"
		,"RUMAH"."lebar_lantai"
		,(SELECT "DLM".GetKepalaKeluargaByNoKk("RUMAH"."no_kk") FROM dual) AS "nama"
		,(SELECT "DLM".GetPekerjaanTerakhirKKByNoKk("RUMAH"."no_kk") FROM dual) AS "pekerjaan"
		,(SELECT "DLM".GetPenghasilanTerakhirKKByNoKk("RUMAH"."no_kk") FROM dual) AS "penghasilan"
		,"BIODATA"."no_kab"	
		,"BIODATA"."no_kec"
		,"BIODATA"."no_kel"
FROM "DLM"."RUMAH"
JOIN "DLM"."BIODATA" 
ON "RUMAH"."nik" = "BIODATA"."nik"
ORDER BY "RUMAH"."no_kk","RUMAH"."nik"
;
REM INSERTING into DLM.ARTIKEL
SET DEFINE OFF;
Insert into DLM.ARTIKEL ("id","is_visible","userid","tglinput","tglupdate","title") values (2,0,7,to_date('09-AUG-13','DD-MON-RR'),null,'Antrean Exit Tol Ciawi Macet 2 Kilometer ');
Insert into DLM.ARTIKEL ("id","is_visible","userid","tglinput","tglupdate","title") values (1,1,7,to_date('09-AUG-13','DD-MON-RR'),null,'Polisi: Security TransJ Hanya Dengar Suara Letusan');
REM INSERTING into DLM.BANTUAN_LAINNYA
SET DEFINE OFF;
REM INSERTING into DLM.BANTUAN_MODAL
SET DEFINE OFF;
REM INSERTING into DLM.BANTUAN_SOSIAL
SET DEFINE OFF;
REM INSERTING into DLM.BIODATA
SET DEFINE OFF;
Insert into DLM.BIODATA ("nik","nama_lgkp","tmpt_lhr","tgl_lhr","akta_lhr","no_akta_lhr","gol_drh","jenis_kelamin","agama","stat_kwn","akta_kwn","no_akta_kwn","tgl_kwn","akta_crai","no_akta_crai","tgl_crai","pnydng_cct","pddk_akh","kode_pkrj","stat_hidup","no_kab","no_kec","no_kel","userid","tglinput","tglupdate","tgl_meninggal","no_akta_kematian") values ('5203200107670703','Zarina','Permas',to_date('01-JAN-70','DD-MON-RR'),2,null,4,2,1,1,null,null,null,null,null,null,2,7,null,2,4,2,4,7,to_date('10-AUG-13','DD-MON-RR'),to_date('10-AUG-13','DD-MON-RR'),null,null);
Insert into DLM.BIODATA ("nik","nama_lgkp","tmpt_lhr","tgl_lhr","akta_lhr","no_akta_lhr","gol_drh","jenis_kelamin","agama","stat_kwn","akta_kwn","no_akta_kwn","tgl_kwn","akta_crai","no_akta_crai","tgl_crai","pnydng_cct","pddk_akh","kode_pkrj","stat_hidup","no_kab","no_kec","no_kel","userid","tglinput","tglupdate","tgl_meninggal","no_akta_kematian") values ('33221703074500001','Broto','Danpar',to_date('03-JUL-45','DD-MON-RR'),2,null,4,1,1,2,1,'12/KWN/3021',to_date('05-AUG-69','DD-MON-RR'),2,null,null,2,3,null,2,5,1,2,7,to_date('10-AUG-13','DD-MON-RR'),null,null,null);
Insert into DLM.BIODATA ("nik","nama_lgkp","tmpt_lhr","tgl_lhr","akta_lhr","no_akta_lhr","gol_drh","jenis_kelamin","agama","stat_kwn","akta_kwn","no_akta_kwn","tgl_kwn","akta_crai","no_akta_crai","tgl_crai","pnydng_cct","pddk_akh","kode_pkrj","stat_hidup","no_kab","no_kec","no_kel","userid","tglinput","tglupdate","tgl_meninggal","no_akta_kematian") values ('5203200107670701','Zainuddin','Prako',to_date('01-JUL-67','DD-MON-RR'),2,null,4,1,1,2,2,null,to_date('07-JUN-88','DD-MON-RR'),2,null,null,2,2,null,2,4,2,4,7,to_date('10-AUG-13','DD-MON-RR'),null,null,null);
Insert into DLM.BIODATA ("nik","nama_lgkp","tmpt_lhr","tgl_lhr","akta_lhr","no_akta_lhr","gol_drh","jenis_kelamin","agama","stat_kwn","akta_kwn","no_akta_kwn","tgl_kwn","akta_crai","no_akta_crai","tgl_crai","pnydng_cct","pddk_akh","kode_pkrj","stat_hidup","no_kab","no_kec","no_kel","userid","tglinput","tglupdate","tgl_meninggal","no_akta_kematian") values ('5203200107670702','Muniah','Prako',to_date('01-JAN-70','DD-MON-RR'),2,null,1,2,1,2,2,null,to_date('07-JUN-88','DD-MON-RR'),2,null,null,2,3,null,2,4,2,4,7,to_date('10-AUG-13','DD-MON-RR'),null,null,null);
Insert into DLM.BIODATA ("nik","nama_lgkp","tmpt_lhr","tgl_lhr","akta_lhr","no_akta_lhr","gol_drh","jenis_kelamin","agama","stat_kwn","akta_kwn","no_akta_kwn","tgl_kwn","akta_crai","no_akta_crai","tgl_crai","pnydng_cct","pddk_akh","kode_pkrj","stat_hidup","no_kab","no_kec","no_kel","userid","tglinput","tglupdate","tgl_meninggal","no_akta_kematian") values ('5203200107670704','Zaenab','Permas',to_date('01-FEB-78','DD-MON-RR'),2,null,2,2,1,1,null,null,null,null,null,null,2,5,null,2,4,2,4,7,to_date('10-AUG-13','DD-MON-RR'),null,null,null);
Insert into DLM.BIODATA ("nik","nama_lgkp","tmpt_lhr","tgl_lhr","akta_lhr","no_akta_lhr","gol_drh","jenis_kelamin","agama","stat_kwn","akta_kwn","no_akta_kwn","tgl_kwn","akta_crai","no_akta_crai","tgl_crai","pnydng_cct","pddk_akh","kode_pkrj","stat_hidup","no_kab","no_kec","no_kel","userid","tglinput","tglupdate","tgl_meninggal","no_akta_kematian") values ('33221703074500002','Sugiyem','Danpar',to_date('01-JAN-70','DD-MON-RR'),2,null,1,1,1,2,2,null,to_date('17-AUG-69','DD-MON-RR'),2,null,null,2,3,null,2,5,1,2,7,to_date('10-AUG-13','DD-MON-RR'),null,null,null);
Insert into DLM.BIODATA ("nik","nama_lgkp","tmpt_lhr","tgl_lhr","akta_lhr","no_akta_lhr","gol_drh","jenis_kelamin","agama","stat_kwn","akta_kwn","no_akta_kwn","tgl_kwn","akta_crai","no_akta_crai","tgl_crai","pnydng_cct","pddk_akh","kode_pkrj","stat_hidup","no_kab","no_kec","no_kel","userid","tglinput","tglupdate","tgl_meninggal","no_akta_kematian") values ('33221703074500003','Joko Santoso','Danpar',to_date('01-JAN-70','DD-MON-RR'),2,null,4,1,1,1,null,null,null,null,null,null,2,6,null,2,5,1,2,7,to_date('10-AUG-13','DD-MON-RR'),null,null,null);
Insert into DLM.BIODATA ("nik","nama_lgkp","tmpt_lhr","tgl_lhr","akta_lhr","no_akta_lhr","gol_drh","jenis_kelamin","agama","stat_kwn","akta_kwn","no_akta_kwn","tgl_kwn","akta_crai","no_akta_crai","tgl_crai","pnydng_cct","pddk_akh","kode_pkrj","stat_hidup","no_kab","no_kec","no_kel","userid","tglinput","tglupdate","tgl_meninggal","no_akta_kematian") values ('33221703074500004','Purwanti Jaklis','Danpar',to_date('01-JAN-70','DD-MON-RR'),2,null,2,2,1,1,null,null,null,null,null,null,2,1,null,2,5,1,2,7,to_date('10-AUG-13','DD-MON-RR'),null,null,null);
REM INSERTING into DLM.DAYAH
SET DEFINE OFF;
Insert into DLM.DAYAH ("id","id_kab","nama_dayah","nama_pimpinan","type_dayah","jlh_pengasuh","jlh_santri","nama_tingkat","jlh_kelas","jlh_perpustakaan","jlh_lab_komputer","jlh_mushalla","jlh_kamar_mandi_wc","userid","tglinput","tglupdate") values (1,5,'Dayah 1','Teuku Cik Nothing',2,1,2,1,2,1,2,1,2,6,to_date('19-JUL-13','DD-MON-RR'),null);
Insert into DLM.DAYAH ("id","id_kab","nama_dayah","nama_pimpinan","type_dayah","jlh_pengasuh","jlh_santri","nama_tingkat","jlh_kelas","jlh_perpustakaan","jlh_lab_komputer","jlh_mushalla","jlh_kamar_mandi_wc","userid","tglinput","tglupdate") values (2,4,'Dayah Djuang','Taslim',1,2,12,2,1,1,1,1,1,7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.GAMPONG
SET DEFINE OFF;
Insert into DLM.GAMPONG ("id","id_kab","id_kec","nama_gampong","alamat","keuchik","visi","misi","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (3,4,2,'Gampong Barat','Jl. Besar Simaskot','Riyadi','No Visi','No Misi','393030','00e0ee',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.GAMPONG ("id","id_kab","id_kec","nama_gampong","alamat","keuchik","visi","misi","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (1,5,1,'Gampong Singkil','Jl. Merdeka Selatan No. 1','Jafar','Visi cannot be blank.','Misi cannot be blank.','2323','3232',6,to_date('10-JUL-13','DD-MON-RR'),to_date('23-JUL-13','DD-MON-RR'));
Insert into DLM.GAMPONG ("id","id_kab","id_kec","nama_gampong","alamat","keuchik","visi","misi","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (2,5,1,'Gampong Di Danau Paris','Jl. Robbie Williams No. 12','Jeronimo','test','test','34343','343434',6,to_date('10-JUL-13','DD-MON-RR'),to_date('10-JUL-13','DD-MON-RR'));
Insert into DLM.GAMPONG ("id","id_kab","id_kec","nama_gampong","alamat","keuchik","visi","misi","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (4,4,2,'Gampong Bubon','Jl. Bubon Jaya No.93d','Quraiz','No Vision','No Mision','23232','22323',7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.INSTANSI
SET DEFINE OFF;
Insert into DLM.INSTANSI ("id","id_kab","nama_instansi","alamat_instansi","userid","tglinput","tglupate") values (1,4,'Instansi 1','Jl.Majalah Tech No.1',6,to_date('10-JUL-13','DD-MON-RR'),null);
Insert into DLM.INSTANSI ("id","id_kab","nama_instansi","alamat_instansi","userid","tglinput","tglupate") values (2,5,'Instansi 2','Jl. Gelas No. 23',6,to_date('06-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.JENIS_PMKS
SET DEFINE OFF;
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (7,'Anak Yang Memerlukan Perlindungan Khusus',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (8,'Lanjut Usia Terlantar',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (9,'Penyandang Disabilitas',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (10,'Tuna Susila',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (11,'Gelandangan',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (12,'Pengemis',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (13,'Pemulung',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (14,'Kelompok Minoritas',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (15,'Bekas Warga Binaan Lembaga Pemasyarakatan',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (16,'Orang Dengan HIV/AIDS (ODHA)',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (17,'Korban Penyalahgunaan NAPZA',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (18,'Korban Trafficking',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (19,'Korban Tindak Kekerasan',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (1,'Anak Balita Terlantar',6,to_date('08-JUL-13','DD-MON-RR'),to_date('12-AUG-13','DD-MON-RR'));
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (5,'Anak Dengan Kedisabilitasan (ADK)',6,to_date('08-JUL-13','DD-MON-RR'),to_date('10-AUG-13','DD-MON-RR'));
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (20,'Pekerja Migran Bermasalah Sosial (PMBS)',7,to_date('12-AUG-13','DD-MON-RR'),to_date('12-AUG-13','DD-MON-RR'));
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (21,'Korban Bencana Alam',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (22,'Korban Bencana Sosial',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (23,'Perempuan Rawan Sosial Ekonomi',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (24,'Fakir Miskin',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (25,'Keluarga Bermasalah Sosial Psikologis',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (26,'Komunitas Adat Terpencil',7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (2,'Anak Terlantar',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (3,'Anak Yang Berhadapan Dengan Hukum',7,to_date('10-AUG-13','DD-MON-RR'),to_date('10-AUG-13','DD-MON-RR'));
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (4,'Anak Jalanan',7,to_date('10-AUG-13','DD-MON-RR'),to_date('10-AUG-13','DD-MON-RR'));
Insert into DLM.JENIS_PMKS ("id","nama_pmks","userid","tglinput","tglupdate") values (6,'Anak Yang Menjadi Korban Tindak Kekerasan Atau Diperlakukan Salah',7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.KABUPATEN
SET DEFINE OFF;
Insert into DLM.KABUPATEN ("id","nama_kabkot","ibukota","alamat","bupati","wakil","luas_wilayah","visi","misi","koordinat_x","koordinat_y","userid","tglinput","tglupdate","lambang") values (4,'Aceh Barat','Meulaboh','Jl. Kenari No.345','Jamaluddin','Sidik',2000,'No data','No Data','30000','2000',6,to_date('09-JUL-03','DD-MON-RR'),to_date('09-AUG-13','DD-MON-RR'),'130809105853.jpg');
Insert into DLM.KABUPATEN ("id","nama_kabkot","ibukota","alamat","bupati","wakil","luas_wilayah","visi","misi","koordinat_x","koordinat_y","userid","tglinput","tglupdate","lambang") values (5,'Aceh Singkil','Singkil','Jl. Merpati Putih No. 24','Sujana','Ridho',6877565,'Visi','Misi','93939','939999',6,to_date('09-JUL-05','DD-MON-RR'),null,null);
REM INSERTING into DLM.KECAMATAN
SET DEFINE OFF;
Insert into DLM.KECAMATAN ("id","id_kab","nama_kec","ibukota","alamat","camat","visi","misi","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (1,5,'Danau Paris','Danau Paris','Jl. Cut Nyak Dien No.1','Mirza','Visi Kecamatan','Misi Kecamatan','0030300','002993',6,to_date('09-JUL-13','DD-MON-RR'),null);
Insert into DLM.KECAMATAN ("id","id_kab","nama_kec","ibukota","alamat","camat","visi","misi","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (2,4,'Bubon','Bubon','Jl. Pertiwi No. 343','Zulkiplinov','No Visi','No Misi','393o3','03030',7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.KELAS_BANTUAN
SET DEFINE OFF;
Insert into DLM.KELAS_BANTUAN ("id","nama_kelas_bantuan","userid","tglinput","tglupdate") values (1,'Pertanian',6,to_date('28-JUL-13','DD-MON-RR'),null);
Insert into DLM.KELAS_BANTUAN ("id","nama_kelas_bantuan","userid","tglinput","tglupdate") values (2,'Perkebunan',6,to_date('28-JUL-13','DD-MON-RR'),null);
Insert into DLM.KELAS_BANTUAN ("id","nama_kelas_bantuan","userid","tglinput","tglupdate") values (3,'Perikanan',6,to_date('28-JUL-13','DD-MON-RR'),null);
Insert into DLM.KELAS_BANTUAN ("id","nama_kelas_bantuan","userid","tglinput","tglupdate") values (4,'Peternakan',6,to_date('28-JUL-13','DD-MON-RR'),null);
Insert into DLM.KELAS_BANTUAN ("id","nama_kelas_bantuan","userid","tglinput","tglupdate") values (5,'Kehutanan',6,to_date('28-JUL-13','DD-MON-RR'),null);
REM INSERTING into DLM.KELAS_BANTUAN_LAINNYA
SET DEFINE OFF;
REM INSERTING into DLM.KELOMPOK
SET DEFINE OFF;
Insert into DLM.KELOMPOK ("id","id_kab","nama_kelompok","ketua_kelompok","bidang_usaha","no_telp","alamat","userid","tglinput","tglupdate") values (1,5,'Kelompok Tani Maju Bersama','Fadoli','Pertanian Bawang','09303030303','Jl. Laptop 10 No.3',6,to_date('09-JUL-13','DD-MON-RR'),to_date('09-JUL-13','DD-MON-RR'));
Insert into DLM.KELOMPOK ("id","id_kab","nama_kelompok","ketua_kelompok","bidang_usaha","no_telp","alamat","userid","tglinput","tglupdate") values (2,4,'Kirana Jaya','Sarmin','Bertani','030303030','Jl. Ganggang No. 90',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.KELOMPOK ("id","id_kab","nama_kelompok","ketua_kelompok","bidang_usaha","no_telp","alamat","userid","tglinput","tglupdate") values (3,4,'Citra Usaha','Centini','Perkebunan','0940400404','Jl. Merbabu No. 9393',7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.KEPALA_KELUARGA
SET DEFINE OFF;
Insert into DLM.KEPALA_KELUARGA ("no_kk","nik","hub_keluarga","userid","tglinput","tglupdate") values ('5203202312070001','5203200107670701',1,7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.KEPALA_KELUARGA_DETAILS
SET DEFINE OFF;
Insert into DLM.KEPALA_KELUARGA_DETAILS ("id","no_kk","nik","hub_keluarga","userid","tglinput","tglupdate") values (3,'5203202312070001','5203200107670704',4,7,to_date('12-AUG-13','DD-MON-RR'),to_date('13-AUG-13','DD-MON-RR'));
Insert into DLM.KEPALA_KELUARGA_DETAILS ("id","no_kk","nik","hub_keluarga","userid","tglinput","tglupdate") values (2,'5203202312070001','5203200107670703',4,7,to_date('12-AUG-13','DD-MON-RR'),null);
Insert into DLM.KEPALA_KELUARGA_DETAILS ("id","no_kk","nik","hub_keluarga","userid","tglinput","tglupdate") values (1,'5203202312070001','5203200107670702',3,7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.KEPEMILIKAN_ASSET
SET DEFINE OFF;
REM INSERTING into DLM.KOPERASI
SET DEFINE OFF;
Insert into DLM.KOPERASI ("id","id_kab","nama_koperasi","ketua_koperasi","bidang_usaha","no_telp","alamat","userid","tglinput","tglupdate") values (3,5,'Koperasi Wijaya Kesuma','Rusmini','Perikanan','09494994491','Jl. Hartono Winata No.12',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.KOPERASI ("id","id_kab","nama_koperasi","ketua_koperasi","bidang_usaha","no_telp","alamat","userid","tglinput","tglupdate") values (1,5,'Koperasi Satu Hati','Deni','Pertanian','093839393','Jl. Merpati No.3
Aceh Singkil',6,to_date('09-JUL-13','DD-MON-RR'),null);
Insert into DLM.KOPERASI ("id","id_kab","nama_koperasi","ketua_koperasi","bidang_usaha","no_telp","alamat","userid","tglinput","tglupdate") values (2,4,'Koperasi Bersama Kita Bisa','Jumadi','Sewa Tenda','09393939','Jl. Smarty No. 390',7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.PANTI_ASUHAN
SET DEFINE OFF;
Insert into DLM.PANTI_ASUHAN ("id","nama_pantiasuhan","type_pantiasuhan","alamat_pantiasuhan","jlh_kamar","jlh_perpustakaan","jlh_aula","jlh_mushalla","jlh_kamar_mandi_wc","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (2,'Panti Asuhan Cinta Sesama',1,'Jl. Mercury No.434',3,1,0,1,10,'232323','232323',6,to_date('09-JUL-13','DD-MON-RR'),null);
Insert into DLM.PANTI_ASUHAN ("id","nama_pantiasuhan","type_pantiasuhan","alamat_pantiasuhan","jlh_kamar","jlh_perpustakaan","jlh_aula","jlh_mushalla","jlh_kamar_mandi_wc","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (1,'Panti Asuhan Anak Bangsa',2,'Jl. Kacamata No. 34',2,1,2,1,1,'2121','12121',7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.PEKERJAAN
SET DEFINE OFF;
Insert into DLM.PEKERJAAN ("id","nama_pekerjaan","userid","tglinput","tglupdate") values (1,'Belum/Tidak Bekerja',6,to_date('09-JUL-13','DD-MON-RR'),null);
Insert into DLM.PEKERJAAN ("id","nama_pekerjaan","userid","tglinput","tglupdate") values (2,'Mengurus Rumah Tangga',6,to_date('09-JUL-13','DD-MON-RR'),null);
Insert into DLM.PEKERJAAN ("id","nama_pekerjaan","userid","tglinput","tglupdate") values (3,'Dokter',6,to_date('18-JUL-13','DD-MON-RR'),null);
Insert into DLM.PEKERJAAN ("id","nama_pekerjaan","userid","tglinput","tglupdate") values (4,'Guru',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.PEKERJAAN ("id","nama_pekerjaan","userid","tglinput","tglupdate") values (5,'Perawat',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.PEKERJAAN ("id","nama_pekerjaan","userid","tglinput","tglupdate") values (6,'Petani',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.PEKERJAAN ("id","nama_pekerjaan","userid","tglinput","tglupdate") values (7,'Pegawai Swasta',7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.PENYAKIT
SET DEFINE OFF;
Insert into DLM.PENYAKIT ("id","nama_penyakit","userid","tglinput","tglupdate") values (4,'Kolera',6,to_date('28-JUL-13','DD-MON-RR'),to_date('10-AUG-13','DD-MON-RR'));
Insert into DLM.PENYAKIT ("id","nama_penyakit","userid","tglinput","tglupdate") values (1,'Demam',6,to_date('25-JUL-13','DD-MON-RR'),null);
Insert into DLM.PENYAKIT ("id","nama_penyakit","userid","tglinput","tglupdate") values (2,'TBC',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.PENYAKIT ("id","nama_penyakit","userid","tglinput","tglupdate") values (3,'Malaria',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.PENYAKIT ("id","nama_penyakit","userid","tglinput","tglupdate") values (5,'Typus',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.PENYAKIT ("id","nama_penyakit","userid","tglinput","tglupdate") values (6,'Flu Burung',7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.PMKS
SET DEFINE OFF;
Insert into DLM.PMKS ("id","id_jenis_pmks","nik","no_kk","jenis_cacat","dititip_di","id_pantiasuhan","bantuan_diterima","userid","tglinput","tglupdate") values (1,1,'5203200107670704','5203202312070001',4,1,2,'Tidak ada',7,to_date('12-AUG-13','DD-MON-RR'),to_date('12-AUG-13','DD-MON-RR'));
REM INSERTING into DLM.PUSKESMAS
SET DEFINE OFF;
Insert into DLM.PUSKESMAS ("id","nama_puskesmas","jenis_puskesmas","alamat_puskesmas","jlh_dokter_umum","jlh_dokter_gigi","jlh_tenaga_lab","jlh_tenaga_farmasi","jlh_perawat","jlh_ruang_inap","jlh_ruang_layanan","userid","tglinput","tglupdate") values ('3','Puskesmas Danau Paris',1,'Jl. Danau Paris No. 23',1,1,1,1,1,1,1,7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.PUSKESMAS ("id","nama_puskesmas","jenis_puskesmas","alamat_puskesmas","jlh_dokter_umum","jlh_dokter_gigi","jlh_tenaga_lab","jlh_tenaga_farmasi","jlh_perawat","jlh_ruang_inap","jlh_ruang_layanan","userid","tglinput","tglupdate") values ('1','Puskesmas Pembantu Danau Paris',1,'Jl. Cut Nyak Dien No. 34',2,1,2,1,1,2,1,6,to_date('19-JUL-13','DD-MON-RR'),to_date('10-AUG-13','DD-MON-RR'));
Insert into DLM.PUSKESMAS ("id","nama_puskesmas","jenis_puskesmas","alamat_puskesmas","jlh_dokter_umum","jlh_dokter_gigi","jlh_tenaga_lab","jlh_tenaga_farmasi","jlh_perawat","jlh_ruang_inap","jlh_ruang_layanan","userid","tglinput","tglupdate") values ('2','Puskesmas Bubon',2,'Jl. Mercuri No. 23',1,1,0,0,6,4,2,7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.RIWAYAT_KESEHATAN
SET DEFINE OFF;
REM INSERTING into DLM.RIWAYAT_PEKERJAAN
SET DEFINE OFF;
Insert into DLM.RIWAYAT_PEKERJAAN ("id","nik","id_pek","tempat_bekerja","rata_penghasilan","tahun","userid","tglinput","tglupdate") values (3,'5203200107670701',6,'Desa Suluh',500,1986,7,to_date('16-AUG-13','DD-MON-RR'),null);
Insert into DLM.RIWAYAT_PEKERJAAN ("id","nik","id_pek","tempat_bekerja","rata_penghasilan","tahun","userid","tglinput","tglupdate") values (4,'5203200107670703',7,'CV. Merpati Raya, Aceh',1200000,1989,7,to_date('17-AUG-13','DD-MON-RR'),null);
Insert into DLM.RIWAYAT_PEKERJAAN ("id","nik","id_pek","tempat_bekerja","rata_penghasilan","tahun","userid","tglinput","tglupdate") values (1,'5203200107670703',5,'RS. Kencana',1600000,1979,7,to_date('14-AUG-13','DD-MON-RR'),null);
Insert into DLM.RIWAYAT_PEKERJAAN ("id","nik","id_pek","tempat_bekerja","rata_penghasilan","tahun","userid","tglinput","tglupdate") values (2,'5203200107670701',1,'Desa Suluh',200,1985,7,to_date('16-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.RIWAYAT_PENDIDIKAN
SET DEFINE OFF;
Insert into DLM.RIWAYAT_PENDIDIKAN ("id","nik","no_kk","jenjang_sekolah","kode_sekolah","kelas","tahun","dapat_beasiswa","jlh_beasiswa","pemberi_beasiswa","status_pendidikan","mengapa_tdk_sekolah","thn_putus_sekolah","userid","tglinput","tglupdate") values (1,'5203200107670703','5203202312070001','Tamat SD/Sederajat',5,'6',1974,2,null,null,2,null,null,7,to_date('12-AUG-13','DD-MON-RR'),to_date('14-AUG-13','DD-MON-RR'));
Insert into DLM.RIWAYAT_PENDIDIKAN ("id","nik","no_kk","jenjang_sekolah","kode_sekolah","kelas","tahun","dapat_beasiswa","jlh_beasiswa","pemberi_beasiswa","status_pendidikan","mengapa_tdk_sekolah","thn_putus_sekolah","userid","tglinput","tglupdate") values (3,'5203200107670701','5203202312070001','Tamat SD/Sederajat',5,'6',1973,2,null,null,2,null,null,7,to_date('17-AUG-13','DD-MON-RR'),null);
Insert into DLM.RIWAYAT_PENDIDIKAN ("id","nik","no_kk","jenjang_sekolah","kode_sekolah","kelas","tahun","dapat_beasiswa","jlh_beasiswa","pemberi_beasiswa","status_pendidikan","mengapa_tdk_sekolah","thn_putus_sekolah","userid","tglinput","tglupdate") values (4,'5203200107670701','5203202312070001','Tamat SLTP/Sederajat',6,'3',1976,2,null,null,2,null,null,7,to_date('17-AUG-13','DD-MON-RR'),to_date('17-AUG-13','DD-MON-RR'));
Insert into DLM.RIWAYAT_PENDIDIKAN ("id","nik","no_kk","jenjang_sekolah","kode_sekolah","kelas","tahun","dapat_beasiswa","jlh_beasiswa","pemberi_beasiswa","status_pendidikan","mengapa_tdk_sekolah","thn_putus_sekolah","userid","tglinput","tglupdate") values (2,'5203200107670703','5203202312070001','Tamat SLTP/Sederajat',6,'3',1977,2,null,null,2,null,null,7,to_date('14-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM.RUMAH
SET DEFINE OFF;
Insert into DLM.RUMAH ("id","nik","no_kk","status_rumah","jenis_lantai","jenis_dinding","fasilitas_wc","penerangan","air_minum","bahan_bakar_masak","rumah_bantuan","pemberi_rumah_bantuan","tahun_rumah_bantuan","koordinat_x","koordinat_y","photo","jlh_makan","lauk_pauk","daging","pakaian","userid","tglinput","tglupdate","panjang_lantai","lebar_lantai") values (1,'5203200107670701','5203202312070001',1,1,1,2,1,1,1,1,'Dinas Sosial',1975,'12121','12121','5203202312070001.png',1,1,1,1,7,to_date('16-AUG-13','DD-MON-RR'),null,12,7);
REM INSERTING into DLM.SEKOLAH
SET DEFINE OFF;
Insert into DLM.SEKOLAH ("id","nama_sekolah","jenjang_sekolah","alamat_sekolah","jlh_ruang_kelas","jlh_perpustakaan","jlh_lab_ipa","jlh_lab_ips","jlh_mushalla","jlh_kamar_mandi_wc","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (5,'SD Negeri 20010010','Sekolah Dasar','Jl. Merdeka No. 12',12,1,1,0,1,2,'11�11''11','11�11''11',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.SEKOLAH ("id","nama_sekolah","jenjang_sekolah","alamat_sekolah","jlh_ruang_kelas","jlh_perpustakaan","jlh_lab_ipa","jlh_lab_ips","jlh_mushalla","jlh_kamar_mandi_wc","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (1,'TK Anak Bangsa','Pra Sekolah','Jl. Anak Bangsa No.100',3,1,2,1,1,2,'11�11''11','12�31''21',6,to_date('19-JUL-13','DD-MON-RR'),to_date('10-AUG-13','DD-MON-RR'));
Insert into DLM.SEKOLAH ("id","nama_sekolah","jenjang_sekolah","alamat_sekolah","jlh_ruang_kelas","jlh_perpustakaan","jlh_lab_ipa","jlh_lab_ips","jlh_mushalla","jlh_kamar_mandi_wc","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (6,'SMP Swasta Dwipana','Sekolah Menengah Pertama','Jl. Merdeka No. 12',14,1,1,1,1,1,'12�12''12','21�21''21',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.SEKOLAH ("id","nama_sekolah","jenjang_sekolah","alamat_sekolah","jlh_ruang_kelas","jlh_perpustakaan","jlh_lab_ipa","jlh_lab_ips","jlh_mushalla","jlh_kamar_mandi_wc","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (7,'SMA 3 Banda Aceh','Sekolah Menengah Atas','JL. Tritura No. 83',23,2,1,1,1,2,'11�11''11','23�23''23',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.SEKOLAH ("id","nama_sekolah","jenjang_sekolah","alamat_sekolah","jlh_ruang_kelas","jlh_perpustakaan","jlh_lab_ipa","jlh_lab_ips","jlh_mushalla","jlh_kamar_mandi_wc","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (8,'AMIK Nusa Bangsa','Diploma III','Jl. Salak No. 34',12,1,0,0,1,2,'54�54''54','54�54''55',7,to_date('10-AUG-13','DD-MON-RR'),null);
Insert into DLM.SEKOLAH ("id","nama_sekolah","jenjang_sekolah","alamat_sekolah","jlh_ruang_kelas","jlh_perpustakaan","jlh_lab_ipa","jlh_lab_ips","jlh_mushalla","jlh_kamar_mandi_wc","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (9,'Universitas Syahkuala','Sarjana','Jl. Muara HIjau No. 23',21,12,0,0,11,121,'45�45''45','23�23''23',7,to_date('10-AUG-13','DD-MON-RR'),to_date('10-AUG-13','DD-MON-RR'));
Insert into DLM.SEKOLAH ("id","nama_sekolah","jenjang_sekolah","alamat_sekolah","jlh_ruang_kelas","jlh_perpustakaan","jlh_lab_ipa","jlh_lab_ips","jlh_mushalla","jlh_kamar_mandi_wc","koordinat_x","koordinat_y","userid","tglinput","tglupdate") values (10,'SD Prime One','Sekolah Dasar','Jl. Menari No. 399',1,1,1,1,1,1,'11�11''11','11�11''11',7,to_date('10-AUG-13','DD-MON-RR'),null);
REM INSERTING into DLM."USER"
SET DEFINE OFF;
Insert into DLM."USER" ("id","username","password","nik","role_id") values (6,'superadmin','planb',null,0);
Insert into DLM."USER" ("id","username","password","nik","role_id") values (7,'admin','admin',null,0);
REM INSERTING into DLM.ZAKAT
SET DEFINE OFF;
--------------------------------------------------------
--  Constraints for Table KEPALA_KELUARGA
--------------------------------------------------------

  ALTER TABLE "DLM"."KEPALA_KELUARGA" MODIFY ("no_kk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA" MODIFY ("hub_keluarga" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA" ADD PRIMARY KEY ("no_kk")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table KEPEMILIKAN_ASSET
--------------------------------------------------------

  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("no_kk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_tanah_rumah" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_tanah_sawah" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_tanah_kebun" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_tambak" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_lembu" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_kerbau" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_ternak_lain" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_mobil" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_motor" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_tabungan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_asuransi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_motor_tempel" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_kapal_motor" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("punya_perahu" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PMKS
--------------------------------------------------------

  ALTER TABLE "DLM"."PMKS" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PMKS" MODIFY ("id_jenis_pmks" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PMKS" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PMKS" MODIFY ("no_kk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PMKS" MODIFY ("jenis_cacat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PMKS" MODIFY ("dititip_di" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PMKS" MODIFY ("bantuan_diterima" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PMKS" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PMKS" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."PMKS" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SEKOLAH
--------------------------------------------------------

  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("nama_sekolah" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("jenjang_sekolah" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("alamat_sekolah" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("jlh_ruang_kelas" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("jlh_perpustakaan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("jlh_lab_ipa" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("jlh_lab_ips" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("jlh_mushalla" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("jlh_kamar_mandi_wc" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("koordinat_x" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("koordinat_y" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."SEKOLAH" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."SEKOLAH" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table RIWAYAT_PENDIDIKAN
--------------------------------------------------------

  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" MODIFY ("no_kk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" MODIFY ("jenjang_sekolah" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" MODIFY ("kelas" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" MODIFY ("tahun" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" MODIFY ("dapat_beasiswa" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" MODIFY ("status_pendidikan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table GAMPONG
--------------------------------------------------------

  ALTER TABLE "DLM"."GAMPONG" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("id_kab" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("id_kec" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("nama_gampong" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("alamat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("keuchik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("visi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("misi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("koordinat_x" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("koordinat_y" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."GAMPONG" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."GAMPONG" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table KABUPATEN
--------------------------------------------------------

  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("nama_kabkot" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("ibukota" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("alamat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("bupati" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("wakil" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("luas_wilayah" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("visi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("misi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("koordinat_x" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("koordinat_y" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KABUPATEN" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table ARTIKEL
--------------------------------------------------------

  ALTER TABLE "DLM"."ARTIKEL" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ARTIKEL" MODIFY ("is_visible" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ARTIKEL" MODIFY ("userid" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ARTIKEL" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ARTIKEL" MODIFY ("title" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ARTIKEL" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table JENIS_PMKS
--------------------------------------------------------

  ALTER TABLE "DLM"."JENIS_PMKS" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."JENIS_PMKS" MODIFY ("nama_pmks" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."JENIS_PMKS" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."JENIS_PMKS" MODIFY ("userid" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."JENIS_PMKS" MODIFY ("tglinput" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PANTI_ASUHAN
--------------------------------------------------------

  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("nama_pantiasuhan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("type_pantiasuhan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("alamat_pantiasuhan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("jlh_kamar" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("jlh_perpustakaan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("jlh_aula" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("jlh_mushalla" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("jlh_kamar_mandi_wc" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("koordinat_x" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("koordinat_y" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."PANTI_ASUHAN" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PENYAKIT
--------------------------------------------------------

  ALTER TABLE "DLM"."PENYAKIT" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PENYAKIT" MODIFY ("nama_penyakit" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PENYAKIT" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PENYAKIT" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."PENYAKIT" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table KECAMATAN
--------------------------------------------------------

  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("id_kab" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("nama_kec" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("ibukota" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("alamat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("camat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("visi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("misi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("koordinat_x" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("koordinat_y" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KECAMATAN" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table KEPALA_KELUARGA_DETAILS
--------------------------------------------------------

  ALTER TABLE "DLM"."KEPALA_KELUARGA_DETAILS" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA_DETAILS" MODIFY ("no_kk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA_DETAILS" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA_DETAILS" MODIFY ("hub_keluarga" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA_DETAILS" MODIFY ("userid" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA_DETAILS" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA_DETAILS" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table ZAKAT
--------------------------------------------------------

  ALTER TABLE "DLM"."ZAKAT" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ZAKAT" MODIFY ("id_jenis_zakat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ZAKAT" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ZAKAT" MODIFY ("id_instansi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ZAKAT" MODIFY ("jlh_zakat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ZAKAT" MODIFY ("tgl_diberikan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ZAKAT" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."ZAKAT" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."ZAKAT" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table BIODATA
--------------------------------------------------------

  ALTER TABLE "DLM"."BIODATA" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("nama_lgkp" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("tmpt_lhr" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" ADD PRIMARY KEY ("nik")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("tgl_lhr" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("akta_lhr" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("gol_drh" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("jenis_kelamin" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("agama" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("stat_kwn" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("pnydng_cct" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("pddk_akh" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("stat_hidup" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("no_kab" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("no_kec" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("no_kel" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BIODATA" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table BANTUAN_SOSIAL
--------------------------------------------------------

  ALTER TABLE "DLM"."BANTUAN_SOSIAL" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" MODIFY ("no_kk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" MODIFY ("status_bantuan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" MODIFY ("id_instansi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" MODIFY ("tahun_diberikan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USER
--------------------------------------------------------

  ALTER TABLE "DLM"."USER" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."USER" MODIFY ("username" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."USER" MODIFY ("password" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."USER" MODIFY ("role_id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."USER" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table RUMAH
--------------------------------------------------------

  ALTER TABLE "DLM"."RUMAH" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("no_kk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("status_rumah" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("jenis_lantai" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("jenis_dinding" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("fasilitas_wc" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("penerangan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("air_minum" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("bahan_bakar_masak" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("rumah_bantuan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("koordinat_x" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("koordinat_y" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("photo" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("jlh_makan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("lauk_pauk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("daging" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("pakaian" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RUMAH" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."RUMAH" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table KELAS_BANTUAN
--------------------------------------------------------

  ALTER TABLE "DLM"."KELAS_BANTUAN" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELAS_BANTUAN" MODIFY ("nama_kelas_bantuan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELAS_BANTUAN" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELAS_BANTUAN" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."KELAS_BANTUAN" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table INSTANSI
--------------------------------------------------------

  ALTER TABLE "DLM"."INSTANSI" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."INSTANSI" MODIFY ("id_kab" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."INSTANSI" MODIFY ("nama_instansi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."INSTANSI" MODIFY ("alamat_instansi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."INSTANSI" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."INSTANSI" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."INSTANSI" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table KELOMPOK
--------------------------------------------------------

  ALTER TABLE "DLM"."KELOMPOK" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELOMPOK" MODIFY ("id_kab" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELOMPOK" MODIFY ("nama_kelompok" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELOMPOK" MODIFY ("ketua_kelompok" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELOMPOK" MODIFY ("bidang_usaha" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELOMPOK" MODIFY ("no_telp" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELOMPOK" MODIFY ("alamat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELOMPOK" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELOMPOK" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."KELOMPOK" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table DAYAH
--------------------------------------------------------

  ALTER TABLE "DLM"."DAYAH" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("id_kab" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("nama_dayah" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("nama_pimpinan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("type_dayah" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("jlh_pengasuh" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("jlh_santri" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("nama_tingkat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("jlh_kelas" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("jlh_perpustakaan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("jlh_lab_komputer" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("jlh_mushalla" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("jlh_kamar_mandi_wc" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."DAYAH" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."DAYAH" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table BANTUAN_MODAL
--------------------------------------------------------

  ALTER TABLE "DLM"."BANTUAN_MODAL" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" MODIFY ("no_kk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" MODIFY ("status_bantuan_modal" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" MODIFY ("kategory_bantuan_modal" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" MODIFY ("id_instansi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" MODIFY ("tahun_diberi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" MODIFY ("sumber_bantuan_modal" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table BANTUAN_LAINNYA
--------------------------------------------------------

  ALTER TABLE "DLM"."BANTUAN_LAINNYA" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" MODIFY ("no_kk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" MODIFY ("status_bantuan_lainnya" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" MODIFY ("id_instansi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" MODIFY ("tahun_diberikan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" MODIFY ("kategory_bantuan_lainnya" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" MODIFY ("sumber_modal_bantuan_lainnya" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PUSKESMAS
--------------------------------------------------------

  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("nama_puskesmas" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("jenis_puskesmas" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("alamat_puskesmas" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("jlh_dokter_umum" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("jlh_dokter_gigi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("jlh_tenaga_lab" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("jlh_tenaga_farmasi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("jlh_perawat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("jlh_ruang_inap" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("jlh_ruang_layanan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PUSKESMAS" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."PUSKESMAS" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PEKERJAAN
--------------------------------------------------------

  ALTER TABLE "DLM"."PEKERJAAN" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PEKERJAAN" MODIFY ("nama_pekerjaan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PEKERJAAN" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."PEKERJAAN" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."PEKERJAAN" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table KELAS_BANTUAN_LAINNYA
--------------------------------------------------------

  ALTER TABLE "DLM"."KELAS_BANTUAN_LAINNYA" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELAS_BANTUAN_LAINNYA" MODIFY ("id_bantuan_lainnya" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELAS_BANTUAN_LAINNYA" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."KELAS_BANTUAN_LAINNYA" MODIFY ("id_kelas_bantuan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELAS_BANTUAN_LAINNYA" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KELAS_BANTUAN_LAINNYA" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table RIWAYAT_KESEHATAN
--------------------------------------------------------

  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" MODIFY ("no_kk" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" MODIFY ("id_penyakit" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" MODIFY ("tgl_sakit" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" MODIFY ("id_puskesmas" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table KOPERASI
--------------------------------------------------------

  ALTER TABLE "DLM"."KOPERASI" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KOPERASI" MODIFY ("id_kab" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KOPERASI" MODIFY ("nama_koperasi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KOPERASI" MODIFY ("ketua_koperasi" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KOPERASI" MODIFY ("bidang_usaha" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KOPERASI" MODIFY ("no_telp" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KOPERASI" MODIFY ("alamat" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KOPERASI" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."KOPERASI" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."KOPERASI" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table RIWAYAT_PEKERJAAN
--------------------------------------------------------

  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" MODIFY ("id" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" MODIFY ("nik" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" MODIFY ("id_pek" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" MODIFY ("tempat_bekerja" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" MODIFY ("rata_penghasilan" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" MODIFY ("tahun" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" MODIFY ("tglinput" NOT NULL ENABLE);
 
  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" ADD PRIMARY KEY ("id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" MODIFY ("userid" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table ARTIKEL
--------------------------------------------------------

  ALTER TABLE "DLM"."ARTIKEL" ADD CONSTRAINT "FK_ARTIKEL_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BANTUAN_LAINNYA
--------------------------------------------------------

  ALTER TABLE "DLM"."BANTUAN_LAINNYA" ADD CONSTRAINT "FK_BANTUAN_LAINNYA_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" ADD CONSTRAINT "FK_BANTUAN_LAINNYA_INSTANSI" FOREIGN KEY ("id_instansi")
	  REFERENCES "DLM"."INSTANSI" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" ADD CONSTRAINT "FK_BANTUAN_LAINNYA_KELOMPOK" FOREIGN KEY ("id_kelompok")
	  REFERENCES "DLM"."KELOMPOK" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" ADD CONSTRAINT "FK_BANTUAN_LAINNYA_KK" FOREIGN KEY ("no_kk")
	  REFERENCES "DLM"."KEPALA_KELUARGA" ("no_kk") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" ADD CONSTRAINT "FK_BANTUAN_LAINNYA_KOPERASI" FOREIGN KEY ("id_koperasi")
	  REFERENCES "DLM"."KOPERASI" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_LAINNYA" ADD CONSTRAINT "FK_BANTUAN_LAINNYA_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BANTUAN_MODAL
--------------------------------------------------------

  ALTER TABLE "DLM"."BANTUAN_MODAL" ADD CONSTRAINT "FK_BANTUAN_MODAL_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" ADD CONSTRAINT "FK_BANTUAN_MODAL_INSTANSI" FOREIGN KEY ("id_instansi")
	  REFERENCES "DLM"."INSTANSI" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" ADD CONSTRAINT "FK_BANTUAN_MODAL_KELOMPOK" FOREIGN KEY ("id_kelompok")
	  REFERENCES "DLM"."KELOMPOK" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" ADD CONSTRAINT "FK_BANTUAN_MODAL_KK" FOREIGN KEY ("no_kk")
	  REFERENCES "DLM"."KEPALA_KELUARGA" ("no_kk") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" ADD CONSTRAINT "FK_BANTUAN_MODAL_KOPERASI" FOREIGN KEY ("id_koperasi")
	  REFERENCES "DLM"."KOPERASI" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_MODAL" ADD CONSTRAINT "FK_BANTUAN_MODAL_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BANTUAN_SOSIAL
--------------------------------------------------------

  ALTER TABLE "DLM"."BANTUAN_SOSIAL" ADD CONSTRAINT "FK_BANTUAN_SOSIAL_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" ADD CONSTRAINT "FK_BANTUAN_SOSIAL_INSTANSI" FOREIGN KEY ("id_instansi")
	  REFERENCES "DLM"."INSTANSI" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" ADD CONSTRAINT "FK_BANTUAN_SOSIAL_KK" FOREIGN KEY ("no_kk")
	  REFERENCES "DLM"."KEPALA_KELUARGA" ("no_kk") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BANTUAN_SOSIAL" ADD CONSTRAINT "FK_BANTUAN_SOSIAL_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BIODATA
--------------------------------------------------------

  ALTER TABLE "DLM"."BIODATA" ADD CONSTRAINT "FK_BIODATA_GAMPONG" FOREIGN KEY ("no_kel")
	  REFERENCES "DLM"."GAMPONG" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BIODATA" ADD CONSTRAINT "FK_BIODATA_KABUPATEN" FOREIGN KEY ("no_kab")
	  REFERENCES "DLM"."KABUPATEN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BIODATA" ADD CONSTRAINT "FK_BIODATA_KECAMATAN" FOREIGN KEY ("no_kec")
	  REFERENCES "DLM"."KECAMATAN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BIODATA" ADD CONSTRAINT "FK_BIODATA_RIWAYAT_PEKERJAAN" FOREIGN KEY ("kode_pkrj")
	  REFERENCES "DLM"."RIWAYAT_PEKERJAAN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."BIODATA" ADD CONSTRAINT "FK_BIODATA_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table DAYAH
--------------------------------------------------------

  ALTER TABLE "DLM"."DAYAH" ADD CONSTRAINT "FK_DAYAH_KABUPATEN" FOREIGN KEY ("id_kab")
	  REFERENCES "DLM"."KABUPATEN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."DAYAH" ADD CONSTRAINT "FK_DAYAH_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table GAMPONG
--------------------------------------------------------

  ALTER TABLE "DLM"."GAMPONG" ADD CONSTRAINT "FK_GAMPONG_KABUPATEN" FOREIGN KEY ("id_kab")
	  REFERENCES "DLM"."KABUPATEN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."GAMPONG" ADD CONSTRAINT "FK_GAMPONG_KECAMATAN" FOREIGN KEY ("id_kec")
	  REFERENCES "DLM"."KECAMATAN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."GAMPONG" ADD CONSTRAINT "FK_GAMPONG_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table INSTANSI
--------------------------------------------------------

  ALTER TABLE "DLM"."INSTANSI" ADD CONSTRAINT "FK_INSTANSI_KABUPATEN" FOREIGN KEY ("id_kab")
	  REFERENCES "DLM"."KABUPATEN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."INSTANSI" ADD CONSTRAINT "FK_INSTANSI_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table JENIS_PMKS
--------------------------------------------------------

  ALTER TABLE "DLM"."JENIS_PMKS" ADD CONSTRAINT "FK_JENIS_PMKS_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table KABUPATEN
--------------------------------------------------------

  ALTER TABLE "DLM"."KABUPATEN" ADD CONSTRAINT "FK_KABUPATEN_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table KECAMATAN
--------------------------------------------------------

  ALTER TABLE "DLM"."KECAMATAN" ADD CONSTRAINT "FK_KECAMATAN_KABUPATEN" FOREIGN KEY ("id_kab")
	  REFERENCES "DLM"."KABUPATEN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."KECAMATAN" ADD CONSTRAINT "FK_KECAMATAN_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table KELAS_BANTUAN
--------------------------------------------------------

  ALTER TABLE "DLM"."KELAS_BANTUAN" ADD CONSTRAINT "FK_KELAS_BANTUAN_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table KELAS_BANTUAN_LAINNYA
--------------------------------------------------------

  ALTER TABLE "DLM"."KELAS_BANTUAN_LAINNYA" ADD CONSTRAINT "FK_KBL_BL" FOREIGN KEY ("id_bantuan_lainnya")
	  REFERENCES "DLM"."BANTUAN_LAINNYA" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."KELAS_BANTUAN_LAINNYA" ADD CONSTRAINT "FK_KBL_KB" FOREIGN KEY ("id_kelas_bantuan")
	  REFERENCES "DLM"."KELAS_BANTUAN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."KELAS_BANTUAN_LAINNYA" ADD CONSTRAINT "FK_KELAS_BANTUAN_LAINNYA_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table KELOMPOK
--------------------------------------------------------

  ALTER TABLE "DLM"."KELOMPOK" ADD CONSTRAINT "FK_KELOMPOK_KABUPATEN" FOREIGN KEY ("id_kab")
	  REFERENCES "DLM"."KABUPATEN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."KELOMPOK" ADD CONSTRAINT "FK_KELOMPOK_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table KEPALA_KELUARGA
--------------------------------------------------------

  ALTER TABLE "DLM"."KEPALA_KELUARGA" ADD CONSTRAINT "FK_KEPALAKELUARGA_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA" ADD CONSTRAINT "FK_KEPALA_KELUARGA_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table KEPALA_KELUARGA_DETAILS
--------------------------------------------------------

  ALTER TABLE "DLM"."KEPALA_KELUARGA_DETAILS" ADD CONSTRAINT "FK_KKD_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA_DETAILS" ADD FOREIGN KEY ("no_kk")
	  REFERENCES "DLM"."KEPALA_KELUARGA" ("no_kk") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."KEPALA_KELUARGA_DETAILS" ADD FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table KEPEMILIKAN_ASSET
--------------------------------------------------------

  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" ADD CONSTRAINT "FK_KEPEMILIKAN_ASSET_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" ADD CONSTRAINT "FK_KEPEMILIKAN_ASSET_KK" FOREIGN KEY ("no_kk")
	  REFERENCES "DLM"."KEPALA_KELUARGA" ("no_kk") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."KEPEMILIKAN_ASSET" ADD CONSTRAINT "FK_KEPEMILIKAN_ASSET_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table KOPERASI
--------------------------------------------------------

  ALTER TABLE "DLM"."KOPERASI" ADD CONSTRAINT "FK_KOPERASI_KABUPATEN" FOREIGN KEY ("id_kab")
	  REFERENCES "DLM"."KABUPATEN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."KOPERASI" ADD CONSTRAINT "FK_KOPERASI_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PANTI_ASUHAN
--------------------------------------------------------

  ALTER TABLE "DLM"."PANTI_ASUHAN" ADD CONSTRAINT "FK_PANTI_ASUHAN_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PEKERJAAN
--------------------------------------------------------

  ALTER TABLE "DLM"."PEKERJAAN" ADD CONSTRAINT "FK_PEKERJAAN_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PENYAKIT
--------------------------------------------------------

  ALTER TABLE "DLM"."PENYAKIT" ADD CONSTRAINT "FK_PENYAKIT_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PMKS
--------------------------------------------------------

  ALTER TABLE "DLM"."PMKS" ADD CONSTRAINT "FK_PMKS_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."PMKS" ADD CONSTRAINT "FK_PMKS_JENIS_PMKS" FOREIGN KEY ("id_jenis_pmks")
	  REFERENCES "DLM"."JENIS_PMKS" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."PMKS" ADD CONSTRAINT "FK_PMKS_KEPALA_KELUARGA" FOREIGN KEY ("no_kk")
	  REFERENCES "DLM"."KEPALA_KELUARGA" ("no_kk") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."PMKS" ADD CONSTRAINT "FK_PMKS_PANTI_ASUHAN" FOREIGN KEY ("id_pantiasuhan")
	  REFERENCES "DLM"."PANTI_ASUHAN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."PMKS" ADD CONSTRAINT "FK_PMKS_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PUSKESMAS
--------------------------------------------------------

  ALTER TABLE "DLM"."PUSKESMAS" ADD CONSTRAINT "FK_PUSKESMAS_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table RIWAYAT_KESEHATAN
--------------------------------------------------------

  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" ADD CONSTRAINT "FK_RIWAYAT_KESEHATAN_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" ADD CONSTRAINT "FK_RIWAYAT_KESEHATAN_KK" FOREIGN KEY ("no_kk")
	  REFERENCES "DLM"."KEPALA_KELUARGA" ("no_kk") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" ADD CONSTRAINT "FK_RIWAYAT_KESEHATAN_PENYAKIT" FOREIGN KEY ("id_penyakit")
	  REFERENCES "DLM"."PENYAKIT" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" ADD CONSTRAINT "FK_RIWAYAT_KESEHATAN_PUSKESMAS" FOREIGN KEY ("id_puskesmas")
	  REFERENCES "DLM"."PUSKESMAS" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_KESEHATAN" ADD CONSTRAINT "FK_RIWAYAT_KESEHATAN_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table RIWAYAT_PEKERJAAN
--------------------------------------------------------

  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" ADD CONSTRAINT "FK_RIWAYAT_PEKERJAAN_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" ADD CONSTRAINT "FK_RIWAYAT_PEKERJAAN_PEKERJAAN" FOREIGN KEY ("id_pek")
	  REFERENCES "DLM"."PEKERJAAN" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_PEKERJAAN" ADD CONSTRAINT "FK_RIWAYAT_PEKERJAAN_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table RIWAYAT_PENDIDIKAN
--------------------------------------------------------

  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" ADD CONSTRAINT "FK_RIWAYAT_PENDIDIKAN_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" ADD CONSTRAINT "FK_RIWAYAT_PENDIDIKAN_KK" FOREIGN KEY ("no_kk")
	  REFERENCES "DLM"."KEPALA_KELUARGA" ("no_kk") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" ADD CONSTRAINT "FK_RIWAYAT_PENDIDIKAN_SEKOLAH" FOREIGN KEY ("kode_sekolah")
	  REFERENCES "DLM"."SEKOLAH" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RIWAYAT_PENDIDIKAN" ADD CONSTRAINT "FK_RIWAYAT_PENDIDIKAN_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table RUMAH
--------------------------------------------------------

  ALTER TABLE "DLM"."RUMAH" ADD CONSTRAINT "FK_RUMAH_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RUMAH" ADD CONSTRAINT "FK_RUMAH_KEPALA_KELUARGA" FOREIGN KEY ("no_kk")
	  REFERENCES "DLM"."KEPALA_KELUARGA" ("no_kk") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."RUMAH" ADD CONSTRAINT "FK_RUMAH_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SEKOLAH
--------------------------------------------------------

  ALTER TABLE "DLM"."SEKOLAH" ADD CONSTRAINT "FK_SEKOLAH_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table USER
--------------------------------------------------------

  ALTER TABLE "DLM"."USER" ADD CONSTRAINT "FK_USER_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ZAKAT
--------------------------------------------------------

  ALTER TABLE "DLM"."ZAKAT" ADD CONSTRAINT "FK_ZAKAT_BIODATA" FOREIGN KEY ("nik")
	  REFERENCES "DLM"."BIODATA" ("nik") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."ZAKAT" ADD CONSTRAINT "FK_ZAKAT_INSTANSI" FOREIGN KEY ("id_instansi")
	  REFERENCES "DLM"."INSTANSI" ("id") ON DELETE CASCADE ENABLE;
 
  ALTER TABLE "DLM"."ZAKAT" ADD CONSTRAINT "FK_ZAKAT_USER" FOREIGN KEY ("userid")
	  REFERENCES "DLM"."USER" ("id") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_ARTIKEL
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_ARTIKEL" 
   before insert on "DLM"."ARTIKEL" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_ARTIKEL.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_ARTIKEL" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_BANTUAN_LAINNYA
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_BANTUAN_LAINNYA" 
   before insert on "DLM"."BANTUAN_LAINNYA" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_BANTUAN_LAINNYA.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_BANTUAN_LAINNYA" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_BANTUAN_MODAL
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_BANTUAN_MODAL" 
   before insert on "DLM"."BANTUAN_MODAL" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_BANTUAN_MODAL.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_BANTUAN_MODAL" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_BANTUAN_SOSIAL
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_BANTUAN_SOSIAL" 
   before insert on "DLM"."BANTUAN_SOSIAL" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_BANTUAN_SOSIAL.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_BANTUAN_SOSIAL" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_DAYAH
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_DAYAH" 
   before insert on "DLM"."DAYAH" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_DAYAH.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_DAYAH" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_GAMPONG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_GAMPONG" 
   before insert on "DLM"."GAMPONG" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_GAMPONG.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_GAMPONG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_INSTANSI
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_INSTANSI" 
   before insert on "DLM"."INSTANSI" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_INSTANSI.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_INSTANSI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_JENIS_PMKS
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_JENIS_PMKS" 
   before insert on "DLM"."JENIS_PMKS" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_JENIS_PMKS.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_JENIS_PMKS" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_KABUPATEN
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_KABUPATEN" 
   before insert on "DLM"."KABUPATEN" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_KABUPATEN.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_KABUPATEN" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_KECAMATAN
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_KECAMATAN" 
   before insert on "DLM"."KECAMATAN" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_KECAMATAN.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_KECAMATAN" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_KELAS_BANTUAN
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_KELAS_BANTUAN" 
   before insert on "DLM"."KELAS_BANTUAN" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_KELAS_BANTUAN.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_KELAS_BANTUAN" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_KELAS_BANTUAN_LAINNYA
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_KELAS_BANTUAN_LAINNYA" 
   before insert on "DLM"."KELAS_BANTUAN_LAINNYA" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_KELAS_BANTUAN_LAINNYA.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_KELAS_BANTUAN_LAINNYA" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_KELOMPOK
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_KELOMPOK" 
   before insert on "DLM"."KELOMPOK" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_KELOMPOK.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_KELOMPOK" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_KEPALA_KELUARGA_DETAILS
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_KEPALA_KELUARGA_DETAILS" 
   before insert on "DLM"."KEPALA_KELUARGA_DETAILS" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_KEPALA_KELUARGA_DETAILS.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_KEPALA_KELUARGA_DETAILS" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_KEPEMILIKAN_ASSET
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_KEPEMILIKAN_ASSET" 
   before insert on "DLM"."KEPEMILIKAN_ASSET" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_KEPEMILIKAN_ASSET.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_KEPEMILIKAN_ASSET" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_KOPERASI
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_KOPERASI" 
   before insert on "DLM"."KOPERASI" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_KOPERASI.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_KOPERASI" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_PANTI_ASUHAN
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_PANTI_ASUHAN" 
   before insert on "DLM"."PANTI_ASUHAN" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_PANTI_ASUHAN.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_PANTI_ASUHAN" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_PEKERJAAN
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_PEKERJAAN" 
   before insert on "DLM"."PEKERJAAN" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_PEKERJAAN.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_PEKERJAAN" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_PENYAKIT
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_PENYAKIT" 
   before insert on "DLM"."PENYAKIT" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_PENYAKIT.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_PENYAKIT" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_PMKS
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_PMKS" 
   before insert on "DLM"."PMKS" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_PMKS.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_PMKS" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_PUSKESMAS
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_PUSKESMAS" 
   before insert on "DLM"."PUSKESMAS" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_PUSKESMAS.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_PUSKESMAS" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_RIWAYAT_KESEHATAN
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_RIWAYAT_KESEHATAN" 
   before insert on "DLM"."RIWAYAT_KESEHATAN" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_RIWAYAT_KESEHATAN.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_RIWAYAT_KESEHATAN" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_RIWAYAT_PEKERJAAN
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_RIWAYAT_PEKERJAAN" 
   before insert on "DLM"."RIWAYAT_PEKERJAAN" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_RIWAYAT_PEKERJAAN.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_RIWAYAT_PEKERJAAN" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_RIWAYAT_PENDIDIKAN
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_RIWAYAT_PENDIDIKAN" 
   before insert on "DLM"."RIWAYAT_PENDIDIKAN" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_RIWAYAT_PENDIDIKAN.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_RIWAYAT_PENDIDIKAN" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_RUMAH
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_RUMAH" 
   before insert on "DLM"."RUMAH" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_RUMAH.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_RUMAH" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_SEKOLAH
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_SEKOLAH" 
   before insert on "DLM"."SEKOLAH" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_SEKOLAH.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_SEKOLAH" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_USER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_USER" 
   before insert on "DLM"."USER" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_USER.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_USER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRG_ZAKAT
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "DLM"."TRG_ZAKAT" 
   before insert on "DLM"."ZAKAT" 
   for each row 
begin  
   if inserting then 
      if :NEW."id" is null then 
         select SEC_ZAKAT.nextval into :NEW."id" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "DLM"."TRG_ZAKAT" ENABLE;
--------------------------------------------------------
--  DDL for Function GETHUBUNGANKELUARGATERAKHIR
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "DLM"."GETHUBUNGANKELUARGATERAKHIR" (nik NVARCHAR2 )
RETURN NVARCHAR2 IS
 rslt NVARCHAR2(50);
BEGIN
  rslt := '';
  SELECT "VIEW_BIODATA"."hub_keluarga" into  rslt 
  FROM "DLM"."VIEW_BIODATA"
  WHERE "VIEW_BIODATA"."nik"= nik
  AND ROWNUM = 1
  ORDER BY "VIEW_BIODATA"."hub_keluarga" ASC;
 RETURN rslt;
END GetHubunganKeluargaTerakhir;

/
--------------------------------------------------------
--  DDL for Function GETKEPALAKELUARGABYNOKK
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "DLM"."GETKEPALAKELUARGABYNOKK" (no_kk NVARCHAR2 )
RETURN NVARCHAR2 IS
 rslt NVARCHAR2(50);
BEGIN
  rslt := '';
  SELECT "VIEW_BIODATA"."nama_lgkp" into  rslt 
  FROM "DLM"."VIEW_BIODATA"
  WHERE "VIEW_BIODATA"."no_kk"= no_kk
  AND "VIEW_BIODATA"."hub_keluarga" = 1;
 RETURN rslt;
END GetKepalaKeluargaByNoKk;

/
--------------------------------------------------------
--  DDL for Function GETNIKNAMAAYAH
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "DLM"."GETNIKNAMAAYAH" (nik NVARCHAR2 )
RETURN NVARCHAR2 IS
 no_kk NVARCHAR2(200); 
 rslt NVARCHAR2 (200);
BEGIN
  rslt := '';
  SELECT "VIEW_BIODATA"."no_kk" into  no_kk 
  FROM "DLM"."VIEW_BIODATA"
  WHERE "VIEW_BIODATA"."nik"= nik
  AND "VIEW_BIODATA"."hub_keluarga" = 4;
  
  IF no_kk IS NOT NULL THEN
    SELECT "VIEW_BIODATA"."nik" || '<br/>' || "VIEW_BIODATA"."nama_lgkp" into rslt
    FROM "DLM"."VIEW_BIODATA"
    WHERE "VIEW_BIODATA"."no_kk"= no_kk
    AND (("VIEW_BIODATA"."hub_keluarga" = 1) OR ("VIEW_BIODATA"."hub_keluarga" = 1));
  END IF;
  
 	RETURN rslt;
END GetNikNamaAyah;

/
--------------------------------------------------------
--  DDL for Function GETNIKNAMAIBU
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "DLM"."GETNIKNAMAIBU" (nik NVARCHAR2 )
RETURN NVARCHAR2 IS
 no_kk NVARCHAR2(200); 
 rslt NVARCHAR2 (200);
BEGIN
  rslt := '';
  SELECT "VIEW_BIODATA"."no_kk" into  no_kk 
  FROM "DLM"."VIEW_BIODATA"
  WHERE "VIEW_BIODATA"."nik"= nik
  AND "VIEW_BIODATA"."hub_keluarga" = 4;
  
  IF no_kk IS NOT NULL THEN
    SELECT "VIEW_BIODATA"."nik" || '<br/>' || "VIEW_BIODATA"."nama_lgkp" into rslt
    FROM "DLM"."VIEW_BIODATA"
    WHERE "VIEW_BIODATA"."no_kk"= no_kk
    AND "VIEW_BIODATA"."hub_keluarga" = 3;
  END IF;
  
 	RETURN rslt;
END GetNikNamaIbu;

/
--------------------------------------------------------
--  DDL for Function GETNOKKTERAKHIR
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "DLM"."GETNOKKTERAKHIR" (nik NVARCHAR2 )
RETURN NVARCHAR2 IS
 rslt NVARCHAR2(50);
BEGIN
  rslt := '';
  SELECT "VIEW_KEPALA_KELUARGA"."no_kk" into  rslt 
  FROM "DLM"."VIEW_KEPALA_KELUARGA"
  WHERE "VIEW_KEPALA_KELUARGA"."nik"= nik
  AND ROWNUM = 1
  ORDER BY "VIEW_KEPALA_KELUARGA"."hub_keluarga" ASC;
 RETURN rslt;
END GetNoKkTerakhir;

/
--------------------------------------------------------
--  DDL for Function GETPEKERJAANTERAKHIRKKBYNOKK
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "DLM"."GETPEKERJAANTERAKHIRKKBYNOKK" (no_kk NVARCHAR2 )
RETURN NVARCHAR2 IS
 nik NVARCHAR2(50);
 rslt NVARCHAR2(50);
BEGIN
  rslt := '';
  SELECT "VIEW_BIODATA"."nik" into  nik 
  FROM "DLM"."VIEW_BIODATA"
  WHERE "VIEW_BIODATA"."no_kk"= no_kk
  AND "VIEW_BIODATA"."hub_keluarga" = 1;
  
   IF nik IS NOT NULL THEN
   		SELECT "DLM".GetRiwayatPekerjaanTerakhir(nik) into rslt FROM dual;
   END IF;
   
 RETURN rslt;
END GetPekerjaanTerakhirKKByNoKk;

/
--------------------------------------------------------
--  DDL for Function GETPENGHASILANTERAKHIRKKBYNOKK
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "DLM"."GETPENGHASILANTERAKHIRKKBYNOKK" (no_kk NVARCHAR2 )
RETURN NVARCHAR2 IS
 nik NVARCHAR2(50);
 rslt NVARCHAR2(50);
BEGIN
  rslt := '';
  SELECT "VIEW_BIODATA"."nik" into  nik 
  FROM "DLM"."VIEW_BIODATA"
  WHERE "VIEW_BIODATA"."no_kk"= no_kk
  AND "VIEW_BIODATA"."hub_keluarga" = 1;
  
   IF nik IS NOT NULL THEN
   	  SELECT "RIWAYAT_PEKERJAAN"."rata_penghasilan" into  rslt 
	  FROM "DLM"."RIWAYAT_PEKERJAAN"
	  JOIN "DLM"."PEKERJAAN"
	  ON "RIWAYAT_PEKERJAAN"."id_pek" = "PEKERJAAN"."id"
	  WHERE "RIWAYAT_PEKERJAAN"."nik"= nik
	  AND ROWNUM = 1
	  ORDER BY "RIWAYAT_PEKERJAAN"."id" DESC;
   END IF;
   
 RETURN rslt;
END GetPenghasilanTerakhirKKByNoKk;

/
--------------------------------------------------------
--  DDL for Function GETRIWAYATPEKERJAANTERAKHIR
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "DLM"."GETRIWAYATPEKERJAANTERAKHIR" (nik NVARCHAR2 )
RETURN NVARCHAR2 IS
 rslt NVARCHAR2(50);
BEGIN
  rslt := '';
  SELECT "PEKERJAAN"."nama_pekerjaan" into  rslt 
  FROM "DLM"."RIWAYAT_PEKERJAAN"
  JOIN "DLM"."PEKERJAAN"
  ON "RIWAYAT_PEKERJAAN"."id_pek" = "PEKERJAAN"."id"
  WHERE "RIWAYAT_PEKERJAAN"."nik"= nik
  AND ROWNUM = 1
  ORDER BY "RIWAYAT_PEKERJAAN"."id" DESC;
 RETURN rslt;
END GetRiwayatPekerjaanTerakhir;

/
--------------------------------------------------------
--  DDL for Function GETRIWAYATPENDIDIKANTERAKHIR
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "DLM"."GETRIWAYATPENDIDIKANTERAKHIR" (nik NVARCHAR2 )
RETURN NVARCHAR2 IS
 rslt NVARCHAR2(50);
BEGIN
  rslt := '';
  SELECT "RIWAYAT_PENDIDIKAN"."jenjang_sekolah" into  rslt 
  FROM "DLM"."RIWAYAT_PENDIDIKAN"
  WHERE "RIWAYAT_PENDIDIKAN"."nik"= nik
  AND ROWNUM = 1
  ORDER BY "RIWAYAT_PENDIDIKAN"."id" DESC;
 RETURN rslt;
END GetRiwayatPendidikanTerakhir;

/
--------------------------------------------------------
--  DDL for Function GETRIWAYATPENDIDIKANTERAKHIRID
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "DLM"."GETRIWAYATPENDIDIKANTERAKHIRID" (nik NVARCHAR2 )
RETURN NVARCHAR2 IS
 rslt NUMBER;
BEGIN
  rslt := NULL;
  SELECT "RIWAYAT_PENDIDIKAN"."id" into  rslt 
  FROM "DLM"."RIWAYAT_PENDIDIKAN"
  WHERE "RIWAYAT_PENDIDIKAN"."nik"= nik
  AND ROWNUM = 1
  ORDER BY "RIWAYAT_PENDIDIKAN"."id" DESC;
 RETURN rslt;
END GetRiwayatPendidikanTerakhirId;

/
