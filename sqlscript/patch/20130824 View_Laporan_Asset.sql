﻿CREATE OR REPLACE FORCE VIEW "DLM"."VIEW_LAPORAN_ASSET" ("nik", "no_kk", "punya_tanah_rumah",
 "luas_tanah_rumah", "punya_tanah_sawah", "luas_tanah_sawah", "punya_tanah_kebun",
 "luast_tanah_kebun", "punya_tambak", "luas_tambak", "punya_lembu", 
 "jlh_lembu", "punya_kerbau", "jlh_kerbau", "punya_ternak_lain", 
 "jlh_ternak_lain", "punya_mobil", "jlh_mobil", "punya_motor", "jlh_motor", 
 "punya_tabungan", "punya_asuransi", "punya_motor_tempel", "punya_kapal_motor", 
 "punya_perahu", "punya_aset_lain", "nama_lgkp", "hub_keluarga", "tgl_lhr",
 "agama", "pekerjaan", "alamat") 
 AS
  SELECT "KEPEMILIKAN_ASSET"."nik"
		,"KEPEMILIKAN_ASSET"."no_kk"
		,"KEPEMILIKAN_ASSET"."punya_tanah_rumah"
		,"KEPEMILIKAN_ASSET"."luas_tanah_rumah"
		,"KEPEMILIKAN_ASSET"."punya_tanah_sawah"
		,"KEPEMILIKAN_ASSET"."luas_tanah_sawah"
		,"KEPEMILIKAN_ASSET"."punya_tanah_kebun"
		,"KEPEMILIKAN_ASSET"."luast_tanah_kebun"
		,"KEPEMILIKAN_ASSET"."punya_tambak"
		,"KEPEMILIKAN_ASSET"."luas_tambak"
		,"KEPEMILIKAN_ASSET"."punya_lembu"
		,"KEPEMILIKAN_ASSET"."jlh_lembu"
		,"KEPEMILIKAN_ASSET"."punya_kerbau"
		,"KEPEMILIKAN_ASSET"."jlh_kerbau"
		,"KEPEMILIKAN_ASSET"."punya_ternak_lain"
		,"KEPEMILIKAN_ASSET"."jlh_ternak_lain"
		,"KEPEMILIKAN_ASSET"."punya_mobil"
		,"KEPEMILIKAN_ASSET"."jlh_mobil"
		,"KEPEMILIKAN_ASSET"."punya_motor"
		,"KEPEMILIKAN_ASSET"."jlh_motor"
		,"KEPEMILIKAN_ASSET"."punya_tabungan"
		,"KEPEMILIKAN_ASSET"."punya_asuransi"
		,"KEPEMILIKAN_ASSET"."punya_motor_tempel"
		,"KEPEMILIKAN_ASSET"."punya_kapal_motor"
		,"KEPEMILIKAN_ASSET"."punya_perahu"
		,"KEPEMILIKAN_ASSET"."punya_aset_lain"
		,"BIODATA"."nama_lgkp"
		,(SELECT "DLM".GetHubunganKeluargaTerakhir("KEPEMILIKAN_ASSET"."nik") FROM dual) AS "hub_keluarga"
		,"BIODATA"."tgl_lhr"
		,"BIODATA"."agama"
		,(SELECT "DLM".GetRiwayatPekerjaanTerakhir("KEPEMILIKAN_ASSET"."nik") FROM dual) AS "pekerjaan"
		,"GAMPONG"."nama_gampong" || '<br/>' 
		|| "KECAMATAN"."nama_kec" || '<br/>'
		|| "KABUPATEN"."nama_kabkot" AS "alamat"
	FROM "DLM"."KEPEMILIKAN_ASSET" 
	JOIN "DLM"."BIODATA"
	ON "BIODATA"."nik" = "KEPEMILIKAN_ASSET"."nik"
	JOIN "DLM"."KABUPATEN"
	ON "KABUPATEN"."id" = "BIODATA"."no_kab"
	JOIN "DLM"."KECAMATAN"
	ON "KECAMATAN"."id" = "BIODATA"."no_kec"
	JOIN "DLM"."GAMPONG"
	ON "GAMPONG"."id" = "BIODATA"."no_kel"