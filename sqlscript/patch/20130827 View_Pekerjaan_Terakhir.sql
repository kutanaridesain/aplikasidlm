﻿CREATE OR REPLACE FORCE VIEW "DLM"."VIEW_PEKERJAAN_TERAKHIR" ("nik"
		,"id_pek"
		,"tempat_bekerja"
		,"rata_penghasilan"
		,"tahun"
		,"rk"
)
AS
WITH summary AS (
SELECT "nik"
		,"id_pek"
		,"tempat_bekerja"
		,"rata_penghasilan"
		,"tahun"
		,ROW_NUMBER() OVER(PARTITION BY "RIWAYAT_PEKERJAAN"."nik"
                                 ORDER BY "RIWAYAT_PEKERJAAN"."tahun" DESC) AS rk
	FROM "DLM"."RIWAYAT_PEKERJAAN" 
	)
SELECT S.*
  FROM summary S
 WHERE S.rk = 1