﻿CREATE OR REPLACE FORCE VIEW "DLM"."VIEW_USER_LOCATION" ("id", "name", "role_id")
AS
SELECT "KABUPATEN"."id" AS id,
	   "KABUPATEN"."nama_kabkot" || ' (Kabupaten)' as name,
	   '2' AS role_id
FROM "DLM"."KABUPATEN"
UNION
SELECT "KECAMATAN"."id" AS id,
	   "KECAMATAN"."nama_kec" || ' - ' || "KABUPATEN"."nama_kabkot" || ' (Kecamatan)' as name,
	   '3' AS role_id
FROM "DLM"."KECAMATAN"
JOIN "DLM"."KABUPATEN"
ON "KECAMATAN"."id_kab" = "KABUPATEN"."id"
UNION
SELECT "GAMPONG"."id" AS id,
	   "GAMPONG"."nama_gampong" || ' - ' ||  "KECAMATAN"."nama_kec" || ' - ' || "KABUPATEN"."nama_kabkot" || ' (Gampong)' as name,
	   '4' AS role_id
FROM "DLM"."GAMPONG"
JOIN "DLM"."KECAMATAN"
ON "GAMPONG"."id_kec" = "KECAMATAN"."id"
JOIN "DLM"."KABUPATEN"
ON "KECAMATAN"."id_kab" = "KABUPATEN"."id"