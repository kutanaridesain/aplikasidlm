﻿create or replace 
FUNCTION       "GETNAMAPENYAKITBYID" (id_penyakit NUMBER )
RETURN NVARCHAR2 IS
 rslt NVARCHAR2(50);
BEGIN
  rslt := '';
  SELECT "PENYAKIT"."nama_penyakit" into  rslt 
  FROM "DLM"."PENYAKIT"
  WHERE "PENYAKIT"."id"= id_penyakit;
 RETURN rslt;
END GetNamaPenyakitById;