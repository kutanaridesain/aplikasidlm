﻿CREATE OR REPLACE 
FUNCTION  GetNikNamaIbu (nik NVARCHAR2 )
RETURN NVARCHAR2 IS
 no_kk NVARCHAR2(200); 
 rslt NVARCHAR2 (200);
BEGIN
  rslt := '-';
  SELECT "VIEW_BIODATA"."no_kk" into  no_kk 
  FROM "DLM"."VIEW_BIODATA"
  WHERE "VIEW_BIODATA"."nik"= nik
  AND "VIEW_BIODATA"."hub_keluarga" = 4;
  
  IF no_kk IS NOT NULL THEN
    SELECT "VIEW_BIODATA"."nik" || '<br/>' || "VIEW_BIODATA"."nama_lgkp" into rslt
    FROM "DLM"."VIEW_BIODATA"
    WHERE "VIEW_BIODATA"."no_kk"= no_kk
    AND "VIEW_BIODATA"."hub_keluarga" = 3;
  END IF;
  
 	RETURN rslt;
END GetNikNamaIbu;