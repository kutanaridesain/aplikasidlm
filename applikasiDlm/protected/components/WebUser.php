<?php
class WebUser extends CWebUser {
 
	// Store model to not repeat query.
	private $_model;
 
	function isSuperAdmin(){
		$user = $this->loadUser(Yii::app()->user->id);
		return intval($user->role_id) == 0;
	}

	function isAdmin(){
		$user = $this->loadUser(Yii::app()->user->id);
		return intval($user->role_id) == 1;
	}
	
	function isAllowManageUser(){
		$user = $this->loadUser(Yii::app()->user->id);
		if($user!==null){
			return ((intval($user->role_id) == 0 ) || (intval($user->role_id) == 1 ) );
		}else{
			return false;
		}
	}
	
	function getRoleId(){
		$user = $this->loadUser(Yii::app()->user->id);
		return $user->role_id;
	}	
	
	function getLocationId(){
		$user = $this->loadUser(Yii::app()->user->id);
		return $user->location_id;
	}
	
	function getKabupatenId(){
		$user = $this->loadUser(Yii::app()->user->id);
		$sql = 'SELECT "kabupaten_id"
				FROM view_user_location 
				WHERE  "id"='.$user->location_id.' AND "role_id"=' .$user->role_id;
		
		$kabupatens = Yii::app()->db->createCommand($sql)->queryAll();

		return $kabupatens[0]["kabupaten_id"];
	}
 
	protected function loadUser($id=null)
    {
        if($this->_model===null)
        {
            if($id!==null)
                $this->_model=User::model()->findByPk($id);
        }
        return $this->_model;
    }
}
