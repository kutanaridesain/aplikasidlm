<?php

class Utils 
{
	public static function toOracleDateFormat($strDate)
    {
		if((!isset($strDate) || trim($strDate)==='')){
			return $strDate;
		}
		
		list($a, $b, $c) = explode('/', $strDate);
		$strDate = $b."/".$a."/".$c;
		
		
        $time = strtotime($strDate);
		$newformat = date('d-M-y',$time);
		return $newformat;
    }
	
	public static function fromOracleDateFormat ($dtDate){
		$newformat = date("d/m/Y",strtotime($dtDate));
		return $newformat;
	}
	
	public static function IsNullOrEmpty($question){
		return (!isset($question) || trim($question)==='');
	}
	
		public static function idDate($date = null, $format = null)
	{
		//TODO: user generic setting
		$date = !empty($date) ? $date : time() ;
		$format = !empty($format) ? $format : 'Y-m-d';


		$idDayName = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');//kode w
		$idMonthName = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

		$formatedDate = sprintf('%s, %d %s %d', $idDayName[date('w')], date('d'), $idMonthName[date('n')-1], date('Y'));

		return $formatedDate;
	}
}
