<?php

class DlmController extends Controller
{
	/*
		Pekerjaan
	*/
	public function getPekerjaanOptions()
	{
		$pekerjaanDataProvider = Yii::app()->db->createCommand()
		  ->from('PEKERJAAN')
		  ->order('PEKERJAAN.nama_pekerjaan')
		  ->queryAll();
		$pekerjaanArray = CHtml::listData($pekerjaanDataProvider, 'id', 'nama_pekerjaan'); 
		return $pekerjaanArray;
	}
	
	/*
		Kabupaten
	*/
	public function getKabupatenOptions()
	{
		$kabupatenDataProvider = Yii::app()->db->createCommand()
		  ->from('KABUPATEN')
		  ->order('KABUPATEN.nama_kabkot')
		  ->queryAll();
		$kabupatenArray = CHtml::listData($kabupatenDataProvider, 'id', 'nama_kabkot'); 
		return $kabupatenArray;
	}
	
	/*
		Instansi
	*/
	public function getInstansiOptions()
	{
		$instansiDataProvider = Yii::app()->db->createCommand()
		  ->from('INSTANSI')
		  ->order('INSTANSI.nama_instansi')
		  ->queryAll();
		$instansiArray = CHtml::listData($instansiDataProvider, 'id', 'nama_instansi'); 
		return $instansiArray;
	}
	
	/*
		Kelompok
	*/
	public function getKelompokOptions()
	{
		$kelompokDataProvider = Yii::app()->db->createCommand()
		  ->from('KELOMPOK')
		  ->order('KELOMPOK.nama_kelompok')
		  ->queryAll();
		$kelompokArray = CHtml::listData($kelompokDataProvider, 'id', 'nama_kelompok'); 
		return $kelompokArray;
	}
	
	/*
		Jenis PMKS
	*/
	public function getJenisPMKSOptions()
	{
		$jenisPMKSDataProvider = Yii::app()->db->createCommand()
		  ->from('JENIS_PMKS')
		  ->order('JENIS_PMKS.nama_pmks')
		  ->queryAll();
		$jenisPMKSArray = CHtml::listData($jenisPMKSDataProvider, 'id', 'nama_pmks'); 
		return $jenisPMKSArray;
	}
	
	/*
		Panti Asuhan
	*/
	public function getPantiAsuhanOptions()
	{
		$pantiAsuhanDataProvider = Yii::app()->db->createCommand()
		  ->from('PANTI_ASUHAN')
		  ->order('PANTI_ASUHAN.nama_pantiasuhan')
		  ->queryAll();
		$pantiAsuhanArray = CHtml::listData($pantiAsuhanDataProvider, 'id', 'nama_pantiasuhan'); 
		return $pantiAsuhanArray;
	}
	
	/*
		Penyakit
	*/
	public function getPenyakitOptions()
	{
		$penyakitDataProvider = Yii::app()->db->createCommand()
		  ->from('PENYAKIT')
		  ->order('PENYAKIT.nama_penyakit')
		  ->queryAll();
		$penyakitArray = CHtml::listData($penyakitDataProvider, 'id', 'nama_penyakit'); 
		return $penyakitArray;
	}
	
	/*
		Puskesmas
	*/
	public function getPuskesmasOptions()
	{
		$puskesmasDataProvider = Yii::app()->db->createCommand()
		  ->from('PUSKESMAS')
		  ->order('PUSKESMAS.nama_puskesmas')
		  ->queryAll();
		$puskesmasArray = CHtml::listData($puskesmasDataProvider, 'id', 'nama_puskesmas'); 
		return $puskesmasArray;
	}
	
	/*
		Koperasi
	*/
	public function getKoperasiOptions()
	{
		$koperasiDataProvider = Yii::app()->db->createCommand()
		  ->from('KOPERASI')
		  ->order('KOPERASI.nama_koperasi')
		  ->queryAll();
		$koperasiArray = CHtml::listData($koperasiDataProvider, 'id', 'nama_koperasi'); 
		return $koperasiArray;
	}
}
