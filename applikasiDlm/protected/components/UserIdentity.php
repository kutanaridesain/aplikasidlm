<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	protected $userId;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		//TODO : don't forget to hash the password from database
        $users = array();
        $modelUser = User::model()->findAll();
        foreach ($modelUser as $user) {
        	$users[$user->username] = array('password'=>$user->password, 'id'=>$user->id);
        }
        
		if(!isset($users[$this->username])){
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		elseif($users[$this->username]['password']!==$this->password){
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		}
		else{
			$this->setId($users[$this->username]['id']);
			$this->errorCode=self::ERROR_NONE;
		}

		return !$this->errorCode;
	}

	/**
	* return login id
	*
	*/
	public function getId()
	{
		return $this->userId;
	}

	protected function setId($userId)
	{
		$this->userId = $userId;
	}
}