<?php

class DlmActiveRecord extends CActiveRecord
{

	/**
    * Return properly quoted/escaped column name
    * @param string $col column name like "id" or "t.id"
    */
    public function quoteCol($col){
            return $this->getDbConnection()->quoteColumnName($col);
    }

    /**
    * Return properly quoted table name
    * @param string $name Table name
    */
    public function quoteTable($t){
            return $this->getDbConnection()->quoteTableName($t);
    }

	/*
		Status Akta Lahir
	*/
	const STATUS_YA = 1;
	const STATUS_TIDAK = 2;

	public static function getStatusYaTidakOptions()
	{
		return array(
			self::STATUS_YA=>'Ya',
			self::STATUS_TIDAK=>'Tidak',
		);
	}
	
	public static function getNamaStatusYaTidakById($idx) {
		
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/* Golongan Darah */
	const GOL_A = 1;
	const GOL_B = 2;
	const GOL_AB = 3;
	const GOL_O = 4;
	const GOL_A_PLUS = 5;
	const GOL_A_MIN = 6;
	const GOL_B_PLUS = 7;
	const GOL_B_MIN = 8;
	const GOL_AB_PLUS = 9;
	const GOL_AB_MIN = 10;
	const GOL_O_PLUS = 11;
	const GOL_O_MIN = 12;
	const GOL_TIDAK_TAHU = 13;
	
	public function getGolonganDarahOptions()
	{
		return array(
			self::GOL_A => 'A',
			self::GOL_B => 'B',
			self::GOL_AB => 'AB',
			self::GOL_O => 'O',
			self::GOL_A_PLUS => 'A+',
			self::GOL_A_MIN => 'A-',
			self::GOL_B_PLUS => 'B+',
			self::GOL_B_MIN => 'B-',
			self::GOL_AB_PLUS => 'AB+',
			self::GOL_AB_MIN => 'AB-',
			self::GOL_O_PLUS => 'O+',
			self::GOL_O_MIN => 'O-',
			self::GOL_TIDAK_TAHU => 'Tidak Tahu',
		);
	}
	
	/*
		Jenis Kelamin
	*/
	const LAKI = 1;
	const PEREMPUAN = 2;

	public static function getJenisKelaminOptions()
	{
		return array(
			self::LAKI=>'Laki-laki',
			self::PEREMPUAN=>'Perempuan',
		);
	}
	
	/*
		Agama
	*/
	const ISLAM = 1;
	const KRISTEN = 2;
	const KATOLIK = 3;
	const HINDU = 4;
	const BUDHA = 5;
	const AGAMA_LAINNYA = 6;

	public static function getAgamaOptions()
	{
		return array(
			self::ISLAM=>'Islam',
			self::KRISTEN=>'Kristen',
			self::KATOLIK=>'Katolik',
			self::HINDU=>'Hindu',
			self::BUDHA=>'Budha',
			self::AGAMA_LAINNYA=>'Lainnya',
		);
	}
	
	public static function getAgamaById($idx) {
		
		$arrOptions = DlmActiveRecord::getAgamaOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	
	/*
		Status Kawin
	*/
	const STATUS_BELUM_KAWIN = 1;
	const STATUS_KAWIN = 2;
	const STATUS_CERAI_HIDUP = 3;
	const STATUS_CERAI_MATI = 4;

	public static function getStatusKawinOptions()
	{
		return array(
			self::STATUS_BELUM_KAWIN=>'Belum Kawin',
			self::STATUS_KAWIN=>'Kawin',
			self::STATUS_CERAI_HIDUP=>'Cerai Hidup',
			self::STATUS_CERAI_MATI=>'Cerai Mati',
		);
	}
	
	public static function getStatusKawinById($idx) {
		
		$arrOptions = DlmActiveRecord::getStatusKawinOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Pendidikan Terakhir
	*/
	const BELUM_SEKOLAH = 1;
	const TIDAK_TAMAT_SD = 2;
	const SD = 3;
	const SLTP = 4;
	const SLTA = 5;
	const DIPLOMA_I_II = 6;
	const DIPLOMA_III = 7;
	const S_I = 8;
	const S_II = 9;
	const S_III = 10;

	public function getPendidikanTerakhirOptions()
	{
		return array(
			self::BELUM_SEKOLAH => 'Tidak/Belum Sekolah',
			self::TIDAK_TAMAT_SD => 'Belum Tamat SD/Sederajat',
			self::SD => 'Tamat SD/Sederajat',
			self::SLTP => 'Tamat SLTP/Sederajat',
			self::SLTA => 'Tamat SLTA/Sederajat',
			self::DIPLOMA_I_II => 'Diploma I/II',
			self::DIPLOMA_III => 'Akademi/Diploma III/Sarjana Muda',
			self::S_I => 'Diploma IV/Strata I',
			self::S_II => 'Strata II',
			self::S_III => 'Strata III',
		);
	}
	
	public function getStrPendidikanTerakhirOptions()
	{
		return array(
			'Tidak/Belum Sekolah' => 'Tidak/Belum Sekolah',
			'Belum Tamat SD/Sederajat' => 'Belum Tamat SD/Sederajat',
			'Tamat SD/Sederajat' => 'Tamat SD/Sederajat',
			'Tamat SLTP/Sederajat' => 'Tamat SLTP/Sederajat',
			'Tamat SLTA/Sederajat' => 'Tamat SLTA/Sederajat',
			'Diploma I/II' => 'Diploma I/II',
			'Akademi/Diploma III/Sarjana Muda' => 'Akademi/Diploma III/Sarjana Muda',
			'Diploma IV/Strata I' => 'Diploma IV/Strata I',
			'Strata II' => 'Strata II',
			'Strata III' => 'Strata III',
		);
	}
	
	
	
	/*
		Hubungan Keluarga
	*/
	const KEPALA_KELUARGA = 1;
	const SUAMI = 2;
	const ISTRI = 3;
	const ANAK = 4;
	const MENANTU = 5;
	const CUCU = 6;
	const ORANG_TUA = 7;
	const MERTUA = 8;
	const FAMILI_LAIN = 9;
	const PEMBANTU = 10;
	const HUBUNGAN_LAINNYA = 11;

	public static function getHubunganKeluargaOptions($isKepalaKeluarga=null)
	{
		if($isKepalaKeluarga===true){
			return array(
				self::KEPALA_KELUARGA => 'Kepala Keluarga',
			);
		}else if($isKepalaKeluarga===false){
			return array(
				//self::SUAMI => 'Suami',
				self::ISTRI=> 'Istri',
				self::ANAK => 'Anak',
				self::MENANTU => 'Menantu',
				self::CUCU => 'Cucu',
				self::ORANG_TUA => 'Orang Tua',
				self::MERTUA => 'Mertua',
				self::FAMILI_LAIN => 'Famili',
				self::PEMBANTU => 'Pembantu',
				self::HUBUNGAN_LAINNYA => 'Lainnya',
			);
		} if ($isKepalaKeluarga === null){return array(
				self::KEPALA_KELUARGA => 'Kepala Keluarga',
				//self::SUAMI => 'Suami',
				self::ISTRI=> 'Istri',
				self::ANAK => 'Anak',
				self::MENANTU => 'Menantu',
				self::CUCU => 'Cucu',
				self::ORANG_TUA => 'Orang Tua',
				self::MERTUA => 'Mertua',
				self::FAMILI_LAIN => 'Famili',
				self::PEMBANTU => 'Pembantu',
				self::HUBUNGAN_LAINNYA => 'Lainnya',
				);
		}
	}
	
	public static function getHubunganKeluargaById($idx) {
		
		$arrOptions = DlmActiveRecord::getHubunganKeluargaOptions(null);

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Kepemilikan Rumah
	*/
	const MILIK_SENDIRI = 1;
	const MENYEWA = 2;
	const MENUMPANG = 3;

	public static function getKepemilikanRumahOptions()
	{
		return array(
			self::MILIK_SENDIRI=>'Milik Sendiri',
			self::MENYEWA=>'Menyewa',
			self::MENUMPANG=>'Menumpang',
		);
	}
	
	public static function getKepemilikanRumahById($idx) {
		$arrOptions = DlmActiveRecord::getKepemilikanRumahOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Jenis Lantai Rumah
	*/
	const TANAH = 1;
	const SEMEN = 2;
	const KERAMIK = 3;
	const LANTAI_PAPAN = 4;

	public static function getLantaiRumahOptions()
	{
		return array(
			self::TANAH=>'Tanah',
			self::SEMEN=>'Semen',
			self::KERAMIK=>'Keramik',
			self::LANTAI_PAPAN=>'Papan',
		);
	}
	
	public static function getJenisLantaiById($idx) {
		$arrOptions = DlmActiveRecord::getLantaiRumahOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Jenis Dinding Rumah
	*/
	const BAMBU = 1;
	const DINDING_PAPAN = 2;
	const TEMBOK = 3;

	public static function getDindingRumahOptions()
	{
		return array(
			self::BAMBU=>'Bambu/Rumbia',
			self::DINDING_PAPAN=>'Papan',
			self::TEMBOK=>'Tembok',
		);
	}
	
	public static function getJenisDindingById($idx) {
		$arrOptions = DlmActiveRecord::getDindingRumahOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Sumber Penerangan
	*/
	const TIDAK_MENGGUNAKAN_LISTRIK = 1;
	const LISTRIK = 2;

	public function getSumberPeneranganOptions()
	{
		return array(
			self::TIDAK_MENGGUNAKAN_LISTRIK=>'Tidak Menggunakan Listrik',
			self::LISTRIK=>'Listrik',
		);
	}
	
	/*
		Sumber Air Minum
	*/
	const SUNGAI = 1;
	const SUMUR = 2;
	const AIR_PDAM = 3;

	public static function getSumberAirOptions()
	{
		return array(
			self::SUNGAI=>'Sungai',
			self::SUMUR=>'Sumur',
			self::AIR_PDAM=>'Air PDAM',
		);
	}
	
	public static function getSumberAirById($idx) {
		$arrOptions = DlmActiveRecord::getSumberAirOptions();
		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	/*
		Bahan Bakar Masak
	*/
	const KAYU_BAKAR = 1;
	const MINYAK_TANAH = 2;
	const GAS = 3;

	public static function getBahanBakarMasakOptions()
	{
		return array(
			self::KAYU_BAKAR=>'Kayu Bakar/Arang',
			self::MINYAK_TANAH=>'Minyak Tanah',
			self::GAS=>'Gas',
		);
	}
	
	public static function getBahanBakarMasakById($idx) {
		$arrOptions = DlmActiveRecord::getBahanBakarMasakOptions();
		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Status Rumah
	*/
	const RUMAH_BANTUAN = 1;
	const BUKAN_RUMAH_BANTUAN = 2;

	public static function getStatusRumahOptions()
	{
		return array(
			self::RUMAH_BANTUAN=>'Rumah Bantuan',
			self::BUKAN_RUMAH_BANTUAN=>'Bukan Rumah Bantuan',
		);
	}
	
	public static function getStatusRumahById($idx) {
		$arrOptions = DlmActiveRecord::getStatusRumahOptions();
		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Konsumsi Nasi
	*/
	const SATU_KALI = 1;
	const DUA_KALI = 2;
	const TIGA_KALI = 3;

	public static function getKonsumsiNasiOptions()
	{
		return array(
			self::SATU_KALI=>'1 Kali',
			self::DUA_KALI=>'2 Kali',
			self::TIGA_KALI=>'3 Kali',
		);
	}
	
	public static function getKonsumsiNasiById($idx) {
		$arrOptions = DlmActiveRecord::getKonsumsiNasiOptions();
		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Konsumsi Lauk-Pauk/Daging-Susu
	*/
	const TIDAK_MENGKONSUMSI = 1;
	const KADANG = 2;
	const SELALU = 3;

	public static function getKonsumsiLaukPaukDagingOptions()
	{
		return array(
			self::TIDAK_MENGKONSUMSI=>'Tidak Mengkonsumsi',
			self::KADANG=>'Kadang-kadang',
			self::SELALU=>'Selalu',
		);
	}
	
	public static function getKonsumsiLaukPaukDagingById($idx) {
		$arrOptions = DlmActiveRecord::getKonsumsiLaukPaukDagingOptions();
		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Konsumsi Sandang
	*/
	const TAHUN_2 = 1;
	const TAHUN_1 = 2;
	const TAHUN_0 = 3;

	public static function getKonsumsiSandangOptions()
	{
		return array(
			self::TAHUN_2=>'> 2 Tahun Sekali',
			self::TAHUN_1=>'1 Tahun Sekali',
			self::TAHUN_0=>'< 1 Tahun Sekali',
		);
	}
	
	
	public static function getKonsumsiSandangById($idx) {
		$arrOptions = DlmActiveRecord::getKonsumsiSandangOptions();
		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Jenis Cacat
	*/
	const NO_TUNA = 0;
	const TUNA_NETRA = 1;
	const TUNA_RUNGU = 2;
	const TUNA_WICARA = 3;
	const TUNA_DAKSA = 4;
	const TUNA_LARAS_A = 5;
	const TUNA_LARAS_B = 6;
	const TUNA_GRAHITA =7;
	const TUNA_GANDA = 8;

	public function getJenisCacatOptions()
	{
		return array(
			self::NO_TUNA=>'',
			self::TUNA_NETRA=>'Tuna Netra',
			self::TUNA_RUNGU=>'Tuna Rungu',
			self::TUNA_WICARA=>'Tunga Wicara',
			self::TUNA_DAKSA=>'Tunga Daksa',
			self::TUNA_LARAS_A=>'Tunga Laras A',
			self::TUNA_LARAS_B=>'Tunga Laras B',
			self::TUNA_GRAHITA=>'Tunga Grahita',
			self::TUNA_GANDA=>'Tunga Ganda',
		);
	}
	
	/*
		Dititip Di Panti Asuhan
	*/
	const PANTI_ASUHAN = 1;
	const BUKAN_PANTI_ASUHAN = 2;

	public function getDititipDiPantiAsuhanOptions()
	{
		return array(
			self::PANTI_ASUHAN=>'Panti Asuhan',
			self::BUKAN_PANTI_ASUHAN=>'Tidak',
		);
	}
	
	/*
		Type Dayah
	*/
	const DAYAH_A = 1;
	const DAYAH_B = 2;
	const DAYAH_C = 3;
	const DAYAH_D = 4;

	public static function getTypeDayahOptions()
	{
		return array(
			self::DAYAH_A=>'1. A',
			self::DAYAH_B=>'2. B',
			self::DAYAH_C=>'3. C',
			self::DAYAH_D=>'4. D',
		);
	}
	
	/*
		Status Pendidikan
	*/
	const PUTUS_SEKOLAH = 1;
	const TIDAK_PUTUS_SEKOLAH = 2;

	public function getStatusPendidikanOptions()
	{
		return array(
			self::PUTUS_SEKOLAH=>'Putus Sekolah',
			self::TIDAK_PUTUS_SEKOLAH=>'Tidak Putus Sekolah',
		);
	}
	
	/*
		Jenis Zakat
	*/
	const PERDAGANGAN = 1;
	const PENGHASILAN = 2;
	const PERTANIAN = 3;
	const PETERNAKAN = 4;
	const EMAS_DAN_PERAK = 5;
	const UANG_SIMPANAN = 6;

	public static function getJenisZakatOptions()
	{
		return array(
			self::PERDAGANGAN=>'Perdagangan',
			self::PENGHASILAN=>'Penghasilan',
			self::PERTANIAN=>'Pertanian',
			self::PETERNAKAN=>'Peternakan',
			self::EMAS_DAN_PERAK=>'Emas Dan Perak',
			self::UANG_SIMPANAN=>'Uang Simpanan',
		);
	}
	
	/*
		Kategori Bantuan Modal
	*/
	const PERORANGAN = 1;
	const KELOMPOK = 2;

	public function getKategoriBantuanModalOptions()
	{
		return array(
			self::PERORANGAN=>'Perorangan',
			self::KELOMPOK=>'Kelompok',
		);
	}
	
	/*
		Sumber Bantuan Modal
	*/
	const KOPERASI = 1;
	const BUKAN_KOPERASI = 2;

	public function getSumberBantuanModalOptions()
	{
		return array(
			self::KOPERASI=>'Koperasi',
			self::BUKAN_KOPERASI=>'Bukan Koperasi',
		);
	}
	
	/*
		Luas Tanah
	*/
	const LUAS_1 = 1;
	const LUAS_2 = 2;
	const LUAS_3 = 3;

	public static function getLuasTanahOptions()
	{
		return array(
			self::LUAS_1=>'< 500',
			self::LUAS_2=>'500 s.d. 1000',
			self::LUAS_3=>'> 1000',
		);
	}
	
	public static function getNamaLuasTanahById($idx) {
		
		$arrOptions = DlmActiveRecord::getLuasTanahOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Jumlah Ternak
	*/
	const TERNAK_1 = 1;
	const TERNAK_2 = 2;
	const TERNAK_3 = 3;

	public static function getJumlahTernakOptions()
	{
		return array(
			self::TERNAK_1=>'1 s.d 3',
			self::TERNAK_2=>'4 s.d. 5',
			self::TERNAK_3=>'> 5',
		);
	}
	
	public static function getTernakById($idx) {
		
		$arrOptions = DlmActiveRecord::getJumlahTernakOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/*
		Jumlah Kendaraan
	*/
	//const KENDARAAN_0 = 0;
	const KENDARAAN_1 = 1;
	const KENDARAAN_2 = 2;

	public function getJumlahKendaraanOptions()
	{
		return array(
			//self::KENDARAAN_0=>'',
			self::KENDARAAN_1=>'1 s.d 2 Unit',
			self::KENDARAAN_2=>'> 3 Unit',
		);
	}
	
	/*
		Type Panti Asuhan
	*/
	const PANTIASUHAN_1 = 1;
	const PANTIASUHAN_2 = 2;
	const PANTIASUHAN_3 = 3;
	
	public static function getPantiAsuhanOptions()
	{
		return array(
			self::PANTIASUHAN_1=>'Panti Asuhan Type A',
			self::PANTIASUHAN_2=>'Panti Asuhan Type B',
			self::PANTIASUHAN_3=>'Panti Asuhan Type C',
		);
	}
	
	/*
		Jenis Puskesmas
	*/
	const PUSKESMAS_1 = 1;
	const PUSKESMAS_2 = 2;
	const PUSKESMAS_3 = 3;

	public static function getPuskesmasOptions()
	{
		return array(
			self::PUSKESMAS_1=>'Puskesmas Type A',
			self::PUSKESMAS_2=>'Puskesmas Type B',
			self::PUSKESMAS_3=>'Puskesmas Type C',
		);
	}
	
	
	protected function beforeSave()
	{
		if(parent::beforeSave()){
			if(empty($this->userid)){
				$this->userid = Yii::app()->user->id;
			}
			  
			if($this->getIsNewRecord()){
				$this->setAttribute('tglinput', strtoupper(date('d-M-y')));
			}else{
				$this->setAttribute('tglupdate', strtoupper(date('d-M-y')));
			}
			
		   return true;
	   }
		
		return false;
	}

	/*
	TIMEOUT.....
	protected function beforeFind()
	{
		$roleid = Yii::app()->user->getRoleId();
		$locaid = Yii::app()->user->getLocationId();
		
		$cont = Yii::app()->controller->id;
		$act = Yii::app()->controller->action->id;
		
		if( $cont == "biodata" && $act != "index"){
			$id = Yii::app()->request->getParam('id');
			$biodata =Biodata::model()->findByPk($id);
			//print_r($biodata);
		}
		
		parent::beforeFind();
	}*/
}
