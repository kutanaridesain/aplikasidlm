<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Pekerjaan'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Pekerjaan','url'=>array('index')),
);
?>

<h1>Tambah Pekerjaan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>