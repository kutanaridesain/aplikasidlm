<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Pekerjaan'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Pekerjaan','url'=>array('index')),
	array('label'=>'Tambah Pekerjaan','url'=>array('create')),
	array('label'=>'Ubah Pekerjaan','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Pekerjaan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Pekerjaan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_pekerjaan',
	),
)); ?>
