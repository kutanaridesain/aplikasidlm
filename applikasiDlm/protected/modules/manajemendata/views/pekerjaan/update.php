<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Pekerjaan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Pekerjaan','url'=>array('index')),
	array('label'=>'Tambah Pekerjaan','url'=>array('create')),
	array('label'=>'Detil Pekerjaan','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Pekerjaan </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>