<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Riwayat Kesehatans'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Riwayat Kesehatan','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('riwayat-kesehatan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Riwayat Kesehatan</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'riwayat-kesehatan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nik',
		'no_kk',
		'no_jka',
		'no_jamkesmas',
		'asuransilain1',
		/*
		'no_asuransilain1',
		'asuransilain2',
		'no_asuransilain2',
		'id_penyakit',
		'tgl_sakit',
		'id_puskesmas',
		'userid',
		'tglinput',
		'tglupdate',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
