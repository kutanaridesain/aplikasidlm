<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'riwayat-kesehatan-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'readonly'=>'readonly')); ?>
	
	<?php echo $form->textFieldRow($model,'nama_penduduk',array('class'=>'span5', 'readonly'=>'readonly')); ?>
	
	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'no_jka',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'no_jamkesmas',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'asuransilain1',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'no_asuransilain1',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'asuransilain2',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'no_asuransilain2',array('class'=>'span5','maxlength'=>100)); ?>
	
	<?php echo $form->textFieldRow($model,'nama_penyakit',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	<?php echo $form->hiddenField($model,'id_penyakit'); ?>
	
	<?php echo $form->textFieldRow($model,'nama_penyakit1',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	<?php echo $form->hiddenField($model,'id_penyakit1'); ?>
	
	<?php echo $form->textFieldRow($model,'nama_penyakit2',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	<?php echo $form->hiddenField($model,'id_penyakit2'); ?>
	
	<?php echo $form->datepickerRow($model, 'tgl_sakit',
        array('append'=>'<i class="icon-calendar"></i>', 'class'=>'span1', 'options'=>array(
    		'format'=>'yyyy',
    		'viewMode'=>'years',
    		'minViewMode'=>'years',
    		'autoclose'=>'true',
    	)));
	?>
	<!-- <div class="input-append date datepicker" data-date-format="dd/mm/yyyy" id="tgl_sakit">
		<?php //echo $form->textFieldRow($model,'tgl_sakit',array('class'=>'span2 datepicker-textfield', 'readonly'=>'readonly')); ?>
		<span class="add-on"><i class="icon-th"></i></span>
	</div> -->

	<?php echo $form->textFieldRow($model,'nama_puskesmas',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>

	<?php echo $form->hiddenField($model,'id_puskesmas'); ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
		
		<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'danger',
			'label'=>'Batal',
			'url'=>'/manajemendata/biodata/update/id/'.$model->nik,
		)); ?>
		
		<?php 
		if(!$model->isNewRecord ){
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'link',
				'type'=>'primary',
				'label'=>'+ Buat Riwayat Kesehatan Baru',
				'url' => '/manajemendata/riwayatkesehatan/create/nik/' . $model->nik,
			));
		} 
		?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
			if($("#RiwayatKesehatan_nama_penyakit").size()> 0){
				$("#RiwayatKesehatan_nama_penyakit").keyup(function(){
					parameter = "term=" + $('#RiwayatKesehatan_nama_penyakit').val();
				});
				TextFieldAutoComplete("/general/getPenyakit", "RiwayatKesehatan_nama_penyakit", "RiwayatKesehatan_id_penyakit");
			}
			
			if($("#RiwayatKesehatan_nama_penyakit1").size()> 0){
				$("#RiwayatKesehatan_nama_penyakit1").keyup(function(){
					parameter = "term=" + $('#RiwayatKesehatan_nama_penyakit1').val();
				});
				TextFieldAutoComplete("/general/getPenyakit", "RiwayatKesehatan_nama_penyakit1", "RiwayatKesehatan_id_penyakit1");
			}
			
			if($("#RiwayatKesehatan_nama_penyakit2").size()> 0){
				$("#RiwayatKesehatan_nama_penyakit2").keyup(function(){
					parameter = "term=" + $('#RiwayatKesehatan_nama_penyakit2').val();
				});
				TextFieldAutoComplete("/general/getPenyakit", "RiwayatKesehatan_nama_penyakit2", "RiwayatKesehatan_id_penyakit2");
			}
			
			if($("#RiwayatKesehatan_nama_puskesmas").size()> 0){
				$("#RiwayatKesehatan_nama_puskesmas").keyup(function(){
					parameter = "term=" + $('#RiwayatKesehatan_nama_puskesmas').val();
				});
				TextFieldAutoComplete("/general/getPuskesmas", "RiwayatKesehatan_nama_puskesmas", "RiwayatKesehatan_id_puskesmas");
			}
	});
</script>
