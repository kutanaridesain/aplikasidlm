<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Riwayat Kesehatan'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar RiwayatKesehatan','url'=>array('index')),
	array('label'=>'Tambah RiwayatKesehatan','url'=>array('create')),
	array('label'=>'Ubah RiwayatKesehatan','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus RiwayatKesehatan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil RiwayatKesehatan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nik',
		'no_kk',
		'no_jka',
		'no_jamkesmas',
		'asuransilain1',
		'no_asuransilain1',
		'asuransilain2',
		'no_asuransilain2',
		'nama_penyakit',
		'nama_penyakit1',
		'nama_penyakit2',
		'tgl_sakit',
		'id_puskesmas',
		'userid',
		'tglinput',
		'tglupdate',
	),
)); ?>
