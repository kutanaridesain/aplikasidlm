<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_jka',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'no_jamkesmas',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'asuransilain1',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'no_asuransilain1',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'asuransilain2',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'no_asuransilain2',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'id_penyakit',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tgl_sakit',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_puskesmas',array('class'=>'span5','maxlength'=>40)); ?>

	<?php echo $form->textFieldRow($model,'userid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglinput',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglupdate',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
