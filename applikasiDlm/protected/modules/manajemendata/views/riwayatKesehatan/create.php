<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Riwayat Kesehatan'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Kembali','url'=>array('biodata/update', 'id'=>$model->nik)),
	//array('label'=>'Daftar Biodata','url'=>array('biodata/index')),
);
?>

<h1>Tambah Riwayat Kesehatan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<br/>
	<span class="section-block">Daftar Riwayat Kesehatan</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'riwayat-kesehatan-details-grid',
		'dataProvider'=>$riwayatDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_penyakit',
			'tgl_sakit',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update} {delete}'
			),
		),
	)); ?>
	
<script type="text/javascript">
	$(function(){	
		$(".update, .delete").each(function(){
			var strUrl = $(this).attr("href") + "/nik/"+<?php echo $model->nik; ?>+"";
			
			$(this).attr("href", strUrl);
		});
	});
</script>