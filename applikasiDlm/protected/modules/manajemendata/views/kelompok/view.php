<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelompok'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Kelompok','url'=>array('index')),
	array('label'=>'Tambah Kelompok','url'=>array('create')),
	array('label'=>'Ubah Kelompok','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Kelompok','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Kelompok</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_kelompok',
		'ketua_kelompok',
		'bidang_usaha',
		'no_telp',
		'alamat',
		'nama_kab',
	),
)); ?>
