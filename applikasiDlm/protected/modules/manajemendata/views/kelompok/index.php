<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelompoks'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Kelompok','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kelompok-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Kelompok</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'kelompok-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nama_kelompok',
		'ketua_kelompok',
		'bidang_usaha',
		array(      
            'name' => 'id_kab',      
            'type' => 'raw',      
            'value' => 'Kabupaten::getById($data->id_kab)',      
            'filter' => Kabupaten::getOptionList(), 
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
