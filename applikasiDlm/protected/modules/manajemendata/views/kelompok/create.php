<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelompok'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Kelompok','url'=>array('index')),
);
?>

<h1>Tambah Kelompok</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>