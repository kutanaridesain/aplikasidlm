<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelompok'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Kelompok','url'=>array('index')),
	array('label'=>'Tambah Kelompok','url'=>array('create')),
	array('label'=>'Detil Kelompok','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Kelompok </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>