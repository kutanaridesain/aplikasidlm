<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Panti Asuhan'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Panti Asuhan','url'=>array('index')),
);
?>

<h1>Tambah Panti Asuhan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>