<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Panti Asuhans'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Panti Asuhan','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('panti-asuhan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Panti Asuhan</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'panti-asuhan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nama_pantiasuhan',
		array(      
            'name' => 'type_pantiasuhan',      
            'type' => 'raw',      
            'value' => 'PantiAsuhan::getNamaTypePantiasuhanById($data->type_pantiasuhan)',      
            'filter' => PantiAsuhan::getPantiAsuhanOptions(),    
        ),
		'alamat_pantiasuhan',
		'koordinat_x',
		'koordinat_y',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
