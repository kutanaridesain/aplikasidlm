<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Panti Asuhan'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar PantiAsuhan','url'=>array('index')),
	array('label'=>'Tambah PantiAsuhan','url'=>array('create')),
	array('label'=>'Ubah PantiAsuhan','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus PantiAsuhan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil PantiAsuhan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_pantiasuhan',
		'nama_type_pantiasuhan',
		'alamat_pantiasuhan',
		'jlh_kamar',
		'jlh_perpustakaan',
		'jlh_aula',
		'jlh_mushalla',
		'jlh_kamar_mandi_wc',
		'koordinat_x',
		'koordinat_y',
	),
)); ?>
