<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'panti-asuhan-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama_pantiasuhan',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->dropDownListRow($model, 'type_pantiasuhan', $model->getPantiAsuhanOptions()); ?>

	<?php echo $form->textAreaRow($model,'alamat_pantiasuhan',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->textFieldRow($model,'jlh_kamar',array('class'=>'span1 updown-spinner')); ?>

	<?php echo $form->textFieldRow($model,'jlh_perpustakaan',array('class'=>'span1 updown-spinner')); ?>

	<?php echo $form->textFieldRow($model,'jlh_aula',array('class'=>'span1 updown-spinner')); ?>

	<?php echo $form->textFieldRow($model,'jlh_mushalla',array('class'=>'span1 updown-spinner')); ?>

	<?php echo $form->textFieldRow($model,'jlh_kamar_mandi_wc',array('class'=>'span1 updown-spinner')); ?>

	<?php echo $form->textFieldRow($model,'koordinat_x',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'koordinat_y',array('class'=>'span5','maxlength'=>100)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

