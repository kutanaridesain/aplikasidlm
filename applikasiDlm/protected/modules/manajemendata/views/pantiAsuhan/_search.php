<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nama_pantiasuhan',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'type_pantiasuhan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'alamat_pantiasuhan',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->textFieldRow($model,'jlh_kamar',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_perpustakaan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_aula',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_mushalla',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_kamar_mandi_wc',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'koordinat_x',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'koordinat_y',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'userid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglinput',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglupdate',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
