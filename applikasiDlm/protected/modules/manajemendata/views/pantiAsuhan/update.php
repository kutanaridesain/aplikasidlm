<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Panti Asuhan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Panti Asuhan','url'=>array('index')),
	array('label'=>'Tambah Panti Asuhan','url'=>array('create')),
	array('label'=>'Detil Panti Asuhan','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Panti Asuhan </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>