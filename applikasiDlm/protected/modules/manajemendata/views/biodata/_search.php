<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nama_lgkp',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'tmpt_lhr',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'tgl_lhr',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'akta_lhr',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_akta_lhr',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'gol_drh',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jenis_kelamin',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'agama',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'stat_kwn',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'akta_kwn',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_akta_kwn',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'tgl_kwn',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'akta_crai',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_akta_crai',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textFieldRow($model,'tgl_crai',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pnydng_cct',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pddk_akh',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'kode_pkrj',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'stat_hidup',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_kab',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_kec',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_kel',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'userid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglinput',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglupdate',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Cari',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
