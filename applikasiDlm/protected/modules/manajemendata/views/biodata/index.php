<?php
$this->breadcrumbs=array(
    ucfirst($this->module->id)=>'/'.$this->module->id,
	'Biodata'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Biodata','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('biodata-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Biodata</h1>
<?php //echo CHtml::link(Yii::t('dlm', 'advanced search'),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); 
?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'biodata-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nik',
		'nama_lgkp',
		'tmpt_lhr',
		'tgl_lhr',
		array(      
            'name' => 'jenis_kelamin',      
            'type' => 'raw',      
            'value' => 'Biodata::getNamaJenisKelaminById($data->jenis_kelamin)',      
            'filter' => Biodata::getJenisKelaminOptions(),    
        ),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
