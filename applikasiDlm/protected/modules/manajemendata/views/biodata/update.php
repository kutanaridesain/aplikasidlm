<?php
$this->breadcrumbs=array(
    ucfirst($this->module->id)=>'/'.$this->module->id,
	'Biodata'=>array('index'),
	$model->nik=>array('view','id'=>$model->nik),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Biodata','url'=>array('index')),
	array('label'=>'Tambah Biodata','url'=>array('create')),
	array('label'=>'Detil Biodata','url'=>array('view','id'=>$model->nik)),
	array('label'=>'Bantuan Modal','url'=>array('bantuanmodal/create', 'nik'=>$model->nik)),
	array('label'=>'Bantuan Sosial','url'=>array('bantuansosial/create', 'nik'=>$model->nik)),
	array('label'=>'Bantuan Lainnya','url'=>array('bantuanlainnya/create', 'nik'=>$model->nik)),
	array('label'=>'Riwayat Kesehatan','url'=>array('riwayatkesehatan/create', 'nik'=>$model->nik)),
	array('label'=>'Riwayat Pekerjaan','url'=>array('riwayatpekerjaan/create', 'nik'=>$model->nik)),
	array('label'=>'Riwayat Pendidikan','url'=>array('riwayatpendidikan/create', 'nik'=>$model->nik)),
);
?>

<h1>Ubah Biodata <?php echo $model->nik; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>