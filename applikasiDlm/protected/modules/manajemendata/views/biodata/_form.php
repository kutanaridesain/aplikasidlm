<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'biodata-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>
	
	<?php 
		if($model->isNewRecord){
			echo $form->textFieldRow($model,'nik',array('class'=>'span5','maxlength'=>200)); 
		}else{
			echo $form->textFieldRow($model,'nik',array('class'=>'span5','maxlength'=>200, 'readonly'=>'readonly')); 
		}
	?>

	<?php echo $form->textFieldRow($model,'nama_lgkp',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'tmpt_lhr',array('class'=>'span5','maxlength'=>200)); ?>
	
	<?php echo $form->datepickerRow($model, 'tgl_lhr',
        array('append'=>'<i class="icon-calendar"></i>', 'format'=>'mm/dd/yyyy', 'class'=>'dateselector span2')); 
    ?>

	<?php echo $form->radioButtonListInlineRow($model, 'akta_lhr', $model->getStatusYaTidakOptions()); ?>
	
	<div id="no_akta_lhr">
		<?php echo $form->textFieldRow($model,'no_akta_lhr',array('class'=>'span5','maxlength'=>50)); ?>
	</div>

	<?php echo $form->dropDownListRow($model, 'gol_drh', $model->getGolonganDarahOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'jenis_kelamin', $model->getJenisKelaminOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'agama', $model->getAgamaOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'stat_kwn', $model->getStatusKawinOptions()); ?>
	<div id="tgl_kwn">
		<?php echo $form->datepickerRow($model, 'tgl_kwn',
        array('append'=>'<i class="icon-calendar"></i>', 'format'=>'mm/dd/yyyy', 'class'=>'dateselector span2')); 
    ?>
	</div>

	<div id="akta_kwn">
		<?php echo $form->radioButtonListInlineRow($model, 'akta_kwn', $model->getStatusYaTidakOptions()); ?>
		<div id="no_akta_kwn">
			<?php echo $form->textFieldRow($model,'no_akta_kwn',array('class'=>'span5','maxlength'=>50)); ?>
		</div>
	</div>

	<div id="akta_crai">

		<div id="tgl_crai">
			<?php echo $form->datepickerRow($model, 'tgl_crai',
				array('append'=>'<i class="icon-calendar"></i>', 'format'=>'mm/dd/yyyy', 'class'=>'dateselector span2'));
			?>
		</div>
	
		<?php echo $form->radioButtonListInlineRow($model, 'akta_crai', $model->getStatusYaTidakOptions()); ?>
		<div id="no_akta_crai">
			<?php echo $form->textFieldRow($model,'no_akta_crai',array('class'=>'span5','maxlength'=>4000)); ?>
		</div>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'pnydng_cct', $model->getStatusYaTidakOptions()); ?>
	
	<div style="display:none;">
		<?php echo $form->dropDownListRow($model, 'pddk_akh', $model->getPendidikanTerakhirOptions()); ?>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'stat_hidup', $model->getStatusYaTidakOptions()); ?>
	
	<div id="tgl_meninggal">
		<?php echo $form->datepickerRow($model, 'tgl_meninggal',
			array('append'=>'<i class="icon-calendar"></i>', 'format'=>'mm/dd/yyyy', 'class'=>'dateselector span2'));
		?>
	</div>

	<?php echo $form->textFieldRow($model,'nama_kab',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	<?php echo $form->hiddenField($model,'no_kab'); ?>

	<?php echo $form->textFieldRow($model,'nama_kec',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	<?php echo $form->hiddenField($model,'no_kec'); ?>

	<?php echo $form->textFieldRow($model,'nama_kel',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	<?php echo $form->hiddenField($model,'no_kel'); ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	ToggleStatusHidup=function(){
		$("#tgl_meninggal").toggle();
	};
	
	ToggleAktaLahir=function(){
		$("#no_akta_lhr").toggle();
	};
	
	ToggleStatusKawin=function(){
		var marriedStat = $("#Biodata_stat_kwn").val();
		var brokenStat = $("#Biodata_akta_crai").val();
		
		$("#tgl_kwn").hide();
		$("#akta_kwn").hide();
		$("#no_akta_kwn").hide();
		$("#akta_crai").hide();
		$("#no_akta_crai").hide();
		
		if(marriedStat > 1){
			$("#tgl_kwn").show();
		}
		
		if(marriedStat >= 2){
			$("#akta_kwn").show();
			if(($("#Biodata_akta_kwn_0").is(':checked'))==true){
				$("#no_akta_kwn").show();
			}
		}
		
		if(marriedStat > 2){
			$("#akta_crai").show();
			if(($("#Biodata_akta_crai_0").is(':checked'))==true){
				$("#no_akta_crai").show();
			}
		}
	};
	
	$(function(){
		if(($("#Biodata_akta_lhr_0").is(':checked'))==false){
			$("#no_akta_lhr").hide();
		}
		
		$("#Biodata_akta_lhr_0, #Biodata_akta_lhr_1").click(function() {
			ToggleAktaLahir();
		});
		
		ToggleStatusKawin();
		
		$("#Biodata_stat_kwn").change(function(){
			ToggleStatusKawin();
		});
		
		$("#Biodata_akta_kwn_0, #Biodata_akta_kwn_1, #Biodata_akta_crai_0, #Biodata_akta_crai_1").click(function(){
			ToggleStatusKawin();
		});
		
		/* BIODATA */
		if($("#Biodata_nama_kab").size()> 0){
			$("#Biodata_nama_kab").keyup(function(){
				parameter = "term=" + $('#Biodata_nama_kab').val();
			});
			TextFieldAutoComplete("/general/getKabupatenByRole", "Biodata_nama_kab", "Biodata_no_kab");
		}
		
		if($("#Biodata_nama_kec").size()> 0){
			$("#Biodata_nama_kec").keyup(function(){
				parameter = "term=" + $('#Biodata_nama_kec').val() + "&id="+$("#Biodata_no_kab").val();
			});
			TextFieldAutoComplete("/general/getKecamatanByKabupatenAndRole", "Biodata_nama_kec", "Biodata_no_kec");
		}
		
		if($("#Biodata_nama_kel").size()> 0){
			$("#Biodata_nama_kel").keyup(function(){
				parameter = "term=" + $('#Biodata_nama_kel').val() + "&id="+$("#Biodata_no_kec").val();
			});
			TextFieldAutoComplete("/general/getGampongByKecamatanAndRole", "Biodata_nama_kel", "Biodata_no_kel");
		}
		
		if(($("#Biodata_stat_hidup_0").is(':checked'))==false){
			$("#tgl_meninggal").hide();
		}
		
		$("#Biodata_stat_hidup_0, #Biodata_stat_hidup_1").click(function() {
			ToggleStatusHidup();
		});
	});
</script>

