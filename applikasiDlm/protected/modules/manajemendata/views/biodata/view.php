<?php
$this->breadcrumbs=array(
    ucfirst($this->module->id)=>'/'.$this->module->id,
	'Biodata'=>array('index'),
	$model->nik,
);

$this->menu=array(
	array('label'=>'Daftar Biodata','url'=>array('index')),
	array('label'=>'Tambah Biodata','url'=>array('create')),
	array('label'=>'Ubah Biodata','url'=>array('update','id'=>$model->nik)),
	array('label'=>'Hapus Biodata','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->nik),'confirm'=>Yii::t('dlm', 'confirm delete'))),
	array('label'=>'Bantuan Modal','url'=>array('bantuanmodal/create', 'nik'=>$model->nik)),
	array('label'=>'Bantuan Sosial','url'=>array('bantuansosial/create', 'nik'=>$model->nik)),
	array('label'=>'Bantuan Lainnya','url'=>array('bantuanlainnya/create', 'nik'=>$model->nik)),
	array('label'=>'Riwayat Kesehatan','url'=>array('riwayatkesehatan/create', 'nik'=>$model->nik)),
	array('label'=>'Riwayat Pekerjaan','url'=>array('riwayatpekerjaan/create', 'nik'=>$model->nik)),
	array('label'=>'Riwayat Pendidikan','url'=>array('riwayatpendidikan/create', 'nik'=>$model->nik)),
);
?>

<h1>Detil Biodata <?php echo $model->nama_lgkp; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nik',
		'nama_lgkp',
		'tmpt_lhr',
		'tgl_lhr',
		'nama_status_akta_lahir',
		'no_akta_lhr',
		'nama_golongan_darah',
		'nama_jenis_kelamin',
		'nama_agama',
		'nama_status_kawin',
		'nama_akta_kawin',
		'no_akta_kwn',
		'tgl_kwn',
		'nama_akta_cerai',
		'no_akta_crai',
		'tgl_crai',
		'nama_cacat',
		//'nama_pendidikan_terakhir',
		'nama_status_hidup',
		'nama_kab',
		'nama_kec',
		'nama_kel',
	),
)); ?>

	<br/>
	<span class="section-block">Daftar Bantuan Modal</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'bantuan-modal-details-grid',
		'dataProvider'=>$bantuanModalDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_instansi',
			'jenis_bantuan',
			'tahun_diberi',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>''
			),
		),
	)); ?>
	
	<br/>
	<span class="section-block">Daftar Bantuan Sosial</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'bantuan-sosial-details-grid',
		'dataProvider'=>$bantuanSosialDataProvider,
		'template' => "{items}", 
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_instansi',
			'jenis_bantuan',
			'tahun_diberikan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>''
			),
		),
	)); ?>
	
	<br/>
	<span class="section-block">Daftar Bantuan Lainnya</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'bantuan-lainnya-details-grid',
		'dataProvider'=>$bantuanLainnyaDataProvider,
		'template' => "{items}", 
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_instansi',
			'jenis_bantuan',
			'tahun_diberikan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>''
			),
		),
	)); ?>
	
	<br/>
	<span class="section-block">Daftar Riwayat Kesehatan</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'riwayat-kesehatan-details-grid',
		'dataProvider'=>$riwayatKesehatanDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_penyakit',
			'tgl_sakit',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>''
			),
		),
	)); ?>
	
	<br/>
	<span class="section-block">Daftar Riwayat Pekerjaan</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'riwayat-pekerjaan-details-grid',
		'dataProvider'=>$riwayatPekerjaanDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_pekerjaan',
			'tempat_bekerja',
			'rata_penghasilan',
			'tahun',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>''
			),
		),
	)); ?>

	<br/>
	<span class="section-block">Daftar Riwayat Pendidikan</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'riwayat-pendidikan-details-grid',
		'dataProvider'=>$riwayatPendidikanDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'jenjang_sekolah',
			'nama_sekolah',
			'kelas',
			'tahun',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>''
			),
		),
	)); ?>