<?php
$this->breadcrumbs=array(
    ucfirst($this->module->id)=>'/'.$this->module->id,
	'Biodata'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Biodata','url'=>array('index')),
);
?>

<h1>Tambah Biodata</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>