<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kecamatan'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Kecamatan','url'=>array('index')),
	array('label'=>'Tambah Kecamatan','url'=>array('create')),
	array('label'=>'Ubah Kecamatan','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Kecamatan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Kecamatan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_kab',
		'nama_kec',
		'ibukota',
		'alamat',
		'camat',
		'visi',
		'misi',
		'koordinat_x',
		'koordinat_y',
	),
)); ?>
