<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kecamatans'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Kecamatan','url'=>array('index')),
);
?>

<h1>Tambah Kecamatan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>