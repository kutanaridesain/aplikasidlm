<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kecamatan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Kecamatan','url'=>array('index')),
	array('label'=>'Tambah Kecamatan','url'=>array('create')),
	array('label'=>'Detil Kecamatan','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Kecamatan </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>