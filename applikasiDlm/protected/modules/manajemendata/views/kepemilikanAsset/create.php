<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kepemilikan Asset'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Kepemilikan Asset','url'=>array('index')),
);
?>

<h1>Tambah Kepemilikan Asset</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>