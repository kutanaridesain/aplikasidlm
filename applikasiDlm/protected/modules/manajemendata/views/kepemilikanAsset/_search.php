<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_tanah_rumah',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'luas_tanah_rumah',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_tanah_sawah',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'luas_tanah_sawah',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_tanah_kebun',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'luast_tanah_kebun',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_tambak',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'luas_tambak',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_lembu',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_lembu',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_kerbau',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_kerbau',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_ternak_lain',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_ternak_lain',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_mobil',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_mobil',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_motor',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_motor',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_tabungan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_asuransi',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_motor_tempel',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_kapal_motor',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_perahu',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'punya_aset_lain',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textFieldRow($model,'userid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglinput',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglupdate',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
