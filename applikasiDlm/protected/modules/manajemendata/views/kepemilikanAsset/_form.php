<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'kepemilikan-asset-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	
	<?php echo $form->textFieldRow($model,'nama_penduduk',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	
	<span class="section-block">Memiliki Tanah</span>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_tanah_rumah', $model->getStatusYaTidakOptions(), array('data-index'=>'0')); ?>

	<div id="punya_tanah_rumah">
		<?php echo $form->dropDownListRow($model, 'luas_tanah_rumah', $model->getLuasTanahOptions()); ?>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_tanah_sawah', $model->getStatusYaTidakOptions(), array('data-index'=>'1')); ?>
	
	<div id="punya_tanah_sawah">
		<?php echo $form->dropDownListRow($model, 'luas_tanah_sawah', $model->getLuasTanahOptions()); ?>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_tanah_kebun', $model->getStatusYaTidakOptions(), array('data-index'=>'2')); ?>

	<div id="punya_tanah_kebun">
		<?php echo $form->dropDownListRow($model, 'luast_tanah_kebun', $model->getLuasTanahOptions()); ?>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_tambak', $model->getStatusYaTidakOptions(), array('data-index'=>'3')); ?>

	<div id="punya_tambak">
		<?php echo $form->dropDownListRow($model, 'luas_tambak', $model->getLuasTanahOptions()); ?>
	</div>

	<span class="section-block">Memiliki Ternak</span>
	
	<?php echo $form->radioButtonListInlineRow($model, 'punya_lembu', $model->getStatusYaTidakOptions(), array('data-index'=>'4')); ?>

	<div id="punya_lembu">
		<?php echo $form->dropDownListRow($model, 'jlh_lembu', $model->getJumlahTernakOptions()); ?>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_kerbau', $model->getStatusYaTidakOptions(), array('data-index'=>'5')); ?>

	<div id="punya_kerbau">
		<?php echo $form->dropDownListRow($model, 'jlh_kerbau', $model->getJumlahTernakOptions()); ?>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_ternak_lain', $model->getStatusYaTidakOptions(), array('data-index'=>'6')); ?>

	<div id="punya_ternak_lain">
		<?php echo $form->dropDownListRow($model, 'jlh_ternak_lain', $model->getJumlahTernakOptions()); ?>
	</div>
	
	<span class="section-block">Memiliki Kendaraan</span>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_mobil', $model->getStatusYaTidakOptions(), array('data-index'=>'7')); ?>

	<div id="punya_mobil">
		<?php echo $form->dropDownListRow($model, 'jlh_mobil', $model->getJumlahKendaraanOptions()); ?>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_motor', $model->getStatusYaTidakOptions(), array('data-index'=>'8')); ?>
	
	<div id="punya_motor">
		<?php echo $form->dropDownListRow($model, 'jlh_motor', $model->getJumlahKendaraanOptions()); ?>
	</div>
	
	<span class="section-block">Memiliki Tabungan/Asuransi</span>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_tabungan', $model->getStatusYaTidakOptions()); ?>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_asuransi', $model->getStatusYaTidakOptions()); ?>
	
	<span class="section-block">Memiliki Kapal Penangkap Ikan</span>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_motor_tempel', $model->getStatusYaTidakOptions()); ?>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_kapal_motor', $model->getStatusYaTidakOptions()); ?>

	<?php echo $form->radioButtonListInlineRow($model, 'punya_perahu', $model->getStatusYaTidakOptions()); ?>
	
	<span class="section-block">Aset Lain</span>

	<?php echo $form->textAreaRow($model,'punya_aset_lain',array('class'=>'span5','maxlength'=>4000)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">

	var arrId = ["punya_tanah_rumah","punya_tanah_sawah","punya_tanah_kebun","punya_tambak", "punya_lembu",
			"punya_kerbau", "punya_ternak_lain", "punya_mobil", "punya_motor"];
	
	ToggleRadioButton=function(id){
		if(($("#KepemilikanAsset_" + id + "_0").is(':checked'))==false){
			$("#"+id).hide();
		}else{
			$("#"+id).show();
		}
	};
	
		
	$(function(){	
	
		if($("#KepemilikanAsset_nik").size()> 0){
			$("#KepemilikanAsset_nik").keyup(function(){
				parameter = "term=" + $('#KepemilikanAsset_nik').val();
			});
			TextFieldAutoComplete("/general/getBiodataViewByNik", "KepemilikanAsset_nik", "KepemilikanAsset_nama_penduduk", "KepemilikanAsset_no_kk");
		}
		
		if($("#KepemilikanAsset_nama_penduduk").size()> 0){
			$("#KepemilikanAsset_nama_penduduk").keyup(function(){
				parameter = "term=" + $('#KepemilikanAsset_nama_penduduk').val();
			});
			TextFieldAutoComplete("/general/getBiodataViewByNama", "KepemilikanAsset_nama_penduduk", "KepemilikanAsset_nik", "KepemilikanAsset_no_kk");
		}
		
		if($("#KepemilikanAsset_no_kk").size()> 0){
			$("#KepemilikanAsset_no_kk").keyup(function(){
				parameter = "term=" + $('#KepemilikanAsset_no_kk').val();
			});
			TextFieldAutoComplete("/general/getBiodataViewByNoKK", "KepemilikanAsset_no_kk", "KepemilikanAsset_nama_penduduk", "KepemilikanAsset_nik");
		}
		
		
		for(var i=0;i<arrId.length;i++){
			ToggleRadioButton(arrId[i]);
				
			var id= "#KepemilikanAsset_" + arrId[i] + "_0, #KepemilikanAsset_" + arrId[i] + "_1";
				
			$(id).click(function(){					
				var index = $(this).attr("data-index");
				if(($("#KepemilikanAsset_" + arrId[index] + "_0").is(':checked'))==false){
					$("#" + arrId[index]).hide();
				}else{
					$("#" + arrId[index]).show();
				}
			});
		}
	});
</script>

