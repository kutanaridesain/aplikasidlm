<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kepemilikan Asset'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar KepemilikanAsset','url'=>array('index')),
	array('label'=>'Tambah KepemilikanAsset','url'=>array('create')),
	array('label'=>'Ubah KepemilikanAsset','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus KepemilikanAsset','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil KepemilikanAsset</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nik',
		'no_kk',
		'nama_punya_tanah_rumah',
		'nama_luas_tanah_rumah',
		'nama_punya_tanah_sawah',
		'nama_luas_tanah_sawah',
		'nama_punya_tanah_kebun',
		'nama_luas_tanah_kebun',
		'nama_punya_tambak',
		'nama_luas_tambak',
		'nama_punya_lembu',
		'nama_jlh_lembu',
		'nama_punya_kerbau',
		'nama_jlh_kerbau',
		'nama_punya_ternak_lain',
		'nama_jlh_ternak_lain',
		'nama_punya_mobil',
		'nama_jlh_mobil',
		'nama_punya_motor',
		'nama_jlh_motor',
		'nama_punya_tabungan',
		'nama_punya_asuransi',
		'nama_punya_motor_tempel',
		'nama_punya_kapal_motor',
		'nama_punya_perahu',
		'punya_aset_lain',
	),
)); ?>
