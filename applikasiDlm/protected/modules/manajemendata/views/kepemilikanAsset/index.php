<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kepemilikan Assets'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Kepemilikan Asset','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kepemilikan-asset-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Kepemilikan Asset</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'kepemilikan-asset-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nik',
		'no_kk',
		array(      
            'name' => 'punya_tanah_rumah',      
            'type' => 'raw',      
            'value' => 'KepemilikanAsset::getNamaStatusYaTidakById($data->punya_tanah_rumah)',      
            'filter' => KepemilikanAsset::getStatusYaTidakOptions(),    
        ),
		array(      
            'name' => 'luas_tanah_rumah',      
            'type' => 'raw',      
            'value' => 'KepemilikanAsset::getNamaLuasTanahById($data->luas_tanah_rumah)',      
            'filter' => KepemilikanAsset::getLuasTanahOptions(),    
        ),
		array(      
            'name' => 'punya_tanah_sawah',      
            'type' => 'raw',      
            'value' => 'KepemilikanAsset::getNamaStatusYaTidakById($data->punya_tanah_sawah)',      
            'filter' => KepemilikanAsset::getStatusYaTidakOptions(),    
        ),
		array(      
            'name' => 'luas_tanah_sawah',      
            'type' => 'raw',      
            'value' => 'KepemilikanAsset::getNamaLuasTanahById($data->luas_tanah_sawah)',      
            'filter' => KepemilikanAsset::getLuasTanahOptions(),    
        ),
		
		/*'punya_tanah_kebun',
		'luast_tanah_kebun',
		'punya_tambak',
		'luas_tambak',
		'punya_lembu',
		'jlh_lembu',
		'punya_kerbau',
		'jlh_kerbau',
		'punya_ternak_lain',
		'jlh_ternak_lain',
		'punya_mobil',
		'jlh_mobil',
		'punya_motor',
		'jlh_motor',
		'punya_tabungan',
		'punya_asuransi',
		'punya_motor_tempel',
		'punya_kapal_motor',
		'punya_perahu',
		'punya_aset_lain',
		'userid',
		'tglinput',
		'tglupdate',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
