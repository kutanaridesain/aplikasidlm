<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kepemilikan Asset'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Kepemilikan Asset','url'=>array('index')),
	array('label'=>'Tambah Kepemilikan Asset','url'=>array('create')),
	array('label'=>'Detil Kepemilikan Asset','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Kepemilikan Asset </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>