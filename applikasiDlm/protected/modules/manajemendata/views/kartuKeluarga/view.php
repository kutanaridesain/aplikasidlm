<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kartu Keluarga'=>array('index'),
	$model->no_kk,
);

$this->menu=array(
	array('label'=>'Daftar Kartu Keluarga','url'=>array('index')),
	array('label'=>'Tambah Kartu Keluarga','url'=>array('create')),
	array('label'=>'Ubah Kartu Keluarga','url'=>array('update','id'=>$model->no_kk)),
	array('label'=>'Hapus Kartu Keluarga','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->no_kk),'confirm'=>Yii::t('dlm', 'confirm delete'))),
	array('label'=>'PMKS','url'=>array('pmks/create','nokk'=>$model->no_kk, 'nik'=>$model->nik)),
);
?>

<h1>Detil Kartu Keluarga #<?php echo $model->no_kk; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'no_kk',
		'nik',
		'nama_hubungan',
	),
)); ?>
