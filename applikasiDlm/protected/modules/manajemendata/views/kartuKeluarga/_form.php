<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'kepala-keluarga-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5')); ?>
	
	<?php 
		if($model->isNewRecord){
			echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>'));
		}else{
			echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'readonly'=>'readonly'));
		}			
	?>
	
	<?php echo $form->textFieldRow($model,'nama_penduduk',array('class'=>'span5', 'disabled'=>'disabled')); ?>

	<div style="display:none;">
		<?php echo $form->dropDownListRow($model, 'hub_keluarga', $model->getHubunganKeluargaOptions(true)); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
		
		
		<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'primary',
			'label'=>'Batal',
			'url'=>'/manajemendata/kartukeluarga/',
		)); ?>
		
		<?php 
		if(!$model->isNewRecord ){
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'link',
				'type'=>'primary',
				'label'=>'+ Buat Anggota Keluarga',
				'url' => '/manajemendata/kartukeluargadetails/create/nokk/' . $model->no_kk,
			));
		} 
		?>
	</div>
<?php $this->endWidget(); ?>



<script type="text/javascript">
	$(function(){	
	
		if($("#KepalaKeluarga_nik").size()> 0){
			$("#KepalaKeluarga_nik").keyup(function(){
				parameter = "term=" + $('#KepalaKeluarga_nik').val();
			});
			TextFieldAutoComplete("/general/getNikKepalaKeluarga", "KepalaKeluarga_nik", "KepalaKeluarga_nama_penduduk");
		}
	});
</script>
