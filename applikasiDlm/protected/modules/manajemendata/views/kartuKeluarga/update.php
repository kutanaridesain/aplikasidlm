<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kartu Keluarga'=>array('index'),
	$model->no_kk=>array('view','id'=>$model->no_kk),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Kartu Keluarga','url'=>array('index')),
	array('label'=>'Tambah Kartu Keluarga','url'=>array('create')),
	array('label'=>'Detil Kartu Keluarga','url'=>array('view','id'=>$model->no_kk)),
	array('label'=>'PMKS','url'=>array('pmks/create','nokk'=>$model->no_kk, 'nik'=>$model->nik)),
);
?>

<h1>Ubah Kartu Keluarga <?php echo $model->no_kk; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

	<br/>
	<span class="section-block">Anggota Keluarga</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'kepala-keluarga-details-grid',
		'dataProvider'=>$detailsDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_hubungan',			
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update} {delete}'
			),
		),
	)); ?>


<script type="text/javascript">
	$(function(){	
		$(".update, .delete").each(function(){
			var strUrl = $(this).attr("href").replace("kartukeluarga", "kartukeluargadetails");
			strUrl = strUrl + "/nokk/"+ <?php echo $model->no_kk; ?>;
			$(this).attr("href", strUrl);
		});
	});
</script>
