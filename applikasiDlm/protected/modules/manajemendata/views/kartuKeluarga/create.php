<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kartu Keluarga'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Kartu Keluarga','url'=>array('index')),
);
?>

<h1>Tambah Kartu Keluarga</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>