<?php
/* @var $this KepalaKeluargaDetailsController */
/* @var $model KepalaKeluargaDetails */

$this->breadcrumbs=array(
	'Anggota Keluarga'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Kembali', 'url'=>array('/manajemendata/kartukeluarga/update/id/'.$model->no_kk)),
	//array('label'=>'Daftar KepalaKeluarga', 'url'=>array('kepalakeluarga/index')),
	//array('label'=>'Tambah KepalaKeluarga ', 'url'=>array('kepalakeluarga/create')),
	//array('label'=>'Detil KepalaKeluarga ', 'url'=>array('kepalakeluargav/iew', 'id'=>$model->id)),
);
?>

<h1>Ubah Anggota Keluarga </h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>