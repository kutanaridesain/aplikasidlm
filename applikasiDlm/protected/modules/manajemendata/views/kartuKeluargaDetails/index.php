<?php
/* @var $this KepalaKeluargaDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Anggota Keluargas',
);

$this->menu=array(
	array('label'=>'Tambah Anggota Keluarga', 'url'=>array('create')),
);
?>

<h1>Anggota Keluarga</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
