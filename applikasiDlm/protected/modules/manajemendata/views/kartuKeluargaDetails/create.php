<?php
/* @var $this KepalaKeluargaDetailsController */
/* @var $model KepalaKeluargaDetails */

$this->breadcrumbs=array(
	'Anggota Keluarga'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Kembali', 'url'=>array('/manajemendata/kartukeluarga/update/id/'.$model->no_kk)),
	//array('label'=>'Daftar KepalaKeluarga', 'url'=>array('kepalakeluarga/index')),
);
?>

<h1>Tambah Anggota Keluarga</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>