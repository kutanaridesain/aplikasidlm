<?php
/* @var $this Anggota KeluargaController */
/* @var $model Anggota Keluarga */

$this->breadcrumbs=array(
	'Anggota Keluarga'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Anggota Keluarga', 'url'=>array('index')),
	array('label'=>'Tambah Anggota Keluarga', 'url'=>array('create')),
	array('label'=>'Ubah Anggota Keluarga', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Hapus Anggota Keluarga', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Anggota Keluarga</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'no_kk',
		'nik',
		'hub_keluarga',
		'userid',
		'tglinput',
		'tglupdate',
	),
)); ?>
