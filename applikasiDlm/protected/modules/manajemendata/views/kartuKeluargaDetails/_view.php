<?php
/* @var $this KepalaKeluargaDetailsController */
/* @var $data KepalaKeluargaDetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kk')); ?>:</b>
	<?php echo CHtml::encode($data->no_kk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik')); ?>:</b>
	<?php echo CHtml::encode($data->nik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hub_keluarga')); ?>:</b>
	<?php echo CHtml::encode($data->hub_keluarga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::encode($data->userid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglinput')); ?>:</b>
	<?php echo CHtml::encode($data->tglinput); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglupdate')); ?>:</b>
	<?php echo CHtml::encode($data->tglupdate); ?>
	<br />


</div>