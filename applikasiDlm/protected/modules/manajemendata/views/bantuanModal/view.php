<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Bantuan Modal'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar BantuanModal','url'=>array('index')),
	array('label'=>'Tambah BantuanModal','url'=>array('create')),
	array('label'=>'Ubah BantuanModal','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus BantuanModal','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil BantuanModal</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nik',
		'nama_penduduk',
		'no_kk',
		'nama_status_bantuan_modal',
		'jenis_bantuan_modal',
		'nama_kategory_bantuan_modal',
		'nama_kelompok',
		'nama_instansi',
		'tahun_diberi',
		'nama_sumber_bantuan_modal',
		'nama_koperasi',
	),
)); ?>
