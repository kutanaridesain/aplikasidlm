<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'status_bantuan_modal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jenis_bantuan_modal',array('class'=>'span5','maxlength'=>400)); ?>

	<?php echo $form->textFieldRow($model,'kategory_bantuan_modal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_kelompok',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_instansi',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tahun_diberi',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'sumber_bantuan_modal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_koperasi',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'userid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglinput',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglupdate',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
