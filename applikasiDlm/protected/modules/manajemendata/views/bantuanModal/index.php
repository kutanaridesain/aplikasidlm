<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Bantuan Modals'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Bantuan Modal','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('bantuan-modal-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Bantuan Modal</h1>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'bantuan-modal-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nik',
		'no_kk',
		'status_bantuan_modal',
		'jenis_bantuan_modal',
		'kategory_bantuan_modal',
		/*
		'id_kelompok',
		'id_instansi',
		'tahun_diberi',
		'sumber_bantuan_modal',
		'id_koperasi',
		'userid',
		'tglinput',
		'tglupdate',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
