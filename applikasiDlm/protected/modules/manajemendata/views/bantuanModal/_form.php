<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'bantuan-modal-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'readonly'=>'readonly', 'autocomplete'=>'off')); ?>
	
	<?php echo $form->textFieldRow($model,'nama_penduduk',array('class'=>'span5', 'readonly'=>'readonly', 'autocomplete'=>'off')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5', 'readonly'=>'readonly', 'autocomplete'=>'off')); ?>

	<?php echo $form->radioButtonListInlineRow($model, 'status_bantuan_modal', $model->getStatusYaTidakOptions()); ?>

	<?php echo $form->textAreaRow($model,'jenis_bantuan_modal',array('class'=>'span5','maxlength'=>400)); ?>

	<?php echo $form->radioButtonListInlineRow($model, 'kategory_bantuan_modal', $model->getKategoriBantuanModalOptions()); ?>

	<div id="id_kelompok">
		<?php echo $form->dropDownListRow($model, 'id_kelompok', $this->getKelompokOptions()); ?>
	</div>
	
	<?php echo $form->dropDownListRow($model, 'id_instansi', $this->getInstansiOptions()); ?>

	<?php echo $form->datepickerRow($model, 'tahun_diberi',
        array('append'=>'<i class="icon-calendar"></i>', 'class'=>'span1', 'options'=>array(
    		'format'=>'yyyy',
    		'viewMode'=>'years',
    		'minViewMode'=>'years',
    		'autoclose'=>'true',
    	)));
	?>

	<?php echo $form->radioButtonListInlineRow($model, 'sumber_bantuan_modal', $model->getSumberBantuanModalOptions()); ?>
	
	<div id="id_koperasi">
		<?php echo $form->dropDownListRow($model, 'id_koperasi', $this->getKoperasiOptions()); ?>
	</div>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		?>
		
		<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'danger',
			'label'=>'Batal',
			'url'=>'/manajemendata/biodata/update/id/'.$model->nik,
		)); ?>
		
		<?php 
		if(!$model->isNewRecord ){
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'link',
				'type'=>'primary',
				'label'=>'+ Buat Bantuan Modal Baru',
				'url' => '/manajemendata/bantuanmodal/create/nik/' . $model->nik,
			));
		} 
		?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	ToggleKategoryBantuanModal=function(){
		$("#id_kelompok").toggle();
	};
	
	ToggleSumberBantuanModal=function(){
		$("#id_koperasi").toggle();
	};

	$(function(){
		if(($("#BantuanModal_kategory_bantuan_modal_0").is(':checked'))==true){
			$("#id_kelompok").hide();
		}
		
		$("#BantuanModal_kategory_bantuan_modal_0, #BantuanModal_kategory_bantuan_modal_1").click(function() {
			ToggleKategoryBantuanModal();
		});
		
		if(($("#BantuanModal_sumber_bantuan_modal_0").is(':checked'))==false){
			$("#id_koperasi").hide();
		}
		
		$("#BantuanModal_sumber_bantuan_modal_0, #BantuanModal_sumber_bantuan_modal_1").click(function() {
			ToggleSumberBantuanModal();
		});
		
	});
	
</script>
