<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kepala Keluarga'=>array('index'),
	$model->no_kk=>array('view','id'=>$model->no_kk),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Kepala Keluarga','url'=>array('index')),
	array('label'=>'Tambah Kepala Keluarga','url'=>array('create')),
	array('label'=>'Detil Kepala Keluarga','url'=>array('view','id'=>$model->no_kk)),
	array('label'=>'PMKS','url'=>array('pmks/create','nokk'=>$model->no_kk, 'nik'=>$model->nik)),
);
?>

<h1>Ubah Kepala Keluarga <?php echo $model->no_kk; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

	<br/>
	<span class="section-block">Anggota Keluarga</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'kepala-keluarga-details-grid',
		'dataProvider'=>$detailsDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_hubungan',
			
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update} {delete}'
			),
		),
	)); ?>


<script type="text/javascript">
	$(function(){	
		$(".update, .delete").each(function(){
			var strUrl = $(this).attr("href").replace("kepalakeluarga", "kepalakeluargadetails");
			strUrl = strUrl + "/nokk/"+ <?php echo $model->no_kk; ?>;
			$(this).attr("href", strUrl);
		});
	});
</script>
