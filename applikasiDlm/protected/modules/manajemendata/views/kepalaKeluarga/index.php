<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kepala Keluargas'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah KepalaKeluarga','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kepala-keluarga-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Kepala Keluarga</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'kepala-keluarga-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'no_kk',
		'nik',
		array(      
            'name' => 'hub_keluarga',      
            'type' => 'raw',      
            'value' => 'KepalaKeluarga::getNamaHubunganKeluargaById($data->hub_keluarga)',      
            'filter' => KepalaKeluarga::getHubunganKeluargaOptions(true),    
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
