<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kepala Keluarga'=>array('index'),
	$model->no_kk,
);

$this->menu=array(
	array('label'=>'Daftar KepalaKeluarga','url'=>array('index')),
	array('label'=>'Tambah KepalaKeluarga','url'=>array('create')),
	array('label'=>'Ubah KepalaKeluarga','url'=>array('update','id'=>$model->no_kk)),
	array('label'=>'Hapus KepalaKeluarga','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->no_kk),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil KepalaKeluarga #<?php echo $model->no_kk; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'no_kk',
		'nik',
		'nama_hubungan',
	),
)); ?>
