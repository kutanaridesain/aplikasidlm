<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kepala Keluarga'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Kepala Keluarga','url'=>array('index')),
);
?>

<h1>Tambah Kepala Keluarga</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>