<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Koperasi'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Koperasi','url'=>array('index')),
	array('label'=>'Tambah Koperasi','url'=>array('create')),
	array('label'=>'Detil Koperasi','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Koperasi </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>