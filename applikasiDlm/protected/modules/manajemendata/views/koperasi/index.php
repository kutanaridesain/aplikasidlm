<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Koperasis'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Daftar Koperasi','url'=>array('index')),
	array('label'=>'Tambah Koperasi','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('koperasi-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Koperasi</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'koperasi-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nama_koperasi',
		'ketua_koperasi',
		'bidang_usaha',
		'no_telp',	
		array(      
            'name' => 'id_kab',      
            'type' => 'raw',      
            'value' => 'Kabupaten::getById($data->id_kab)',      
            'filter' => Kabupaten::getOptionList(), 
        ),
		/*
		'alamat',
		'userid',
		'tglinput',
		'tglupdate',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
