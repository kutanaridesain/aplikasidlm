<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Koperasi'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Koperasi','url'=>array('index')),
	array('label'=>'Tambah Koperasi','url'=>array('create')),
	array('label'=>'Ubah Koperasi','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Koperasi','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Koperasi</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_koperasi',
		'ketua_koperasi',
		'bidang_usaha',
		'no_telp',
		'alamat',
		'nama_kab',
	),
)); ?>
