<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Koperasi'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Koperasi','url'=>array('index')),
);
?>

<h1>Tambah Koperasi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>