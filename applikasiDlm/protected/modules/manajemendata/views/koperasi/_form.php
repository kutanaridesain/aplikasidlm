<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'koperasi-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama_koperasi',array('class'=>'span5','maxlength'=>300)); ?>

	<?php echo $form->textFieldRow($model,'ketua_koperasi',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'bidang_usaha',array('class'=>'span5','maxlength'=>300)); ?>

	<?php echo $form->textFieldRow($model,'no_telp',array('class'=>'span5','maxlength'=>40)); ?>

	<?php echo $form->textAreaRow($model,'alamat',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->textFieldRow($model,'nama_kab',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>

	<?php echo $form->hiddenField($model,'id_kab'); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		if($("#Koperasi_nama_kab").size()> 0){
			$("#Koperasi_nama_kab").keyup(function(){
				parameter = "term=" + $('#Koperasi_nama_kab').val();
			});
			TextFieldAutoComplete("/general/getKabupatenByRole", "Koperasi_nama_kab", "Koperasi_id_kab");
		}
	});
</script>