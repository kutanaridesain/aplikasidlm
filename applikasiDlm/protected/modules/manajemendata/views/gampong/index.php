<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Gampongs'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Gampong','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gampong-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Gampong</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'gampong-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(      
            'name' => 'id_kab',      
            'type' => 'raw',      
            'value' => 'Kabupaten::getById($data->id_kab)',      
            'filter' => Kabupaten::getOptionList(), 
        ),
		array(      
            'name' => 'id_kec',      
            'type' => 'raw',      
            'value' => 'Kecamatan::getById($data->id_kec)',      
            'filter' => Kecamatan::getOptionList(), 
        ),
		'nama_gampong',
		'alamat',
		'keuchik',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
