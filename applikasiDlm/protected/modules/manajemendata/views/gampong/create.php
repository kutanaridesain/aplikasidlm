<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Gampong'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Gampong','url'=>array('index')),
);
?>

<h1>Tambah Gampong</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>