<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'gampong-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama_kab',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	<?php echo $form->hiddenField($model,'id_kab'); ?>
	
	<?php echo $form->textFieldRow($model,'nama_kec',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	<?php echo $form->hiddenField($model,'id_kec'); ?>

	<?php echo $form->textFieldRow($model,'nama_gampong',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textAreaRow($model,'alamat',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->textFieldRow($model,'keuchik',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textAreaRow($model,'visi',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textAreaRow($model,'misi',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textFieldRow($model,'koordinat_x',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'koordinat_y',array('class'=>'span5','maxlength'=>100)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		if($("#Gampong_nama_kab").size()> 0){
			$("#Gampong_nama_kab").keyup(function(){
				parameter = "term=" + $('#Gampong_nama_kab').val();
			});
			TextFieldAutoComplete("/general/getKabupaten", "Gampong_nama_kab", "Gampong_id_kab");
		}
		
		if($("#Gampong_nama_kec").size()> 0){
			$("#Gampong_nama_kec").keyup(function(){
				parameter = "term=" + $('#Gampong_nama_kec').val() + "&id="+$("#Gampong_id_kab").val();
			});
			TextFieldAutoComplete("/general/getKecamatanByKabupaten", "Gampong_nama_kec", "Gampong_id_kec");
		}
	});
</script>
