<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Gampong'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Gampong','url'=>array('index')),
	array('label'=>'Tambah Gampong','url'=>array('create')),
	array('label'=>'Ubah Gampong','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Gampong','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Gampong</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_kab',
		'nama_kec',
		'nama_gampong',
		'alamat',
		'keuchik',
		'visi',
		'misi',
		'koordinat_x',
		'koordinat_y',
	),
)); ?>
