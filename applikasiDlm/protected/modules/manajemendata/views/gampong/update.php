<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Gampong'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Gampong','url'=>array('index')),
	array('label'=>'Tambah Gampong','url'=>array('create')),
	array('label'=>'Detil Gampong','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Gampong </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>