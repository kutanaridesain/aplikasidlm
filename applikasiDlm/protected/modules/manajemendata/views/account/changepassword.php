<?php
/* @var $this AccountController */

$this->breadcrumbs=array(
	'Account'=>array('/manajemendata/account'),
	'Changepassword',
);
?>
<legend>Menngganti Password</legend>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'biodata-form',
	'enableAjaxValidation'=>false,
)); ?>
	<fieldset>		
		<!-- <label>Password lama</label>
		<input type="password" placeholder="Password lama" name="password" value=""> -->
		<?php if(!empty($message)): ?>
			<?php if($message['type'] == 'error'):?>
				<div class="alert alert-block alert-error"><p>Silahkan perbaiki kesalahan input berikut:</p>
				<ul>
					<li><?php echo $message['error']; ?></li>
				</ul>
				</div>
			<?php else: ?>
				<p class="help-block">Sukses mengganti password</p>
			<?php endif; ?>
		<?php endif; ?>
		<label>Password baru :</label>
		<input type="password" placeholder="Password baru" class="span3" name="new-password" value="" autocomplete="off">
		<label>Ketik ulang password baru :</label>
		<div class="input-append">
			<input type="password" placeholder="Password baru" class="span3" name="renew-password" value="" autocomplete="off">
			<span class="add-on">
				<a href="#" rel="popover" title="Informasi" data-content="Masukkan nilai yang sama dengan password diatas!">
					<i class="icon icon-info-sign"></i>
				</a>
			</span>
		</div>
	</fieldset>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Kirim',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

