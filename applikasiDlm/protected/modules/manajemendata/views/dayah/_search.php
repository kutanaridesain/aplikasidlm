<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_kab',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nama_dayah',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'nama_pimpinan',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'type_dayah',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_pengasuh',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_santri',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nama_tingkat',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_kelas',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_perpustakaan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_lab_komputer',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_mushalla',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_kamar_mandi_wc',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'userid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglinput',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglupdate',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
