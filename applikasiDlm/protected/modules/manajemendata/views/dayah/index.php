<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Dayahs'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Dayah','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('dayah-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Dayah</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'dayah-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nama_dayah',
		'nama_pimpinan',
		array(      
            'name' => 'type_dayah',      
            'type' => 'raw',      
            'value' => 'Dayah::getNamaTypeDayahById($data->type_dayah)',      
            'filter' => Dayah::getTypeDayahOptions(),    
        ),	
		array(      
            'name' => 'id_kab',      
            'type' => 'raw',      
            'value' => 'Kabupaten::getById($data->id_kab)',      
            'filter' => Kabupaten::getOptionList(), 
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
