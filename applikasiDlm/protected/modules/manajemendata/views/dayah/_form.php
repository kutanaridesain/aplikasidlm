<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'dayah-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama_dayah',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'nama_pimpinan',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->dropDownListRow($model, 'type_dayah', $model->getTypeDayahOptions()); ?>

	<?php echo $form->textFieldRow($model,'jlh_pengasuh',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_santri',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'nama_tingkat',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_kelas',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_perpustakaan',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_lab_komputer',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_mushalla',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_kamar_mandi_wc',array('class'=>'span1')); ?>
	
	<?php echo $form->textFieldRow($model,'nama_kab',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	<?php echo $form->hiddenField($model,'id_kab'); ?>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		if($("#Dayah_nama_kab").size()> 0){
			$("#Dayah_nama_kab").keyup(function(){
				parameter = "term=" + $('#Dayah_nama_kab').val();
			});
			TextFieldAutoComplete("/general/getKabupatenByRole", "Dayah_nama_kab", "Dayah_id_kab");
		}
	});
</script>
