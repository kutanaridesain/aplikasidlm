<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Dayah'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Dayah','url'=>array('index')),
	array('label'=>'Tambah Dayah','url'=>array('create')),
	array('label'=>'Detil Dayah','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Dayah </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>