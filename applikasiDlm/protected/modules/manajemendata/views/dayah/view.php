<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Dayah'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Dayah','url'=>array('index')),
	array('label'=>'Tambah Dayah','url'=>array('create')),
	array('label'=>'Ubah Dayah','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Dayah','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Dayah</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_dayah',
		'nama_pimpinan',
		'nama_type_dayah',
		'jlh_pengasuh',
		'jlh_santri',
		'nama_tingkat',
		'jlh_kelas',
		'jlh_perpustakaan',
		'jlh_lab_komputer',
		'jlh_mushalla',
		'jlh_kamar_mandi_wc',
		'nama_kab',
	),
)); ?>
