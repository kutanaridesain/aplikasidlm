<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Dayah'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Dayah','url'=>array('index')),
);
?>

<h1>Tambah Dayah</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>