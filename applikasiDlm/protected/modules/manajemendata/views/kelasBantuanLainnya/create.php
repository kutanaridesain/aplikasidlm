<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelas Bantuan Lainnyas'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar KelasBantuanLainnya','url'=>array('index')),
);
?>

<h1>Tambah KelasBantuanLainnya</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>