<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelas Bantuan Lainnyas'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Kelas Bantuan Lainnya','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kelas-bantuan-lainnya-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Kelas Bantuan Lainnya</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'kelas-bantuan-lainnya-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'id_bantuan_lainnya',
		'id_kelas_bantuan',
		'userid',
		'tglinput',
		'tglupdate',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
