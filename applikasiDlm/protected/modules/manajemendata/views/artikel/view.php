<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Artikel'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'Daftar Artikel','url'=>array('index')),
	array('label'=>'Tambah Artikel','url'=>array('create')),
	array('label'=>'Ubah Artikel Ini','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Artikel Ini','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Anda yakin menghapus item ini?')),
);
?>


<div class="page-header">
	<h1><?php echo $model->title; ?> <small>dikirim <?php echo date('d-m-Y',strtotime($model->tglinput)); ?></small></h1>
	<?php if((bool)$model->is_visible):?>
	<div class=""><span class="label label-info pull-right">Dipublikasi!</span></div>
	<?php else: ?>
	<div class=""><span class="label label-important pull-right">Tidak dipublikasi!</span></div>
	<?php endif; ?>
</div>

<?php echo '<p>' . $model->text_intro . '</p>'; ?>
<?php echo $model->text_full; ?>