<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Artikel'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Artikel','url'=>array('index')),
	array('label'=>'Tambah Artikel','url'=>array('create')),
	array('label'=>'Detil Artikel','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Artikel <small><?php echo $model->title; ?></small></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>