<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'artikel-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'title', array(
		'class'=>'span5'
	)); ?>

	<?php echo $form->html5EditorRow($model,'text_intro',array('class'=>'span5','maxlength'=>500, 'height'=>'100px', 'width'=>'98%')); ?>

	<?php echo $form->ckEditorRow($model,'text_full',array('class'=>'span5','maxlength'=>4000)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
