<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Artikels'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Artikel','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('artikel-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Artikel</h1>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'artikel-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		// 'id',
		// 'text_intro',
		// 'text_full',
		'title',
		array(
			'name'=>'is_visible',
			'type'=>'boolean',
			'value'=>'$data->is_visible',
			'filter'=>array('1'=>'Yes', '0'=>'No')
		),
		
		// array(
		// 	'name'=>'is_visible',
		// 	'type'=>'raw',
		// 	'value'=>array('Ya'=>1, 'Tidak'=>0),
		// ),
		array(
			'name'=>'user.username',
			'value'=>'$data->user->username',
			'header'=>'Pengirim'
		),
		array(
			'name'=>'tglinput',
		),
		// 'tglupdate',
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
		array(
			'class'=>'CLinkColumn',
			'labelExpression'=>'$data->checkVisibility($data)',
			'urlExpression'=>'array("artikel/togglevisibility", "id"=>$data->id, "status"=>$data->is_visible)',
			'linkHtmlOptions'=>array(
				'url'=>'artikel',
			),
		),
		// array(
	 //        'header'=>'Actions',
	 //        'type'=>'raw',
	 //        'value'=>'CHtml::ajaxLink(CHtml::image("' . Yii::app()->request->baseUrl . '/path/to/icon.png", "Delete", array("title"=>"Delete","class"=>"grid_icon")), Yii::app()->createUrl("controller/ajaxDelete"), array("type"=>"POST", "data"=>array("id"=>$data->id, "action"=>"delete"), "success"=>"jsDelete"), array("onclick"=>"$(\'.grid-view\').addClass(\'grid-view-loading\')", "class"=>"delete", "confirm"=>"Are you sure?\r\nDrafts are permanently deleted and are not recoverable.") )',
		// ),			
	),
)); ?>
