<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Artikel'=>array('index'),
	'Tambah Artikel',
);

$this->menu=array(
	array('label'=>'Daftar Artikel','url'=>array('index')),
);
?>

<h1>Tambah Artikel</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>