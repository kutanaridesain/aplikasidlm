<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Jenis PMKS'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Jenis PMKS','url'=>array('index')),
);
?>

<h1>Tambah Jenis PMKS</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>