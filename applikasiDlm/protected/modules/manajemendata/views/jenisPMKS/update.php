<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Jenis PMKS'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Jenis PMKS','url'=>array('index')),
	array('label'=>'Tambah Jenis PMKS','url'=>array('create')),
	array('label'=>'Detil Jenis PMKS','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Jenis PMKS </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>