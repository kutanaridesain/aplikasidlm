<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Jenis PMKS'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Jenis PMKS','url'=>array('index')),
	array('label'=>'Tambah Jenis PMKS','url'=>array('create')),
	array('label'=>'Ubah Jenis PMKS','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Jenis PMKS','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Jenis PMKS</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_pmks',
	),
)); ?>
