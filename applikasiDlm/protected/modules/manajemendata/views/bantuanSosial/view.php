<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Bantuan Sosials'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar BantuanSosial','url'=>array('index')),
	array('label'=>'Tambah BantuanSosial','url'=>array('create')),
	array('label'=>'Ubah BantuanSosial','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus BantuanSosial','url'=>'#','linkOption'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil BantuanSosial</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nik',
		'nama_penduduk',
		'no_kk',
		'nama_status_bantuan',
		'jenis_bantuan',
		'nama_instansi',
		'tahun_diberikan',
	),
)); ?>
