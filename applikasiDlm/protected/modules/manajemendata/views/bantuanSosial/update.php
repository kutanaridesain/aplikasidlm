<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Bantuan Sosial'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Kembali','url'=>array('biodata/update', 'id'=>$model->nik)),
	//array('label'=>'Daftar Biodata','url'=>array('biodata/index')),
	//array('label'=>'Tambah Biodata','url'=>array('biodata/create')),
	//array('label'=>'Detil BantuanSosial','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Bantuan Sosial </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

<br/>
	<span class="section-block">Daftar Bantuan Sosial</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'bantuan-sosial-details-grid',
		'dataProvider'=>$bantuanSosialDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_instansi',
			'jenis_bantuan',
			'tahun_diberikan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update} {delete}'
			),
		),
	)); ?>
	
	
<script type="text/javascript">
	$(function(){	
		$(".update, .delete").each(function(){
			var strUrl = $(this).attr("href") + "/nik/"+<?php echo $model->nik; ?>+"";
			
			$(this).attr("href", strUrl);
		});
	});
</script>