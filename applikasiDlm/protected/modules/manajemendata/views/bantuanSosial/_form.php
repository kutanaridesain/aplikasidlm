<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'bantuan-sosial-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'readonly'=>'readonly', 'autocomplete'=>'off')); ?>
	
	<?php echo $form->textFieldRow($model,'nama_penduduk',array('class'=>'span5', 'readonly'=>'readonly', 'autocomplete'=>'off')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5', 'readonly'=>'readonly', 'autocomplete'=>'off')); ?>

	<?php echo $form->radioButtonListInlineRow($model, 'status_bantuan', $model->getStatusYaTidakOptions()); ?>

	<?php echo $form->textAreaRow($model,'jenis_bantuan',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->dropDownListRow($model, 'id_instansi', $this->getInstansiOptions()); ?>

	<?php echo $form->datepickerRow($model, 'tahun_diberikan',
        array('append'=>'<i class="icon-calendar"></i>', 'class'=>'span1', 'options'=>array(
    		'format'=>'yyyy',
    		'viewMode'=>'years',
    		'minViewMode'=>'years',
    		'autoclose'=>'true',
    	))); 
    ?>
	

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); 
		?>
		
		<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'danger',
			'label'=>'Batal',
			'url'=>'/manajemendata/biodata/update/id/'.$model->nik,
		)); ?>
		
		<?php 
		if(!$model->isNewRecord ){
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'link',
				'type'=>'primary',
				'label'=>'+ Buat Bantuan Sosial Baru',
				'url' => '/manajemendata/bantuansosial/create/nik/' . $model->nik,
			));
		} 
		?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	
	$(function(){	
	
		if($("#BantuanSosial_nik").size()> 0){
			$("#BantuanSosial_nik").keyup(function(){
				parameter = "term=" + $('#BantuanSosial_nik').val();
			});
			TextFieldAutoComplete("/general/getBiodataViewByNik", "BantuanSosial_nik", "BantuanSosial_nama_penduduk", "BantuanSosial_no_kk");
		}
		
		if($("#BantuanSosial_nama_penduduk").size()> 0){
			$("#BantuanSosial_nama_penduduk").keyup(function(){
				parameter = "term=" + $('#BantuanSosial_nama_penduduk').val();
			});
			TextFieldAutoComplete("/general/getBiodataViewByNama", "BantuanSosial_nama_penduduk", "BantuanSosial_nik", "BantuanSosial_no_kk");
		}
		
		if($("#BantuanSosial_no_kk").size()> 0){
			$("#BantuanSosial_no_kk").keyup(function(){
				parameter = "term=" + $('#BantuanSosial_no_kk').val();
			});
			TextFieldAutoComplete("/general/getBiodataViewByNoKK", "BantuanSosial_no_kk", "BantuanSosial_nama_penduduk", "BantuanSosial_nik");
		}
	});
</script>

