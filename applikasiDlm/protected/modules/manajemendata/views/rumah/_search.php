<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'status_rumah',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'panjang_lantai',array('class'=>'span5')); ?>
	
	<?php echo $form->textFieldRow($model,'lebar_lantai',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jenis_lantai',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jenis_dinding',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'fasilitas_wc',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'penerangan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'air_minum',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'bahan_bakar_masak',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'rumah_bantuan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pemberi_rumah_bantuan',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'tahun_rumah_bantuan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'koordinat_x',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->textFieldRow($model,'koordinat_y',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->textFieldRow($model,'photo',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'jlh_makan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'lauk_pauk',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'daging',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pakaian',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'userid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglinput',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglupdate',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
