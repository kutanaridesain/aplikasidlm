<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Rumah'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Rumah','url'=>array('index')),
);
?>

<h1>Tambah Rumah</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>