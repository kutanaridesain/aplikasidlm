<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Rumahs'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Rumah','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('rumah-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Rumah</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'rumah-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nik',
		'no_kk',
		array(      
            'name' => 'status_rumah',      
            'type' => 'raw',      
            'value' => 'Rumah::getNamaStatusRumahById($data->status_rumah)',      
            'filter' => Rumah::getKepemilikanRumahOptions(),    
        ),
		'koordinat_x',
		'koordinat_y',
		/*
		'nama_jenis_lantai',
		//'panjang_lantai',
		//'lebar_lantai',
		'jenis_dinding',
		'fasilitas_wc',
		'penerangan',
		'air_minum',
		'bahan_bakar_masak',
		'rumah_bantuan',
		'pemberi_rumah_bantuan',
		'tahun_rumah_bantuan',
		'photo',
		'jlh_makan',
		'lauk_pauk',
		'daging',
		'pakaian',
		'userid',
		'tglinput',
		'tglupdate',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
