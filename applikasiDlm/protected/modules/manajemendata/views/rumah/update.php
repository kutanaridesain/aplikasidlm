<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Rumah'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Rumah','url'=>array('index')),
	array('label'=>'Tambah Rumah','url'=>array('create')),
	array('label'=>'Detil Rumah','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Rumah </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>