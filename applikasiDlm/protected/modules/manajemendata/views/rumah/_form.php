<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'rumah-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>

	<span class="section-block">A. Papan</span>

	<?php echo $form->dropDownListRow($model, 'status_rumah', $model->getKepemilikanRumahOptions()); ?>

	<?php echo $form->textFieldRow($model,'panjang_lantai',array('class'=>'span2')); ?>
	
	<?php echo $form->textFieldRow($model,'lebar_lantai',array('class'=>'span2')); ?>

	<?php echo $form->dropDownListRow($model, 'jenis_lantai', $model->getLantaiRumahOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'jenis_dinding', $model->getDindingRumahOptions()); ?>

	<?php echo $form->radioButtonListInlineRow($model, 'fasilitas_wc', $model->getStatusYaTidakOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'penerangan', $model->getSumberPeneranganOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'air_minum', $model->getSumberAirOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'bahan_bakar_masak', $model->getBahanBakarMasakOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'rumah_bantuan', $model->getStatusRumahOptions()); ?>
	
	<div id="rumah_bantuan">
		<?php echo $form->textFieldRow($model,'pemberi_rumah_bantuan',array('class'=>'span5','maxlength'=>200)); ?>
		
		<div class="input-append date yearly-datepicker" data-date-format="yyyy" id="tahun_rumah_bantuan">
			<?php echo $form->textFieldRow($model,'tahun_rumah_bantuan',array('class'=>'span2 datepicker-textfield')); ?>
			<span class="add-on"><i class="icon-th"></i></span>
		</div>
	</div>

	<?php echo $form->textFieldRow($model,'koordinat_x',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->textFieldRow($model,'koordinat_y',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->fileFieldRow($model, 'photo'); ?> 
	<?php echo ".jpg, .png, .gif"; ?>
	<?php 
		if(!$model->isNewRecord){ ?>
			<div id="rumah-picture">
				<br/>
				<img src="/<?php echo Rumah::PATH . $model->photo;?>" alt="" />
				<br/>
				<br/>
			</div>
	<?php } ?>
	
	<span class="section-block">B. Pangan</span>

	<?php echo $form->dropDownListRow($model, 'jlh_makan', $model->getKonsumsiNasiOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'lauk_pauk', $model->getKonsumsiLaukPaukDagingOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'daging', $model->getKonsumsiLaukPaukDagingOptions()); ?>
	
	<span class="section-block">C. Sandang</span>

	<?php echo $form->dropDownListRow($model, 'pakaian', $model->getKonsumsiSandangOptions()); ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>
	
<?php $this->endWidget(); ?>

<script type="text/javascript">
	ToggleRumahBantuan=function(){
		var rumahStat = $("#Rumah_rumah_bantuan").val();
		if(rumahStat == 1){
			$("#rumah_bantuan").show();
		}else{
			$("#rumah_bantuan").hide();
		}
		
	};
	
	$(function(){
		$("#Rumah_rumah_bantuan").change(function(){
			ToggleRumahBantuan();
		});
		
		ToggleRumahBantuan();
		
		if($("#Rumah_nik").size()> 0){
			$("#Rumah_nik").keyup(function(){
				parameter = "term=" + $('#Rumah_nik').val();
			});
			TextFieldAutoComplete("/general/getKKByNIK", "Rumah_nik", "Rumah_no_kk");
		}
		
		if($("#Rumah_no_kk").size()> 0){
			$("#Rumah_no_kk").keyup(function(){
				parameter = "term=" + $('#Rumah_no_kk').val();
			});
			TextFieldAutoComplete("/general/getNIKByKK", "Rumah_no_kk", "Rumah_nik");
		}
	});
</script>
