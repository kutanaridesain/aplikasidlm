<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Rumah'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Rumah','url'=>array('index')),
	array('label'=>'Tambah Rumah','url'=>array('create')),
	array('label'=>'Ubah Rumah','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Rumah','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Rumah <?php echo $model->nik; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nik',
		'no_kk',
		'nama_status_rumah',
		'panjang_lantai',
		'lebar_lantai',
		'nama_jenis_lantai',
		'nama_jenis_dinding',
		'nama_fasilitas_wc',
		'nama_penerangan',
		'nama_air_minum',
		'nama_bahan_bakar_masak',
		'nama_rumah_bantuan',
		'pemberi_rumah_bantuan',
		'tahun_rumah_bantuan',
		'koordinat_x',
		'koordinat_y',
		'nama_jlh_makan',
		'nama_lauk_pauk',
		'nama_daging',
		'nama_pakaian',
	),
)); ?>

<div id="rumah-picture">
	<br/>
	<img src="/<?php echo Rumah::PATH . $model->photo;?>" alt="" />
	<br/>
	<br/>
</div>
