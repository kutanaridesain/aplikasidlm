<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelas Bantuan Lainnya'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Kelas Bantuan Lainnya','url'=>array('index')),
);
?>

<h1>Tambah Kelas Bantuan Lainnya</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>