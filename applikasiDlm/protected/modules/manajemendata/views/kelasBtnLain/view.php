<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelas Bantuan Lainnya'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar KelasBantuanLainnya','url'=>array('index')),
	array('label'=>'Tambah KelasBantuanLainnya','url'=>array('create')),
	array('label'=>'Ubah KelasBantuanLainnya','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus KelasBantuanLainnya','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil KelasBantuanLainnya</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_bantuan_lainnya',
		'id_kelas_bantuan',
		'userid',
		'tglinput',
		'tglupdate',
	),
)); ?>
