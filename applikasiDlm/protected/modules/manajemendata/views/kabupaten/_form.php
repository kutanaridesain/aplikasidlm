<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'kabupaten-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama_kabkot',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'ibukota',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textAreaRow($model,'alamat',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->textFieldRow($model,'bupati',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'wakil',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'luas_wilayah',array('class'=>'span5')); ?>

	<?php echo $form->fileFieldRow($model,'lambang'); ?>
	<?php echo ".jpg, .png, .gif"; ?>
	<?php 
		if(!$model->isNewRecord){ ?>
			<div id="kabupaten-picture">
				<br/>
				<img src="/<?php echo Kabupaten::PATH . $model->lambang;?>" alt="" />
				<br/>
				<br/>
			</div>
	<?php } ?>

	<?php echo $form->textAreaRow($model,'visi',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textAreaRow($model,'misi',array('class'=>'span5','maxlength'=>4000)); ?>

	<?php echo $form->textFieldRow($model,'koordinat_x',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'koordinat_y',array('class'=>'span5','maxlength'=>50)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
