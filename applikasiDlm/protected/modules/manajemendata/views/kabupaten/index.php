<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kabupatens'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Kabupaten','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kabupaten-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Kabupaten</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'kabupaten-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nama_kabkot',
		'ibukota',
		'alamat',
		'bupati',
		'wakil',
		/*
		'luas_wilayah',
		'lambang',
		'visi',
		'misi',
		'koordinat_x',
		'koordinat_y',
		'userid',
		'tglinput',
		'tglupdate',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
