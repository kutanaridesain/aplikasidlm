<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kabupaten'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Kabupaten','url'=>array('index')),
	array('label'=>'Tambah Kabupaten','url'=>array('create')),
	array('label'=>'Ubah Kabupaten','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Kabupaten','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Kabupaten</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_kabkot',
		'ibukota',
		'alamat',
		'bupati',
		'wakil',
		'luas_wilayah',
		'lambang',
		'visi',
		'misi',
		'koordinat_x',
		'koordinat_y',
	),
)); ?>


<div id="kabupaten-picture">
	<br/>
	<img src="/<?php echo Kabupaten::PATH . $model->lambang;?>" alt="" />
	<br/>
	<br/>
</div>
