<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kabupaten'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Kabupaten','url'=>array('index')),
	array('label'=>'Tambah Kabupaten','url'=>array('create')),
	array('label'=>'Detil Kabupaten','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Kabupaten </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>