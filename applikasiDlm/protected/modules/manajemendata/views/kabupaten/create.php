<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kabupatens'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Kabupaten','url'=>array('index')),
);
?>

<h1>Tambah Kabupaten</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>