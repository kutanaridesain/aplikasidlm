<h1>Daftar Album</h1>
<div>
    <?php if (!empty($errors)) : ?>
    <div class="alert alert-error">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <?php echo $errors; ?>
    </div>
    <?php endif; ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Nama Album</th>
          <th>
          Proses
          <?php 
            echo CHtml::link('Tambah [+]', '#albumForm', array(
              'class'=>'pull-right btn btn-primary',
              'role'=>'button',
              'data-toggle'=>'modal'
            ));
          ?>
          </th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($albums as $album) :?>
        <tr>
          <td>
            <?php echo CHtml::link(ucfirst($album), array('uploading', 'id'=>$album),
              array(
                'rel'=>'tooltip',
                'class'=>'view',
                'data-original-title'=>'Tampilkan'
              )
            );
            ?>
          </td>
          <td class="button-column">
            <?php echo CHtml::link('<i class="icon-eye-open"></i>', array('uploading', 'id'=>$album),
              array(
                'rel'=>'tooltip',
                'class'=>'view',
                'data-original-title'=>'Tampilkan'
              )
            );
            echo CHtml::link('<i class="icon-trash"></i>', array('uploading', 'id'=>$album),
              array(
                'rel'=>'tooltip',
                'class'=>'delete',
                'data-original-title'=>'Hapus'
              )
            ); 
            ?>
          </td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>
</div>
<!-- Modal -->
<div id="albumForm" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Tambah Album</h3>
  </div>
  <form action="" method="post" accept-charset="utf-8">
    <div class="modal-body">
        <label for="albumName">Nama Album :</label>
        <input type="text" id="albumName" name="albumName" value="" placeholder="Nama Album">
        <br/>
    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary pull-right">Tambah</button>
    </div>
  </form>
</div>