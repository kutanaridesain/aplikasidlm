<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'User'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar User','url'=>array('index')),
	array('label'=>'Tambah User','url'=>array('create')),
	array('label'=>'Ubah User','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus User','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil User</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		// 'id',
		'username',
		'fullname',
		'email',		
		array(    
			'header' => 'Role',
            'name' => 'role_id',      
            'type' => 'raw',      
            'value' => User::getRoleById($model->role_id),
        ),
		'location_name',
	),
)); ?>
