<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'User'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar User','url'=>array('index')),
	array('label'=>'Tambah User','url'=>array('create')),
	array('label'=>'Detil User','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah User </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>