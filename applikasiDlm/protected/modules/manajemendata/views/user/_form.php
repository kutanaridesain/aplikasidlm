<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'username',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->passwordFieldRow($model,'pwd',array('class'=>'span5','maxlength'=>200)); ?>
	<?php echo $form->passwordFieldRow($model,'pwd_repeat',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'fullname',array('class'=>'span5')); ?>
	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5')); ?>
	<?php echo $form->dropDownListRow($model, 'role_id', $model->getRoleOptions()); ?>
	<?php echo $form->textFieldRow($model,'location_name',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	<?php echo $form->hiddenField($model,'location_id'); ?>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		
		if($("#User_location_name").size()> 0){
			$("#User_location_name").keyup(function(){
				parameter = "term=" + $('#User_location_name').val()+"&role="+ $("#User_role_id").val();
			});
			TextFieldAutoComplete("/general/getLocation", "User_location_name", "User_location_id");
		}
		
		var roleid = $("#User_role_id").val();
		if(roleid<=1){
			$("#User_location_name").val("Semua");
			$("#User_location_id").val("0");
			$("#User_location_name").attr("readonly","readonly");
		}
		
		$("#User_role_id").change(function(){
			var roleid = $("#User_role_id").val();
			if(roleid<=1){
				$("#User_location_name").val("Semua");
				$("#User_location_id").val("0");
				$("#User_location_name").attr("readonly","readonly");
			}else{
				$("#User_location_name").val("");
				$("#User_location_id").val("");
				$("#User_location_name").removeAttr("readonly");
			}
		});
	});
</script>