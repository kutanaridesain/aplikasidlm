<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'User'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar User','url'=>array('index')),
);
?>

<h1>Tambah User</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>