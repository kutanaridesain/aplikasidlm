<?php
/* @var $this KepalaKeluargaDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Kepala Keluarga Details',
);

$this->menu=array(
	array('label'=>'Tambah Kepala Keluarga Detail', 'url'=>array('create')),
);
?>

<h1>Kepala Keluarga Detail</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
