<?php
/* @var $this KepalaKeluargaDetailsController */
/* @var $model KepalaKeluargaDetails */

$this->breadcrumbs=array(
	'Kepala Keluarga Detail'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Kembali', 'url'=>array('/manajemendata/kepalakeluarga/update/id/'.$model->no_kk)),
	//array('label'=>'Daftar KepalaKeluarga', 'url'=>array('kepalakeluarga/index')),
);
?>

<h1>Tambah Kepala Keluarga Details</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>