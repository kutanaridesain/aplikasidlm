<?php
/* @var $this KepalaKeluargaDetailsController */
/* @var $model KepalaKeluargaDetails */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'kepala-keluarga-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>
	
	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5', 'readonly'=>'readonly')); ?>
	
	<?php 
	
		if($model->isNewRecord){
			echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>'));
		}else{
			echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'readonly'=>'readonly'));
		}
	?>
	
	<?php echo $form->textFieldRow($model,'nama_penduduk',array('class'=>'span5', 'disabled'=>'disabled')); ?>

	<?php echo $form->dropDownListRow($model, 'hub_keluarga', $model->getHubunganKeluargaOptions(false)); ?>

	

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
		
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'danger',
			'label'=>'Batal',
			'url'=>'/manajemendata/kepalakeluarga/update/id/'.$model->no_kk,
		)); ?>
		
		<?php 
		if(!$model->isNewRecord ){
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'link',
				'type'=>'primary',
				'label'=>'+ Buat Anggota Keluarga Baru',
				'url' => '/manajemendata/kepalakeluarga/create/id/' . $model->id ."/nokk/".$model->no_kk,
			));
		} 
		?>
	</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
	$(function(){	
	
		if($("#KepalaKeluargaDetails_nik").size()> 0){
			$("#KepalaKeluargaDetails_nik").keyup(function(){
				parameter = "term=" + $('#KepalaKeluargaDetails_nik').val();
			});
			TextFieldAutoComplete("/general/getNikAnggotaKeluarga", "KepalaKeluargaDetails_nik", "KepalaKeluargaDetails_nama_penduduk");
		}
	});
</script>
