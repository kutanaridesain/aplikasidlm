<?php
/* @var $this KepalaKeluargaDetailsController */
/* @var $model KepalaKeluargaDetails */

$this->breadcrumbs=array(
	'Kepala Keluarga Detail'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar KepalaKeluargaDetails', 'url'=>array('index')),
	array('label'=>'Tambah KepalaKeluargaDetails', 'url'=>array('create')),
	array('label'=>'Ubah KepalaKeluargaDetails', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Hapus KepalaKeluargaDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil KepalaKeluargaDetails</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'no_kk',
		'nik',
		'hub_keluarga',
		'userid',
		'tglinput',
		'tglupdate',
	),
)); ?>
