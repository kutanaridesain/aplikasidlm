<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'bantuan-lainnya-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'readonly'=>'readonly', 'autocomplete'=>'off')); ?>
	
	<?php echo $form->textFieldRow($model,'nama_penduduk',array('class'=>'span5', 'readonly'=>'readonly', 'autocomplete'=>'off')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5', 'readonly'=>'readonly', 'autocomplete'=>'off')); ?>
	
	
	<?php //echo $form->radioButtonListRow($model, 'kELASBANTUANLAINNYAs',$model->kELASBANTUANLAINNYAs); ?>
	<?php echo $form->checkboxRow($model, 'kb_pertanian'); ?>
	<?php echo $form->checkboxRow($model, 'kb_perikanan'); ?>
	<?php echo $form->checkboxRow($model, 'kb_kehutanan'); ?>
	<?php echo $form->checkboxRow($model, 'kb_perkebunan'); ?>
	<?php echo $form->checkboxRow($model, 'kb_peternakan'); ?>

	<?php echo $form->radioButtonListInlineRow($model, 'status_bantuan_lainnya', $model->getStatusYaTidakOptions()); ?>

	<?php echo $form->textAreaRow($model,'jenis_bantuan_lainnya',array('class'=>'span5','maxlength'=>400)); ?>

	<?php echo $form->dropDownListRow($model, 'id_instansi', $this->getInstansiOptions()); ?>
	
	<div class="input-append date yearly-datepicker" data-date-format="yyyy" id="tahun_diberikan">
		<?php echo $form->textFieldRow($model,'tahun_diberikan',array('class'=>'span1 datepicker-textfield')); ?>
		<span class="add-on"><i class="icon-th"></i></span>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'kategory_bantuan_lainnya', $model->getKategoriBantuanModalOptions()); ?>

	<div id="id_kelompok">
		<?php echo $form->dropDownListRow($model, 'id_kelompok', $this->getKelompokOptions()); ?>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'sumber_modal_bantuan_lainnya', $model->getSumberBantuanModalOptions()); ?>

	<div id="id_koperasi">
		<?php echo $form->dropDownListRow($model, 'id_koperasi', $this->getKoperasiOptions()); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
		
		<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'primary',
			'label'=>'Cancel',
			'url'=>'/manajemendata/biodata/update/id/'.$model->nik,
		)); ?>
		
		<?php 
		if(!$model->isNewRecord ){
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'link',
				'type'=>'primary',
				'label'=>'+ Buat Bantuan Lainnya Baru',
				'url' => '/manajemendata/bantuanlainnya/create/nik/' . $model->nik,
			));
		} 
		?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	ToggleKategoryBantuanLainnya=function(){
		$("#id_kelompok").toggle();
	};
	
	ToggleSumberBantuanLainnya=function(){
		$("#id_koperasi").toggle();
	};

	$(function(){
		if(($("#BantuanLainnya_kategory_bantuan_lainnya_0").is(':checked'))==true){
			$("#id_kelompok").hide();
		}
		
		$("#BantuanLainnya_kategory_bantuan_lainnya_0, #BantuanLainnya_kategory_bantuan_lainnya_1").click(function() {
			ToggleKategoryBantuanLainnya();
		});
		
		if(($("#BantuanLainnya_sumber_bantuan_lainnya_0").is(':checked'))==false){
			$("#id_koperasi").hide();
		}
		
		$("#BantuanLainnya_sumber_modal_bantuan_lainnya_0, #BantuanLainnya_sumber_modal_bantuan_lainnya_1").click(function() {
			ToggleSumberBantuanLainnya();
		});
		
	});
	
</script>
