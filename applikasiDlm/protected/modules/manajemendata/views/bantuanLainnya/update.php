<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Bantuan Lainnya'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	//array('label'=>'Daftar Bantuan Lainnya','url'=>array('index')),
	//array('label'=>'Tambah Bantuan Lainnya','url'=>array('create')),
	//array('label'=>'Detil Bantuan Lainnya','url'=>array('view','id'=>$model->id)),
	array('label'=>'Kembali','url'=>array('biodata/update', 'id'=>$model->nik)),
	//array('label'=>'Create BantuanLainnya','url'=>array('create')),
	//array('label'=>'View BantuanLainnya','url'=>array('view','id'=>$model->id)),
	//array('label'=>'Manage BantuanLainnya','url'=>array('admin')),
);
?>

<h1>Ubah Bantuan Lainnya </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

<br/>
	<span class="section-block">Daftar Bantuan Lainnya</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'bantuan-lainnya-details-grid',
		'dataProvider'=>$bantuanLainnyaDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_instansi',
			'jenis_bantuan_lainnya',
			'tahun_diberikan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update} {delete}'
			),
		),
	)); ?>
	
	
<script type="text/javascript">
	$(function(){	
		$(".update, .delete").each(function(){
			var strUrl = $(this).attr("href") + "/nik/"+<?php echo $model->nik; ?>+"";
			
			$(this).attr("href", strUrl);
		});
	});
</script>
