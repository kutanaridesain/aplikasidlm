<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Bantuan Lainnyas'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Bantuan Lainnya','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('bantuan-lainnya-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Bantuan Lainnya</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'bantuan-lainnya-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'nik',
		'no_kk',
		'status_bantuan_lainnya',
		'jenis_bantuan_lainnya',
		'id_instansi',
		/*
		'tahun_diberikan',
		'kategory_bantuan_lainnya',
		'id_kelompok',
		'sumber_modal_bantuan_lainnya',
		'id_koperasi',
		'userid',
		'tglinput',
		'tglupdate',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
