<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Bantuan Lainnya'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar BantuanLainnya','url'=>array('index')),
	array('label'=>'Tambah BantuanLainnya','url'=>array('create')),
	array('label'=>'Ubah BantuanLainnya','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus BantuanLainnya','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil BantuanLainnya</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nik',
		'no_kk',
		'status_bantuan_lainnya',
		'jenis_bantuan_lainnya',
		'id_instansi',
		'tahun_diberikan',
		'kategory_bantuan_lainnya',
		'id_kelompok',
		'sumber_modal_bantuan_lainnya',
		'id_koperasi',
		'userid',
		'tglinput',
		'tglupdate',
	),
)); ?>
