<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Bantuan Lainnya'=>array('index'),
	'Tambah',
);

$this->menu=array(
	//array('label'=>'Daftar BantuanLainnya','url'=>array('index')),
	array('label'=>'Kembali','url'=>array('biodata/update', 'id'=>$model->nik)),
	//array('label'=>'Manage BantuanLainnya','url'=>array('admin')),
);
?>

<h1>Tambah Bantuan Lainnya</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<br/>
	<span class="section-block">Daftar Bantuan Lainnya</span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'bantuan-lainnya-details-grid',
		'dataProvider'=>$bantuanLainnyaDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'no_kk',
			'nik',
			'nama_penduduk',
			'nama_instansi',
			'jenis_bantuan',
			'tahun_diberi',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update} {delete}'
			),
		),
	)); ?>
	
	
<script type="text/javascript">
	$(function(){	
		$(".update, .delete").each(function(){
			var strUrl = $(this).attr("href") + "/nik/"+<?php echo $model->nik; ?>+"";
			
			$(this).attr("href", strUrl);
		});
	});
</script>
