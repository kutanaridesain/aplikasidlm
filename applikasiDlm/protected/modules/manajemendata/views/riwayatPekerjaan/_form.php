<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'riwayat-pekerjaan-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'readonly'=>'readonly')); ?>
	
	<?php echo $form->textFieldRow($model,'nama_penduduk',array('class'=>'span5', 'readonly'=>'readonly')); ?>
	
	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'nama_pekerjaan',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	
	<?php echo $form->hiddenField($model,'id_pek'); ?>

	<?php echo $form->textFieldRow($model,'tempat_bekerja',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'rata_penghasilan',array('class'=>'span5')); ?>

	<?php echo $form->datepickerRow($model, 'tahun',
        array('append'=>'<i class="icon-calendar"></i>', 'class'=>'span1', 'options'=>array(
    		'format'=>'yyyy',
    		'viewMode'=>'years',
    		'minViewMode'=>'years',
    		'autoclose'=>'true',
    	)));
	?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
		
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'danger',
			'label'=>'Batal',
			'url'=>'/manajemendata/biodata/update/id/'.$model->nik,
		)); ?>
		
		<?php 
		if(!$model->isNewRecord ){
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'link',
				'type'=>'primary',
				'label'=>'+ Buat Riwayat Pekerjaan Baru',
				'url' => '/manajemendata/riwayatpekerjaan/create/nik/' . $model->nik,
			));
		} 
		?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		if($("#RiwayatPekerjaan_nama_pekerjaan").size()> 0){
			$("#RiwayatPekerjaan_nama_pekerjaan").keyup(function(){
				parameter = "term=" + $('#RiwayatPekerjaan_nama_pekerjaan').val();
			});
			TextFieldAutoComplete("/general/getPekerjaan", "RiwayatPekerjaan_nama_pekerjaan", "RiwayatPekerjaan_id_pek");
		}
	});
</script>
