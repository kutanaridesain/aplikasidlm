<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Riwayat Pekerjaan'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar RiwayatPekerjaan','url'=>array('index')),
	array('label'=>'Tambah RiwayatPekerjaan','url'=>array('create')),
	array('label'=>'Ubah RiwayatPekerjaan','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus RiwayatPekerjaan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil RiwayatPekerjaan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nik',
		'id_pek',
		'tempat_bekerja',
		'rata_penghasilan',
		'tahun',
		'userid',
		'tglinput',
		'tglupdate',
	),
)); ?>
