<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_pek',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tempat_bekerja',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'rata_penghasilan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tahun',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'userid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglinput',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglupdate',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
