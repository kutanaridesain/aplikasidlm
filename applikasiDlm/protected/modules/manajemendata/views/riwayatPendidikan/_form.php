<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'riwayat-pendidikan-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'readonly'=>'readonly')); ?>
	
	<?php echo $form->textFieldRow($model,'nama_penduduk',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->dropDownListRow($model, 'jenjang_sekolah', $model->getStrPendidikanTerakhirOptions()); ?>

	<?php echo $form->textFieldRow($model,'nama_sekolah',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>
	
	<?php echo $form->hiddenField($model,'kode_sekolah'); ?>

	<?php echo $form->textFieldRow($model,'kelas',array('class'=>'span1','maxlength'=>100)); ?>
	
	<?php echo $form->datepickerRow($model, 'tahun',
        array('append'=>'<i class="icon-calendar"></i>', 'class'=>'span1', 'options'=>array(
    		'format'=>'yyyy',
    		'viewMode'=>'years',
    		'minViewMode'=>'years',
    		'autoclose'=>'true',
    	)));
	?>

	<?php echo $form->radioButtonListInlineRow($model, 'dapat_beasiswa', $model->getStatusYaTidakOptions()); ?>

	<div id="dapat_beasiswa">
		<?php echo $form->textFieldRow($model,'jlh_beasiswa',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'pemberi_beasiswa',array('class'=>'span5','maxlength'=>200)); ?>
		
		<?php echo $form->datepickerRow($model, 'thn_beasiswa',
	        array('append'=>'<i class="icon-calendar"></i>', 'class'=>'span1', 'options'=>array(
	    		'format'=>'yyyy',
	    		'viewMode'=>'years',
	    		'minViewMode'=>'years',
	    		'autoclose'=>'true',
	    	)));
		?>
	</div>

	<?php echo $form->radioButtonListInlineRow($model, 'status_pendidikan', $model->getStatusPendidikanOptions()); ?>
	
	<div id="status_pendidikan">
		<?php echo $form->textFieldRow($model,'mengapa_tdk_sekolah',array('class'=>'span5','maxlength'=>400)); ?>
		
		<?php echo $form->datepickerRow($model, 'thn_putus_sekolah',
	        array('append'=>'<i class="icon-calendar"></i>', 'class'=>'span1', 'options'=>array(
	    		'format'=>'yyyy',
	    		'viewMode'=>'years',
	    		'minViewMode'=>'years',
	    		'autoclose'=>'true',
	    	)));
		?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
		
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'danger',
			'label'=>'Batal',
			'url'=>'/manajemendata/biodata/update/id/'.$model->nik,
		)); ?>
		
		<?php 
		if(!$model->isNewRecord ){
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'link',
				'type'=>'primary',
				'label'=>'+ Buat Riwayat Pendidikan Baru',
				'url' => '/manajemendata/riwayatpendidikan/create/nik/' . $model->nik,
			));
		} 
		?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	ToggleDapatBeasiswa=function(){
		if(($("#RiwayatPendidikan_dapat_beasiswa_0").is(':checked'))==false){
			$("#dapat_beasiswa").hide();
		}else{
			$("#dapat_beasiswa").show();
		}
	};
	
	ToggleStatusPendidikan=function(){
		if(($("#RiwayatPendidikan_status_pendidikan_0").is(':checked'))==false){
			$("#status_pendidikan").hide();
		}else{
			$("#status_pendidikan").show();
		}
	};

	$(function(){
		if($("#RiwayatPendidikan_nama_sekolah").size()> 0){
			$("#RiwayatPendidikan_nama_sekolah").keyup(function(){
				parameter = "term=" + $('#RiwayatPendidikan_nama_sekolah').val();
			});
			TextFieldAutoComplete("/general/getSekolah", "RiwayatPendidikan_nama_sekolah", "RiwayatPendidikan_kode_sekolah");
		}
		
		ToggleDapatBeasiswa();
		
		ToggleStatusPendidikan();
		
		$("#RiwayatPendidikan_status_pendidikan_0, #RiwayatPendidikan_status_pendidikan_1").click(function(){
			ToggleStatusPendidikan();
		});
		
		$("#RiwayatPendidikan_dapat_beasiswa_0, #RiwayatPendidikan_dapat_beasiswa_1").click(function(){
			ToggleDapatBeasiswa();
		});
	});
</script>
