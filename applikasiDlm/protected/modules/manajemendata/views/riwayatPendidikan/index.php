<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Riwayat Pendidikans'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Riwayat Pendidikan','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('riwayat-pendidikan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Riwayat Pendidikan</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'riwayat-pendidikan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nik',
		'no_kk',
		'jenjang_sekolah',
		'kode_sekolah',
		'kelas',
		/*
		'tahun',
		'dapat_beasiswa',
		'jlh_beasiswa',
		'pemberi_beasiswa',
		'status_pendidikan',
		'mengapa_tdk_sekolah',
		'thn_putus_sekolah',
		'userid',
		'tglinput',
		'tglupdate',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
