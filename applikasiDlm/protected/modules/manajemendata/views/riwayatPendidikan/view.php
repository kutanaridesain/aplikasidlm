<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Riwayat Pendidikan'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar RiwayatPendidikan','url'=>array('index')),
	array('label'=>'Tambah RiwayatPendidikan','url'=>array('create')),
	array('label'=>'Ubah RiwayatPendidikan','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus RiwayatPendidikan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil RiwayatPendidikan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nik',
		'no_kk',
		'jenjang_sekolah',
		'kode_sekolah',
		'kelas',
		'tahun',
		'dapat_beasiswa',
		'jlh_beasiswa',
		'pemberi_beasiswa',
		'status_pendidikan',
		'mengapa_tdk_sekolah',
		'thn_putus_sekolah',
		'userid',
		'tglinput',
		'tglupdate',
	),
)); ?>
