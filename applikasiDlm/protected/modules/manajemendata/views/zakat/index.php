<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Zakat'=>array('index'),
	'Daftar Zakat',
);

$this->menu=array(
	array('label'=>'Tambah Zakat','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('zakat-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Zakat</h1>

<?php //echo CHtml::link('Pencarian Lanjut <i class="icon-search"></i>','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'zakat-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(	
		'nik',	
		array(      
            'name' => 'id_instansi',      
            'type' => 'raw',      
            'value' => 'Instansi::getById($data->id_instansi)',      
            'filter' => Instansi::getOptionList(), 
        ),
		array(      
            'name' => 'id_jenis_zakat',      
            'type' => 'raw',      
            'value' => 'Zakat::getNamaZakatById($data->id_jenis_zakat)',      
            'filter' => Zakat::getJenisZakatOptions(),    
        ),
		'jlh_zakat',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
