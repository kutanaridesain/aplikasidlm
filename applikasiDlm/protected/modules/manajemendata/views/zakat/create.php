<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Zakat'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Zakat','url'=>array('index')),
);
?>

<h1>Tambah Zakat</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>