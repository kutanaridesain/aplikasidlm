<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_jenis_zakat',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_instansi',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jlh_zakat',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tgl_diberikan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'userid',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglinput',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglupdate',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Cari',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
