<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Zakat'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Zakat','url'=>array('index')),
	array('label'=>'Tambah Zakat','url'=>array('create')),
	array('label'=>'Ubah Zakat','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Zakat','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Anda yakin menghapus item ini?')),
);
?>

<h1>Detil Zakat</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_zakat',
		'nik',
		'nama_penduduk',
		'nama_instansi',
		'jlh_zakat',
		'tgl_diberikan',
	),
)); ?>
