<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'zakat-form',
	'enableAjaxValidation'=>true,
)); ?>
	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nik',array('class'=>'span5', 'autocomplete'=>'off', 'append'=>'<i class="icon-search"></i>')); ?>

	<?php echo $form->textFieldRow($model,'nama_penduduk',array('class'=>'span5', 'disabled'=>'disabled')); ?>
	
	<?php echo $form->dropDownListRow($model, 'id_jenis_zakat', $model->getJenisZakatOptions()); ?>
	
	<?php echo $form->textFieldRow($model,'jlh_zakat',array('class'=>'span2')); ?>

	<?php echo $form->datepickerRow($model, 'tgl_diberikan',
        array('append'=>'<i class="icon-calendar"></i>', 'format'=>'dd/mm/yyyy', 'class'=>'dateselector span2')); 
    ?>

	<?php echo $form->dropDownListRow($model, 'id_instansi', $this->getInstansiOptions()); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
	$(function(){	
	
		if($("#Zakat_nik").size()> 0){
			$("#Zakat_nik").keyup(function(){
				parameter = "term=" + $('#Zakat_nik').val();
			});
			TextFieldAutoComplete("/general/getNik", "Zakat_nik", "Zakat_nama_penduduk");
		}
	});
</script>