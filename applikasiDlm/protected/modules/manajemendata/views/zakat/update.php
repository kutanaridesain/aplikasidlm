<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Zakat'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Zakat','url'=>array('index')),
	array('label'=>'Tambah Zakat','url'=>array('create')),
	array('label'=>'Detil Zakat','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Zakat</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>