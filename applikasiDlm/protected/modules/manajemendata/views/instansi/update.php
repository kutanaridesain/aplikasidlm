<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Instansi'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Instansi','url'=>array('index')),
	array('label'=>'Tambah Instansi','url'=>array('create')),
	array('label'=>'Detil Instansi','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Instansi </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>