<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Instansi'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Instansi','url'=>array('index')),
);
?>

<h1>Tambah Instansi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>