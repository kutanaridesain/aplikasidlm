<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Instansi'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Instansi','url'=>array('index')),
	array('label'=>'Tambah Instansi','url'=>array('create')),
	array('label'=>'Ubah Instansi','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Instansi','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Instansi</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_instansi',
		'alamat_instansi',
		'nama_kab',
	),
)); ?>
