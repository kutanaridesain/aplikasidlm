<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelas Bantuan'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar KelasBantuan','url'=>array('index')),
	array('label'=>'Tambah KelasBantuan','url'=>array('create')),
	array('label'=>'Ubah KelasBantuan','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus KelasBantuan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil KelasBantuan</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_kelas_bantuan',
	),
)); ?>
