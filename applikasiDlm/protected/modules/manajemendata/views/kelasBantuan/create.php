<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelas Bantuans'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Kelas Bantuan','url'=>array('index')),
);
?>

<h1>Tambah Kelas Bantuan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>