<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Kelas Bantuan'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Kelas Bantuan','url'=>array('index')),
	array('label'=>'Tambah Kelas Bantuan','url'=>array('create')),
	array('label'=>'Detil Kelas Bantuan','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Kelas Bantuan </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>