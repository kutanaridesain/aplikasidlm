<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'puskesmas-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama_puskesmas',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->dropDownListRow($model, 'jenis_puskesmas', $model->getPuskesmasOptions()); ?>

	<?php echo $form->textAreaRow($model,'alamat_puskesmas',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->textFieldRow($model,'jlh_dokter_umum',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_dokter_gigi',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_tenaga_lab',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_tenaga_farmasi',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_perawat',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_ruang_inap',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'jlh_ruang_layanan',array('class'=>'span1')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
