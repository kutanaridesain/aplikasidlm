<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Puskesmas'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Puskesmas','url'=>array('index')),
);
?>

<h1>Tambah Puskesmas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>