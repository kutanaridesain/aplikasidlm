<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Puskesmase'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Puskesmas','url'=>array('index')),
	array('label'=>'Tambah Puskesmas','url'=>array('create')),
	array('label'=>'Ubah Puskesmas','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Puskesmas','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Puskesmas</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_puskesmas',
		'nama_jenis_puskesmas',
		'alamat_puskesmas',
		'jlh_dokter_umum',
		'jlh_dokter_gigi',
		'jlh_tenaga_lab',
		'jlh_tenaga_farmasi',
		'jlh_perawat',
		'jlh_ruang_inap',
		'jlh_ruang_layanan',
	),
)); ?>
