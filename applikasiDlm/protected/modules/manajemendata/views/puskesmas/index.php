<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Puskesmases'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Puskesmas','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('puskesmas-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Puskesmase</h1>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'puskesmas-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nama_puskesmas',
		array(      
            'name' => 'jenis_puskesmas',      
            'type' => 'raw',      
            'value' => 'Puskesmas::getNamaPuskesmasById($data->jenis_puskesmas)',      
            'filter' => Puskesmas::getPuskesmasOptions(),    
        ),
		'alamat_puskesmas',
		/*'jlh_dokter_umum',
		'jlh_dokter_gigi',		
		'jlh_tenaga_lab',
		'jlh_tenaga_farmasi',
		'jlh_perawat',
		'jlh_ruang_inap',
		'jlh_ruang_layanan',
		'userid',
		'tglinput',
		'tglupdate',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
