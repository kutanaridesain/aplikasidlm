<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Puskesmas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Puskesmas','url'=>array('index')),
	array('label'=>'Tambah Puskesmas','url'=>array('create')),
	array('label'=>'Detil Puskesmas','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Puskesmas </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>