<?php
$this->breadcrumbs=array(
	'Penyakit'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Penyakit','url'=>array('index')),
	array('label'=>'Tambah Penyakit','url'=>array('create')),
	array('label'=>'Ubah Penyakit','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Penyakit','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Penyakit</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_penyakit',
	),
)); ?>
