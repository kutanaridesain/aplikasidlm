<?php
$this->breadcrumbs=array(
	'Penyakit'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Penyakit','url'=>array('index')),
	array('label'=>'Tambah Penyakit','url'=>array('create')),
	array('label'=>'Detil Penyakit','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Penyakit </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>