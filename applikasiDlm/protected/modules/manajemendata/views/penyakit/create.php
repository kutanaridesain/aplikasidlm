<?php
$this->breadcrumbs=array(
	'Penyakit'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Penyakit','url'=>array('index')),
);
?>

<h1>Tambah Penyakit</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>