<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pmks-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nik_kk',array('class'=>'span5', 'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'no_kk',array('class'=>'span5', 'readonly'=>'readonly')); ?>
	
	<h4>Anggota Keluarga</h4>

	<?php echo $form->dropDownListRow($model, 'nik', $model->nokkDataProvider); ?>
	
	<?php echo $form->dropDownListRow($model, 'id_jenis_pmks', $this->getJenisPMKSOptions()); ?>

	<?php echo $form->dropDownListRow($model, 'jenis_cacat', $model->getJenisCacatOptions()); ?>
	
	<?php echo $form->dropDownListRow($model, 'dititip_di', $model->getDititipDiPantiAsuhanOptions()); ?>

	<div id="dititip_di">
		<?php echo $form->dropDownListRow($model, 'id_pantiasuhan', $this->getPantiAsuhanOptions()); ?>
	</div>

	<?php echo $form->textFieldRow($model,'bantuan_diterima',array('class'=>'span5','maxlength'=>200)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
		
		<?php 
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'danger',
			'label'=>'Batal',
			'url'=>'/manajemendata/kepalakeluarga/update/id/'.$model->no_kk,
		)); ?>
		
		<?php 
		if(!$model->isNewRecord ){
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'link',
				'type'=>'primary',
				'label'=>'+ Buat Daftar PMKS Baru',
				'url' => '/manajemendata/pmks/create/nokk/' . $model->no_kk.'/nik/'. $model->nik_kk ,
			));
		} ?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	ToggleDititipDi=function(){
		if($("#Pmks_dititip_di").val()==2){
			$("#dititip_di").hide();
		}else{
			$("#dititip_di").show();
		}
	}
		
	$(function(){
	
		ToggleDititipDi();
		
		$("#Pmks_dititip_di").change(function() {
			ToggleDititipDi();
		});
	});
</script>
