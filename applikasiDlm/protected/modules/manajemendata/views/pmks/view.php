<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Pmk'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Pmks','url'=>array('index')),
	array('label'=>'Tambah Pmks','url'=>array('create')),
	array('label'=>'Ubah Pmks','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Pmks','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detil Pmks</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_jenis_pmks',
		'nik',
		'no_kk',
		'jenis_cacat',
		'dititip_di',
		'id_pantiasuhan',
		'bantuan_diterima',
		'userid',
		'tglinput',
		'tglupdate',
	),
)); ?>
