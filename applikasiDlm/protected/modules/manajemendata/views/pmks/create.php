<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Pmks'=>array('index'),
	'Tambah',
);

$this->menu=array(
	//array('label'=>'Daftar Pmks','url'=>array('index')),
	array('label'=>'Kembali', 'url'=>array('/manajemendata/kepalakeluarga/update/id/'.$model->no_kk)),
);
?>

<h1>Tambah Pmks</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<br/>
	<span class="section-block">Daftar PMKS Dengan No. KK : <?php echo $model->no_kk ?></span>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pmks-details-grid',
		'dataProvider'=>$pmksDataProvider,
		'template' => "{items}",
		'enableSorting' => false,
		'columns'=>array(
			'nik',
			'nama_penduduk',
			'nama_jenis_pmks',
			'nama_jenis_cacat',
			'nama_dititip_di',
			'nama_pantiasuhan',
			'bantuan_diterima',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update} {delete}'
			),
		),
	)); ?>
	
	
<script type="text/javascript">
	$(function(){	
		$(".update, .delete").each(function(){
			var strUrl = $(this).attr("href") + "/nokk/"+<?php echo $model->no_kk; ?>+ "/nik/"+<?php echo $model->nik_kk; ?>+"";
			
			$(this).attr("href", strUrl);
		});
	});
</script>