<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Sekolah'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Ubah',
);

$this->menu=array(
	array('label'=>'Daftar Sekolah','url'=>array('index')),
	array('label'=>'Tambah Sekolah','url'=>array('create')),
	array('label'=>'Detil Sekolah','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Ubah Sekolah "<?php echo $model->nama_sekolah; ?>"</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>