<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Sekolah'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Daftar Sekolah','url'=>array('index')),
	array('label'=>'Tambah Sekolah','url'=>array('create')),
	array('label'=>'Ubah Sekolah','url'=>array('update','id'=>$model->id)),
	array('label'=>'Hapus Sekolah','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('dlm', 'confirm delete'))),
);
?>

<h1>Detail Sekolah "<i><?php echo $model->nama_sekolah; ?></i>"</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'nama_sekolah',
		'jenjang_sekolah',
		'alamat_sekolah',
		'jlh_ruang_kelas',
		'jlh_perpustakaan',
		'jlh_lab_ipa',
		'jlh_lab_ips',
		'jlh_mushalla',
		'jlh_kamar_mandi_wc',
		'koordinat_x',
		'koordinat_y',
	),
)); ?>
