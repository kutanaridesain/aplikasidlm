<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Sekolah'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Tambah Sekolah','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sekolah-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Daftar Sekolah</h1>

<?php //echo CHtml::link('Pencarian lanjut','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php
/*$this->widget('bootstrap.widgets.TbJsonGridView', array(
	'dataProvider' => $model->search(),
	'filter' => $model,
	'type' => 'striped bordered condensed',
	'summaryText' => false,
	'cacheTTL' => 10, // cache will be stored 10 seconds (see cacheTTLType)
	'cacheTTLType' => 's', // type can be of seconds, minutes or hours
	'columns' => array(
		'id',
		'nama_sekolah',
		array(
			'name' => 'tglinput',
			'type' => 'datetime'
		),
		array(
			'header' => Yii::t('ses', 'Edit'),
			'class' => 'bootstrap.widgets.TbJsonButtonColumn',
			'template' => '{view} {delete}',
		),
	),
	));*/
	?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'sekolah-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nama_sekolah',
		'jenjang_sekolah',
		'alamat_sekolah',
		'koordinat_x',
		'koordinat_y',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
