<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'sekolah-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('dlm', 'required field'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama_sekolah',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'jenjang_sekolah',array('class'=>'span5','maxlength'=>200)); ?>
	<?php //echo $form->dropDownListRow($model, 'jenjang_sekolah', $model->getStrPendidikanTerakhirOptions()); ?>

	<?php echo $form->textFieldRow($model,'alamat_sekolah',array('class'=>'span5','maxlength'=>1000)); ?>

	<?php echo $form->maskedTextFieldRow($model,'jlh_ruang_kelas','9',array('class'=>'span1')); ?>
    <span class="section-block">Fasilitas</span>
	<?php echo $form->maskedTextFieldRow($model,'jlh_perpustakaan','9',array('class'=>'span1')); ?>

	<?php echo $form->maskedTextFieldRow($model,'jlh_lab_ipa','9',array('class'=>'span1')); ?>

	<?php echo $form->maskedTextFieldRow($model,'jlh_lab_ips','9',array('class'=>'span1')); ?>

	<?php echo $form->maskedTextFieldRow($model,'jlh_mushalla','9',array('class'=>'span1')); ?>

	<?php echo $form->maskedTextFieldRow($model,'jlh_kamar_mandi_wc','9',array('class'=>'span1')); ?>
    <span class="section-block">Koordinat</span>
	<?php echo $form->maskedTextFieldRow($model,'koordinat_x','99°99\'99',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->maskedTextFieldRow($model,'koordinat_y','99°99\'99',array('class'=>'span5','maxlength'=>100)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
