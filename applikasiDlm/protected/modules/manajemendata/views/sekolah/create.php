<?php
$this->breadcrumbs=array(
	ucfirst($this->module->id)=>'/'.$this->module->id,
	'Sekolah'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'Daftar Sekolah','url'=>array('index')),
);
?>

<h1>Tambah Sekolah</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>