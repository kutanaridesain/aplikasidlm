<?php

class AccountController extends Controller
{
	public $layout ="//layouts/column2";
	public function actionChangepassword()
	{
		$message = array();
		if (Yii::app()->request->isPostRequest) {
			$newPass = Yii::app()->request->getParam('new-password');
			$reNewPass = Yii::app()->request->getParam('renew-password');
			if(empty($newPass) && empty($reNewPass)){
				$message['type'] = 'error';
				$message['error'] = 'Kedua field Password tidak boleh kosong';
			}else{
				if ($newPass != $reNewPass) {
					$message['type'] = 'error';
					$message['error'] = 'Password tidak sama, pastikan password yang anda masukkan sama!';
				}else{
					$id = Yii::app()->user->id;
					$model = User::model()->findByPk($id);
					$model->password = md5($reNewPass);
					$model->save();
					$message['type'] = 'success';
					$message['success'] = 'Password sukses diganti';
				}
			}
		}
		$this->render('changepassword', array('message'=>$message));
	}

	public function actionForgetpass()
	{
		$this->render('forgetpass');
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}