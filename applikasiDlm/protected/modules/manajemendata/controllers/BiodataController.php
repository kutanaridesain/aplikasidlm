<?php

class BiodataController extends DlmController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$bantuanModalDataProvider =new CActiveDataProvider('BantuanModal', array(
			'criteria'=>array(
			'condition'=>'"nik"=:nik',
			'params'=>array(':nik'=>$id),
			),
		));
		
		$bantuanSosialDataProvider =new CActiveDataProvider('BantuanSosial', array(
			'criteria'=>array(
			'condition'=>'"nik"=:nik',
			'params'=>array(':nik'=>$id),
			),
		));
		
		$bantuanLainnyaDataProvider =new CActiveDataProvider('BantuanLainnya', array(
			'criteria'=>array(
			'condition'=>'"nik"=:nik',
			'params'=>array(':nik'=>$id),
			),
		));

		$riwayatKesehatanDataProvider=new CActiveDataProvider('RiwayatKesehatan', array(
			'criteria'=>array(
			'condition'=>'"nik"=:nik',
			'params'=>array(':nik'=>$id),
			),
		));
		
		$riwayatPekerjaanDataProvider=new CActiveDataProvider('RiwayatPekerjaan', array(
			'criteria'=>array(
			'condition'=>'"nik"=:nik',
			'params'=>array(':nik'=>$id),
			),
		));
		
		$riwayatPendidikanDataProvider=new CActiveDataProvider('RiwayatPendidikan', array(
			'criteria'=>array(
			'condition'=>'"nik"=:nik',
			'params'=>array(':nik'=>$id),
			),
		));
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'bantuanModalDataProvider'=>$bantuanModalDataProvider,
			'bantuanSosialDataProvider'=>$bantuanSosialDataProvider,
			'bantuanLainnyaDataProvider'=>$bantuanLainnyaDataProvider,
			'riwayatKesehatanDataProvider'=>$riwayatKesehatanDataProvider,
			'riwayatPekerjaanDataProvider'=>$riwayatPekerjaanDataProvider,
			'riwayatPendidikanDataProvider'=>$riwayatPendidikanDataProvider,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Biodata;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Biodata']))
		{
			$model->attributes=$_POST['Biodata'];
			$model->tgl_lhr = Utils::toOracleDateFormat($model->tgl_lhr);
			if(!Utils::IsNullOrEmpty($model->tgl_kwn)){
				$model->tgl_kwn = Utils::toOracleDateFormat($model->tgl_kwn);		
			}	
			
			if(!Utils::IsNullOrEmpty($model->tgl_crai)){
				$model->tgl_crai = Utils::toOracleDateFormat($model->tgl_crai);	
			}	
				
			if(!Utils::IsNullOrEmpty($model->tgl_meninggal)){
				$model->tgl_meninggal = Utils::toOracleDateFormat($model->tgl_meninggal);	
			}
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->nik));
		}
		
		if(Utils::IsNullOrEmpty($model->akta_lhr)){
			$model->akta_lhr = 2;
		}
		
		if(Utils::IsNullOrEmpty($model->stat_kwn)){
			$model->stat_kwn = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->akta_kwn)){
			$model->akta_kwn = 2;
		}
		
		if(Utils::IsNullOrEmpty($model->akta_crai)){
			$model->akta_crai = 2;
		}
		
		if(Utils::IsNullOrEmpty($model->pnydng_cct)){
			$model->pnydng_cct = 2;
		}
		
		if(Utils::IsNullOrEmpty($model->stat_hidup)){
			$model->stat_hidup = 2;
		}
		
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Biodata']))
		{
			$model->attributes=$_POST['Biodata'];
			$model->tgl_lhr = Utils::toOracleDateFormat($model->tgl_lhr);
			if(!Utils::IsNullOrEmpty($model->tgl_kwn)){
				$model->tgl_kwn = Utils::toOracleDateFormat($model->tgl_kwn);		
			}	
			
			if(!Utils::IsNullOrEmpty($model->tgl_crai)){
				$model->tgl_crai = Utils::toOracleDateFormat($model->tgl_crai);	
			}	
				
			if(!Utils::IsNullOrEmpty($model->tgl_meninggal)){
				$model->tgl_meninggal = Utils::toOracleDateFormat($model->tgl_meninggal);	
			}		
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->nik));
		}
		
		$model->tgl_lhr = Utils::fromOracleDateFormat($model->tgl_lhr);	
		$model->tgl_kwn = Utils::fromOracleDateFormat($model->tgl_kwn);	
		$model->tgl_crai = Utils::fromOracleDateFormat($model->tgl_crai);	
		
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Biodata('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Biodata'])){
			$model->attributes=$_GET['Biodata'];

			// $nmJenKel = $_GET['Biodata']['nama_jenis_kelamin'];
			// $model->attributes->jenis_kelamin = 1;//array_search($nmJenKel, $model->getJenisKelaminOptions());

		}

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Biodata::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='biodata-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
