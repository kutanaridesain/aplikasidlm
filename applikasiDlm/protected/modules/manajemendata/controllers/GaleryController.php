<?php
Yii::import("xupload.models.XUploadForm");
class GaleryController extends Controller
{
	public $albums;
	public function actions() {
		FB::info(Yii::app()->request->getParam('id'));
		return array(
			'upload' => array(
				'class' => 'xupload.actions.XUploadAction',
				'path' => Yii::app() -> getBasePath() . '/../pictures/galery/'.Yii::app()->request->getParam('id'),
				'publicPath' => Yii::app()->getBaseUrl().'/pictures/galery/'.Yii::app()->request->getParam('id'),
				'subfolderVar'=>Yii::app()->request->getParam('id')
			), 
		);
	}

	public function actionUploads($parent_id)
	{
		// [{"name":"IMG_7120.jpg","type":"image\/jpeg","size":145459,"url":"\/pictures\/galery\/08172013\/IMG_7120.jpg","thumbnail_url":"\/pictures\/galery\/08172013\/IMG_7120.jpg","delete_url":"\/manajemendata\/galery\/upload\/_method\/delete\/file\/IMG_7120.jpg","delete_type":"POST"}]
		FB::info($parent_id);
		echo '[{"name":"IMG_7120.jpg","type":"image\/jpeg","size":145459,"url":"\/pictures\/galery\/08172013\/IMG_7120.jpg","thumbnail_url":"\/pictures\/galery\/08172013\/IMG_7120.jpg","delete_url":"\/manajemendata\/galery\/upload\/_method\/delete\/file\/IMG_7120.jpg","delete_type":"POST"}]';
	}

	public function actionIndex()
	{
		$this->layout='//layouts/column2';
		$availableAlbums = glob("pictures/galery/*", GLOB_ONLYDIR);
		$errors = null;

		$albums = array();
		$albumCompare = array();

		foreach ($availableAlbums as $filename) {
			$albumArr = explode('/', $filename);
			$albumLabel = $albumArr[count($albumArr)-1];	
			$albums[] = $albumLabel;
			$albumCompare[] = strtolower($albumLabel);
		}

		if(Yii::app()->request->isPostRequest){
			$albumName = Yii::app()->request->getParam('albumName');
			if(!empty($albumName)){
				if(in_array($albumName, $albumCompare)){
					$errors = 'Nama album sudah ada, coba nama lain!';
				}else{
					if(!mkdir('pictures/galery/'.$albumName)){
						$errors = 'Terjadi kesalahan sistem';
					}else{
						$albums = array();
						$availableAlbums = glob("pictures/galery/*", GLOB_ONLYDIR);
						foreach ($availableAlbums as $filename) {
							$albumArr = explode('/', $filename);
							$albumLabel = $albumArr[count($albumArr)-1];	
							$albums[] = $albumLabel;
						}
					}
				}
			}else{
				$errors = 'Nama album tidak bisa kosong!';
			}
		}

		$this->render('index', array('albums'=>$albums, 'errors'=>$errors));

	}

	public function actionUploading()
	{
		$model = new XUploadForm;
		$this -> render('uploading', array('model' => $model, ));
	}

	public function actionListgalleries()
	{
		$this->_listGalleries();
		
	}

	private function _listGalleries()
	{
		return glob("pictures/galery/*", GLOB_ONLYDIR);
	}

	private function _listGalleryItems($galeryId)
	{
		if(!empty($galeryId)){
			return glob("pictures/".$galeryId."/*.jp|*.png");	
		}else{
			throw new Exception("Invalid galeryId", 1);
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/

}