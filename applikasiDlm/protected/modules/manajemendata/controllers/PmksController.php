<?php

class PmksController extends DlmController
{
	private $_kepalaKeluarga;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'KepalaKeluargaContext + create update',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pmks;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pmks']))
		{
			$model->attributes=$_POST['Pmks'];
			if($model->save())
				$this->redirect(array('update',"nokk"=>$model->no_kk,"nik"=>$model->nik_kk,'id'=>$model->id ));
				//$this->redirect(array('view','id'=>$model->id));
		}
		
		$model->nik_kk = $this->_kepalaKeluarga["nik"];
		$model->no_kk = $this->_kepalaKeluarga["no_kk"];
		
		$sql = 'SELECT "nik" as "nik", "nama_lgkp" as "name"
				FROM view_biodata 
				WHERE  upper("no_kk") =  upper(\''. $model->no_kk. '\')
				ORDER BY "no_kk" ';
	
		$kk= Yii::app()->db->createCommand($sql)->queryAll();
	
		$model->nokkDataProvider = CHtml::listData($kk, 'nik', 'name'); 
		
		$pmksDataProvider=new CActiveDataProvider('Pmks', array(
			'criteria'=>array(
			'condition'=>'"no_kk"=:nokk',
			'params'=>array(':nokk'=>$model->no_kk),
			),
		));
		
		$this->render('create',array(
			'model'=>$model,
			'pmksDataProvider'=>$pmksDataProvider,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id, $ajax="")
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pmks']))
		{
			$model->attributes=$_POST['Pmks'];
			if($model->save())
				$this->redirect(array('update',"nokk"=>$model->no_kk,"nik"=>$model->nik_kk,'id'=>$model->id ));
				//$this->redirect(array('view','id'=>$model->id));
		}
		
		$model->nik_kk = $this->_kepalaKeluarga["nik"];
		$model->no_kk = $this->_kepalaKeluarga["no_kk"];
		
		$sql = 'SELECT "nik" as "nik", "nama_lgkp" as "name"
				FROM view_biodata 
				WHERE  upper("no_kk") =  upper(\''. $model->no_kk. '\')
				ORDER BY "no_kk" ';
	
		$kk= Yii::app()->db->createCommand($sql)->queryAll();
	
		$model->nokkDataProvider = CHtml::listData($kk, 'nik', 'name'); 
		
		$pmksDataProvider=new CActiveDataProvider('Pmks', array(
			'criteria'=>array(
			'condition'=>'"no_kk"=:nokk',
			'params'=>array(':nokk'=>$model->no_kk),
			),
		));

		$this->render('update',array(
			'model'=>$model,
			'pmksDataProvider'=>$pmksDataProvider,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Pmks('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pmks']))
			$model->attributes=$_GET['Pmks'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Pmks::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pmks-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			if($this->dititip_di === 2){
				$this->id_pantiasuhan = NULL;
			}
			return true;
	   }
		
		return false;
	}
	
	/* load kepala keluarga */
	private function loadKepalaKeluarga($nokk, $nik) {
		if($this->_kepalaKeluarga===null)
		{
			
			
			$sql = 'SELECT * 
				FROM kepala_keluarga 
				WHERE  "nik" = \''. $nik. '\' 
				AND  "no_kk" = \''. $nokk . '\' 
				ORDER BY "nik" '; 
		
			$kk = Yii::app()->db->createCommand($sql)->queryAll();
			
			if(count($kk)> 0){
				$this->_kepalaKeluarga = $kk[0];
			}
			
			
			if($this->_kepalaKeluarga===null || count($this->_kepalaKeluarga) == 0 )
			{
				throw new CHttpException(404,'No. KK tidak valid.'); 
			}
		}

		return $this->_kepalaKeluarga; 
	} 
	
	/* filter Kepala Keluarga */
	public function filterKepalaKeluargaContext($filterChain)
	{ 
		$nokk = null;
		if(isset($_GET['nokk'])) {
			$nokk = $_GET['nokk'];
		}else{
			if(isset($_POST['nokk'])) 
				$nokk = $_POST['nokk'];
		}
		
		$nik = null;
		if(isset($_GET['nik'])) {
			$nik = $_GET['nik'];
		}else{
			if(isset($_POST['nik'])) 
				$nik = $_POST['nik'];
		}
		
		$this->loadKepalaKeluarga($nokk, $nik); 
		
		$filterChain->run(); 
	}
}
