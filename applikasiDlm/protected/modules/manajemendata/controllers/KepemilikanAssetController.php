<?php

class KepemilikanAssetController extends DlmController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new KepemilikanAsset;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KepemilikanAsset']))
		{
			$model->attributes=$_POST['KepemilikanAsset'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		
		if(Utils::IsNullOrEmpty($model->punya_tanah_rumah)){
			$model->punya_tanah_rumah = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->punya_tanah_sawah)){
			$model->punya_tanah_sawah = 1;
		}		
		
		if(Utils::IsNullOrEmpty($model->punya_tanah_kebun)){
			$model->punya_tanah_kebun = 1;
		}

		if(Utils::IsNullOrEmpty($model->punya_tambak)){
			$model->punya_tambak = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->punya_lembu)){
			$model->punya_lembu = 1;
		}		
		
		if(Utils::IsNullOrEmpty($model->punya_kerbau)){
			$model->punya_kerbau = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->punya_ternak_lain)){
			$model->punya_ternak_lain = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->punya_mobil)){
			$model->punya_mobil = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->punya_motor)){
			$model->punya_motor = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->punya_tabungan)){
			$model->punya_tabungan = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->punya_asuransi)){
			$model->punya_asuransi = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->punya_motor_tempel)){
			$model->punya_motor_tempel = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->punya_kapal_motor)){
			$model->punya_kapal_motor = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->punya_perahu)){
			$model->punya_perahu = 1;
		}
		
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KepemilikanAsset']))
		{
			$model->attributes=$_POST['KepemilikanAsset'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new KepemilikanAsset('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KepemilikanAsset']))
			$model->attributes=$_GET['KepemilikanAsset'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=KepemilikanAsset::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kepemilikan-asset-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
