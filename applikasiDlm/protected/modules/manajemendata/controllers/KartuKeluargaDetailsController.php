<?php

class KartuKeluargaDetailsController extends DlmController
{
	private $_kepalaKeluarga;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
			'KartuKeluargaContext + create update',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($nokk)
	{
		$model=new KepalaKeluargaDetails;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KepalaKeluargaDetails']))
		{
			$model->attributes=$_POST['KepalaKeluargaDetails'];
		
			if($model->save())
				$this->redirect(array('kepalakeluarga/update','id'=>$model->no_kk));
		}else{
			$model->no_kk = $nokk;
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KepalaKeluargaDetails']))
		{
			$model->attributes=$_POST['KepalaKeluargaDetails'];
			if($model->save())
				$this->redirect(array('kepalakeluarga/update','id'=>$model->no_kk));
				//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KepalaKeluargaDetails');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new KepalaKeluargaDetails('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KepalaKeluargaDetails']))
			$model->attributes=$_GET['KepalaKeluargaDetails'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return KepalaKeluargaDetails the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=KepalaKeluargaDetails::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param KepalaKeluargaDetails $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kepala-keluarga-details-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/* load kepala keluarga */
	private function loadKepalaKeluarga($nokk) {
		if($this->_kepalaKeluarga===null)
		{
			$this->_kepalaKeluarga=KepalaKeluarga::model()->findbyPk($nokk);
			if($this->_kepalaKeluarga===null)
			{
				throw new CHttpException(404,'No. KK tidak valid.'); 
			}
		}
		return $this->_kepalaKeluarga; 
	} 
	
	/* filter Kepala Keluarga */
	public function filterKartuKeluargaContext($filterChain)
	{ 
		$nokk = null;
		if(isset($_GET['nokk'])) {
			$nokk = $_GET['nokk'];
		}else{
			if(isset($_POST['nokk'])) 
				$nokk = $_POST['nokk'];
		}
		
		$this->loadKepalaKeluarga($nokk); 
		
		$filterChain->run(); 
	}
}
