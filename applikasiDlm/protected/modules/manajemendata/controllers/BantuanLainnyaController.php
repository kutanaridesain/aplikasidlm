<?php

class BantuanLainnyaController extends DlmController
{
	private $_biodata;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'BiodataContext + create update',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BantuanLainnya;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BantuanLainnya']))
		{
			$model->attributes=$_POST['BantuanLainnya'];
			if($model->save())
				$this->redirect(array('update','id'=>$model->id, "nik"=>$model->nik));
		}
		
		$model->nik = $this->_biodata->nik;
		$model->nama_penduduk = $this->_biodata->nama_lgkp;
		$model->no_kk = $this->_biodata->no_kk;
		
		$bantuanLainnyaDataProvider =new CActiveDataProvider('BantuanLainnya', array(
			'criteria'=>array(
			'condition'=>'"nik"=:nik',
			'params'=>array(':nik'=>$this->_biodata->nik),
			),
		));
		
		if(Utils::IsNullOrEmpty($model->status_bantuan_lainnya)){
			$model->status_bantuan_lainnya = 2;
		}
		
		if(Utils::IsNullOrEmpty($model->kategory_bantuan_lainnya)){
			$model->kategory_bantuan_lainnya = 1;
		}
		
		if(Utils::IsNullOrEmpty($model->sumber_modal_bantuan_lainnya)){
			$model->sumber_modal_bantuan_lainnya = 2;
		}

		$this->render('create',array(
			'model'=>$model,
			'bantuanLainnyaDataProvider'=>$bantuanLainnyaDataProvider,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BantuanLainnya']))
		{
			$model->attributes=$_POST['BantuanLainnya'];
			if($model->save())
				$this->redirect(array('update','id'=>$model->id, "nik"=>$model->nik));
		}
		
			
		$bantuanLainnyaDataProvider =new CActiveDataProvider('BantuanLainnya', array(
			'criteria'=>array(
			'condition'=>'"nik"=:nik',
			'params'=>array(':nik'=>$this->_biodata->nik),
			),
		));

		$this->render('update',array(
			'model'=>$model,
			'bantuanLainnyaDataProvider'=>$bantuanLainnyaDataProvider,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new BantuanLainnya('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BantuanLainnya']))
			$model->attributes=$_GET['BantuanLainnya'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=BantuanLainnya::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bantuan-lainnya-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/* load biodata */
	private function loadBiodata($nik) {
		if($this->_biodata===null)
		{
			$this->_biodata=Biodata::model()->findbyPk($nik);
			if($this->_biodata===null)
			{
				throw new CHttpException(404,'NIK tidak valid.'); 
			}
		}
		return $this->_biodata; 
	} 
	
	/* filter Biodata */
	public function filterBiodataContext($filterChain)
	{ 
		$nik = null;
		if(isset($_GET['nik'])) {
			$nik = $_GET['nik'];
		}else{
			if(isset($_POST['nik'])) 
				$nik = $_POST['nik'];
		}
		
		$this->loadBiodata($nik); 
		
		$filterChain->run(); 
	}
}
