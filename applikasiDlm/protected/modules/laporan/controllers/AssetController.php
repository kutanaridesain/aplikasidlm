<?php

class AssetController extends DlmController
{
	private $_id;
	private $_no_kab;
	private $_no_kec;
	private $_no_kel;
	private $_dataProvider;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'ajaxOnly + actionSearch',
			'AssetContext + index',
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('deny', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{	
		$criteria=new CDbCriteria;
		$params = array();
		
			
		if($this->_id > 0)
		{
			if($this->_id == 1){
				$criteria->addCondition('"punya_tanah_rumah"=1');
			}
			
			if($this->_id == 2){
				$criteria->addCondition('"punya_tanah_sawah"=1');
			}
			
			if($this->_id == 3){
				$criteria->addCondition('"punya_tanah_kebun"=1');
			}			
			
			if($this->_id == 4){
				$criteria->addCondition('"punya_tambak"=1');
			}
			
			if($this->_id == 5){
				$criteria->addCondition('"punya_lembu"=1');
			}
			
			if($this->_id == 6){
				$criteria->addCondition('"punya_kerbau"=1');
			}
			
			if($this->_id == 7){
				$criteria->addCondition('"punya_ternak_lain"=1');
			}			
			
			if($this->_id == 8){
				$criteria->addCondition('"punya_mobil"=1');
			}
			
			if($this->_id == 9){
				$criteria->addCondition('"punya_motor"=1');
			}
			
			if($this->_id == 10){
				$criteria->addColumnCondition(
					array('"punya_tabungan"' => '1', '"punya_asuransi"' => '1'), 'OR'
				);
			}
			
			if($this->_id == 11){
				$criteria->addColumnCondition(
					array('"punya_motor_tempel"' => '1', '"punya_kapal_motor"' => '1'), 'OR'
				);
			}
			
			if($this->_id == 12){
				$criteria->addCondition('"Perahu Tanpa Motor"=1');
			}
		}
		
		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
			
		if(count($params))
			$criteria->params=$params;
			
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanAsset',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial('_index', array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('index',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	public function filterAssetContext($filterChain)
	{ 
		$this->_id = null;
		$this->_no_kab = 0;
		$this->_no_kec = 0;
		$this->_no_kel = 0;
		
		if(isset($_GET['id'])) {
			$this->_id = Yii::app()->request->getParam('id');
		}
		
		if(isset($_GET['no_kab'])) {
			$this->_no_kab = Yii::app()->request->getParam('no_kab');
		}
		
		if(isset($_GET['no_kec'])) {
			$this->_no_kec = Yii::app()->request->getParam('no_kec');
		}
		
		if(isset($_GET['no_kel'])) {
			$this->_no_kel = Yii::app()->request->getParam('no_kel');
		}
				
		$filterChain->run(); 
	}
}