<?php

class PmksController extends DlmController
{
	private $_jenisPmks;
	private $_title;
	private $_id;
	private $_dititip_di;
	private $_no_kab;
	private $_no_kec;
	private $_no_kel;
	private $_pmksDataProvider;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'ajaxOnly + actionSearch',
			'JenisPmksContext + index',
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','search'),
				'users'=>array('@'),
			),
			array('deny', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('deny', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{	
		$criteria=new CDbCriteria;
		$params = array();
		$criteria->condition='"id_jenis_pmks"=:id_jenis_pmks';
		$params[':id_jenis_pmks'] = $this->_id;
			
		if($this->_dititip_di > 0)
		{
			$criteria->addCondition('"dititip_di"=:dititip_di');
			$params[':dititip_di'] = $this->_dititip_di;
		}
		
		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
			
		if(count($params))
			$criteria->params=$params;
			
		$this->_pmksDataProvider=new CActiveDataProvider(
			'ViewLaporanPmks',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial('_search', array(
				'pmksDataProvider'=>$this->_pmksDataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('index',array(
			'pmksDataProvider'=>$this->_pmksDataProvider,
			'grid_id' => $grid_id,
			'id' => $this->_jenisPmks->id,
			'title'=>$this->_jenisPmks->nama_pmks,
		));		
	}
	
	/* filter pmks */
	private function loadJenisPmks($id) {
		if($this->_jenisPmks===null)
		{
			$this->_jenisPmks=JenisPmks::model()->findbyPk($id);
			if($this->_jenisPmks===null)
			{
				throw new CHttpException(404,'Kode PMKS tidak valid.'); 
			}
		}
		
		return $this->_jenisPmks; 
	} 
	
	public function filterJenisPmksContext($filterChain)
	{ 
		$this->_id = null;
		$this->_dititip_di = 0;
		$this->_no_kab = 0;
		$this->_no_kec = 0;
		$this->_no_kel = 0;
		
		if(isset($_GET['id'])) {
			$this->_id = Yii::app()->request->getParam('id');
		}
		
		if(isset($_GET['dititip_di'])) {
			$this->_dititip_di = Yii::app()->request->getParam('dititip_di');
		}
		
		if(isset($_GET['no_kab'])) {
			$this->_no_kab = Yii::app()->request->getParam('no_kab');
		}
		
		if(isset($_GET['no_kec'])) {
			$this->_no_kec = Yii::app()->request->getParam('no_kec');
		}
		
		if(isset($_GET['no_kel'])) {
			$this->_no_kel = Yii::app()->request->getParam('no_kel');
		}
		
		$this->loadJenisPmks($this->_id); 
		
		$filterChain->run(); 
	}
}