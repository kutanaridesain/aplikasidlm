<?php

class RumahController extends DlmController
{
	private $_no_kab;
	private $_no_kec;
	private $_no_kel;
	private $_status_rumah;
	private $_panjang_lantai;
	private $_lebar_lantai;
	private $_jenis_lantai;
	private $_jenis_dinding;
	private $_fasilitas_wc;
	private $_bahan_bakar_masak;
	private $_rumah_bantuan;
	private $_jlh_makan;
	private $_lauk_pauk;
	private $_daging;
	private $_pakaian;
	private $_dataProvider;
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'ajaxOnly + actionSearch',
			'RumahContext + statusRumah luasLantaiRumah jenisLantaiRumah 
			jenisDindingRumah fasilitasWc bahanBakarMasak rumahBantuan 
			jumlahMakan laukPauk daging pakaian semua',
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('statusRumah', 'luasLantaiRumah', 'jenisLantaiRumah',
				'jenisDindingRumah', 'fasilitasWc', 'bahanBakarMasak', 'rumahBantuan',
				'jumlahMakan', 'laukPauk', 'daging', 'pakaian', 'semua'),
				'users'=>array('@'),
			),
			array('deny', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('deny', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/* STATUS RUMAH */
	public function actionStatusRumah()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
				
		if($this->_status_rumah > 0)
		{
			$criteria->addCondition('"status_rumah"=:status_rumah');
			$params[':status_rumah'] = $this->_status_rumah;
		}
		
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('statusRumah',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* LUAS LANTAI RUMAH */
	public function actionLuasLantaiRumah()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
			
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_panjang_lantai > 0)
		{
			$criteria->addCondition('"panjang_lantai"=:panjang_lantai');
			$params[':panjang_lantai'] = $this->_panjang_lantai;
		}
		
		if($this->_lebar_lantai > 0)
		{
			$criteria->addCondition('"lebar_lantai"=:lebar_lantai');
			$params[':lebar_lantai'] = $this->_lebar_lantai;
		}
		
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('luasLantaiRumah',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* JENIS LANTAI RUMAH */
	public function actionJenisLantaiRumah()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
			
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_jenis_lantai > 0)
		{
			$criteria->addCondition('"jenis_lantai"=:jenis_lantai');
			$params[':jenis_lantai'] = $this->_jenis_lantai;
		}

		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('jenisLantaiRumah',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* JENIS DINDING RUMAH */
	public function actionJenisDindingRumah()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
			
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_jenis_dinding > 0)
		{
			$criteria->addCondition('"jenis_dinding"=:jenis_dinding');
			$params[':jenis_dinding'] = $this->_jenis_dinding;
		}

		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('jenisDindingRumah',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* FASILITAS WC */
	public function actionFasilitasWc()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
			
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_fasilitas_wc > 0)
		{
			$criteria->addCondition('"fasilitas_wc"=:fasilitas_wc');
			$params[':fasilitas_wc'] = $this->_fasilitas_wc;
		}

		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('fasilitasWc',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* BAHAN BAKAR MASAK */
	public function actionBahanBakarMasak()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
			
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_bahan_bakar_masak > 0)
		{
			$criteria->addCondition('"bahan_bakar_masak"=:bahan_bakar_masak');
			$params[':bahan_bakar_masak'] = $this->_bahan_bakar_masak;
		}

		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('bahanBakarMasak',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* RUMAH BANTUAN */
	public function actionRumahBantuan()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
			
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_rumah_bantuan > 0)
		{
			$criteria->addCondition('"rumah_bantuan"=:rumah_bantuan');
			$params[':rumah_bantuan'] = $this->_rumah_bantuan;
		}

		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('rumahBantuan',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* JUMLAH MAKAN */
	public function actionJumlahMakan()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
			
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_jlh_makan > 0)
		{
			$criteria->addCondition('"jlh_makan"=:jlh_makan');
			$params[':jlh_makan'] = $this->_jlh_makan;
		}

		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('jumlahMakan',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* LAUK PAUK */
	public function actionLaukPauk()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
			
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_lauk_pauk > 0)
		{
			$criteria->addCondition('"lauk_pauk"=:lauk_pauk');
			$params[':lauk_pauk'] = $this->_lauk_pauk;
		}

		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('laukPauk',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* DAGING */
	public function actionDaging()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_daging > 0)
		{
			$criteria->addCondition('"daging"=:daging');
			$params[':daging'] = $this->_daging;
		}

		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('daging',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* PAKAIAN */
	public function actionPakaian()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_pakaian > 0)
		{
			$criteria->addCondition('"pakaian"=:pakaian');
			$params[':pakaian'] = $this->_pakaian;
		}

		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('pakaian',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* SEMUA */
	public function actionSemua()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
				
		if($this->_status_rumah > 0)
		{
			$criteria->addCondition('"status_rumah"=:status_rumah');
			$params[':status_rumah'] = $this->_status_rumah;
		}
		
		if($this->_panjang_lantai > 0)
		{
			$criteria->addCondition('"panjang_lantai"=:panjang_lantai');
			$params[':panjang_lantai'] = $this->_panjang_lantai;
		}
		
		if($this->_lebar_lantai > 0)
		{
			$criteria->addCondition('"lebar_lantai"=:lebar_lantai');
			$params[':lebar_lantai'] = $this->_lebar_lantai;
		}
		
		if($this->_jenis_lantai > 0)
		{
			$criteria->addCondition('"jenis_lantai"=:jenis_lantai');
			$params[':jenis_lantai'] = $this->_jenis_lantai;
		}
		
		if($this->_jenis_dinding > 0)
		{
			$criteria->addCondition('"jenis_dinding"=:jenis_dinding');
			$params[':jenis_dinding'] = $this->_jenis_dinding;
		}
		
		if($this->_fasilitas_wc > 0)
		{
			$criteria->addCondition('"fasilitas_wc"=:fasilitas_wc');
			$params[':fasilitas_wc'] = $this->_fasilitas_wc;
		}
		
		if($this->_bahan_bakar_masak > 0)
		{
			$criteria->addCondition('"bahan_bakar_masak"=:bahan_bakar_masak');
			$params[':bahan_bakar_masak'] = $this->_bahan_bakar_masak;
		}
		
		if($this->_rumah_bantuan > 0)
		{
			$criteria->addCondition('"rumah_bantuan"=:rumah_bantuan');
			$params[':rumah_bantuan'] = $this->_rumah_bantuan;
		}
		
		if($this->_jlh_makan > 0)
		{
			$criteria->addCondition('"jlh_makan"=:jlh_makan');
			$params[':jlh_makan'] = $this->_jlh_makan;
		}
		
		if($this->_lauk_pauk > 0)
		{
			$criteria->addCondition('"lauk_pauk"=:lauk_pauk');
			$params[':lauk_pauk'] = $this->_lauk_pauk;
		}
		
		if($this->_daging > 0)
		{
			$criteria->addCondition('"daging"=:daging');
			$params[':daging'] = $this->_daging;
		}
		
		if($this->_pakaian > 0)
		{
			$criteria->addCondition('"pakaian"=:pakaian');
			$params[':pakaian'] = $this->_pakaian;
		}

		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanRumah',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_rumah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('semua',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	public function filterRumahContext($filterChain)
	{ 
		$this->_no_kab = 0;
		$this->_no_kec = 0;
		$this->_no_kel = 0;
		$this->_status_rumah = 0;
		$this->_panjang_lantai = 0;
		$this->_lebar_lantai = 0;
		$this->_jenis_lantai = 0;
		$this->_jenis_dinding = 0;
		$this->_fasilitas_wc = 0;
		$this->_bahan_bakar_masak = 0;
		$this->_rumah_bantuan = 0;
		$this->_jlh_makan = 0;
		$this->_lauk_pauk = 0;
		$this->_daging = 0;
		$this->_pakaian = 0;
				
		if(isset($_GET['no_kab'])) {
			$this->_no_kab = Yii::app()->request->getParam('no_kab');
		}
		
		if(isset($_GET['no_kec'])) {
			$this->_no_kec = Yii::app()->request->getParam('no_kec');
		}
		
		if(isset($_GET['no_kel'])) {
			$this->_no_kel = Yii::app()->request->getParam('no_kel');
		}
		
		if(isset($_GET['status_rumah'])) {
			$this->_status_rumah = Yii::app()->request->getParam('status_rumah');
		}
		
		if(isset($_GET['panjang_lantai'])) {
			$this->_panjang_lantai = Yii::app()->request->getParam('panjang_lantai');
		}
		
		if(isset($_GET['lebar_lantai'])) {
			$this->_lebar_lantai = Yii::app()->request->getParam('lebar_lantai');
		}
		
		if(isset($_GET['jenis_lantai'])) {
			$this->_jenis_lantai = Yii::app()->request->getParam('jenis_lantai');
		}
		
		if(isset($_GET['jenis_dinding'])) {
			$this->_jenis_dinding = Yii::app()->request->getParam('jenis_dinding');
		}
		
		if(isset($_GET['fasilitas_wc'])) {
			$this->_fasilitas_wc = Yii::app()->request->getParam('bahan_bakar_masak');
		}
		
		if(isset($_GET['bahan_bakar_masak'])) {
			$this->_bahan_bakar_masak = Yii::app()->request->getParam('bahan_bakar_masak');
		}
		
		if(isset($_GET['rumah_bantuan'])) {
			$this->_rumah_bantuan = Yii::app()->request->getParam('rumah_bantuan');
		}
		
		if(isset($_GET['jlh_makan'])) {
			$this->_jlh_makan = Yii::app()->request->getParam('jlh_makan');
		}
		
		if(isset($_GET['lauk_pauk'])) {
			$this->_lauk_pauk = Yii::app()->request->getParam('lauk_pauk');
		}
		
		if(isset($_GET['daging'])) {
			$this->_daging = Yii::app()->request->getParam('daging');
		}
		
		if(isset($_GET['pakaian'])) {
			$this->_pakaian = Yii::app()->request->getParam('pakaian');
		}
		
		$filterChain->run(); 
	}
}