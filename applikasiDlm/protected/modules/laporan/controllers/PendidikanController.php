<?php

class PendidikanController extends DlmController
{
	private $_no_kab;
	private $_no_kec;
	private $_no_kel;
	private $_jenjang_sekolah;
	private $_kode_sekolah;
	private $_pemberi_beasiswa;
	private $_thn_beasiswa;
	private $_tahun;
	private $_dataProvider;
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'ajaxOnly + actionSearch',
			'PendidikanContext + tingkatPendidikan namaSekolah tingkatNamaSekolah putusSekolah 
			 pemberiBeasiswa namaSekolahTahun',
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('tingkatPendidikan', 'namaSekolah', 'tingkatNamaSekolah', 'putusSekolah',
				'pemberiBeasiswa', 'namaSekolahTahun'),
				'users'=>array('@'),
			),
			array('deny', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('deny', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/* TINGKAT PENDIDIKAN */
	public function actionTingkatPendidikan()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
				
		if($this->_jenjang_sekolah != '')
		{
			$criteria->addCondition('"jenjang_sekolah"=:jenjang_sekolah');
			$params[':jenjang_sekolah'] = $this->_jenjang_sekolah;
		}
		
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanPendidikan',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_pendidikan", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('tingkatPendidikan',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* NAMA SEKOLAH */
	public function actionNamaSekolah()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
				
		if($this->_kode_sekolah > 0)
		{
			$criteria->addCondition('"kode_sekolah"=:kode_sekolah');
			$params[':kode_sekolah'] = $this->_kode_sekolah;
		}
		
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanPendidikan',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_pendidikan", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('namaSekolah',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* TINGKAT PENDIDIKAN & NAMA SEKOLAH */
	public function actionTingkatNamaSekolah()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
			
		if($this->_kode_sekolah > 0)
		{
			$criteria->addCondition('"kode_sekolah"=:kode_sekolah');
			$params[':kode_sekolah'] = $this->_kode_sekolah;
		}
				
		if($this->_jenjang_sekolah != '')
		{
			$criteria->addCondition('"jenjang_sekolah"=:jenjang_sekolah');
			$params[':jenjang_sekolah'] = $this->_jenjang_sekolah;
		}
		
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanPendidikan',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_pendidikan", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('tingkatNamaSekolah',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* PUTUS SEKOLAH */
	public function actionPutusSekolah()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
				
		if($this->_jenjang_sekolah != '')
		{
			$criteria->addCondition('"jenjang_sekolah"=:jenjang_sekolah');
			$params[':jenjang_sekolah'] = $this->_jenjang_sekolah;
		}
		
		$criteria->addCondition('"status_pendidikan"=1');
			
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanPendidikan',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_putusSekolah", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('putusSekolah',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	
	/* PEMBERI BEASISWA */
	public function actionPemberiBeasiswa()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
				
		if($this->_pemberi_beasiswa != '')
		{
			$criteria->addCondition('"pemberi_beasiswa"=:pemberi_beasiswa');
			$params[':pemberi_beasiswa'] = $this->_pemberi_beasiswa;
		}
		
		if($this->_thn_beasiswa != '')
		{
			$criteria->addCondition('"thn_beasiswa"=:thn_beasiswa');
			$params[':thn_beasiswa'] = $this->_thn_beasiswa;
		}
			
		$criteria->addCondition('"dapat_beasiswa"=1');
			
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanPendidikan',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_pemberiBeasiswa", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('pemberiBeasiswa',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	/* NAMA SEKOLAH & TAHUN */
	public function actionNamaSekolahTahun()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
				
		if($this->_kode_sekolah > 0)
		{
			$criteria->addCondition('"kode_sekolah"=:kode_sekolah');
			$params[':kode_sekolah'] = $this->_kode_sekolah;
		}
					
		if($this->_tahun != '')
		{
			$criteria->addCondition('"tahun"=:tahun');
			$params[':tahun'] = $this->_tahun;
		}
			
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanPendidikan',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_namaSekolahTahun", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('namaSekolahTahun',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	public function filterPendidikanContext($filterChain)
	{ 
		$this->_no_kab = 0;
		$this->_no_kec = 0;
		$this->_no_kel = 0;
		$this->_jenjang_sekolah = '';
		$this->_kode_sekolah = '';
		$this->_pemberi_beasiswa = '';
		$this->_thn_beasiswa ='';
				
		if(isset($_GET['no_kab'])) {
			$this->_no_kab = Yii::app()->request->getParam('no_kab');
		}
		
		if(isset($_GET['no_kec'])) {
			$this->_no_kec = Yii::app()->request->getParam('no_kec');
		}
		
		if(isset($_GET['no_kel'])) {
			$this->_no_kel = Yii::app()->request->getParam('no_kel');
		}
		
		if(isset($_GET['jenjang_sekolah'])) {
			$this->_jenjang_sekolah = Yii::app()->request->getParam('jenjang_sekolah');
		}
		
		if(isset($_GET['kode_sekolah'])) {
			$this->_kode_sekolah = Yii::app()->request->getParam('kode_sekolah');
		}
		
		if(isset($_GET['pemberi_beasiswa'])) {
			$this->_pemberi_beasiswa = Yii::app()->request->getParam('pemberi_beasiswa');
		}
		
		if(isset($_GET['thn_beasiswa'])) {
			$this->_thn_beasiswa = Yii::app()->request->getParam('thn_beasiswa');
		}
		
		if(isset($_GET['tahun'])) {
			$this->_tahun = Yii::app()->request->getParam('tahun');
		}
		
		$filterChain->run(); 
	}
}