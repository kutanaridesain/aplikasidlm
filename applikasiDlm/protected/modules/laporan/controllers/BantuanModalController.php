<?php

class BantuanModalController extends DlmController
{
	private $_no_kab;
	private $_no_kec;
	private $_no_kel;
	private $_id_instansi;
	private $_tahun_diberikan;
	private $_id_koperasi;
	private $_jenis_bantuan_modal;
	private $_dataProvider;
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'ajaxOnly + actionSearch',
			'BantuanContext + index',
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('deny', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/* INDEX */
	public function actionIndex()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
				
		if($this->_id_instansi > 0)
		{
			$criteria->addCondition('"id_instansi"=:id_instansi');
			$params[':id_instansi'] = $this->_id_instansi;
		}
		
		if($this->_tahun_diberikan > 0)
		{
			$criteria->addCondition('"tahun_diberikan"=:tahun_diberikan');
			$params[':tahun_diberikan'] = $this->_tahun_diberikan;
		}		
		
		if($this->_id_koperasi > 0)
		{
			$criteria->addCondition('"id_koperasi"=:id_koperasi');
			$params[':id_koperasi'] = $this->_id_koperasi;
		}		
		
		if($this->_jenis_bantuan_modal > 0)
		{
			$criteria->addCondition('"jenis_bantuan_modal"=:jenis_bantuan_modal');
			$params[':jenis_bantuan_modal'] = $this->_jenis_bantuan_modal;
		}
		
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanBantuanModal',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_index", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('index',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	public function filterBantuanContext($filterChain)
	{ 
		$this->_no_kab = 0;
		$this->_no_kec = 0;
		$this->_no_kel = 0;
		$this->_id_instansi = 0;
		$this->_tahun_diberikan = 0;
		$this->_id_koperasi = 0;
		$this->_jenis_bantuan_modal = 0;
				
		if(isset($_GET['no_kab'])) {
			$this->_no_kab = Yii::app()->request->getParam('no_kab');
		}
		
		if(isset($_GET['no_kec'])) {
			$this->_no_kec = Yii::app()->request->getParam('no_kec');
		}
		
		if(isset($_GET['no_kel'])) {
			$this->_no_kel = Yii::app()->request->getParam('no_kel');
		}
		
		if(isset($_GET['id_instansi'])) {
			$this->_id_instansi = Yii::app()->request->getParam('id_instansi');
		}
		
		if(isset($_GET['tahun_diberikan'])) {
			$this->_tahun_diberikan = Yii::app()->request->getParam('tahun_diberikan');
		}
				
		if(isset($_GET['id_koperasi'])) {
			$this->_id_koperasi = Yii::app()->request->getParam('id_koperasi');
		}
		
		if(isset($_GET['jenis_bantuan_modal'])) {
			$this->_jenis_bantuan_modal = Yii::app()->request->getParam('jenis_bantuan_modal');
		}

				
		$filterChain->run(); 
	}
}