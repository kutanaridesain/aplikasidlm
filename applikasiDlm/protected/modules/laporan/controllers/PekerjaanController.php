<?php

class PekerjaanController extends DlmController
{
	private $_no_kab;
	private $_no_kec;
	private $_no_kel;
	private $_id;
	private $_dataProvider;
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'ajaxOnly + actionSearch',
			'PekerjaanContext + jumlahPenghasilan',
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('jumlahPenghasilan'),
				'users'=>array('@'),
			),
			array('deny', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('deny', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/* JUMLAH PENGHASILAN */
	public function actionJumlahPenghasilan()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
				
		if($this->_id > 0)
		{
			if($this->_id == 1){
				$criteria->addCondition('"rata_penghasilan"<1000000');
			}
			
			if($this->_id == 2){
				//$criteria->addCondition('"rata_penghasilan"<1000000');
				$criteria->addBetweenCondition('"rata_penghasilan"', '1000000', '3000000', 'OR');

			}
			
			if($this->_id == 3){
				$criteria->addCondition('"rata_penghasilan">3000000');
			}
		}
		
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanPekerjaan',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_jumlahPenghasilan", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('jumlahPenghasilan',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	public function filterPekerjaanContext($filterChain)
	{ 
		$this->_no_kab = 0;
		$this->_no_kec = 0;
		$this->_no_kel = 0;
		$this->_id = 0;
				
		if(isset($_GET['no_kab'])) {
			$this->_no_kab = Yii::app()->request->getParam('no_kab');
		}
		
		if(isset($_GET['no_kec'])) {
			$this->_no_kec = Yii::app()->request->getParam('no_kec');
		}
		
		if(isset($_GET['no_kel'])) {
			$this->_no_kel = Yii::app()->request->getParam('no_kel');
		}
		
		if(isset($_GET['id'])) {
			$this->_id = Yii::app()->request->getParam('id');
		}
		
		$filterChain->run(); 
	}
}