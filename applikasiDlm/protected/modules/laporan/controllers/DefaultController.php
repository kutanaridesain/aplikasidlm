<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionPdf()
	{
		$content = "
<page>
    <h1>Exemple d'utilisation</h1>
    <br>
    Ceci est un <b>exemple d'utilisation</b>
    de <a href='http://html2pdf.fr/'>HTML2PDF</a>.<br>
</page>
<page>
    <h1>Ini halaan kedua</h1>
    <br>
    Ceci est un <b>exemple d'utilisation</b>
    de <a href='http://html2pdf.fr/'>HTML2PDF</a>.<br>
</page>";
		$html2pdf = Yii::app()->ePdf->HTML2PDF();
		$html2pdf->WriteHTML($this->renderPartial('biodatabantuan', array(), true));
		$html2pdf->Output();
	}

	public function actionBiodatabantuan()
	{
		// $this->render('biodatabantuan');
		echo $this->renderPartial('biodatabantuan', array(), true);
	}
}