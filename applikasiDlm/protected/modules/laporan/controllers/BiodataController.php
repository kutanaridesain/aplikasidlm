<?php

class BiodataController extends DlmController
{
	private $_title;
	private $_no_kab;
	private $_no_kec;
	private $_no_kel;
	private $_pekerjaan;
	private $_dataProvider;
	private $_form_name;
	private $_partial_name;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'ajaxOnly + actionSearch',
			'BiodataContext + penduduk jenisPekerjaan',
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('penduduk', 'jenisPekerjaan'),
				'users'=>array('@'),
			),
			array('deny', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('deny', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionPenduduk()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
			
		$this->getDataPenduduk($criteria, $params);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial($this->_partial_name, array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('penduduk',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
			'title'=>$this->_title,
			'form_name'=>$this->_form_name,
			'partial_name'=>$this->_partial_name,
		));		
	}
	
	private function getDataPenduduk($criteria, $params){
		//hanya yang hidup, kecuali untuk penduduk aceh
		if($this->_form_name != "tab-1" && $this->_form_name != "tab-8" && $this->_form_name != "tab-9"){
			$criteria->addCondition('"stat_hidup"=2');
		}
		
		switch($this->_form_name){
			case "tab-2":	
				$criteria->addCondition('"akta_lhr"=1');
				if(count($params)){
					$criteria->params=$params;
				}
					
				$this->_title="YANG MEMILIKI AKTA KELAHIRAN";
				$this->_partial_name = "_pendudukAktaLahir";
				$this->_dataProvider=new CActiveDataProvider(
					'ViewLaporanBiodata',
					array(
						'criteria'=>$criteria,
						'pagination' => array('pageSize'=>'20'),
					)
				);
				break;
			case "tab-3":	
				$criteria->addCondition('"akta_lhr"=2');
				if(count($params)){
					$criteria->params=$params;
				}
					
				$this->_title="YANG TIDAK MEMILIKI AKTA KELAHIRAN";
				$this->_partial_name = "_pendudukAktaLahir";
				$this->_dataProvider=new CActiveDataProvider(
					'ViewLaporanBiodata',
					array(
						'criteria'=>$criteria,
						'pagination' => array('pageSize'=>'20'),
					)
				);
				break;
			case "tab-4":	
				$criteria->addCondition('"akta_kwn"=1');
				$criteria->addCondition('"akta_kwn" IS NOT NULL');
				if(count($params)){
					$criteria->params=$params;
				}
					
				$this->_title="YANG MEMILIKI AKTA KAWIN";
				$this->_partial_name = "_pendudukAktaKawin";
				$this->_dataProvider=new CActiveDataProvider(
					'ViewLaporanBiodata',
					array(
						'criteria'=>$criteria,
						'pagination' => array('pageSize'=>'20'),
					)
				);
				break;
			case "tab-5":	
				$criteria->addCondition('"akta_kwn"=2');
				$criteria->addCondition('"akta_kwn" IS NOT NULL');
				if(count($params)){
					$criteria->params=$params;
				}
					
				$this->_title="YANG TIDAK MEMILIKI AKTA KAWIN";
				$this->_partial_name = "_pendudukAktaKawin";
				$this->_dataProvider=new CActiveDataProvider(
					'ViewLaporanBiodata',
					array(
						'criteria'=>$criteria,
						'pagination' => array('pageSize'=>'20'),
					)
				);
				break;
			case "tab-6":		
				$criteria->addCondition('"stat_kwn">2');
				$criteria->addCondition('"akta_crai"=1');
				$criteria->addCondition('"akta_crai" IS NOT NULL');
				if(count($params)){
					$criteria->params=$params;
				}
					
				$this->_title="YANG MEMILIKI AKTA CERAI";
				$this->_partial_name = "_pendudukAktaCerai";
				$this->_dataProvider=new CActiveDataProvider(
					'ViewLaporanBiodata',
					array(
						'criteria'=>$criteria,
						'pagination' => array('pageSize'=>'20'),
					)
				);
				break;
			case "tab-7":	
				$criteria->addCondition('"stat_kwn">2');
				$criteria->addCondition('"akta_crai"=2');
				$criteria->addCondition('"akta_crai" IS NOT NULL');
				if(count($params)){
					$criteria->params=$params;
				}
					
				$this->_title="YANG TIDAK MEMILIKI AKTA CERAI";
				$this->_partial_name = "_pendudukAktaCerai";
				$this->_dataProvider=new CActiveDataProvider(
					'ViewLaporanBiodata',
					array(
						'criteria'=>$criteria,
						'pagination' => array('pageSize'=>'20'),
					)
				);
				break;
			case "tab-8":		
				$criteria->addCondition('"stat_hidup"=1');
				$criteria->addCondition('"no_akta_kematian" IS NOT NULL');
				if(count($params)){
					$criteria->params=$params;
				}
					
				$this->_title="YANG MEMILIKI AKTA KEMATIAN";
				$this->_partial_name = "_pendudukAktaKematian";
				$this->_dataProvider=new CActiveDataProvider(
					'ViewLaporanBiodata',
					array(
						'criteria'=>$criteria,
						'pagination' => array('pageSize'=>'20'),
					)
				);
				break;
			case "tab-9":	
				$criteria->addCondition('"stat_hidup"=1');
				$criteria->addCondition('"no_akta_kematian" IS NULL');
				if(count($params)){
					$criteria->params=$params;
				}
					
				$this->_title="YANG TIDAK MEMILIKI AKTA KEMATIAN";
				$this->_partial_name = "_pendudukAktaKematian";
				$this->_dataProvider=new CActiveDataProvider(
					'ViewLaporanBiodata',
					array(
						'criteria'=>$criteria,
						'pagination' => array('pageSize'=>'20'),
					)
				);
				break;
			default:	
				if(count($params)){
					$criteria->params=$params;
				}
				$this->_dataProvider=new CActiveDataProvider(
					'ViewLaporanBiodata',
					array(
						'criteria'=>$criteria,
						'pagination' => array('pageSize'=>'20'),
					)
				);
			break;
		}
	}
	
	public function actionJenisPekerjaan()
	{	
		$criteria=new CDbCriteria;
		$params = array();
		
		$criteria->addCondition('"pekerjaan" IS NOT NULL');

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
		
		if($this->_pekerjaan != "")
		{
			$criteria->addCondition('"pekerjaan"=:pekerjaan');
			$params[':pekerjaan'] = $this->_pekerjaan;
		}
			
		$this->getDataPenduduk($criteria, $params);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial($this->_partial_name, array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
			
		$pekerjaanlist = CHtml::listData(Pekerjaan::model()->findAll(array('order'=>'"nama_pekerjaan"')), 'id', 'nama_pekerjaan' );
		
		$this->render('jenisPekerjaan',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
			'pekerjaanlist' => $pekerjaanlist,
		));		
	}
	
	public function filterBiodataContext($filterChain)
	{ 
		$this->_no_kab = 0;
		$this->_no_kec = 0;
		$this->_no_kel = 0;
		$this->_form_name = "tab-1";
		$this->_title = "";
		$this->_pekerjaan = "";
		$this->_partial_name = "_penduduk";
		
		if(isset($_GET['no_kab'])) {
			$this->_no_kab = Yii::app()->request->getParam('no_kab');
		}
		
		if(isset($_GET['no_kec'])) {
			$this->_no_kec = Yii::app()->request->getParam('no_kec');
		}
		
		if(isset($_GET['no_kel'])) {
			$this->_no_kel = Yii::app()->request->getParam('no_kel');
		}
		
		if(isset($_GET['id'])) {
			$this->_form_name = Yii::app()->request->getParam('id');
		}
		
		if(isset($_GET['pekerjaan'])) {
			$this->_pekerjaan = Yii::app()->request->getParam('pekerjaan');
		}
		
		$filterChain->run(); 
	}
}