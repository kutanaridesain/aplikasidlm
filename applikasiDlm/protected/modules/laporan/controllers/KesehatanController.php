<?php

class KesehatanController extends DlmController
{
	private $_no_kab;
	private $_no_kec;
	private $_no_kel;
	private $_id_penyakit;
	private $_id_puskesmas;
	private $_type_jamkes;
	private $_dataProvider;
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'ajaxOnly + actionSearch',
			'PenyakitContext + index',
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('deny', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/* PENYAKIT */
	public function actionIndex()
	{	
		$criteria=new CDbCriteria;
		$params = array();

		if($this->_no_kab > 0)
		{
			$criteria->addCondition('"no_kab"=:no_kab');
			$params[':no_kab'] = $this->_no_kab;
		}
		
		if($this->_no_kec > 0)
		{
			$criteria->addCondition('"no_kec"=:no_kec');
			$params[':no_kec'] = $this->_no_kec;
		}
		
		if($this->_no_kel > 0)
		{
			$criteria->addCondition('"no_kel"=:no_kel');
			$params[':no_kel'] = $this->_no_kel;
		}
				
		if($this->_id_penyakit > 0)
		{
			$criteria->addColumnCondition(
			array('"id_penyakit"' => $this->_id_penyakit, '"id_penyakit1"' => $this->_id_penyakit, '"id_penyakit2"' => $this->_id_penyakit), 'OR'	);
		}
		
		if($this->_type_jamkes > 0){
			if($this->_type_jamkes == 1){
				$criteria->addCondition('no_jka IS NOT NULL');
			}			
			
			if($this->_type_jamkes == 2){
				$criteria->addCondition('no_jamkesmas IS NOT NULL');			
			}
						
			if($this->_type_jamkes == 3){
				$criteria->addColumnCondition(array('"asuransilain1"' => 'IS NOT NULL', '"asuransilain2"' => 'IS NOT NULL'), 'OR'	);
			}
		}
		
		if($this->_id_puskesmas > 0)
		{
			$criteria->addCondition('"id_puskesmas"=:id_puskesmas');
			$params[':id_puskesmas'] = $this->_id_puskesmas;
		}
		
		if(count($params)){
			$criteria->params=$params;
		}
		
		$this->_dataProvider=new CActiveDataProvider(
			'ViewLaporanKesehatan',
			array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize'=>'20'),
			)
		);
		
		$grid_id = 'laporan';
		
		if(Yii::app()->request->isAjaxRequest && isset($_GET['ajax']) && $_GET['ajax'] === $grid_id) {
			$this->renderPartial("_index", array(
				'dataProvider'=>$this->_dataProvider,
				'grid_id' => $grid_id,
			));
		  Yii::app()->end();
		}
		
		$this->render('index',array(
			'dataProvider'=>$this->_dataProvider,
			'grid_id' => $grid_id,
		));		
	}
	
	public function filterPenyakitContext($filterChain)
	{ 
		$this->_no_kab = 0;
		$this->_no_kec = 0;
		$this->_no_kel = 0;
		$this->_id_penyakit = 0;
		$this->_id_puskesmas = 0;
		$this->_type_jamkes = 0;
				
		if(isset($_GET['no_kab'])) {
			$this->_no_kab = Yii::app()->request->getParam('no_kab');
		}
		
		if(isset($_GET['no_kec'])) {
			$this->_no_kec = Yii::app()->request->getParam('no_kec');
		}
		
		if(isset($_GET['no_kel'])) {
			$this->_no_kel = Yii::app()->request->getParam('no_kel');
		}
		
		if(isset($_GET['id_penyakit'])) {
			$this->_id_penyakit = Yii::app()->request->getParam('id_penyakit');
		}
		
		if(isset($_GET['id_puskesmas'])) {
			$this->_id_puskesmas = Yii::app()->request->getParam('id_puskesmas');
		}
		
		if(isset($_GET['type_jamkes'])) {
			$this->_type_jamkes = Yii::app()->request->getParam('type_jamkes');
		}
		
		$filterChain->run(); 
	}
}