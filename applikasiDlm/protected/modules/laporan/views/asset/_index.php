<?php
/* @var $this AssetController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'header'=>'NAMA<br/>NO. KK<br/>NIK',
						'name'=>'nama_lgkp',
						'type'=>'html',
						'value'=>'ViewLaporanAsset::getNamaNoKkNik($data->nama_lgkp, $data->no_kk, $data->nik)',
					),  
					array(
						'header'=>'STATUS KELUARGA<br/>TGL LAHIR',
						'name'=>'hub_keluarga',
						'type'=>'html',
						'value'=>'ViewLaporanAsset::getStatusKeluargaTanggalLahir($data->hub_keluarga, $data->tgl_lhr)',
					),  
					array(
						'header'=>'AGAMA<br/>PEKERJAAN',
						'name'=>'hub_keluarga',
						'type'=>'html',
						'value'=>'ViewLaporanAsset::getAgamaPekerjaan($data->agama, $data->pekerjaan)',
					),    
					array(
						'header'=>'TANAH RUMAH<br/>LUAS',
						'name'=>'punya_tanah_rumah',
						'type'=>'html',
						'value'=>'ViewLaporanAsset::getTanah($data->punya_tanah_rumah, $data->luas_tanah_rumah)',
					),					   
					array(
						'header'=>'TANAH SAWAH<br/>LUAS',
						'name'=>'punya_tanah_sawah',
						'type'=>'html',
						'value'=>'ViewLaporanAsset::getTanah($data->punya_tanah_sawah, $data->luas_tanah_sawah)',
					),					   
					array(
						'header'=>'TANAH KEBUN<br/>LUAS',
						'name'=>'punya_tanah_kebun',
						'type'=>'html',
						'value'=>'ViewLaporanAsset::getTanah($data->punya_tanah_kebun, $data->luast_tanah_kebun)',
					),					   
					array(
						'header'=>'TAMBAK<br/>LUAS',
						'name'=>'punya_tambak',
						'type'=>'html',
						'value'=>'ViewLaporanAsset::getTanah($data->punya_tambak, $data->luas_tambak)',
					), 
										   
					array(
						'header'=>'LEMBU<br/>JLH EKOR',
						'name'=>'punya_lembu',
						'type'=>'html',
						'value'=>'ViewLaporanAsset::getTernak($data->punya_lembu, $data->jlh_lembu)',
					), 										   
					array(
						'header'=>'KERBAU<br/>JLH EKOR',
						'name'=>'punya_kerbau',
						'type'=>'html',
						'value'=>'ViewLaporanAsset::getTernak($data->punya_kerbau, $data->jlh_kerbau)',
					), 										   
					array(
						'header'=>'TERNAK LAIN<br/>JLH EKOR',
						'name'=>'punya_ternak_lain',
						'type'=>'html',
						'value'=>'ViewLaporanAsset::getTernak($data->punya_ternak_lain, $data->jlh_ternak_lain)',
					),
					array(
						'header'=>'ALAMAT<br/>(GP, KEC, KAB)',
						'name'=>'alamat',
						'type'=>'html',
						'value'=>'$data->alamat',
					),  
				),
			));
		?>
	
	
