<?php
/* @var $this PmksController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$pmksDataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'name'=>'nama',
						'type'=>'html',
						'value'=>'$data->nama',
					),  
					array(
						'name'=>'hub_keluarga',
						'type'=>'html',
						'value'=>'ViewLaporanPmks::getHubunganKeluarga($data->hub_keluarga, $data->agama)',
					),  
					//'tgl_lhr',
					array(
						'name'=>'nik_nama_ibu',
						'type'=>'html',
						'value'=>'$data->nik_nama_ibu',
					),  
					array(
						'name'=>'nik_nama_ayah',
						'type'=>'html',
						'value'=>'$data->nik_nama_ayah',
					),  
					array(
						'name'=>'agama',
						'type'=>'raw',
						'value'=>'DlmActiveRecord::getAgamaById($data->agama)',
					), 
					array(
						'name'=>'alamat',
						'type'=>'html',
						'value'=>'$data->alamat',
					),  
					array(
						'name'=>'nama_pantiasuhan',
						'type'=>'html',
						'value'=>'$data->nama_pantiasuhan',
					),  
				),
			));
		?>
	
	
