<?php
/* @var $this BiodataController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'header'=>'NAMA KEPALA KELUARGA<br/>NIK<br/>NO. KK',
						'name'=>'nama',
						'type'=>'html',
						'value'=>'ViewLaporanRumah::getNamaNikNoKk($data->nama, $data->nik,$data->no_kk)',
					), 
					array(
						'header'=>'STATUS RUMAH<br/>LUAS LANTAI<br/>JENIS LANTAI',
						'name'=>'status_rumah',
						'type'=>'html',
						'value'=>'ViewLaporanRumah::getStatusLuasJenisLantai($data->status_rumah, $data->panjang_lantai, $data->lebar_lantai, $data->jenis_lantai)',
					), 
					array(
						'header'=>'JENIS DINDING<br/>KETERSEDIAAN WC<br/>SUMBER AIR MINUM',
						'name'=>'jenis_dinding',
						'type'=>'html',
						'value'=>'ViewLaporanRumah::getJenisDindingFasilitasWcSumberAirMinum($data->jenis_dinding, $data->fasilitas_wc, $data->air_minum)',
					), 
					array(
						'header'=>'BAHAN BAKAR MEMASAK<br/>RUMAH BANTUAN/BUKAN<br/>BANTUAN DARI',
						'name'=>'bahan_bakar_masak',
						'type'=>'html',
						'value'=>'ViewLaporanRumah::getBahanBakarRumahBantuanPemberi($data->bahan_bakar_masak, $data->rumah_bantuan, $data->pemberi_rumah_bantuan)',
					), 
					array(
						'header'=>'MAKAN DALAM 1 HARI<br/>KONSUMSI LAUK PAUK TIAP HARI<br/>KONSUMSI DAGING/SUSU DALAM SEMINGGU',
						'name'=>'jlh_makan',
						'type'=>'html',
						'value'=>'ViewLaporanRumah::getNasiLaukDagingSusu($data->jlh_makan, $data->lauk_pauk, $data->daging)',
					),
					array(
						'header'=>'KEMAMPUAN MEMBELI PAKAIAN BARU<br/>PEKERJAAN TERAKHIR KEPALA KELUARGA<br/>JML RATA2 PENGHASILAN',
						'name'=>'pakaian',
						'type'=>'html',
						'value'=>'ViewLaporanRumah::getPakaianPekerjaanPenghasilan($data->pakaian, $data->pekerjaan, $data->penghasilan)',
					),
				),
			));
		?>
	
	
