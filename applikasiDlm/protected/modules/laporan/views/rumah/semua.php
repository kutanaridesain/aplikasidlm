<?php
/* @var $this PmksController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'dlm-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="rows center" id="report-wrapper">
		<!-- report header-->
		<div class="rows report-header">
			<div id="report-logo-provinsi" class="center">
				<img src="/images/logo-provinsi-aceh.png" alt="" />
			</div>
			<div id="report-title">DINAS REGISTRASI KEPENDUDUKAN ACEH</div>
			<div id="report-sub-title">
				DAFTAR PENDUDUK ACEH <br/>
				BERDASARKAN KRITERIA SEBAGAI BERIKUT:<br/>
				YANG STATUS RUMAHNYA &nbsp;
				<select id="status_rumah" name="status_rumah">
					<option value="0">-- Semua --</option>
					<option value="1">Milik Sendiri</option>
					<option value="2">Menyewa</option>
					<option value="3">Menumpang</option>
				</select>, &nbsp;
				ATAU LUAS LANTAI &nbsp;
				<input type="text" name="panjang_lantai" placeholder="Panjang" class="span1"/>&nbsp;Meter 
				&nbsp;X&nbsp;
				<input type="text" name="lebar_lantai" placeholder="Lebar"  class="span1"/>&nbsp;Meter &nbsp;
				ATAU JENIS LANTAI &nbsp;
				<select id="jenis_lantai" name="jenis_lantai">
					<option value="0">-- Semua --</option>					
					<option value="1">Tanah</option>
					<option value="2">Semen</option>
					<option value="3">Keramik</option>
					<option value="4">Papan</option>
				</select> &nbsp;
				ATAU JENIS DINDING &nbsp;
				<select id="jenis_dinding" name="jenis_dinding">
					<option value="0">-- Semua --</option>
					<option value="1">Bambu/Rumbia</option>
					<option value="2">Papan</option>
					<option value="3">Tembok</option>
				</select> &nbsp;
				ATAU FASILITAS BUANG AIR BESAR &nbsp;
				<select id="fasilitas_wc" name="fasilitas_wc">
					<option value="0">-- Semua --</option>					
					<option value="1">Ada</option>
					<option value="2">Tidak Ada</option>
				</select>&nbsp;
				ATAU PENGGUNAAN BAHAN BAKAR MEMASAK &nbsp;
				<select id="bahan_bakar_masak" name="bahan_bakar_masak">
					<option value="0">-- Semua --</option>	
					<option value="1">Kayu Bakar/Arang</option>
					<option value="2">Minyak Tanah</option>
					<option value="3">Gas</option>
				</select> &nbsp;
				ATAU STATUS PEMBANGUNAN &nbsp;
				<select id="rumah_bantuan" name="rumah_bantuan">
					<option value="0">-- Semua --</option>			
					<option value="1">Permintaan Bantuan</option>
					<option value="2">Bukan Bantuan</option>
				</select> &nbsp;
				ATAU JUMLAH MAKAN DALAM 1 HARI &nbsp;
				<select id="jlh_makan" name="jlh_makan">
					<option value="0">-- Semua --</option>
					<option value="1">1 Kali</option>
					<option value="2">2 Kali</option>
					<option value="3">3 Kali</option>
				</select>&nbsp;
				ATAU KONSUMSI LAUK PAUK (IKAN/AYAM) TIAP HARI&nbsp;
				<select id="lauk_pauk" name="lauk_pauk">
					<option value="0">-- Semua --</option>
					<option value="1">Tidak Mengkonsumsi</option>
					<option value="2">Kadang-kadang</option>
					<option value="3">Selalu</option>
				</select>&nbsp;
				ATAU ATAU KONSUMSI DAGING/SUSU DALAM SEMINGGU&nbsp;
				<select id="daging" name="daging">
					<option value="0">-- Semua --</option>
					<option value="1">Tidak Mengkonsumsi</option>
					<option value="2">Kadang-kadang</option>
					<option value="3">Selalu</option>
				</select>&nbsp;
				ATAU KEMAMPUAN MEMBELI PAKAIAN BARU&nbsp;
				<select id="pakaian" name="pakaian">
					<option value="0">-- Semua --</option>
					<option value="1">&gt; 2 Tahun Sekali</option>
					<option value="2">1 Tahun Sekali</option>
					<option value="3">&lt; 1 Tahun Sekali</option>

				</select>
				<input type="hidden" name="ajax" value="<?php echo $grid_id;?>" />
			</div>
			<div id="report-filter">
				<div class="rows">
					<div class="span4 left">
						<input type="text" id="nama_kab" name ="nama_kab" value="" class="span7" "autocomplete"="off" placeholder="Kabupaten/Kota"/>
						<input type="hidden" id="no_kab" name ="no_kab" value="" />
					</div>
					<div class="span4">
						<input type="text" id="nama_kec" name ="nama_kec" value="" class="span7" "autocomplete"="off" placeholder="Kecamatan">
						<input type="hidden" id="no_kec" name ="no_kec" value="" />
					</div>
					<div class="span4 right">
						<input type="text" id="nama_gampong" name ="nama_gampong" value="" class="span7" "autocomplete"="off"  placeholder="Gampong"/>
						<input type="hidden" id="no_kel" name ="no_kel" value="" />
					
						<?php echo CHtml::ajaxLink(
							'Lihat',         
							array('rumah/semua'), 
							array(
								'update'=>'#laporan',
								//'beforeSend'=>'alert(\'send\')',
								'data' => 'js:$("#dlm-form").serialize()',
							),
							array(
								'class'=>'btn btn-primary',
							)
						); ?>
					</div>
				</div>
			</div>
			
		</div>
		<div id="report-content">
			<?php $this->renderPartial('_rumah', array('dataProvider'=>$dataProvider, 'grid_id' => $grid_id) ); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		if($("#nama_kab").size()> 0){
			$("#nama_kab").keyup(function(){
				parameter = "term=" + $('#nama_kab').val();
			});
			TextFieldAutoComplete("/general/getKabupaten", "nama_kab", "no_kab");
		}
			
		if($("#nama_kec").size()> 0){
			$("#nama_kec").keyup(function(){
				parameter = "term=" + $('#nama_kec').val() + "&id="+$("#no_kab").val();
			});
			TextFieldAutoComplete("/general/getKecamatanByKabupaten", "nama_kec", "no_kec");
		}
			
		if($("#nama_gampong").size()> 0){
			$("#nama_gampong").keyup(function(){
				parameter = "term=" + $('#nama_gampong').val() + "&id="+$("#no_kec").val();
			});
			TextFieldAutoComplete("/general/getGampongByKecamatan", "nama_gampong", "no_kel");
		}
	});
</script>