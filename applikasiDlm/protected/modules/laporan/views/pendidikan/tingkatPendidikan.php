<?php
/* @var $this PendidikanController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'dlm-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="rows center" id="report-wrapper">
		<!-- report header-->
		<div class="rows report-header">
			<div id="report-logo-provinsi" class="center">
				<img src="/images/logo-provinsi-aceh.png" alt="" />
			</div>
			<div id="report-title">DINAS REGISTRASI KEPENDUDUKAN ACEH</div>
			<div id="report-sub-title">
				DAFTAR PENDUDUK ACEH <br/>
				BERDASARKAN TINGKAT PENDIDIKAN &nbsp;
				<select id="jenjang_sekolah" name="jenjang_sekolah">
					<option value="">-- Semua --</option>
					<option value="Tidak/Belum Sekolah">Tidak/Belum Sekolah</option>
					<option value="Belum Tamat SD/Sederajat">Belum Tamat SD/Sederajat</option>
					<option value="Tamat SD/Sederajat">Tamat SD/Sederajat</option>
					<option value="Tamat SLTP/Sederajat">Tamat SLTP/Sederajat</option>
					<option value="Tamat SLTA/Sederajat">Tamat SLTA/Sederajat</option>
					<option value="Diploma I/II">Diploma I/II</option>
					<option value="Akademi/Diploma III/Sarjana Muda">Akademi/Diploma III/Sarjana Muda</option>
					<option value="Diploma IV/Strata I">Diploma IV/Strata I</option>
					<option value="Strata II">Strata II</option>
					<option value="Strata III">Strata III</option>
				</select>
				<input type="hidden" name="ajax" value="<?php echo $grid_id;?>" />
			</div>
			<div id="report-filter">
				<div class="rows">
					<div class="span4 left">
						<input type="text" id="nama_kab" name ="nama_kab" value="" class="span7" "autocomplete"="off" placeholder="Kabupaten/Kota"/>
						<input type="hidden" id="no_kab" name ="no_kab" value="" />
					</div>
					<div class="span4">
						<input type="text" id="nama_kec" name ="nama_kec" value="" class="span7" "autocomplete"="off" placeholder="Kecamatan">
						<input type="hidden" id="no_kec" name ="no_kec" value="" />
					</div>
					<div class="span4 right">
						<input type="text" id="nama_gampong" name ="nama_gampong" value="" class="span7" "autocomplete"="off"  placeholder="Gampong"/>
						<input type="hidden" id="no_kel" name ="no_kel" value="" />
					
						<?php echo CHtml::ajaxLink(
							'Lihat',         
							array('pendidikan/tingkatpendidikan'), 
							array(
								'update'=>'#laporan',
								//'beforeSend'=>'alert(\'send\')',
								'data' => 'js:$("#dlm-form").serialize()',
							),
							array(
								'class'=>'btn btn-primary',
							)
						); ?>
					</div>
				</div>
			</div>
			
		</div>
		<div id="report-content">
			<?php $this->renderPartial('_pendidikan', array('dataProvider'=>$dataProvider, 'grid_id' => $grid_id) ); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		if($("#nama_kab").size()> 0){
			$("#nama_kab").keyup(function(){
				parameter = "term=" + $('#nama_kab').val();
			});
			TextFieldAutoComplete("/general/getKabupaten", "nama_kab", "no_kab");
		}
			
		if($("#nama_kec").size()> 0){
			$("#nama_kec").keyup(function(){
				parameter = "term=" + $('#nama_kec').val() + "&id="+$("#no_kab").val();
			});
			TextFieldAutoComplete("/general/getKecamatanByKabupaten", "nama_kec", "no_kec");
		}
			
		if($("#nama_gampong").size()> 0){
			$("#nama_gampong").keyup(function(){
				parameter = "term=" + $('#nama_gampong').val() + "&id="+$("#no_kec").val();
			});
			TextFieldAutoComplete("/general/getGampongByKecamatan", "nama_gampong", "no_kel");
		}
	});
</script>