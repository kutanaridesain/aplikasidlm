<?php
/* @var $this PendidikanController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'header'=>'NAMA <br/>NIK<br/>NO. KK',
						'name'=>'nama_lgkp',
						'type'=>'html',
						'value'=>'ViewLaporanPendidikan::getNamaNikNoKk($data->nama_lgkp, $data->nik,$data->no_kk)',
					), 
					array(
						'header'=>'STATUS KELUARGA<br/>TANGGAL LAHIR',
						'name'=>'hub_keluarga',
						'type'=>'html',
						'value'=>'ViewLaporanPendidikan::getStatusKeluargaTanggalLahir($data->hub_keluarga, $data->tgl_lhr)',
					), 
					array(
						'header'=>'NIK IBU<br/> NAMA IBU',
						'name'=>'nik_nama_ibu',
						'type'=>'html',
						'value'=>'$data->nik_nama_ibu',
					),  
					array(
						'header'=>'NIK AYAH<br/> NAMA AYAH',
						'name'=>'nik_nama_ayah',
						'type'=>'html',
						'value'=>'$data->nik_nama_ayah',
					), 
					array(
						'header'=>'AGAMA',
						'name'=>'agama',
						'type'=>'html',
						'value'=>'ViewLaporanPendidikan::getAgama($data->agama)',
					), 
					array(
						'header'=>'JENJANG SEKOLAH<br/>THN MULAI TIDAK SEKOLAH',
						'name'=>'nama_sekolah',
						'type'=>'html',
						'value'=>'ViewLaporanPendidikan::getJenjangPendidikanTahun($data->jenjang_sekolah, $data->thn_putus_sekolah)',
					), 
					array(
						'header'=>'ALAMAT<br/>(GAMPONG, KEC, KAB)',
						'name'=>'alamat',
						'type'=>'html',
						'value'=>'$data->alamat',
					), 
				),
			));
		?>
	
	
