<?php
/* @var $this PekerjaanController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'header'=>'NAMA',
						'name'=>'nama_lgkp',
						'type'=>'html',
						'value'=>'$data->nama_lgkp',
					),  
					array(
						'header'=>'NIK',
						'name'=>'nik',
						'type'=>'html',
						'value'=>'$data->nik',
					),  
					array(
						'header'=>'STATUS KELUARGA',
						'name'=>'hub_keluarga',
						'type'=>'html',
						'value'=>'ViewLaporanPekerjaan::getStatusKeluarga($data->hub_keluarga)',
					),  					 
					array(
						'header'=>'TGL LAHIR',
						'name'=>'tgl_lhr',
						'type'=>'html',
						'value'=>'$data->tgl_lhr',
					),  
					array(
						'header'=>'AGAMA',
						'name'=>'agama',
						'type'=>'html',
						'value'=>'ViewLaporanPekerjaan::getAgama($data->agama)',
					),    
					array(
						'header'=>'PENDIDIKAN AKHIR',
						'name'=>'pendidikan',
						'type'=>'html',
						'value'=>'$data->pendidikan',
					),
					array(
						'header'=>'PEKERJAAN',
						'name'=>'nama_pekerjaan',
						'type'=>'html',
						'value'=>'$data->nama_pekerjaan',
					),					   
					array(
						'header'=>'STATUS KAWIN',
						'name'=>'stat_kwn',
						'type'=>'html',
						'value'=>'ViewLaporanPekerjaan::getStatusKawin($data->stat_kwn)',
					),
					array(
						'header'=>'ALAMAT<br/>(GP, KEC, KAB)',
						'name'=>'alamat',
						'type'=>'html',
						'value'=>'$data->alamat',
					), 
				),
			));
		?>
	
	
