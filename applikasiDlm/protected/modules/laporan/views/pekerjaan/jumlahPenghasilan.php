<?php
/* @var $this PekerjaanController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'dlm-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="rows center" id="report-wrapper">
		<!-- report header-->
		<div class="rows report-header">
			<div id="report-logo-provinsi" class="center">
				<img src="/images/logo-provinsi-aceh.png" alt="" />
			</div>
			<div id="report-title">DINAS REGISTRASI KEPENDUDUKAN ACEH</div>
			<div id="report-sub-title">
				DAFTAR PENDUDUK ACEH <br/>
				BERDASARKAN JUMLAH PENGHASILAN
				<select id="id" name="id">
					<option value="0">-- Semua --</option>
					<option value="1">Kurang Dari 1 Juta</option>
					<option value="2">Antara 1 Sampai Dengan 3 Juta</option>
					<option value="3">Lebih Dari 3 Juta</option>
				</select>
				<input type="hidden" name="ajax" value="<?php echo $grid_id;?>" />
			</div>
			<div id="report-filter">
				<div class="rows">
					<div class="span4 left">
						<input type="text" id="nama_kab" name ="nama_kab" value="" class="span7" "autocomplete"="off" placeholder="Kabupaten/Kota"/>
						<input type="hidden" id="no_kab" name ="no_kab" value="" />
					</div>
					<div class="span4">
						<input type="text" id="nama_kec" name ="nama_kec" value="" class="span7" "autocomplete"="off" placeholder="Kecamatan">
						<input type="hidden" id="no_kec" name ="no_kec" value="" />
					</div>
					<div class="span4 right">
						<input type="text" id="nama_gampong" name ="nama_gampong" value="" class="span7" "autocomplete"="off"  placeholder="Gampong"/>
						<input type="hidden" id="no_kel" name ="no_kel" value="" />
					
						<?php echo CHtml::ajaxLink(
							'Lihat',         
							array('pekerjaan/jumlahPenghasilan'), 
							array(
								'update'=>'#laporan',
								//'beforeSend'=>'alert(\'send\')',
								'data' => 'js:$("#dlm-form").serialize()',
							),
							array(
								'class'=>'btn btn-primary',
							)
						); ?>
					</div>
				</div>
			</div>
			
		</div>
		<div id="report-content">
			 <?php $this->renderPartial('_jumlahPenghasilan', array('dataProvider'=>$dataProvider, 'grid_id' => $grid_id) ); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(function(){
		if($("#nama_kab").size()> 0){
			$("#nama_kab").keyup(function(){
				parameter = "term=" + $('#nama_kab').val();
			});
			TextFieldAutoComplete("/general/getKabupaten", "nama_kab", "no_kab");
		}
			
		if($("#nama_kec").size()> 0){
			$("#nama_kec").keyup(function(){
				parameter = "term=" + $('#nama_kec').val() + "&id="+$("#no_kab").val();
			});
			TextFieldAutoComplete("/general/getKecamatanByKabupaten", "nama_kec", "no_kec");
		}
			
		if($("#nama_gampong").size()> 0){
			$("#nama_gampong").keyup(function(){
				parameter = "term=" + $('#nama_gampong').val() + "&id="+$("#no_kec").val();
			});
			TextFieldAutoComplete("/general/getGampongByKecamatan", "nama_gampong", "no_kel");
		}
	});
</script>