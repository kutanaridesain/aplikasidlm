<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
p{margin:0}
.section-title{
	margin-left:.25in;
	text-indent:-.25in;
	font-size:11pt;
}
.table-data{
	border-left:1px solid #000000;
	border-top:1px solid #000000;
}
.table-data td{
	border-right:1px solid #000000;
	border-bottom:1px solid #000000;
}
</style>
</head>

<body>
<div style="width:87%;padding:70px 0 70px 70px; font-size:10pt;">
<p align="center">
	<img width="79" height="91" src="images/logo.jpg" alt="pancacita.jpg" />
	<br/>
	<strong style="font-size:16pt">PEMERINTAH ACEH</strong>
</p>
<p align="center"><strong style="font-size:19pt">DINAS REGISTRASI KEPENDUDUKAN ACEH</strong></p>
<div style="border-top:4px solid #000;"></div>
<p align="center"><strong style="font-size:15pt">BIODATA IDENTITAS KEPALA KELUARGA</strong></p>
<br/>
<p><strong>A.</strong><strong class="section-title">IDENTITAS DIRI KEPALA KELUARGA</strong></p>
<br/>

<table cellpadding="0" cellspacing="0">
 <tbody><tr>
  <td width="0" height="0">&nbsp;</td>
  <td width="120">&nbsp;</td>
  <td width="2">&nbsp;</td>
  <td width="119">&nbsp;</td>
  <td width="3">&nbsp;</td>
  <td width="120">&nbsp;</td>
  <td width="43">&nbsp;</td>
  <td width="175">&nbsp;</td>
 </tr>
 <tr>
  <td height="11">&nbsp;</td>
  <td rowspan="3" align="left" valign="top"><img width="120" height="125" src="images/foto.png" alt="FOTO">&nbsp;</td>
  <td>&nbsp;</td>
  <td rowspan="3" align="left" valign="top"><img width="119" height="125" src="images/sidikjari.png" alt="SIDIK JARI">&nbsp;</td>
  <td>&nbsp;</td>
  <td rowspan="3" align="left" valign="top"><img width="120" height="125" src="images/tandatangan.png" alt="TANDA TANGAN">&nbsp;</td>
 </tr>
 <tr>
  <td height="63">&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td align="left" valign="top"><img width="175" height="63" src="images/barcode.png" alt="BARCODE">&nbsp;</td>
 </tr>
</tbody></table>

<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="205">NO KEPALA KELUARGA</td>
    <td>: .................................</td>
  </tr>
  <tr>
    <td>NIK/ NAMA KEPALA KELUARGA</td>
    <td>: ................................. / ........................................</td>
  </tr>
  <tr>
    <td>ALAMAT SEKARANG</td>
    <td>: ............................................................</td>
  </tr>
  <tr>
    <td>KABUPATEN/KOTA</td>
    <td>: ................................. / ........................................</td>
  </tr>
  <tr>
    <td>KECAMATAN</td>
    <td>: ............................................................</td>
  </tr>
  <tr>
    <td>GAMPONG</td>
    <td>: ............................................................</td>
  </tr>
</table>
<br/>
<p><strong>B.</strong><strong class="section-title">ANGOTA KELUARGA</strong></p>
<br/>
<table cellspacing="0" cellpadding="0" class="table-data">
  <tbody>
    <tr bgcolor="#D9D9D9">
      <td width="32" valign="middle" align="center">No</td>
      <td width="175" valign="middle" align="center">Nama</td>
      <td width="85" valign="middle" align="center">NIK</td>
      <td width="67" valign="middle" align="center">Jenis Kelamin</td>
      <td width="78" valign="middle" align="center">Tempat / Tgl Lahir</td>
      <td width="80" valign="middle" align="center">Hubungan Keluarga</td>
      <td width="94" valign="middle" align="center">Agama</td>
    </tr>
    <tr>
      <td width="32" valign="middle" align="center">&nbsp;</td>
      <td width="175" valign="middle" align="center">&nbsp;</td>
      <td width="85" valign="middle" align="center">&nbsp;</td>
      <td width="67" valign="middle" align="center">&nbsp;</td>
      <td width="78" valign="middle" align="center">&nbsp;</td>
      <td width="80" valign="middle" align="center">&nbsp;</td>
      <td width="94" valign="middle" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td width="32" valign="middle" align="center">&nbsp;</td>
      <td width="175" valign="middle" align="center">&nbsp;</td>
      <td width="85" valign="middle" align="center">&nbsp;</td>
      <td width="67" valign="middle" align="center">&nbsp;</td>
      <td width="78" valign="middle" align="center">&nbsp;</td>
      <td width="80" valign="middle" align="center">&nbsp;</td>
      <td width="94" valign="middle" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td width="32" valign="middle" align="center">&nbsp;</td>
      <td width="175" valign="middle" align="center">&nbsp;</td>
      <td width="85" valign="middle" align="center">&nbsp;</td>
      <td width="67" valign="middle" align="center">&nbsp;</td>
      <td width="78" valign="middle" align="center">&nbsp;</td>
      <td width="80" valign="middle" align="center">&nbsp;</td>
      <td width="94" valign="middle" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td width="32" valign="middle" align="center">&nbsp;</td>
      <td width="175" valign="middle" align="center">&nbsp;</td>
      <td width="85" valign="middle" align="center">&nbsp;</td>
      <td width="67" valign="middle" align="center">&nbsp;</td>
      <td width="78" valign="middle" align="center">&nbsp;</td>
      <td width="80" valign="middle" align="center">&nbsp;</td>
      <td width="94" valign="middle" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td width="32" valign="middle" align="center">&nbsp;</td>
      <td width="175" valign="middle" align="center">&nbsp;</td>
      <td width="85" valign="middle" align="center">&nbsp;</td>
      <td width="67" valign="middle" align="center">&nbsp;</td>
      <td width="78" valign="middle" align="center">&nbsp;</td>
      <td width="80" valign="middle" align="center">&nbsp;</td>
      <td width="94" valign="middle" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td width="32" valign="middle" align="center">&nbsp;</td>
      <td width="175" valign="middle" align="center">&nbsp;</td>
      <td width="85" valign="middle" align="center">&nbsp;</td>
      <td width="67" valign="middle" align="center">&nbsp;</td>
      <td width="78" valign="middle" align="center">&nbsp;</td>
      <td width="80" valign="middle" align="center">&nbsp;</td>
      <td width="94" valign="middle" align="center">&nbsp;</td>
    </tr>
  </tbody>
</table>
<br/>
<p>Jika dalam keluarga anda terdapat status anak yatim, piatu, yatim piatu dan miskin yang tinggal di dalam panti asuh sebutkan : </p>
<br/>
<table class="table-data" cellspacing="0" cellpadding="0">
  <tbody>
    <tr bgcolor="#D9D9D9">
      <td width="32" valign="middle" align="center">No</td>
      <td width="148" valign="middle" align="center">N a m a</td>
      <td width="57" valign="middle" align="center">Agama</td>
      <td width="85" valign="middle" align="center">Sekolah</td>
      <td width="47" valign="middle" align="center">Kelas</td>
      <td width="95" valign="middle" align="center">Nama Panti Asuh</td>
      <td width="152" valign="middle" align="center">Lokasi Panti Asuh (Desa, Kec,Kab/Kota)</td>
    </tr>
    <tr>
      <td width="32" valign="middle" align="center">&nbsp;</td>
      <td width="148" valign="middle" align="center">&nbsp;</td>
      <td width="57" valign="middle" align="center">&nbsp;</td>
      <td width="85" valign="middle" align="center">&nbsp;</td>
      <td width="47" valign="middle" align="center">&nbsp;</td>
      <td width="95" valign="middle" align="center">&nbsp;</td>
      <td width="152" valign="middle" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td width="32" valign="middle" align="center">&nbsp;</td>
      <td width="148" valign="middle" align="center">&nbsp;</td>
      <td width="57" valign="middle" align="center">&nbsp;</td>
      <td width="85" valign="middle" align="center">&nbsp;</td>
      <td width="47" valign="middle" align="center">&nbsp;</td>
      <td width="95" valign="middle" align="center">&nbsp;</td>
      <td width="152" valign="middle" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td width="32" valign="middle" align="center">&nbsp;</td>
      <td width="148" valign="middle" align="center">&nbsp;</td>
      <td width="57" valign="middle" align="center">&nbsp;</td>
      <td width="85" valign="middle" align="center">&nbsp;</td>
      <td width="47" valign="middle" align="center">&nbsp;</td>
      <td width="95" valign="middle" align="center">&nbsp;</td>
      <td width="152" valign="middle" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td width="32" valign="middle" align="center">&nbsp;</td>
      <td width="148" valign="middle" align="center">&nbsp;</td>
      <td width="57" valign="middle" align="center">&nbsp;</td>
      <td width="85" valign="middle" align="center">&nbsp;</td>
      <td width="47" valign="middle" align="center">&nbsp;</td>
      <td width="95" valign="middle" align="center">&nbsp;</td>
      <td width="152" valign="middle" align="center">&nbsp;</td>
    </tr>
  </tbody>
</table>
</div>
<page>
<div style="width:87%;padding:70px 0 70px 70px; font-size:10pt;">
<p><strong>C.</strong><strong class="section-title">RUMAH</strong></p>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="205">NO KEPALA KELUARGA</td>
    <td>: .................................</td>
  </tr>
  <tr>
    <td>NIK/ NAMA KEPALA KELUARGA</td>
    <td>: ................................. / ........................................</td>
  </tr>
  <tr>
    <td>ALAMAT SEKARANG</td>
    <td>: ............................................................</td>
  </tr>
  <tr>
    <td>KABUPATEN/KOTA</td>
    <td>: ................................. / ........................................</td>
  </tr>
  <tr>
    <td>KECAMATAN</td>
    <td>: ............................................................</td>
  </tr>
  <tr>
    <td>GAMPONG</td>
    <td>: ............................................................</td>
  </tr>
</table>

</div>
</page>

</body>
</html>
