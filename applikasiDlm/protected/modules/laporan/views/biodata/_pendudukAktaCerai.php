<?php
/* @var $this BiodataController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'header'=>'NAMA<br/>NIK',
						'name'=>'nama_lgkp',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getNamaNik($data->nama_lgkp, $data->nik)',
					), 
					array(
						'header'=>'STATUS KELUARGA<br/>AGAMA',
						'name'=>'hub_keluarga',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getStatusKeluargaAgama($data->hub_keluarga, $data->agama)',
					), 
					array(
						'header'=>'TANGGAL LAHIR <br/>PEKERJAAN',
						'name'=>'tgl_lhr',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getTglLahirPekerjaan($data->tgl_lhr, $data->pekerjaan)',
					), 
					array(
						'header'=>'STATUS KAWIN <br/>AKTA KAWIN',
						'name'=>'stat_kwn',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getStatusKawinAktaKawin($data->stat_kwn, $data->no_akta_kwn)',
					), 
					array(
						'header'=>'TANGGAL PERNIKAHAN<br/>AKTA PERCERAIAN',
						'name'=>'tgl_kwn',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getTglPernikahanAktaCerai($data->tgl_kwn, $data->akta_crai)',
					),
					array(
						'header'=>'NO AKTA CERAI<br/>TANGGAL PERCERAIAN',
						'name'=>'tgl_kwn',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getNoAktaCeraiTglCerai($data->no_akta_crai, $data->tgl_crai)',
					),
					array(
						'header'=>'NIK IBU KANDUNG<br/> NAMA IBU KANDUNG',
						'name'=>'nik_nama_ibu',
						'type'=>'html',
						'value'=>'$data->nik_nama_ibu',
					),  
					array(
						'header'=>'NIK AYAH KANDUNG<br/> NAMA AYAH KANDUNG',
						'name'=>'nik_nama_ayah',
						'type'=>'html',
						'value'=>'$data->nik_nama_ayah',
					), 
					array(
						'header'=>'ALAMAT<br/>(GAMPONG, KEC, KAB)',
						'name'=>'alamat',
						'type'=>'html',
						'value'=>'$data->alamat',
					),         
					
				),
			));
		?>
	
	
