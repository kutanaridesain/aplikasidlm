<?php
/* @var $this BiodataController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'header'=>'NAMA',
						'name'=>'nama_lgkp',
						'type'=>'html',
						'value'=>'$data->nama_lgkp',
					), 
					array(
						'header'=>'NIK',
						'name'=>'nik',
						'type'=>'html',
						'value'=>'$data->nik',
					), 
					array(
						'header'=>'STATUS KELUARGA',
						'name'=>'hub_keluarga',
						'type'=>'html',
						'value'=>'DlmActiveRecord::getHubunganKeluargaById($data->hub_keluarga)',
					), 
					array(
						'header'=>'TGL LAHIR',
						'name'=>'tgl_lhr',
						'type'=>'html',
						'value'=>'$data->tgl_lhr',
					), 
					array(
						'header'=>'TANGGAL MENINGGAL',
						'name'=>'tgl_meninggal',
						'type'=>'html',
						'value'=>'$data->tgl_meninggal',
					),
					array(
						'header'=>'NO AKTA KEMATIAN',
						'name'=>'no_akta_kematian',
						'type'=>'html',
						'value'=>'$data->no_akta_kematian',
					),
					array(
						'header'=>'NIK IBU KANDUNG<br/> NAMA IBU KANDUNG',
						'name'=>'nik_nama_ibu',
						'type'=>'html',
						'value'=>'$data->nik_nama_ibu',
					),  
					array(
						'header'=>'NIK AYAH KANDUNG<br/> NAMA AYAH KANDUNG',
						'name'=>'nik_nama_ayah',
						'type'=>'html',
						'value'=>'$data->nik_nama_ayah',
					), 
					array(
						'header'=>'ALAMAT<br/>(GAMPONG, KEC, KAB)',
						'name'=>'alamat',
						'type'=>'html',
						'value'=>'$data->alamat',
					),           
					
				),
			));
		?>
	
	
