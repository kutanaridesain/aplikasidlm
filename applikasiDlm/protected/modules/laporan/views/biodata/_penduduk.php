<?php
/* @var $this BiodataController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'header'=>'NAMA <br/>NIK<br/>STATUS KELUARGA',
						'name'=>'nama_lgkp',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getNamaNikStatusKeluarga($data->nama_lgkp, $data->nik, $data->hub_keluarga)',
					),  
					array(
						'header'=>'AGAMA <br/>PENDIDIKAN TERAKHIR <br/>PEKERJAAN',
						'name'=>'agama',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getAgamaPendidikanPekerjaan($data->agama, $data->pendidikan, $data->pekerjaan)',
					), 
					array(
						'header'=>'TANGGAL LAHIR <br/>AKTA LAHIR <br/> NO AKTA LAHIR',
						'name'=>'akta_lhr',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getDataLahir($data->tgl_lhr, $data->akta_lhr, $data->no_akta_lhr)',
					),  
					 
					array(
						'header'=>'STATUS KAWIN <br/>AKTA KAWIN<br/>TANGGAL PERNIKAHAN',
						'name'=>'akta_kwn',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getDataKawin($data->stat_kwn, $data->no_akta_kwn, $data->tgl_kwn)',
					),  
					array(
						'header'=>'AKTA CERAI<br/>NO AKTA CERAI<br/>TANGGAL PERCERAIAN',
						'name'=>'akta_crai',
						'type'=>'raw',
						'value'=>'ViewLaporanBiodata::getDataCerai($data->akta_crai, $data->no_akta_crai, $data->tgl_crai)',
					), 
					array(
						'header'=>'KELAINAN FISIK<br/>PENYANDANG CACAT',
						'name'=>'pnydng_cct',
						'type'=>'html',
						'value'=>'DlmActiveRecord::getNamaStatusYaTidakById($data->pnydng_cct)',
					),
					array(
						'header'=>'NIK IBU KANDUNG<br/> NAMA IBU KANDUNG',
						'name'=>'nik_nama_ibu',
						'type'=>'html',
						'value'=>'$data->nik_nama_ibu',
					),  
					array(
						'header'=>'NIK AYAH KANDUNG<br/> NAMA AYAH KANDUNG',
						'name'=>'nik_nama_ayah',
						'type'=>'html',
						'value'=>'$data->nik_nama_ayah',
					),   
					array(
						'header'=>'MENINGGAL<br/>TANGGAL<br/>NO AKTA',
						'name'=>'stat_hidup',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getDataMeninggal($data->stat_hidup, $data->tgl_meninggal, $data->no_akta_kematian)',
					),  
				),
			));
		?>
	
	
