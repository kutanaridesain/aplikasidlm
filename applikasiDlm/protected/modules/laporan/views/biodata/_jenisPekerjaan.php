<?php
/* @var $this BiodataController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'header'=>'NAMA <br/>NIK',
						'name'=>'nama_lgkp',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getNamaNik($data->nama_lgkp, $data->nik)',
					),  
					array(
						'header'=>'TANGGAL LAHIR<br/>STATUS KELUARGA',
						'name'=>'tgl_lhr',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getTglLahirStatusKeluarga($data->tgl_lhr, $data->hub_keluarga)',
					), 
					array(
						'header'=>'AGAMA<br/>PENDIDIKAN TERAKHIR',
						'name'=>'agama',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getAgamaPendidikan($data->agama, $data->pendidikan)',
					),
					array(
						'header'=>'STATUS KAWIN <br/>PEKERJAN TERAKHIR',
						'name'=>'stat_kwn',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getStatusKawinPekerjaanTerakhir($data->stat_kwn, $data->pekerjaan)',
					),  
					array(
						'header'=>'KELAINAN FISIK<br/>PENYANDANG CACAT',
						'name'=>'pnydng_cct',
						'type'=>'html',
						'value'=>'DlmActiveRecord::getNamaStatusYaTidakById($data->pnydng_cct)',
					),
					array(
						'header'=>'NIK IBU KANDUNG<br/> NAMA IBU KANDUNG',
						'name'=>'nik_nama_ibu',
						'type'=>'html',
						'value'=>'$data->nik_nama_ibu',
					),  
					array(
						'header'=>'NIK AYAH KANDUNG<br/> NAMA AYAH KANDUNG',
						'name'=>'nik_nama_ayah',
						'type'=>'html',
						'value'=>'$data->nik_nama_ayah',
					),   
					array(
						'header'=>'MENINGGAL<br/>TANGGAL<br/>NO AKTA',
						'name'=>'stat_hidup',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getDataMeninggal($data->stat_hidup, $data->tgl_meninggal, $data->no_akta_kematian)',
					),  
				),
			));
		?>
	
	
