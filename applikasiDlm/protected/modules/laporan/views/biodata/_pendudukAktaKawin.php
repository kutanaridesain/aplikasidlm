<?php
/* @var $this BiodataController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'header'=>'NAMA<br/>NIK',
						'name'=>'nama_lgkp',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getNamaNik($data->nama_lgkp, $data->nik)',
					), 
					array(
						'header'=>'STATUS KELUARGA<br/>AGAMA',
						'name'=>'nik',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getStatusKeluargaAgama($data->hub_keluarga, $data->agama)',
					), 
					array(
						'header'=>'PENDIDIKAN TERAKHIR <br/>PEKERJAAN',
						'name'=>'pekerjaan',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getPendidikanPekerjaan($data->pendidikan, $data->pekerjaan)',
					), 
					array(
						'header'=>'TGL LAHIR <br/>STATUS KAWIN',
						'name'=>'tgl_lhr',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getTglLahirStatusKawin($data->tgl_lhr, $data->stat_kwn)',
					),
					 
					array(
						'header'=>'AKTA KAWIN <br/>TANGGAL PERNIKAHAN',
						'name'=>'tgl_lhr',
						'type'=>'html',
						'value'=>'ViewLaporanBiodata::getAktaKawinTglPernikahan($data->no_akta_kwn, $data->tgl_kwn)',
					),
					array(
						'header'=>'NIK IBU KANDUNG<br/> NAMA IBU KANDUNG',
						'name'=>'nik_nama_ibu',
						'type'=>'html',
						'value'=>'$data->nik_nama_ibu',
					),  
					array(
						'header'=>'NIK AYAH KANDUNG<br/> NAMA AYAH KANDUNG',
						'name'=>'nik_nama_ayah',
						'type'=>'html',
						'value'=>'$data->nik_nama_ayah',
					), 
					array(
						'header'=>'ALAMAT<br/>(GAMPONG, KEC, KAB)',
						'name'=>'alamat',
						'type'=>'html',
						'value'=>'$data->alamat',
					),         
					
				),
			));
		?>
	
	
