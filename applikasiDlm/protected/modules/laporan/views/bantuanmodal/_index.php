<?php
/* @var $this BantuanModalController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(					
					array(
						'header'=>'NAMA<br/>NO KK/NIK',
						'name'=>'nama_lgkp',
						'type'=>'html',
						'value'=>'ViewLaporanBantuanSosial::getNamaNoKkNik($data->nama_lgkp, $data->no_kk, $data->nik)',
					), 					
					array(
						'header'=>'STATUS KELUARGA<br/>TANGGAL LAHIR',
						'name'=>'hub_keluarga',
						'type'=>'html',
						'value'=>'ViewLaporanBantuanSosial::getStatusKeluargaTanggalLahir($data->hub_keluarga, $data->tgl_lhr)',
					),
					array(
						'header'=>'PEKERJAAN<br/>STATUS KAWIN',
						'name'=>'pekerjaan',
						'type'=>'html',
						'value'=>'ViewLaporanBantuanSosial::getPekerjaanTerakhirStatusKawin($data->pekerjaan, $data->stat_kwn)',
					),		
					array(
						'header'=>'ALAMAT<br/>(GP, KEC, KAB)',
						'name'=>'alamat',
						'type'=>'html',
						'value'=>'$data->alamat',
					), 										
					array(
						'header'=>'KATEGORI BANTUAN<br/>JENIS BANTUAN',
						'name'=>'kategory_bantuan_modal',
						'type'=>'html',
						'value'=>'ViewLaporanBantuanModal::getKategoriJenis($data->kategory_bantuan_modal, $data->jenis_bantuan_modal)',
					), 			
					array(
						'header'=>'NAMA KELOMPOK',
						'name'=>'nama_kelompok',
						'type'=>'html',
						'value'=>'$data->nama_kelompok',
					), 											
					array(
						'header'=>'INSTANSI PEMBERI<br/>TAHUN TERIMA BANTUAN',
						'name'=>'nama_instansi',
						'type'=>'html',
						'value'=>'ViewLaporanBantuanModal::getInstansiTahun($data->nama_instansi, $data->tahun_diberikan)',
					),							
					array(
						'header'=>'NAMA KOPERASI',
						'name'=>'nama_koperasi',
						'type'=>'html',
						'value'=>'$data->nama_koperasi',
					), 		
				),
			));
		?>
	
	
