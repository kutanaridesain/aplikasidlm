<?php
/* @var $this PekerjaanController */
?>

		<?php 
		    $this->widget('bootstrap.widgets.TbGridView', array(
				'dataProvider' =>$dataProvider,
				'id' => $grid_id,
				'type' => 'striped bordered condensed',
				'summaryText' => false,
				'columns'=>array(
					array(
						'header'=>'NAMA<br/>NIK',
						'name'=>'nama_lgkp',
						'type'=>'html',
						'value'=>'ViewLaporanKesehatan::getNamaNik($data->nama_lgkp, $data->nik)',
					),  
					array(
						'header'=>'STATUS KELUARGA<br/>TANGGAL LAHIR',
						'name'=>'hub_keluarga',
						'type'=>'html',
						'value'=>'ViewLaporanKesehatan::getStatusKeluargaTanggalLahir($data->hub_keluarga, $data->tgl_lhr)',
					), 					 
					array(
						'header'=>'AGAMA<br/>PENDIDIKAN AKHIR',
						'name'=>'agama',
						'type'=>'html',
						'value'=>'ViewLaporanKesehatan::getAgamaPendidikan($data->agama, $data->pendidikan)',
					), 					 
					array(
						'header'=>'PEKERJAAN<br/>STATUS KAWIN',
						'name'=>'agama',
						'type'=>'html',
						'value'=>'ViewLaporanKesehatan::getPekerjaanStatusKawin($data->pekerjaan, $data->stat_kwn)',
					), 						
					array(
						'header'=>'ALAMAT<br/>(GP, KEC, KAB)',
						'name'=>'alamat',
						'type'=>'html',
						'value'=>'$data->alamat',
					), 			 
					array(
						'header'=>'NO KARTU JKA<br/>NO KARTU JAMKESMAS',
						'name'=>'no_jka',
						'type'=>'html',
						'value'=>'ViewLaporanKesehatan::getNoKartuJkaJamkesmas($data->no_jka, $data->no_jamkesmas)',
					), 		 
					array(
						'header'=>'NOMOR <br/> ASURANSI LAIN',
						'name'=>'no_asuransilain1',
						'type'=>'html',
						'value'=>'ViewLaporanKesehatan::getNoAsuransiLain($data->no_asuransilain1, $data->no_asuransilain2)',
					), 
					 		 
					array(
						'header'=>'RUJUKAN KE PUSKESMAS <br/>PENYAKIT YANG SERING DI DERITA',
						'name'=>'id_puskesmas',
						'type'=>'html',
						'value'=>'ViewLaporanKesehatan::getPuskesmasPenyakit($data->nama_puskesmas, $data->nama_penyakit, $data->nama_penyakit1, $data->nama_penyakit2)',
					), 
					
				),
			));
		?>
	
	
