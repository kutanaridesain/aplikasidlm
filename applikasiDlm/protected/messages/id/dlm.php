<?php

return array(
	'required field'=>'Field dengan tanda <span class="required">*</span> harus diisi.',
	'advanced search'=>'Pencarian Lanjut <i class="icon-search"></i>',
	'confirm delete'=>'Apakah anda yakin ingin menghapus item ini?'
);