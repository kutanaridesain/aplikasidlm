<?php
$current_domain='applikasi-dlm.local';
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('messages', dirname(__FILE__).'/../messages');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Aplikasi DLM',
    'theme'=>'ijo',
    'language'=>'id',

    'aliases' => array(
		'xupload' => 'ext.xupload'
	),

	// preloading 'log' component
	'preload'=>array('log', 'bootstrap'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'dlmadmin!',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
            'generatorPaths'=>array(
                'bootstrap.gii'
            ),
		),
        'manajemendata',
        'laporan',
	),

	// application components
	'components'=>array(
		'coreMessages'=>array(
			'basePath'=>Yiibase::getPathOfAlias('messages'),
		),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class' => 'WebUser',
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>'
			),
		),
        'db'=>array(
            'connectionString'=>'oci:dlm=localhost:1521/orcl;charset=UTF8',
            'username'=>'dlm',
            'password'=>'dlmdba',
            'schemaCachingDuration' => 3600,//a hour 60x60
    		'emulatePrepare' => true,
    		// 'class'=>'CDbConnection',	
        ),
        'dbOracle'=>array(
        	'class'=>'ext.oci8Pdo.OciDbConnection',
        	'connectionString'=>'oci:dlm=localhost:1521/orcl;charset=UTF8',
            'username'=>'dlm',
            'password'=>'dlmdba',
            'schemaCachingDuration' => 3600,//a hour 60x60
    		'emulatePrepare' => true,
    		'enableProfiling' => true,
            'enableParamLogging' => true,
    	),
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
		// uncomment the following to use a MySQL database
		// 'dbMysql'=>array(
		// 	'class'=>'system.db.CDbConnection',
		// 	'connectionString' => 'mysql:host=localhost;dbname=dlm',
		// 	'emulatePrepare' => true,
		// 	'username' => 'root',
		// 	'password' => '',
		// 	'charset' => 'utf8',
		// ),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				// array(
				// 	'class'=>'CWebLogRoute',
				// ),
			),
		),
        'bootstrap'=>array(
            'class'=>'ext.bootstrap.components.Bootstrap',
//            'responsiveCss'=>true,
        ),
        //fery -> oracle is fu**ing slow so we need to make a cache
        'cache'  => array(
            'class'  => 'system.caching.CFileCache',
		    // 'class' => 'CDbCache',
		    // 'servers'=>array(
		    //     array(
		    //         'host'=>'localhost',
		    //         'port'=>11211,
		    //     ),
		    // ),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);