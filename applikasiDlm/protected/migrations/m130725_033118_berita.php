<?php

class m130725_033118_berita extends CDbMigration
{
	public function up()
	{
		$this->createTable('berita', array(
				'id' => 'pk',
				'content' => 'text',
				'title' => 'string NOT NULL'
			));
	}

	public function down()
	{
		$this->dropTable('berita');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}