<?php

/**
 * This is the model class for table "KECAMATAN".
 *
 * The followings are the available columns in table 'KECAMATAN':
 * @property integer $id
 * @property integer $id_kab
 * @property string $nama_kec
 * @property string $ibukota
 * @property string $alamat
 * @property string $camat
 * @property string $visi
 * @property string $misi
 * @property string $koordinat_x
 * @property string $koordinat_y
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA[] $bIODATAs
 * @property KABUPATEN $idKab
 * @property USER $user
 * @property GAMPONG[] $gAMPONGs
 */
class Kecamatan extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Kecamatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'KECAMATAN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_kab, nama_kab, nama_kec, ibukota, alamat, camat, visi, misi, koordinat_x, koordinat_y', 'required'),
			array('id_kab, userid', 'numerical', 'integerOnly'=>true),
			array('nama_kec, ibukota, camat', 'length', 'max'=>200),
			array('alamat, koordinat_y', 'length', 'max'=>1000),
			array('visi, misi', 'length', 'max'=>4000),
			array('koordinat_x', 'length', 'max'=>100),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_kab, nama_kec, ibukota, alamat, camat, visi, misi, koordinat_x, koordinat_y', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bIODATAs' => array(self::HAS_MANY, 'BIODATA', 'no_kec'),
			'idKab' => array(self::BELONGS_TO, 'KABUPATEN', 'id_kab'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
			'gAMPONGs' => array(self::HAS_MANY, 'GAMPONG', 'id_kec'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_kab' => 'Kabupaten/Kota',
			'nama_kab' => 'Kabupaten/Kota',
			'nama_kec' => 'Kecamatan',
			'ibukota' => 'Ibukota',
			'alamat' => 'Alamat',
			'camat' => 'Nama Camat',
			'visi' => 'Visi',
			'misi' => 'Misi',
			'koordinat_x' => 'Koordinat Longitude',
			'koordinat_y' => 'Koordinat Latitude',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_kab',$this->id_kab);
		$criteria->compare('nama_kec',$this->nama_kec,true);
		$criteria->compare('ibukota',$this->ibukota,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('camat',$this->camat,true);
		$criteria->compare('visi',$this->visi,true);
		$criteria->compare('misi',$this->misi,true);
		$criteria->compare('koordinat_x',$this->koordinat_x,true);
		$criteria->compare('koordinat_y',$this->koordinat_y,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	/* get kecamatan for dropdown */
	public static function getOptionList()
	{
		$sql = 'SELECT "id" as "id", "nama_kec" as "name" 
				FROM kecamatan 
				ORDER BY "nama_kec" ';
		
		$arrList = CHtml::listData( Yii::app()->db->createCommand($sql)->queryAll(), 'id' , 'name');
		return $arrList;
	}
	
	public static function getById($id)
	{
		$sql = 'SELECT "nama_kec" as "name"  
				FROM kecamatan 
				WHERE  "id" = '. $id. ' 
				ORDER BY "nama_kec" ';
		
		$name = "";
		$arrName = Yii::app()->db->createCommand($sql)->queryRow();
		if(count($arrName) == 1){
			$name = $arrName["name"];
		}
		return $name;
	}
	
	/* kabupaten */
	private $nama_kab;
	public function setNama_kab($name) {
	  $this->nama_kab = $name;
	}

	public function getNama_kab() {
		if($this->idKab != null){
			$this->nama_kab = $this->idKab->nama_kabkot;
		}
		return $this->nama_kab;
	}
}