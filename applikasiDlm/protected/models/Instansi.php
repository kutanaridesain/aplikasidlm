<?php

/**
 * This is the model class for table "INSTANSI".
 *
 * The followings are the available columns in table 'INSTANSI':
 * @property integer $id
 * @property integer $id_kab
 * @property string $nama_instansi
 * @property string $alamat_instansi
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupate
 *
 * The followings are the available model relations:
 * @property KABUPATEN $idKab
 * @property USER $user
 * @property ZAKAT[] $zAKATs
 * @property BANTUANSOSIAL[] $bANTUANSOSIALs
 * @property BANTUANMODAL[] $bANTUANMODALs
 * @property BANTUANLAINNYA[] $bANTUANLAINNYAs
 */
class Instansi extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Instansi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'INSTANSI';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kab, nama_instansi, alamat_instansi', 'required'),
			array('id_kab', 'numerical', 'integerOnly'=>true),
			array('nama_instansi', 'length', 'max'=>300),
			array('alamat_instansi', 'length', 'max'=>1000),
			array('tglupate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_kab, nama_instansi, alamat_instansi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idKab' => array(self::BELONGS_TO, 'KABUPATEN', 'id_kab'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
			'zAKATs' => array(self::HAS_MANY, 'ZAKAT', 'id_instansi'),
			'bANTUANSOSIALs' => array(self::HAS_MANY, 'BANTUANSOSIAL', 'id_instansi'),
			'bANTUANMODALs' => array(self::HAS_MANY, 'BANTUANMODAL', 'id_instansi'),
			'bANTUANLAINNYAs' => array(self::HAS_MANY, 'BANTUANLAINNYA', 'id_instansi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_kab' => 'Nama Kabupaten',
			'nama_kab' => 'Nama Kabupaten',
			'nama_instansi' => 'Nama Instansi',
			'alamat_instansi' => 'Alamat Instansi',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupate' => 'Tglupate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$roleid = Yii::app()->user->getRoleId();

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_kab',$this->id_kab);
		$criteria->compare('nama_instansi',$this->nama_instansi,true);
		$criteria->compare('alamat_instansi',$this->alamat_instansi,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupate',$this->tglupate,true);
		if($roleid>1){
			$kabid = Yii::app()->user->getKabupatenId();
			$criteria->addCondition(
				'"id_kab" = ' . $kabid
			);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	protected function afterFind()
	{
		$roleid = Yii::app()->user->getRoleId();
		
		$act = Yii::app()->controller->action->id;
		
		if( $act != "index"){
			if($roleid>1){
				$kabid = Yii::app()->user->getKabupatenId();
				if($this->id_kab != $kabid){	
					throw new CHttpException(401,'Tidak ada hak akses.');
				}
			}
		}
	}
	
	/* get instansi for dropdown */
	public static function getOptionList()
	{
		$sql = 'SELECT "id" as "id", "nama_instansi" as "name" 
				FROM instansi 
				ORDER BY "nama_instansi" ';
		
		$arrList = CHtml::listData( Yii::app()->db->createCommand($sql)->queryAll(), 'id' , 'name');
		return $arrList;
	}
	
	public static function getById($id)
	{
		$sql = 'SELECT "nama_instansi" as "name"  
				FROM instansi 
				WHERE  "id" = '. $id. ' 
				ORDER BY "nama_instansi" ';
		
		$name = "";
		$arrName = Yii::app()->db->createCommand($sql)->queryRow();
		if(count($arrName) == 1){
			$name = $arrName["name"];
		}
		return $name;
	}
	
	/* kabupaten */
	private $nama_kab;
	public function setNama_kab($name) {
	  $this->nama_kab = $name;
	}

	public function getNama_kab() {
		if($this->idKab != null){
			$this->nama_kab = $this->idKab->nama_kabkot;
		}
		return $this->nama_kab;
	}
}