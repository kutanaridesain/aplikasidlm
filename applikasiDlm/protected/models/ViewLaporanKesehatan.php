<?php

/**
 * This is the model class for table "VIEW_LAPORAN_KESEHATAN".
 *
 * The followings are the available columns in table 'VIEW_LAPORAN_KESEHATAN':
 * @property string $nama_lgkp
 * @property string $nik
 * @property string $hub_keluarga
 * @property string $tgl_lhr
 * @property integer $agama
 * @property string $pendidikan
 * @property string $pekerjaan
 * @property integer $stat_kwn
 * @property string $no_kk
 * @property string $no_jka
 * @property string $no_jamkesmas
 * @property string $asuransilain1
 * @property string $no_asuransilain1
 * @property string $asuransilain2
 * @property string $no_asuransilain2
 * @property integer $id_penyakit
 * @property string $nama_penyakit
 * @property integer $id_penyakit1
 * @property string $nama_penyakit1
 * @property integer $id_penyakit2
 * @property string $nama_penyakit2
 * @property integer $tgl_sakit
 * @property string $id_puskesmas
 * @property string $nama_puskesmas
 * @property string $alamat
 */
class ViewLaporanKesehatan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewLaporanKesehatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VIEW_LAPORAN_KESEHATAN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_lgkp, nik, tgl_lhr, agama, stat_kwn, no_kk, id_penyakit, tgl_sakit, id_puskesmas, nama_puskesmas', 'required'),
			array('agama, stat_kwn, id_penyakit, id_penyakit1, id_penyakit2, tgl_sakit', 'numerical', 'integerOnly'=>true),
			array('nama_lgkp, nama_puskesmas', 'length', 'max'=>200),
			array('nik, no_kk, no_jka, no_jamkesmas, asuransilain1, no_asuransilain1, asuransilain2, no_asuransilain2', 'length', 'max'=>100),
			array('hub_keluarga, pendidikan, pekerjaan, nama_penyakit, nama_penyakit1, nama_penyakit2', 'length', 'max'=>4000),
			array('id_puskesmas', 'length', 'max'=>40),
			array('alamat', 'length', 'max'=>620),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nama_lgkp, nik, hub_keluarga, tgl_lhr, agama, pendidikan, pekerjaan, stat_kwn, no_kk, no_jka, no_jamkesmas, asuransilain1, no_asuransilain1, asuransilain2, no_asuransilain2, id_penyakit, nama_penyakit, id_penyakit1, nama_penyakit1, id_penyakit2, nama_penyakit2, tgl_sakit, id_puskesmas, nama_puskesmas, alamat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nama_lgkp' => 'Nama Lgkp',
			'nik' => 'Nik',
			'hub_keluarga' => 'Hub Keluarga',
			'tgl_lhr' => 'Tgl Lhr',
			'agama' => 'Agama',
			'pendidikan' => 'Pendidikan',
			'pekerjaan' => 'Pekerjaan',
			'stat_kwn' => 'Stat Kwn',
			'no_kk' => 'No Kk',
			'no_jka' => 'No Jka',
			'no_jamkesmas' => 'No Jamkesmas',
			'asuransilain1' => 'Asuransilain1',
			'no_asuransilain1' => 'No Asuransilain1',
			'asuransilain2' => 'Asuransilain2',
			'no_asuransilain2' => 'No Asuransilain2',
			'id_penyakit' => 'Id Penyakit',
			'nama_penyakit' => 'Nama Penyakit',
			'id_penyakit1' => 'Id Penyakit1',
			'nama_penyakit1' => 'Nama Penyakit1',
			'id_penyakit2' => 'Id Penyakit2',
			'nama_penyakit2' => 'Nama Penyakit2',
			'tgl_sakit' => 'Tgl Sakit',
			'id_puskesmas' => 'Id Puskesmas',
			'nama_puskesmas' => 'Nama Puskesmas',
			'alamat' => 'Alamat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nama_lgkp',$this->nama_lgkp,true);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('hub_keluarga',$this->hub_keluarga,true);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('agama',$this->agama);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);
		$criteria->compare('stat_kwn',$this->stat_kwn);
		$criteria->compare('no_kk',$this->no_kk,true);
		$criteria->compare('no_jka',$this->no_jka,true);
		$criteria->compare('no_jamkesmas',$this->no_jamkesmas,true);
		$criteria->compare('asuransilain1',$this->asuransilain1,true);
		$criteria->compare('no_asuransilain1',$this->no_asuransilain1,true);
		$criteria->compare('asuransilain2',$this->asuransilain2,true);
		$criteria->compare('no_asuransilain2',$this->no_asuransilain2,true);
		$criteria->compare('id_penyakit',$this->id_penyakit);
		$criteria->compare('nama_penyakit',$this->nama_penyakit,true);
		$criteria->compare('id_penyakit1',$this->id_penyakit1);
		$criteria->compare('nama_penyakit1',$this->nama_penyakit1,true);
		$criteria->compare('id_penyakit2',$this->id_penyakit2);
		$criteria->compare('nama_penyakit2',$this->nama_penyakit2,true);
		$criteria->compare('tgl_sakit',$this->tgl_sakit);
		$criteria->compare('id_puskesmas',$this->id_puskesmas,true);
		$criteria->compare('nama_puskesmas',$this->nama_puskesmas,true);
		$criteria->compare('alamat',$this->alamat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* nama nik */
	public static function getNamaNik($nama, $nik) {
		return $nama."<br/>".$nik;
	}
	
	/* status keluarga tanggal lahir */
	public static function getStatusKeluargaTanggalLahir($hub_keluarga, $tgl_lhr) {
		return DlmActiveRecord::getHubunganKeluargaById($hub_keluarga)."<br/>".$tgl_lhr;
	}
	
	/* agama  pendidikan */
	public static function getAgamaPendidikan($agama, $pendidikan) {
		return DlmActiveRecord::getAgamaById($agama)."<br/>".$pendidikan;
	}
	
	/* pekerjaan status kawin */
	public static function getPekerjaanStatusKawin($pekerjaan, $stat_kwn){
		return $pekerjaan ."<br/>".DlmActiveRecord::getStatusKawinById($stat_kwn);
	}
	
	/* nomor kartu jka jamkesmas */
	public static function getNoKartuJkaJamkesmas($no_jka, $no_jamkesmas ){
		return $no_jka."<br/>".$no_jamkesmas;
	}

	/* nomor asuransi lain */
	public static function getNoAsuransiLain($no_asuransilain1, $no_asuransilain2 ){
		return $no_asuransilain1."<br/>".$no_asuransilain2;
	}
	
	/* puskesmas penyakit yang sering diderita */
	public static function getPuskesmasPenyakit($nama_puskesmas, $penyakit, $penyakit1, $penyakit2 ){
		return $nama_puskesmas."<br/>".$penyakit."<br/>".$penyakit1."<br/>".$penyakit2;
	}
}