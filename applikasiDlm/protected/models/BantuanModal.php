<?php

/**
 * This is the model class for table "BANTUAN_MODAL".
 *
 * The followings are the available columns in table 'BANTUAN_MODAL':
 * @property integer $id
 * @property integer $nik
 * @property integer $no_kk
 * @property integer $status_bantuan_modal
 * @property string $jenis_bantuan_modal
 * @property integer $kategory_bantuan_modal
 * @property integer $id_kelompok
 * @property integer $id_instansi
 * @property integer $tahun_diberi
 * @property integer $sumber_bantuan_modal
 * @property integer $id_koperasi
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA $nik0
 * @property INSTANSI $idInstansi
 * @property KELOMPOK $idKelompok
 * @property KEPALAKELUARGA $noKk
 * @property KOPERASI $idKoperasi
 * @property USER $user
 */
class BantuanModal extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BantuanModal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'BANTUAN_MODAL';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, no_kk, status_bantuan_modal, kategory_bantuan_modal, id_instansi, tahun_diberi, sumber_bantuan_modal', 'required'),
			array('nik, no_kk, status_bantuan_modal, kategory_bantuan_modal, id_kelompok, id_instansi, tahun_diberi, sumber_bantuan_modal, id_koperasi, userid', 'numerical', 'integerOnly'=>true),
			array('jenis_bantuan_modal', 'length', 'max'=>400),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nik, no_kk, status_bantuan_modal, jenis_bantuan_modal, kategory_bantuan_modal, id_kelompok, id_instansi, tahun_diberi, sumber_bantuan_modal, id_koperasi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'idInstansi' => array(self::BELONGS_TO, 'INSTANSI', 'id_instansi'),
			'idKelompok' => array(self::BELONGS_TO, 'KELOMPOK', 'id_kelompok'),
			'noKk' => array(self::BELONGS_TO, 'KEPALAKELUARGA', 'no_kk'),
			'idKoperasi' => array(self::BELONGS_TO, 'KOPERASI', 'id_koperasi'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nik' => 'NIK',
			'no_kk' => 'No. KK',
			'status_bantuan_modal' => 'Pernah Menerima Bantuan',
			'nama_status_bantuan_modal' => 'Pernah Menerima Bantuan',
			'jenis_bantuan_modal' => 'Jenis Bantuan',
			'kategory_bantuan_modal' => 'Kategori Bantuan',
			'id_kelompok' => 'Nama Kelompok Usaha',
			'nama_kelompok' => 'Nama Kelompok Usaha',
			'id_instansi' => 'Nama Instansi Pemberi',
			'nama_instansi' => 'Nama Instansi Pemberi',
			'tahun_diberi' => 'Tahun',
			'sumber_bantuan_modal' => 'Sumber Bantuan',
			'nama_sumber_bantuan_modal' => 'Sumber Bantuan',
			'id_koperasi' => 'Nama Koperasi',
			'nama_koperasi' => 'Nama Koperasi',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nik',$this->nik);
		$criteria->compare('no_kk',$this->no_kk);
		$criteria->compare('status_bantuan_modal',$this->status_bantuan_modal);
		$criteria->compare('jenis_bantuan_modal',$this->jenis_bantuan_modal,true);
		$criteria->compare('kategory_bantuan_modal',$this->kategory_bantuan_modal);
		$criteria->compare('id_kelompok',$this->id_kelompok);
		$criteria->compare('id_instansi',$this->id_instansi);
		$criteria->compare('tahun_diberi',$this->tahun_diberi);
		$criteria->compare('sumber_bantuan_modal',$this->sumber_bantuan_modal);
		$criteria->compare('id_koperasi',$this->id_koperasi);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			if($this->kategory_bantuan_modal == 1){
				$this->id_kelompok = NULL;
			}
			
			if($this->sumber_bantuan_modal == 2){
				$this->id_koperasi = NULL;
			}
						
			return true;
	   }
		
		return false;
	}
	
	/* nama penduduk */
	private $nama_penduduk;
	public function setNama_penduduk($name) {
	  $this->nama_penduduk = $name;
	}
	
	public function getNama_penduduk() {
		if($this->nik0 != null){
			$this->nama_penduduk = $this->nik0->nama_lgkp;
		}
		return $this->nama_penduduk;
	}
	
	/* nama status bantuan modal */
	private $nama_status_bantuan_modal;
	public function setNama_status_bantuan_modal($name) {
	  $this->$nama_status_bantuan_modal = $name;
	}
	
	public function getNama_status_bantuan_modal() {
		$arrOptions = $this->getStatusYaTidakOptions();
		$index = $this->status_bantuan_modal;
		if($index > 0){
			$this->nama_status_bantuan_modal = $arrOptions[$index];
		}
		return $this->nama_status_bantuan_modal;
	}
	
	/* nama kelompok */
	private $nama_kelompok;
	public function setNama_kelompok($name) {
	  $this->nama_kelompok = $name;
	}
	
	public function getNama_kelompok() {
		if($this->idKelompok != null){
			$this->nama_kelompok = $this->idKelompok->nama_kelompok;
		}
		return $this->nama_kelompok;
	}
	
	/* nama instansi */
	private $nama_instansi;
	public function setNama_instansi($name) {
	  $this->nama_instansi = $name;
	}
	
	public function getNama_instansi() {
		if($this->idInstansi != null){
			$this->nama_instansi = $this->idInstansi->nama_instansi;
		}
		return $this->nama_instansi;
	}
	
	/* nama sumber bantuan modal */
	private $nama_sumber_bantuan_modal;
	public function setNama_sumber_bantuan_modal($name) {
	  $this->$nama_sumber_bantuan_modal = $name;
	}
	
	public function getNama_sumber_bantuan_modal() {
		$arrOptions = $this->getSumberBantuanModalOptions();
		$index = $this->sumber_bantuan_modal;
		if($index > 0){
			$this->nama_sumber_bantuan_modal = $arrOptions[$index];
		}
		return $this->nama_sumber_bantuan_modal;
	}
	
	/* nama kopearasi */
	private $nama_koperasi;
	public function setNama_koperasi($name) {
	  $this->nama_koperasi = $name;
	}
	
	public function getNama_koperasi() {
		if($this->idKoperasi != null){
			$this->nama_koperasi = $this->idKoperasi->nama_koperasi;
		}
		return $this->nama_koperasi;
	}
	
}