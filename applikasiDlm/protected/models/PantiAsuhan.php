<?php

/**
 * This is the model class for table "PANTI_ASUHAN".
 *
 * The followings are the available columns in table 'PANTI_ASUHAN':
 * @property integer $id
 * @property string $nama_pantiasuhan
 * @property integer $type_pantiasuhan
 * @property string $alamat_pantiasuhan
 * @property integer $jlh_kamar
 * @property integer $jlh_perpustakaan
 * @property integer $jlh_aula
 * @property integer $jlh_mushalla
 * @property integer $jlh_kamar_mandi_wc
 * @property string $koordinat_x
 * @property string $koordinat_y
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property USER $user
 * @property PMKS[] $pMKSs
 */
class PantiAsuhan extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PantiAsuhan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PANTI_ASUHAN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_pantiasuhan, type_pantiasuhan, alamat_pantiasuhan, jlh_kamar, jlh_perpustakaan, jlh_aula, jlh_mushalla, jlh_kamar_mandi_wc, koordinat_x, koordinat_y', 'required'),
			array('type_pantiasuhan, jlh_kamar, jlh_perpustakaan, jlh_aula, jlh_mushalla, jlh_kamar_mandi_wc', 'numerical', 'integerOnly'=>true),
			array('nama_pantiasuhan', 'length', 'max'=>200),
			array('alamat_pantiasuhan', 'length', 'max'=>1000),
			array('koordinat_x, koordinat_y', 'length', 'max'=>100),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_pantiasuhan, type_pantiasuhan, alamat_pantiasuhan, jlh_kamar, jlh_perpustakaan, jlh_aula, jlh_mushalla, jlh_kamar_mandi_wc, koordinat_x, koordinat_y', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
			'pMKSs' => array(self::HAS_MANY, 'PMKS', 'id_pantiasuhan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_pantiasuhan' => 'Nama Panti Asuhan',
			'type_pantiasuhan' => 'Tipe Panti Asuhan',
			'nama_type_pantiasuhan' => 'Tipe Panti Asuhan',
			'alamat_pantiasuhan' => 'Alamat Panti Asuhan',
			'jlh_kamar' => 'Jumlah Kamar',
			'jlh_perpustakaan' => 'Ruang Pustakaan',
			'jlh_aula' => 'Aula',
			'jlh_mushalla' => 'Mushalla',
			'jlh_kamar_mandi_wc' => 'Kamar Mandi/WC',
			'koordinat_x' => 'Koordinat Longitude',
			'koordinat_y' => 'Koordinat Latitude',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('id',$this->id);
		$criteria->compare('nama_pantiasuhan',$this->nama_pantiasuhan,true);
		$criteria->compare('type_pantiasuhan',$this->type_pantiasuhan);
		$criteria->compare('alamat_pantiasuhan',$this->alamat_pantiasuhan,true);
		$criteria->compare('jlh_kamar',$this->jlh_kamar);
		$criteria->compare('jlh_perpustakaan',$this->jlh_perpustakaan);
		$criteria->compare('jlh_aula',$this->jlh_aula);
		$criteria->compare('jlh_mushalla',$this->jlh_mushalla);
		$criteria->compare('jlh_kamar_mandi_wc',$this->jlh_kamar_mandi_wc);
		$criteria->compare('koordinat_x',$this->koordinat_x,true);
		$criteria->compare('koordinat_y',$this->koordinat_y,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* nama zakat */
	private $nama_type_pantiasuhan;
	public function setNama_type_pantiasuhan($name) {
	  $this->$nama_type_pantiasuhan = $name;
	}
	
	public function getNama_type_pantiasuhan() {
		$arrOptions = DlmActiveRecord::getPantiAsuhanOptions();
		$index = $this->type_pantiasuhan;
		if($index > 0){
			$this->nama_type_pantiasuhan = $arrOptions[$index];
		}
		return $this->nama_type_pantiasuhan;
	}
	
	public static function getNamaTypePantiasuhanById($idx) {
		
		$arrOptions = DlmActiveRecord::getPantiAsuhanOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
}