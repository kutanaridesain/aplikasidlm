<?php

/**
 * This is the model class for table "SEKOLAH".
 *
 * The followings are the available columns in table 'SEKOLAH':
 * @property integer $id
 * @property string $nama_sekolah
 * @property string $jenjang_sekolah
 * @property string $alamat_sekolah
 * @property integer $jlh_ruang_kelas
 * @property integer $jlh_perpustakaan
 * @property integer $jlh_lab_ipa
 * @property integer $jlh_lab_ips
 * @property integer $jlh_mushalla
 * @property integer $jlh_kamar_mandi_wc
 * @property string $koordinat_x
 * @property string $koordinat_y
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property USER $user
 * @property RIWAYATPENDIDIKAN[] $rIWAYATPENDIDIKANs
 */
class Sekolah extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sekolah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SEKOLAH';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_sekolah, jenjang_sekolah, alamat_sekolah, jlh_ruang_kelas, jlh_perpustakaan, jlh_lab_ipa, jlh_lab_ips, jlh_mushalla, jlh_kamar_mandi_wc, koordinat_x, koordinat_y', 'required'),
			array('jlh_ruang_kelas, jlh_perpustakaan, jlh_lab_ipa, jlh_lab_ips, jlh_mushalla, jlh_kamar_mandi_wc', 'numerical', 'integerOnly'=>true),
			array('nama_sekolah, jenjang_sekolah', 'length', 'max'=>200),
			array('alamat_sekolah', 'length', 'max'=>1000),
			array('koordinat_x, koordinat_y', 'length', 'max'=>100),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_sekolah, jenjang_sekolah, alamat_sekolah, jlh_ruang_kelas, jlh_perpustakaan, jlh_lab_ipa, jlh_lab_ips, jlh_mushalla, jlh_kamar_mandi_wc, koordinat_x, koordinat_y', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
			'riwayatpendidikan' => array(self::HAS_MANY, 'RIWAYATPENDIDIKAN', 'kode_sekolah'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_sekolah' => 'Nama Sekolah',
			'jenjang_sekolah' => 'Jenjang Sekolah',
			'alamat_sekolah' => 'Alamat Sekolah',
			'jlh_ruang_kelas' => 'Jumlah Ruang Kelas',
			'jlh_perpustakaan' => 'Perpustakaan',
			'jlh_lab_ipa' => 'Laboratorium IPA',
			'jlh_lab_ips' => 'Laboratorium IPS',
			'jlh_mushalla' => 'Mushalla',
			'jlh_kamar_mandi_wc' => 'Kamar Mandi/WC',
			'koordinat_x' => 'Koordinat Longitude',
			'koordinat_y' => 'Koordinat Latitude',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_sekolah',$this->nama_sekolah,true);
		$criteria->compare('jenjang_sekolah',$this->jenjang_sekolah,true);
		$criteria->compare('alamat_sekolah',$this->alamat_sekolah,true);
		$criteria->compare('jlh_ruang_kelas',$this->jlh_ruang_kelas);
		$criteria->compare('jlh_perpustakaan',$this->jlh_perpustakaan);
		$criteria->compare('jlh_lab_ipa',$this->jlh_lab_ipa);
		$criteria->compare('jlh_lab_ips',$this->jlh_lab_ips);
		$criteria->compare('jlh_mushalla',$this->jlh_mushalla);
		$criteria->compare('jlh_kamar_mandi_wc',$this->jlh_kamar_mandi_wc);
		$criteria->compare('koordinat_x',$this->koordinat_x,true);
		$criteria->compare('koordinat_y',$this->koordinat_y,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
}