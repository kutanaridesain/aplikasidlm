<?php

/**
 * This is the model class for table "ARTIKEL".
 *
 * The followings are the available columns in table 'ARTIKEL':
 * @property integer $id
 * @property string $text_intro
 * @property string $text_full
 * @property integer $is_visible
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 * @property string $title
 *
 * The followings are the available model relations:
 * @property USER $user
 */
class Artikel extends CActiveRecord
{
	public function getDbConnection()
	{
		//if(isset(Yii::app()->dbOracle)){
			self::$db = Yii::app()->dbOracle;
		/*}else{
			self::$db = Yii::app()->db;
		}*/
		return self::$db;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Artikel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ARTIKEL';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('text_intro, text_full, is_visible, userid, tglinput, title', 'required'),
			array('is_visible, userid', 'numerical', 'integerOnly'=>true),
			// array('text_full', 'length', 'max'=>4000),
			array('text_intro', 'length', 'max'=>500),
			array('title', 'length', 'max'=>225),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, text_intro, text_full, is_visible, userid, tglinput, tglupdate, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'text_intro' => 'Teks Pendek',
			'text_full' => 'Teks Panjang',
			'is_visible' => 'Dipublikasi',
			'userid' => 'Pengirim',
			'tglinput' => 'Tanggal Kirim',
			'tglupdate' => 'Tanggal Ubah',
			'title' => 'Judul',
		);
	}

	public function updateVisibility($id, $visibility)
	{
		$oci = $this->getDbConnection();
		$sql = 'UPDATE "ARTIKEL" SET "is_visible" = '.$visibility.' WHERE "id" ='.$id;

		$command = $oci->createCommand($sql);
		$dataReader = $command->query();
		//return $dataReader;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('text_intro',$this->text_intro,true);
		$criteria->compare('text_full',$this->text_full,true);
		$criteria->compare('is_visible',$this->is_visible);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function checkVisibility($data, $isUrl = false)
	{
		$text = 'publish';
		if($data->is_visible != 0){
			$text = 'unpublish';
		}
		if($isUrl){
			$text = sprintf('artikel/togglevisibility/%s/%d',$text,$data->id);
			//$text = array('do',$text, $data->id);
		}
		return $text;
	}
}