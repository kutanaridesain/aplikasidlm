<?php

/**
 * This is the model class for table "KEPALA_KELUARGA".
 *
 * The followings are the available columns in table 'KEPALA_KELUARGA':
 * @property integer $no_kk
 * @property integer $nik
 * @property integer $hub_keluarga
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA $nik0
 * @property USER $user
 * @property RIWAYATKESEHATAN[] $rIWAYATKESEHATANs
 * @property PMKS[] $pMKSs
 * @property BANTUANSOSIAL[] $bANTUANSOSIALs
 * @property BANTUANMODAL[] $bANTUANMODALs
 * @property BANTUANLAINNYA[] $bANTUANLAINNYAs
 * @property KEPEMILIKANASSET[] $kEPEMILIKANASSETs
 * @property RIWAYATPENDIDIKAN[] $rIWAYATPENDIDIKANs
 * @property RUMAH[] $rUMAHs
 */
class KepalaKeluarga extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KepalaKeluarga the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'KEPALA_KELUARGA';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_kk, nik, hub_keluarga', 'required'),
			array('nik, hub_keluarga, userid', 'numerical', 'integerOnly'=>true),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('no_kk, nik, hub_keluarga, userid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
			'rIWAYATKESEHATANs' => array(self::HAS_MANY, 'RIWAYATKESEHATAN', 'no_kk'),
			'pMKSs' => array(self::HAS_MANY, 'PMKS', 'no_kk'),
			'bANTUANSOSIALs' => array(self::HAS_MANY, 'BANTUANSOSIAL', 'no_kk'),
			'bANTUANMODALs' => array(self::HAS_MANY, 'BANTUANMODAL', 'no_kk'),
			'bANTUANLAINNYAs' => array(self::HAS_MANY, 'BANTUANLAINNYA', 'no_kk'),
			'kEPEMILIKANASSETs' => array(self::HAS_MANY, 'KEPEMILIKANASSET', 'no_kk'),
			'rIWAYATPENDIDIKANs' => array(self::HAS_MANY, 'RIWAYATPENDIDIKAN', 'no_kk'),
			'rUMAHs' => array(self::HAS_MANY, 'RUMAH', 'no_kk'),
			'dETAILs' => array(self::HAS_MANY, 'KEPALAKELUARGADETAILS', 'no_kk'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_kk' => 'No KK',
			'nik' => 'NIK Kepala Keluarga',
			'nama_penduduk' => 'Nama Kepala Keluarga',
			'hub_keluarga' => 'Hubungan Keluarga',
			'nama_hubungan' => 'Hubungan Keluarga',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$roleid = Yii::app()->user->getRoleId();
		$locaid = Yii::app()->user->getLocationId();
		
		$criteria=new CDbCriteria;
		$criteria->with='nik0';
		$criteria->compare('no_kk',$this->no_kk);
		$criteria->compare('nik',$this->nik);		
		$criteria->compare('hub_keluarga',$this->hub_keluarga);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		
		if($roleid==2){
			$criteria->addCondition(
				'"nik0"."no_kab" = ' . $locaid
			);
		}
		if($roleid==3){
			$criteria->addCondition(
				'"nik0"."no_kec" = ' . $locaid
			);
		}
		if($roleid==4){
			$criteria->addCondition(
				'"nik0"."no_kel" = ' . $locaid
			);
		}
		
		$criteria->together = true;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function details($nokk)
	{
		$sql = 'SELECT *
				FROM kepala_keluarga_details 
				WHERE  "no_kk" = \''.$nokk.'\'
				ORDER BY "hub_keluarga" ';
		
		$details = Yii::app()->db->createCommand($sql)->queryAll();
		
		return $details;
		//return $this->dETAILs;
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	
	/* nama penduduk */
	private $nama_penduduk;
	public function setNama_penduduk($name) {
	  $this->nama_penduduk = $name;
	}
	
	public function getNama_penduduk() {
		if($this->nik0 != null){
			$this->nama_penduduk = $this->nik0->nama_lgkp;
		}
		return $this->nama_penduduk;
	}
	
	/* nama hubungan keluarga */
	private $nama_hubungan;
	public function setNama_hubungan($name) {
		$this->nama_hubungan = $name;
	}

	public function getNama_hubungan() {
		$arrNamaHubungan = DlmActiveRecord::getHubunganKeluargaOptions(null);
		if($this->hub_keluarga != null){
			$this->nama_hubungan = $arrNamaHubungan[$this->hub_keluarga];
		}
		
		return $this->nama_hubungan;
	}
	
	public static function getNamaHubunganKeluargaById($idx) {
		
		$arrOptions = DlmActiveRecord::getHubunganKeluargaOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
}