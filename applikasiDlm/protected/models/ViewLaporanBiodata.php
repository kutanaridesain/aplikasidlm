<?php

/**
 * This is the model class for table "VIEW_LAPORAN_BIODATA".
 *
 * The followings are the available columns in table 'VIEW_LAPORAN_BIODATA':
 * @property string $nik
 * @property string $nama_lgkp
 * @property string $tmpt_lhr
 * @property string $tgl_lhr
 * @property integer $akta_lhr
 * @property string $no_akta_lhr
 * @property integer $gol_drh
 * @property integer $jenis_kelamin
 * @property integer $agama
 * @property integer $stat_kwn
 * @property integer $akta_kwn
 * @property string $no_akta_kwn
 * @property string $tgl_kwn
 * @property integer $akta_crai
 * @property string $no_akta_crai
 * @property string $tgl_crai
 * @property integer $pnydng_cct
 * @property integer $stat_hidup
 * @property string $tgl_meninggal
 * @property string $no_akta_kematian
 * @property integer $no_kab
 * @property string $nama_kabkot
 * @property integer $no_kec
 * @property string $nama_kec
 * @property integer $no_kel
 * @property string $nama_gampong
 * @property string $alamat
 * @property string $nik_nama_ibu
 * @property string $nik_nama_ayah
 * @property string $hub_keluarga
 * @property string $pendidikan
 * @property string $pekerjaan
 */
class ViewLaporanBiodata extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewLaporanBiodata the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VIEW_LAPORAN_BIODATA';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, nama_lgkp, tmpt_lhr, tgl_lhr, akta_lhr, gol_drh, jenis_kelamin, agama, stat_kwn, pnydng_cct, stat_hidup, no_kab, nama_kabkot, no_kec, nama_kec, no_kel, nama_gampong', 'required'),
			array('akta_lhr, gol_drh, jenis_kelamin, agama, stat_kwn, akta_kwn, akta_crai, pnydng_cct, stat_hidup, no_kab, no_kec, no_kel', 'numerical', 'integerOnly'=>true),
			array('nik', 'length', 'max'=>100),
			array('nama_lgkp, tmpt_lhr, nama_kabkot, nama_kec, nama_gampong', 'length', 'max'=>200),
			array('no_akta_lhr, no_akta_kwn, no_akta_kematian', 'length', 'max'=>50),
			array('no_akta_crai, nik_nama_ibu, nik_nama_ayah, hub_keluarga, pendidikan, pekerjaan', 'length', 'max'=>4000),
			array('alamat', 'length', 'max'=>620),
			array('tgl_kwn, tgl_crai, tgl_meninggal', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nik, nama_lgkp, tmpt_lhr, tgl_lhr, akta_lhr, no_akta_lhr, gol_drh, jenis_kelamin, agama, stat_kwn, akta_kwn, no_akta_kwn, tgl_kwn, akta_crai, no_akta_crai, tgl_crai, pnydng_cct, stat_hidup, tgl_meninggal, no_akta_kematian, no_kab, nama_kabkot, no_kec, nama_kec, no_kel, nama_gampong, alamat, nik_nama_ibu, nik_nama_ayah, hub_keluarga, pendidikan, pekerjaan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nik' => 'Nik',
			'nama_lgkp' => 'Nama Lgkp',
			'tmpt_lhr' => 'Tmpt Lhr',
			'tgl_lhr' => 'Tgl Lhr',
			'akta_lhr' => 'Akta Lhr',
			'no_akta_lhr' => 'No Akta Lhr',
			'gol_drh' => 'Gol Drh',
			'jenis_kelamin' => 'Jenis Kelamin',
			'agama' => 'agama',
			'stat_kwn' => 'Stat Kwn',
			'akta_kwn' => 'Akta Kwn',
			'no_akta_kwn' => 'No Akta Kwn',
			'tgl_kwn' => 'Tgl Kwn',
			'akta_crai' => 'Akta Crai',
			'no_akta_crai' => 'No Akta Crai',
			'tgl_crai' => 'Tgl Crai',
			'pnydng_cct' => 'Pnydng Cct',
			'stat_hidup' => 'Stat Hidup',
			'tgl_meninggal' => 'Tgl Meninggal',
			'no_akta_kematian' => 'No Akta Kematian',
			'no_kab' => 'No Kab',
			'nama_kabkot' => 'Nama Kabkot',
			'no_kec' => 'No Kec',
			'nama_kec' => 'Nama Kec',
			'no_kel' => 'No Kel',
			'nama_gampong' => 'Nama Gampong',
			'alamat' => 'Alamat',
			'nik_nama_ibu' => 'Nik Nama Ibu',
			'nik_nama_ayah' => 'Nik Nama Ayah',
			'hub_keluarga' => 'Hub Keluarga',
			'pendidikan' => 'Pendidikan',
			'pekerjaan' => 'Pekerjaan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('nama_lgkp',$this->nama_lgkp,true);
		$criteria->compare('tmpt_lhr',$this->tmpt_lhr,true);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('akta_lhr',$this->akta_lhr);
		$criteria->compare('no_akta_lhr',$this->no_akta_lhr,true);
		$criteria->compare('gol_drh',$this->gol_drh);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin);
		$criteria->compare('agama',$this->agama);
		$criteria->compare('stat_kwn',$this->stat_kwn);
		$criteria->compare('akta_kwn',$this->akta_kwn);
		$criteria->compare('no_akta_kwn',$this->no_akta_kwn,true);
		$criteria->compare('tgl_kwn',$this->tgl_kwn,true);
		$criteria->compare('akta_crai',$this->akta_crai);
		$criteria->compare('no_akta_crai',$this->no_akta_crai,true);
		$criteria->compare('tgl_crai',$this->tgl_crai,true);
		$criteria->compare('pnydng_cct',$this->pnydng_cct);
		$criteria->compare('stat_hidup',$this->stat_hidup);
		$criteria->compare('tgl_meninggal',$this->tgl_meninggal,true);
		$criteria->compare('no_akta_kematian',$this->no_akta_kematian,true);
		$criteria->compare('no_kab',$this->no_kab);
		$criteria->compare('nama_kabkot',$this->nama_kabkot,true);
		$criteria->compare('no_kec',$this->no_kec);
		$criteria->compare('nama_kec',$this->nama_kec,true);
		$criteria->compare('no_kel',$this->no_kel);
		$criteria->compare('nama_gampong',$this->nama_gampong,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('nik_nama_ibu',$this->nik_nama_ibu,true);
		$criteria->compare('nik_nama_ayah',$this->nik_nama_ayah,true);
		$criteria->compare('hub_keluarga',$this->hub_keluarga,true);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* nama nik status keluarga */
	public static function getNamaNikStatusKeluarga($nama, $nik, $hub_keluarga) {
		return $nama."<br/>".$nik."<br/>".DlmActiveRecord::getHubunganKeluargaById($hub_keluarga);
	}
	
	/* agama pendidikan pekerjaan*/
	public static function getAgamaPendidikanPekerjaan($agama, $pendidikan, $pekerjaan) {
		return DlmActiveRecord::getAgamaById($agama)."<br/>".$pendidikan."<br/>".$pekerjaan;
	}
	
	/* data lahir */
	public static function getDataLahir($tgl, $akta, $no_akta) {
		return $tgl ."<br/>".DlmActiveRecord::getNamaStatusYaTidakById($akta)."<br/>".$no_akta;
	}
	
	/* data kawin */
	public static function getDataKawin($status, $no_akta, $tgl) {
		return DlmActiveRecord::getStatusKawinById($status) ."<br/>".$no_akta."<br/>".$tgl;
	}
	
	/* data cerai */
	public static function getDataCerai($akta, $no_akta, $tgl) {
		return DlmActiveRecord::getNamaStatusYaTidakById($akta)."<br/>".$no_akta."<br/>".$tgl;
	}
	
	/* data meninggal */
	public static function getDataMeninggal($status, $tgl, $no_akta) {
		return DlmActiveRecord::getNamaStatusYaTidakById($status)."<br/>".$tgl ."<br/>".$no_akta;
	}
	
	/* nama nik */
	public static function getNamaNik($nama, $nik) {
		return $nama."<br/>".$nik;
	}
	
	/* status keluarga agama */
	public static function getStatusKeluargaAgama($hub_keluarga, $agama) {
		return DlmActiveRecord::getHubunganKeluargaById($hub_keluarga)."<br/>".DlmActiveRecord::getAgamaById($agama);
	}
	
	/* pendidikan pekerjaan*/
	public static function getPendidikanPekerjaan($pendidikan, $pekerjaan) {
		return $pendidikan."<br/>".$pekerjaan;
	}
	
	/* tgl lahir  status kawin*/
	public static function getTglLahirStatusKawin($tgl, $status) {
		return $tgl ."<br/>".DlmActiveRecord::getStatusKawinById($status);
	}
	
	/* akta kawin tanggal pernikahan */
	public static function getAktaKawinTglPernikahan($no_akta, $tgl) {
		return $no_akta."<br/>".$tgl;
	}
	
	/* tgl lahir pekerjaan*/
	public static function getTglLahirPekerjaan($tgl, $pekerjaan) {
		return $tgl ."<br/>".$pekerjaan;
	}
	
	/* status kawin akta kawin*/
	public static function getStatusKawinAktaKawin($status, $no_akta) {
		return DlmActiveRecord::getStatusKawinById($status)."<br/>".$no_akta;
	}

	/* tanggal pernikahan akta cerai */
	public static function getTglPernikahanAktaCerai( $tgl, $akta) {
		return $tgl."<br/>".DlmActiveRecord::getNamaStatusYaTidakById($akta);
	}
	
	/* no akta cerai tgl cerai */
	public static function getNoAktaCeraiTglCerai($no_akta, $tgl) {
		return $no_akta."<br/>".$tgl;
	}
	
	/* tgl lahir  status keluarga*/
	public static function getTglLahirStatusKeluarga($tgl, $hub_keluarga) {
		return $tgl ."<br/>".DlmActiveRecord::getHubunganKeluargaById($hub_keluarga);
	}
	
	/* agama pendidikan */
	public static function getAgamaPendidikan($agama, $pendidikan) {
		return DlmActiveRecord::getAgamaById($agama)."<br/>".$pendidikan;
	}
	
	/* status kawin pekerjaan*/
	public static function getStatusKawinPekerjaanTerakhir($status, $pekerjaan) {
		return DlmActiveRecord::getStatusKawinById($status)."<br/>".$pekerjaan;
	}
	
}