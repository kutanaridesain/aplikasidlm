<?php

/**
 * This is the model class for table "KEPEMILIKAN_ASSET".
 *
 * The followings are the available columns in table 'KEPEMILIKAN_ASSET':
 * @property integer $id
 * @property integer $nik
 * @property integer $no_kk
 * @property integer $punya_tanah_rumah
 * @property integer $luas_tanah_rumah
 * @property integer $punya_tanah_sawah
 * @property integer $luas_tanah_sawah
 * @property integer $punya_tanah_kebun
 * @property integer $luast_tanah_kebun
 * @property integer $punya_tambak
 * @property integer $luas_tambak
 * @property integer $punya_lembu
 * @property integer $jlh_lembu
 * @property integer $punya_kerbau
 * @property integer $jlh_kerbau
 * @property integer $punya_ternak_lain
 * @property integer $jlh_ternak_lain
 * @property integer $punya_mobil
 * @property integer $jlh_mobil
 * @property integer $punya_motor
 * @property integer $jlh_motor
 * @property integer $punya_tabungan
 * @property integer $punya_asuransi
 * @property integer $punya_motor_tempel
 * @property integer $punya_kapal_motor
 * @property integer $punya_perahu
 * @property string $punya_aset_lain
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA $nik0
 * @property KEPALAKELUARGA $noKk
 * @property USER $user
 */
class KepemilikanAsset extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KepemilikanAsset the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'KEPEMILIKAN_ASSET';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, nama_penduduk, no_kk, punya_tanah_rumah, punya_tanah_sawah, punya_tanah_kebun, punya_tambak, punya_lembu, punya_kerbau, punya_ternak_lain, punya_mobil, punya_motor, punya_tabungan, punya_asuransi, punya_motor_tempel, punya_kapal_motor, punya_perahu', 'required'),
			array('nik, no_kk, punya_tanah_rumah, luas_tanah_rumah, punya_tanah_sawah, luas_tanah_sawah, punya_tanah_kebun, luast_tanah_kebun, punya_tambak, luas_tambak, punya_lembu, jlh_lembu, punya_kerbau, jlh_kerbau, punya_ternak_lain, jlh_ternak_lain, punya_mobil, jlh_mobil, punya_motor, jlh_motor, punya_tabungan, punya_asuransi, punya_motor_tempel, punya_kapal_motor, punya_perahu, userid', 'numerical', 'integerOnly'=>true),
			array('punya_aset_lain', 'length', 'max'=>4000),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nik, no_kk, punya_tanah_rumah, luas_tanah_rumah, punya_tanah_sawah, luas_tanah_sawah, punya_tanah_kebun, luast_tanah_kebun, punya_tambak, luas_tambak, punya_lembu, jlh_lembu, punya_kerbau, jlh_kerbau, punya_ternak_lain, jlh_ternak_lain, punya_mobil, jlh_mobil, punya_motor, jlh_motor, punya_tabungan, punya_asuransi, punya_motor_tempel, punya_kapal_motor, punya_perahu, punya_aset_lain', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'noKk' => array(self::BELONGS_TO, 'KEPALAKELUARGA', 'no_kk'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nik' => 'NIK',
			'no_kk' => 'No. KK',
			'punya_tanah_rumah' => 'Pekarangan',
			'nama_punya_tanah_rumah' => 'Pekarangan',
			'luas_tanah_rumah' => 'Luas Pekarangan',
			'nama_luas_tanah_rumah' => 'Luas Pekarangan',
			'punya_tanah_sawah' => 'Sawah',
			'nama_punya_tanah_sawah' => 'Sawah',
			'luas_tanah_sawah' => 'Luas Sawah',
			'nama_luas_tanah_sawah' => 'Luas Sawah',
			'punya_tanah_kebun' => 'Kebun',
			'nama_punya_tanah_kebun' => 'Kebun',
			'luast_tanah_kebun' => 'Luas Kebun',
			'nama_luast_tanah_kebun' => 'Luas Kebun',
			'punya_tambak' => 'Tambak',
			'nama_punya_tambak' => 'Tambak',
			'luas_tambak' => 'Luas Tambak',
			'nama_luas_tambak' => 'Luas Tambak',
			'punya_lembu' => 'Lembu',
			'nama_punya_lembu' => 'Lembu',
			'jlh_lembu' => 'Jumlah Ekor',
			'nama_jlh_lembu' => 'Jumlah Ekor',
			'punya_kerbau' => 'Kerbau',
			'nama_punya_kerbau' => 'Kerbau',
			'jlh_kerbau' => 'Jumlah Ekor',
			'nama_jlh_kerbau' => 'Jumlah Ekor',
			'punya_ternak_lain' => 'Ternak Lain',
			'nama_punya_ternak_lain' => 'Ternak Lain',
			'jlh_ternak_lain' => 'Jumlah Ternak Lain',
			'nama_jlh_ternak_lain' => 'Jumlah Ternak Lain',
			'punya_mobil' => 'Mobil',
			'nama_punya_mobil' => 'Mobil',
			'jlh_mobil' => 'Jumlah Unit',
			'nama_jlh_mobil' => 'Jumlah Unit',
			'punya_motor' => 'Sepeda Motor',
			'nama_punya_motor' => 'Sepeda Motor',
			'jlh_motor' => 'Jumlah Unit',
			'nama_jlh_motor' => 'Jumlah Unit',
			'punya_tabungan' => 'Tabungan Bank',
			'nama_punya_tabungan' => 'Tabungan Bank',
			'punya_asuransi' => 'Asuransi',
			'nama_punya_asuransi' => 'Asuransi',
			'punya_motor_tempel' => 'Motor Tempel',
			'nama_punya_motor_tempel' => 'Motor Tempel',
			'punya_kapal_motor' => 'Kapal Motor',
			'nama_punya_kapal_motor' => 'Kapal Motor',
			'punya_perahu' => 'Perahu Tanpa Motor',
			'nama_punya_perahu' => 'Perahu Tanpa Motor',
			'punya_aset_lain' => 'Aset Lain',
			'nama_punya_aset_lain' => 'Aset Lain',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$roleid = Yii::app()->user->getRoleId();
		$locaid = Yii::app()->user->getLocationId();

		$criteria=new CDbCriteria;
		$criteria->with='nik0';

		$criteria->compare('id',$this->id);
		$criteria->compare('nik',$this->nik);
		$criteria->compare('no_kk',$this->no_kk);
		$criteria->compare('punya_tanah_rumah',$this->punya_tanah_rumah);
		$criteria->compare('luas_tanah_rumah',$this->luas_tanah_rumah);
		$criteria->compare('punya_tanah_sawah',$this->punya_tanah_sawah);
		$criteria->compare('luas_tanah_sawah',$this->luas_tanah_sawah);
		$criteria->compare('punya_tanah_kebun',$this->punya_tanah_kebun);
		$criteria->compare('luast_tanah_kebun',$this->luast_tanah_kebun);
		$criteria->compare('punya_tambak',$this->punya_tambak);
		$criteria->compare('luas_tambak',$this->luas_tambak);
		$criteria->compare('punya_lembu',$this->punya_lembu);
		$criteria->compare('jlh_lembu',$this->jlh_lembu);
		$criteria->compare('punya_kerbau',$this->punya_kerbau);
		$criteria->compare('jlh_kerbau',$this->jlh_kerbau);
		$criteria->compare('punya_ternak_lain',$this->punya_ternak_lain);
		$criteria->compare('jlh_ternak_lain',$this->jlh_ternak_lain);
		$criteria->compare('punya_mobil',$this->punya_mobil);
		$criteria->compare('jlh_mobil',$this->jlh_mobil);
		$criteria->compare('punya_motor',$this->punya_motor);
		$criteria->compare('jlh_motor',$this->jlh_motor);
		$criteria->compare('punya_tabungan',$this->punya_tabungan);
		$criteria->compare('punya_asuransi',$this->punya_asuransi);
		$criteria->compare('punya_motor_tempel',$this->punya_motor_tempel);
		$criteria->compare('punya_kapal_motor',$this->punya_kapal_motor);
		$criteria->compare('punya_perahu',$this->punya_perahu);
		$criteria->compare('punya_aset_lain',$this->punya_aset_lain,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		
		if($roleid==2){
			$criteria->addCondition(
				'"nik0"."no_kab" = ' . $locaid
			);
		}
		if($roleid==3){
			$criteria->addCondition(
				'"nik0"."no_kec" = ' . $locaid
			);
		}
		if($roleid==4){
			$criteria->addCondition(
				'"nik0"."no_kel" = ' . $locaid
			);
		}
		
		$criteria->together = true;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			if($this->punya_tanah_rumah == 2){
				$this->luas_tanah_rumah = NULL;
			}			
			
			if($this->punya_tanah_sawah == 2){
				$this->luas_tanah_sawah = NULL;
			}			
			
			if($this->punya_tanah_kebun == 2){
				$this->luast_tanah_kebun = NULL;
			}
			
			if($this->punya_tambak == 2){
				$this->luas_tambak = NULL;
			}
			
			if($this->punya_lembu == 2){
				$this->jlh_lembu = NULL;
			}
			
			if($this->punya_kerbau == 2){
				$this->jlh_kerbau = NULL;
			}
			
			if($this->punya_ternak_lain == 2){
				$this->jlh_ternak_lain = NULL;
			}
			
			if($this->punya_mobil == 2){
				$this->jlh_mobil = NULL;
			}
			
			if($this->punya_motor == 2){
				$this->jlh_motor = NULL;
			}
			
			return true;
	   }
		
		return false;
	}
	
	/* penduduk */
	private $nama_penduduk;
	public function setNama_penduduk($name) {
	  $this->nama_penduduk = $name;
	}

	public function getNama_penduduk(){
		if($this->nik0 != null){
			$this->nama_penduduk = $this->nik0->nama_lgkp;
		}
		return $this->nama_penduduk;
	}
	
	/* nama punya tanah rumah */
	private $nama_punya_tanah_rumah;
	public function setNama_punya_tanah_rumah($name) {
	  $this->$nama_punya_tanah_rumah = $name;
	}
	
	public function getNama_punya_tanah_rumah() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_tanah_rumah;
		if($index > 0){
			$this->nama_punya_tanah_rumah = $arrOptions[$index];
		}
		return $this->nama_punya_tanah_rumah;
	}
	
	/* nama luas tanah rumah */
	private $nama_luas_tanah_rumah;
	public function setNama_luas_tanah_rumah($name) {
	  $this->$nama_luas_tanah_rumah = $name;
	}
	
	public function getNama_luas_tanah_rumah() {
		$arrOptions = DlmActiveRecord::getLuasTanahOptions();
		$index = $this->luas_tanah_rumah;
		if($index > 0){
			$this->nama_luas_tanah_rumah = $arrOptions[$index];
		}
		return $this->nama_luas_tanah_rumah;
	}
	
	/* nama punya tanah sawah */
	private $nama_punya_tanah_sawah;
	public function setNama_punya_tanah_sawah($name) {
	  $this->$nama_punya_tanah_sawah = $name;
	}
	
	public function getNama_punya_tanah_sawah() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_tanah_sawah;
		if($index > 0){
			$this->nama_punya_tanah_sawah = $arrOptions[$index];
		}
		return $this->nama_punya_tanah_sawah;
	}
	
	/* nama luas tanah sawah */
	private $nama_luas_tanah_sawah;
	public function setNama_luas_tanah_sawah($name) {
	  $this->$nama_luas_tanah_sawah = $name;
	}
	
	public function getNama_luas_tanah_sawah() {
		$arrOptions = DlmActiveRecord::getLuasTanahOptions();
		$index = $this->luas_tanah_sawah;
		if($index > 0){
			$this->nama_luas_tanah_sawah = $arrOptions[$index];
		}
		return $this->nama_luas_tanah_sawah;
	}
	
	/* nama punya tanah kebun */
	private $nama_punya_tanah_kebun;
	public function setNama_punya_tanah_kebun($name) {
	  $this->$nama_punya_tanah_kebun = $name;
	}
	
	public function getNama_punya_tanah_kebun() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_tanah_kebun;
		if($index > 0){
			$this->nama_punya_tanah_kebun = $arrOptions[$index];
		}
		return $this->nama_punya_tanah_kebun;
	}
	
	/* nama luas tanah kebun */
	private $nama_luas_tanah_kebun;
	public function setNama_luas_tanah_kebun($name) {
	  $this->$nama_luas_tanah_kebun = $name;
	}
	
	public function getNama_luas_tanah_kebun() {
		$arrOptions = DlmActiveRecord::getLuasTanahOptions();
		$index = $this->luast_tanah_kebun;
		if($index > 0){
			$this->nama_luas_tanah_kebun = $arrOptions[$index];
		}
		return $this->nama_luas_tanah_kebun;
	}
	
	/* nama punya tambak */
	private $nama_punya_tambak;
	public function setNama_punya_tambak($name) {
	  $this->$nama_punya_tambak = $name;
	}
	
	public function getNama_punya_tambak() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_tambak;
		if($index > 0){
			$this->nama_punya_tambak = $arrOptions[$index];
		}
		return $this->nama_punya_tambak;
	}
	
	/* nama luas tambak */
	private $nama_luas_tambak;
	public function setNama_luas_tambak($name) {
	  $this->$nama_luas_tambak = $name;
	}
	
	public function getNama_luas_tambak() {
		$arrOptions = DlmActiveRecord::getLuasTanahOptions();
		$index = $this->luas_tambak;
		if($index > 0){
			$this->nama_luas_tambak = $arrOptions[$index];
		}
		return $this->nama_luas_tambak;
	}
	
	/* nama punya lembu */
	private $nama_punya_lembu;
	public function setNama_punya_lembu($name) {
	  $this->$nama_punya_lembu = $name;
	}
	
	public function getNama_punya_lembu() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_lembu;
		if($index > 0){
			$this->nama_punya_lembu = $arrOptions[$index];
		}
		return $this->nama_punya_lembu;
	}
	
	/* nama jumlah lembu */
	private $nama_jlh_lembu;
	public function setNama_jlh_lembu($name) {
	  $this->$nama_jlh_lembu = $name;
	}
	
	public function getNama_jlh_lembu() {
		$arrOptions = $this->getJumlahTernakOptions();
		$index = $this->jlh_lembu;
		if($index > 0){
			$this->nama_jlh_lembu = $arrOptions[$index];
		}
		return $this->nama_jlh_lembu;
	}
	
	/* nama punya kerbau */
	private $nama_punya_kerbau;
	public function setNama_punya_kerbau($name) {
	  $this->$nama_punya_kerbau = $name;
	}
	
	public function getNama_punya_kerbau() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_kerbau;
		if($index > 0){
			$this->nama_punya_kerbau = $arrOptions[$index];
		}
		return $this->nama_punya_kerbau;
	}
	
	/* nama jumlah kerbau */
	private $nama_jlh_kerbau;
	public function setNama_jlh_kerbau($name) {
	  $this->$nama_jlh_kerbau = $name;
	}
	
	public function getNama_jlh_kerbau() {
		$arrOptions = $this->getJumlahTernakOptions();
		$index = $this->jlh_kerbau;
		if($index > 0){
			$this->nama_jlh_kerbau = $arrOptions[$index];
		}
		return $this->nama_jlh_kerbau;
	}
	
	/* nama punya ternak lain */
	private $nama_punya_ternak_lain;
	public function setNama_punya_ternak_lain($name) {
	  $this->$nama_punya_ternak_lain = $name;
	}
	
	public function getNama_punya_ternak_lain() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_ternak_lain;
		if($index > 0){
			$this->nama_punya_ternak_lain = $arrOptions[$index];
		}
		return $this->nama_punya_ternak_lain;
	}
	
	/* nama jumlah ternak lain */
	private $nama_jlh_ternak_lain;
	public function setNama_jlh_ternak_lain($name) {
	  $this->$nama_jlh_ternak_lain = $name;
	}
	
	public function getNama_jlh_ternak_lain() {
		$arrOptions = $this->getJumlahTernakOptions();
		$index = $this->jlh_ternak_lain;
		if($index > 0){
			$this->nama_jlh_ternak_lain = $arrOptions[$index];
		}
		return $this->nama_jlh_ternak_lain;
	}
	
	/* nama punya mobil */
	private $nama_punya_mobil;
	public function setNama_punya_mobil($name) {
	  $this->$nama_punya_mobil = $name;
	}
	
	public function getNama_punya_mobil() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_mobil;
		if($index > 0){
			$this->nama_punya_mobil = $arrOptions[$index];
		}
		return $this->nama_punya_mobil;
	}
	
	/* nama jumlah mobil */
	private $nama_jlh_mobil;
	public function setNama_jlh_mobil($name) {
	  $this->$nama_jlh_mobil = $name;
	}
	
	public function getNama_jlh_mobil() {
		$arrOptions = $this->getJumlahKendaraanOptions();
		$index = $this->jlh_mobil;
		if($index > 0){
			$this->nama_jlh_mobil = $arrOptions[$index];
		}
		return $this->nama_jlh_mobil;
	}
	
	/* nama punya motor */
	private $nama_punya_motor;
	public function setNama_punya_motor($name) {
	  $this->$nama_punya_motor = $name;
	}
	
	public function getNama_punya_motor() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_motor;
		if($index > 0){
			$this->nama_punya_motor = $arrOptions[$index];
		}
		return $this->nama_punya_motor;
	}
	
	/* nama jumlah motor */
	private $nama_jlh_motor;
	public function setNama_jlh_motor($name) {
	  $this->$nama_jlh_motor = $name;
	}
	
	public function getNama_jlh_motor() {
		$arrOptions = $this->getJumlahKendaraanOptions();
		$index = $this->jlh_motor;
		if($index > 0){
			$this->nama_jlh_motor = $arrOptions[$index];
		}
		return $this->nama_jlh_motor;
	}
	
	/* nama punya tabungan */
	private $nama_punya_tabungan;
	public function setNama_punya_tabungan($name) {
	  $this->$nama_punya_tabungan = $name;
	}
	
	public function getNama_punya_tabungan() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_tabungan;
		if($index > 0){
			$this->nama_punya_tabungan = $arrOptions[$index];
		}
		return $this->nama_punya_tabungan;
	}
	
	/* nama punya asuransi */
	private $nama_punya_asuransi;
	public function setNama_punya_asuransi($name) {
	  $this->$nama_punya_asuransi = $name;
	}
	
	public function getNama_punya_asuransi() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_asuransi;
		if($index > 0){
			$this->nama_punya_asuransi = $arrOptions[$index];
		}
		return $this->nama_punya_asuransi;
	}
	
	/* nama punya motor tempel */
	private $nama_punya_motor_tempel;
	public function setNama_punya_motor_tempel($name) {
	  $this->$nama_punya_motor_tempel = $name;
	}
	
	public function getNama_punya_motor_tempel() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_motor_tempel;
		if($index > 0){
			$this->nama_punya_motor_tempel = $arrOptions[$index];
		}
		return $this->nama_punya_motor_tempel;
	}
	
	/* nama punya kapal motor */
	private $nama_punya_kapal_motor;
	public function setNama_punya_kapal_motor($name) {
	  $this->$nama_punya_kapal_motor = $name;
	}
	
	public function getNama_punya_kapal_motor() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_kapal_motor;
		if($index > 0){
			$this->nama_punya_kapal_motor = $arrOptions[$index];
		}
		return $this->nama_punya_kapal_motor;
	}
	
	/* nama punya perahu */
	private $nama_punya_perahu;
	public function setNama_punya_perahu($name) {
	  $this->$nama_punya_perahu = $name;
	}
	
	public function getNama_punya_perahu() {
		$arrOptions = DlmActiveRecord::getStatusYaTidakOptions();
		$index = $this->punya_perahu;
		if($index > 0){
			$this->nama_punya_perahu = $arrOptions[$index];
		}
		return $this->nama_punya_perahu;
	}
}