<?php

/**
 * This is the model class for table "USER".
 *
 * The followings are the available columns in table 'USER':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $nik
 * @property integer $role_id
 *
 * The followings are the available model relations:
 * @property ARTIKEL[] $aRTIKELs
 */
class User extends CActiveRecord
{
	public $pwd;
	public $pwd_repeat;
	/*
	* role id
	*/
	const ROLE_SUPERADMIN = 0;
	const ROLE_ADMIN = 1;
	const ROLE_KABUPATEN = 2;
	const ROLE_KECAMATAN = 3;
	const ROLE_GAMPONG = 4;

	public static function getRoleOptions()
	{
		$roleid = Yii::app()->user->getRoleId();
		if($roleid == 0){
			return array(
				self::ROLE_ADMIN => 'Admin',
				self::ROLE_KABUPATEN => 'Kabupaten',
				self::ROLE_KECAMATAN => 'Kecamatan',
				self::ROLE_GAMPONG => 'Gampong'
			);
		}else if($roleid == 1){
			return array(
				self::ROLE_KABUPATEN => 'Kabupaten',
				self::ROLE_KECAMATAN => 'Kecamatan',
				self::ROLE_GAMPONG => 'Gampong'
			);
		}
	}
	
	public static function getRoleById($idx) {
		
		$arrOptions = User::getRoleOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "Superadmin";
		}
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'USER';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, fullname, role_id, location_name', 'required'),
			array('pwd, pwd_repeat', 'required', 'on'=>'insert'),
            array('pwd, pwd_repeat', 'length', 'min'=>5, 'max'=>40),
            array('pwd', 'compare', 'compareAttribute'=>'pwd_repeat'),
			array('role_id', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>100),
			array('username, email', 'unique'),
			// array('password', 'compare', 'compareAttribute'=>'password2', 'on'=>'register'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, role_id, email, fullname, location_name', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aRTIKELs' => array(self::HAS_MANY, 'ARTIKEL', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'role_id' => 'Role',
			'fullname' => 'Nama Lengkap',
			'email' => 'Email',
			'location_id' => 'Lokasi',
			'location_name' => 'Lokasi',
			'pwd' => 'Password',
			'pwd_repeat' => 'Password Repeat'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$roleid = Yii::app()->user->getRoleId();

		$criteria=new CDbCriteria;

		$criteria->compare('username',$this->username,true);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('role_id',$this->role_id);
		$criteria->compare('location_name',$this->fullname,true);
		
		if( $roleid == 0){
			$criteria->addCondition('"role_id">0');
		}else if($roleid == 1){
			$criteria->addCondition('"role_id">1');
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	private function encriptPassword($password)
	{
		if(empty($this->pwd)){
			return $password;
		}
		$new_hash = md5($this->pwd);

		if ($new_hash == $password ){
			return $password;
		}
		return $new_hash;
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave()){
			if($this->getIsNewRecord()){
				$this->setAttribute('password', md5($this->pwd));
			}else{
				$model=User::model()->findByPk($this->id);
				if($model===null){
					throw new CHttpException(404,'The requested page does not exist.');
				}
				
				$this->setAttribute('password', $this->encriptPassword($model->password));
			}
			return true;
		}
		return false;
	}

	/*protected function beforeSave()
	{
		if(parent::beforeSave()){
			if($this->getIsNewRecord()){
				$this->setAttribute('password', md5($this->pwd));
			}else{
				$this->setAttribute('password', $this->encriptPassword());
			}
			return true;
		}
		return false;
	}*/
}