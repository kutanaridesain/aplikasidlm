<?php

/**
 * This is the model class for table "DAYAH".
 *
 * The followings are the available columns in table 'DAYAH':
 * @property integer $id
 * @property integer $id_kab
 * @property string $nama_dayah
 * @property string $nama_pimpinan
 * @property integer $type_dayah
 * @property integer $jlh_pengasuh
 * @property integer $jlh_santri
 * @property integer $nama_tingkat
 * @property integer $jlh_kelas
 * @property integer $jlh_perpustakaan
 * @property integer $jlh_lab_komputer
 * @property integer $jlh_mushalla
 * @property integer $jlh_kamar_mandi_wc
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property KABUPATEN $idKab
 * @property USER $user
 */
class Dayah extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Dayah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'DAYAH';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kab, nama_dayah, nama_pimpinan, type_dayah, jlh_pengasuh, jlh_santri, nama_tingkat, jlh_kelas, jlh_perpustakaan, jlh_lab_komputer, jlh_mushalla, jlh_kamar_mandi_wc', 'required'),
			array('id_kab, type_dayah, jlh_pengasuh, jlh_santri, nama_tingkat, jlh_kelas, jlh_perpustakaan, jlh_lab_komputer, jlh_mushalla, jlh_kamar_mandi_wc', 'numerical', 'integerOnly'=>true),
			array('nama_dayah, nama_pimpinan', 'length', 'max'=>200),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_kab, nama_dayah, nama_pimpinan, type_dayah, nama_type_dayah_search, jlh_pengasuh, jlh_santri, nama_tingkat, jlh_kelas, jlh_perpustakaan, jlh_lab_komputer, jlh_mushalla, jlh_kamar_mandi_wc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idKab' => array(self::BELONGS_TO, 'KABUPATEN', 'id_kab'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_kab' => 'Nama Kabupaten',
			'nama_kab' => 'Nama Kabupaten',
			'nama_dayah' => 'Nama Dayah',
			'nama_pimpinan' => 'Nama Pimpinan',
			'type_dayah' => 'Type Dayah',
			'nama_type_dayah' => 'Type Dayah',
			'jlh_pengasuh' => 'Jumlah Pengasuh',
			'jlh_santri' => 'Jumlah Santri',
			'nama_tingkat' => 'Nama Tingkat',
			'jlh_kelas' => 'Jumlah Kelas',
			'jlh_perpustakaan' => 'Perpustakaan',
			'jlh_lab_komputer' => 'Lab Komputer',
			'jlh_mushalla' => 'Mushalla',
			'jlh_kamar_mandi_wc' => 'Kamar Mandi/WC',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$roleid = Yii::app()->user->getRoleId();

		$criteria=new CDbCriteria;

		$criteria->with = array( 'idKab' );
		$criteria->compare('id',$this->id);
		$criteria->compare('id_kab',$this->id_kab);
		$criteria->compare('nama_dayah',$this->nama_dayah,true);
		$criteria->compare('nama_pimpinan',$this->nama_pimpinan,true);
		$criteria->compare('type_dayah',$this->type_dayah);
		$criteria->compare('jlh_pengasuh',$this->jlh_pengasuh);
		$criteria->compare('jlh_santri',$this->jlh_santri);
		$criteria->compare('nama_tingkat',$this->nama_tingkat);
		$criteria->compare('jlh_kelas',$this->jlh_kelas);
		$criteria->compare('jlh_perpustakaan',$this->jlh_perpustakaan);
		$criteria->compare('jlh_lab_komputer',$this->jlh_lab_komputer);
		$criteria->compare('jlh_mushalla',$this->jlh_mushalla);
		$criteria->compare('jlh_kamar_mandi_wc',$this->jlh_kamar_mandi_wc);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		if($roleid>1){
			$kabid = Yii::app()->user->getKabupatenId();
			$criteria->addCondition(
				'"id_kab" = ' . $kabid
			);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			        'sort'=>array(
						'attributes'=>array(
							'nama_kab_search'=>array(
								'asc'=>'idKab.nama_kabkot',
								'desc'=>'idKab.nama_kabkot DESC',
							),
							'*',
						),
					),
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	protected function afterFind()
	{
		$roleid = Yii::app()->user->getRoleId();
		
		$act = Yii::app()->controller->action->id;
		
		if( $act != "index"){
			if($roleid>1){
				$kabid = Yii::app()->user->getKabupatenId();
				if($this->id_kab != $kabid){	
					throw new CHttpException(401,'Tidak ada hak akses.');
				}
			}
		}
	}
	
	/* kabupaten */
	private $nama_kab;
	public function setNama_kab($name) {
	  $this->nama_kab = $name;
	}

	public function getNama_kab() {
		if($this->idKab != null){
			$this->nama_kab = $this->idKab->nama_kabkot;
		}
		return $this->nama_kab;
	}
	
	/* nama type dayah */
	public $nama_type_dayah_search;
	private $nama_type_dayah;
	public function setNama_type_dayah($name) {
	  $this->$nama_type_dayah = $name;
	}
	
	public function getNama_type_dayah() {
		$arrOptions = DlmActiveRecord::getTypeDayahOptions();
		$index = $this->type_dayah;
		if($index > 0){
			$this->nama_type_dayah = $arrOptions[$index];
		}
		return $this->nama_type_dayah;
	}
	
	public static function getNamaTypeDayahById($idx) {
		
		$arrOptions = DlmActiveRecord::getTypeDayahOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
}