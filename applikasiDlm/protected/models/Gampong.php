<?php

/**
 * This is the model class for table "GAMPONG".
 *
 * The followings are the available columns in table 'GAMPONG':
 * @property integer $id
 * @property integer $id_kab
 * @property integer $id_kec
 * @property string $nama_gampong
 * @property string $alamat
 * @property string $keuchik
 * @property string $visi
 * @property string $misi
 * @property string $koordinat_x
 * @property string $koordinat_y
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA[] $bIODATAs
 * @property KABUPATEN $idKab
 * @property KECAMATAN $idKec
 * @property USER $user
 */
class Gampong extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Gampong the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'GAMPONG';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kab, nama_kec, nama_gampong, alamat, keuchik, visi, misi, koordinat_x, koordinat_y', 'required'),
			array('id_kab, id_kec', 'numerical', 'integerOnly'=>true),
			array('nama_gampong, keuchik', 'length', 'max'=>200),
			array('alamat', 'length', 'max'=>1000),
			array('visi, misi', 'length', 'max'=>4000),
			array('koordinat_x, koordinat_y', 'length', 'max'=>100),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_kab, id_kec, nama_gampong, alamat, keuchik, visi, misi, koordinat_x, koordinat_y', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bIODATAs' => array(self::HAS_MANY, 'BIODATA', 'no_kel'),
			'idKab' => array(self::BELONGS_TO, 'KABUPATEN', 'id_kab'),
			'idKec' => array(self::BELONGS_TO, 'KECAMATAN', 'id_kec'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_kab' => 'Kabupaten',
			'nama_kab' => 'Kabupaten',
			'id_kec' => 'Kecamatan',
			'nama_kec' => 'Kecamatan',
			'nama_gampong' => 'Gampong',
			'alamat' => 'Alamat',
			'keuchik' => 'Keuchik',
			'visi' => 'Visi',
			'misi' => 'Misi',
			'koordinat_x' => 'Koordinat Longitude',
			'koordinat_y' => 'Koordinat Latitude',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_kab',$this->id_kab);
		$criteria->compare('id_kec',$this->id_kec);
		$criteria->compare('nama_gampong',$this->nama_gampong,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('keuchik',$this->keuchik,true);
		$criteria->compare('visi',$this->visi,true);
		$criteria->compare('misi',$this->misi,true);
		$criteria->compare('koordinat_x',$this->koordinat_x,true);
		$criteria->compare('koordinat_y',$this->koordinat_y,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	/* kecamatan */
	private $nama_kec;
	public function setNama_kec($name) {
	  $this->nama_kec = $name;
	}
	
	public function getNama_kec() {
		if($this->idKec != null){
			$this->nama_kec = $this->idKec->nama_kec;
		}
		return $this->nama_kec;
	}
	
	/* kabupaten */
	private $nama_kab;
	public function setNama_kab($name) {
	  $this->nama_kab = $name;
	}

	public function getNama_kab() {
		if($this->idKab != null){
			$this->nama_kab = $this->idKab->nama_kabkot;
		}
		return $this->nama_kab;
	}
}