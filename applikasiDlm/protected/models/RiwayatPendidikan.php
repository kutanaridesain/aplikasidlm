<?php

/**
 * This is the model class for table "RIWAYAT_PENDIDIKAN".
 *
 * The followings are the available columns in table 'RIWAYAT_PENDIDIKAN':
 * @property integer $id
 * @property string $nik
 * @property string $no_kk
 * @property string $jenjang_sekolah
 * @property integer $kode_sekolah
 * @property string $kelas
 * @property integer $tahun
 * @property integer $dapat_beasiswa
 * @property double $jlh_beasiswa
 * @property string $pemberi_beasiswa
 * @property integer $status_pendidikan
 * @property string $mengapa_tdk_sekolah
 * @property integer $thn_putus_sekolah
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 * @property integer $thn_beasiswa
 *
 * The followings are the available model relations:
 * @property BIODATA $nik0
 * @property KEPALAKELUARGA $noKk
 * @property SEKOLAH $kodeSekolah
 * @property USER $user
 */
class RiwayatPendidikan extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RiwayatPendidikan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'RIWAYAT_PENDIDIKAN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, no_kk, jenjang_sekolah, kelas, tahun, dapat_beasiswa, status_pendidikan', 'required'),
			array('kode_sekolah, tahun, dapat_beasiswa, status_pendidikan, thn_putus_sekolah, userid, thn_beasiswa', 'numerical', 'integerOnly'=>true),
			array('jlh_beasiswa', 'numerical'),
			array('nik, no_kk, kelas', 'length', 'max'=>100),
			array('jenjang_sekolah, pemberi_beasiswa', 'length', 'max'=>200),
			array('mengapa_tdk_sekolah', 'length', 'max'=>400),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nik, no_kk, jenjang_sekolah, kode_sekolah, kelas, tahun, dapat_beasiswa, jlh_beasiswa, pemberi_beasiswa, status_pendidikan, mengapa_tdk_sekolah, thn_putus_sekolah, thn_beasiswa', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'noKk' => array(self::BELONGS_TO, 'KEPALAKELUARGA', 'no_kk'),
			'kodeSekolah' => array(self::BELONGS_TO, 'SEKOLAH', 'kode_sekolah'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nik' => 'NIK',
			'nama_penduduk' => 'Nama',
			'no_kk' => 'No. KK',
			'jenjang_sekolah' => 'Jenjang Sekolah',
			'kode_sekolah' => 'Nama Sekolah',
			'nama_sekolah' => 'Nama Sekolah',
			'kelas' => 'Kelas',
			'tahun' => 'Tahun',
			'dapat_beasiswa' => 'Dapat Beasiswa',
			'jlh_beasiswa' => 'Jumlah Beasiswa',
			'pemberi_beasiswa' => 'Pemberi Beasiswa',
			'status_pendidikan' => 'Status Pendidikan',
			'mengapa_tdk_sekolah' => 'Alasan Putus Sekolah',
			'thn_putus_sekolah' => 'Tahun',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
			'thn_beasiswa' => 'Tahun Beasiswa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('no_kk',$this->no_kk,true);
		$criteria->compare('jenjang_sekolah',$this->jenjang_sekolah,true);
		$criteria->compare('kode_sekolah',$this->kode_sekolah);
		$criteria->compare('kelas',$this->kelas,true);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('dapat_beasiswa',$this->dapat_beasiswa);
		$criteria->compare('jlh_beasiswa',$this->jlh_beasiswa);
		$criteria->compare('pemberi_beasiswa',$this->pemberi_beasiswa,true);
		$criteria->compare('status_pendidikan',$this->status_pendidikan);
		$criteria->compare('mengapa_tdk_sekolah',$this->mengapa_tdk_sekolah,true);
		$criteria->compare('thn_putus_sekolah',$this->thn_putus_sekolah);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		$criteria->compare('thn_beasiswa',$this->thn_beasiswa);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave()){  
			
			if($this->dapat_beasiswa == 2){
				$this->jlh_beasiswa = NULL;
				$this->pemberi_beasiswa = NULL;
				$this->thn_beasiswa = NULL;
			}
			
			if($this->status_pendidikan == 2){
				$this->mengapa_tdk_sekolah = NULL;
				$this->thn_putus_sekolah = NULL;
			}
			
			return true;
	   }
		
		return false;
	}
	
	/* sekolah */
	private $nama_sekolah;
	public function setNama_sekolah($name) {
	  $this->nama_sekolah = $name;
	}

	public function getNama_sekolah() {
		if($this->kodeSekolah != null){
			$this->nama_sekolah = $this->kodeSekolah->nama_sekolah;
		}
		return $this->nama_sekolah;
	}
	
	/* penduduk */
	private $nama_penduduk;
	public function setNama_penduduk($name) {
	  $this->nama_penduduk = $name;
	}

	public function getNama_penduduk(){
		if($this->nik0 != null){
			$this->nama_penduduk = $this->nik0->nama_lgkp;
		}
		return $this->nama_penduduk;
	}
	
	/* no kk */
	public $no_kk;
	public function setNo_kk($name) {
	  $this->no_kk = $name;
	}

	public function getNo_kk(){
		if($this->nik0 != null){
			$this->no_kk = $this->nik0->no_kk;
		}
		return $this->no_kk;
	}
}