<?php

/**
 * This is the model class for table "VIEW_LAPORAN_ASSET".
 *
 * The followings are the available columns in table 'VIEW_LAPORAN_ASSET':
 * @property string $nik
 * @property string $no_kk
 * @property integer $punya_tanah_rumah
 * @property integer $luas_tanah_rumah
 * @property integer $punya_tanah_sawah
 * @property integer $luas_tanah_sawah
 * @property integer $punya_tanah_kebun
 * @property integer $luast_tanah_kebun
 * @property integer $punya_tambak
 * @property integer $luas_tambak
 * @property integer $punya_lembu
 * @property integer $jlh_lembu
 * @property integer $punya_kerbau
 * @property integer $jlh_kerbau
 * @property integer $punya_ternak_lain
 * @property integer $jlh_ternak_lain
 * @property integer $punya_mobil
 * @property integer $jlh_mobil
 * @property integer $punya_motor
 * @property integer $jlh_motor
 * @property integer $punya_tabungan
 * @property integer $punya_asuransi
 * @property integer $punya_motor_tempel
 * @property integer $punya_kapal_motor
 * @property integer $punya_perahu
 * @property string $punya_aset_lain
 * @property string $nama_lgkp
 * @property string $hub_keluarga
 * @property string $tgl_lhr
 * @property integer $agama
 * @property string $pekerjaan
 * @property string $alamat
 */
class ViewLaporanAsset extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewLaporanAsset the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VIEW_LAPORAN_ASSET';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, no_kk, punya_tanah_rumah, punya_tanah_sawah, punya_tanah_kebun, punya_tambak, punya_lembu, punya_kerbau, punya_ternak_lain, punya_mobil, punya_motor, punya_tabungan, punya_asuransi, punya_motor_tempel, punya_kapal_motor, punya_perahu, nama_lgkp, tgl_lhr, agama', 'required'),
			array('punya_tanah_rumah, luas_tanah_rumah, punya_tanah_sawah, luas_tanah_sawah, punya_tanah_kebun, luast_tanah_kebun, punya_tambak, luas_tambak, punya_lembu, jlh_lembu, punya_kerbau, jlh_kerbau, punya_ternak_lain, jlh_ternak_lain, punya_mobil, jlh_mobil, punya_motor, jlh_motor, punya_tabungan, punya_asuransi, punya_motor_tempel, punya_kapal_motor, punya_perahu, agama', 'numerical', 'integerOnly'=>true),
			array('nik, no_kk', 'length', 'max'=>100),
			array('punya_aset_lain, hub_keluarga, pekerjaan', 'length', 'max'=>4000),
			array('nama_lgkp', 'length', 'max'=>200),
			array('alamat', 'length', 'max'=>620),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nik, no_kk, punya_tanah_rumah, luas_tanah_rumah, punya_tanah_sawah, luas_tanah_sawah, punya_tanah_kebun, luast_tanah_kebun, punya_tambak, luas_tambak, punya_lembu, jlh_lembu, punya_kerbau, jlh_kerbau, punya_ternak_lain, jlh_ternak_lain, punya_mobil, jlh_mobil, punya_motor, jlh_motor, punya_tabungan, punya_asuransi, punya_motor_tempel, punya_kapal_motor, punya_perahu, punya_aset_lain, nama_lgkp, hub_keluarga, tgl_lhr, agama, pekerjaan, alamat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nik' => 'Nik',
			'no_kk' => 'No Kk',
			'punya_tanah_rumah' => 'Punya Tanah Rumah',
			'luas_tanah_rumah' => 'Luas Tanah Rumah',
			'punya_tanah_sawah' => 'Punya Tanah Sawah',
			'luas_tanah_sawah' => 'Luas Tanah Sawah',
			'punya_tanah_kebun' => 'Punya Tanah Kebun',
			'luast_tanah_kebun' => 'Luast Tanah Kebun',
			'punya_tambak' => 'Punya Tambak',
			'luas_tambak' => 'Luas Tambak',
			'punya_lembu' => 'Punya Lembu',
			'jlh_lembu' => 'Jlh Lembu',
			'punya_kerbau' => 'Punya Kerbau',
			'jlh_kerbau' => 'Jlh Kerbau',
			'punya_ternak_lain' => 'Punya Ternak Lain',
			'jlh_ternak_lain' => 'Jlh Ternak Lain',
			'punya_mobil' => 'Punya Mobil',
			'jlh_mobil' => 'Jlh Mobil',
			'punya_motor' => 'Punya Motor',
			'jlh_motor' => 'Jlh Motor',
			'punya_tabungan' => 'Punya Tabungan',
			'punya_asuransi' => 'Punya Asuransi',
			'punya_motor_tempel' => 'Punya Motor Tempel',
			'punya_kapal_motor' => 'Punya Kapal Motor',
			'punya_perahu' => 'Punya Perahu',
			'punya_aset_lain' => 'Punya Aset Lain',
			'nama_lgkp' => 'Nama Lgkp',
			'hub_keluarga' => 'Hub Keluarga',
			'tgl_lhr' => 'Tgl Lhr',
			'agama' => 'Agama',
			'pekerjaan' => 'Pekerjaan',
			'alamat' => 'Alamat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('no_kk',$this->no_kk,true);
		$criteria->compare('punya_tanah_rumah',$this->punya_tanah_rumah);
		$criteria->compare('luas_tanah_rumah',$this->luas_tanah_rumah);
		$criteria->compare('punya_tanah_sawah',$this->punya_tanah_sawah);
		$criteria->compare('luas_tanah_sawah',$this->luas_tanah_sawah);
		$criteria->compare('punya_tanah_kebun',$this->punya_tanah_kebun);
		$criteria->compare('luast_tanah_kebun',$this->luast_tanah_kebun);
		$criteria->compare('punya_tambak',$this->punya_tambak);
		$criteria->compare('luas_tambak',$this->luas_tambak);
		$criteria->compare('punya_lembu',$this->punya_lembu);
		$criteria->compare('jlh_lembu',$this->jlh_lembu);
		$criteria->compare('punya_kerbau',$this->punya_kerbau);
		$criteria->compare('jlh_kerbau',$this->jlh_kerbau);
		$criteria->compare('punya_ternak_lain',$this->punya_ternak_lain);
		$criteria->compare('jlh_ternak_lain',$this->jlh_ternak_lain);
		$criteria->compare('punya_mobil',$this->punya_mobil);
		$criteria->compare('jlh_mobil',$this->jlh_mobil);
		$criteria->compare('punya_motor',$this->punya_motor);
		$criteria->compare('jlh_motor',$this->jlh_motor);
		$criteria->compare('punya_tabungan',$this->punya_tabungan);
		$criteria->compare('punya_asuransi',$this->punya_asuransi);
		$criteria->compare('punya_motor_tempel',$this->punya_motor_tempel);
		$criteria->compare('punya_kapal_motor',$this->punya_kapal_motor);
		$criteria->compare('punya_perahu',$this->punya_perahu);
		$criteria->compare('punya_aset_lain',$this->punya_aset_lain,true);
		$criteria->compare('nama_lgkp',$this->nama_lgkp,true);
		$criteria->compare('hub_keluarga',$this->hub_keluarga,true);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('agama',$this->agama);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);
		$criteria->compare('alamat',$this->alamat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* nama no kk nik */
	public static function getNamaNoKkNik($nama, $no_kk, $nik) {
		return $nama."<br/>".$no_kk."<br/>".$nik;
	}
	
	/* status keluarga tanggal lahir */
	public static function getStatusKeluargaTanggalLahir($hub_keluarga, $tgl_lhr) {
		return DlmActiveRecord::getHubunganKeluargaById($hub_keluarga)."<br/>".$tgl_lhr;
	}
	
	/* agama pekerjaan */
	public static function getAgamaPekerjaan($agama, $pekerjaan) {
		return DlmActiveRecord::getAgamaById($agama)."<br/>".$pekerjaan;
	}
	
	/* tanah */
	public static function getTanah($punya, $luas) {
		return DlmActiveRecord::getNamaStatusYaTidakById($punya)."<br/>".DlmActiveRecord::getNamaLuasTanahById($luas);
	}
	
	/* ternak */
	public static function getTernak($punya, $jlh) {
		return DlmActiveRecord::getNamaStatusYaTidakById($punya)."<br/>".DlmActiveRecord::getTernakById($jlh);
	}
}