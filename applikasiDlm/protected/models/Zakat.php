<?php

/**
 * This is the model class for table "ZAKAT".
 *
 * The followings are the available columns in table 'ZAKAT':
 * @property integer $id
 * @property integer $id_jenis_zakat
 * @property integer $nik
 * @property integer $id_instansi
 * @property double $jlh_zakat
 * @property string $tgl_diberikan
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 */
class Zakat extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Zakat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ZAKAT';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jenis_zakat, nik, nama_penduduk, nama_instansi, jlh_zakat, tgl_diberikan', 'required'),
			array('id_jenis_zakat, nik, id_instansi, userid', 'numerical', 'integerOnly'=>true),
			array('jlh_zakat', 'numerical'),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_jenis_zakat, nik, id_instansi, jlh_zakat, tgl_diberikan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'idInstansi' => array(self::BELONGS_TO, 'INSTANSI', 'id_instansi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_jenis_zakat' => 'Jenis Zakat',
			'nama_zakat' => 'Jenis Zakat',
			'nik' => 'NIK',
			'nama_penduduk' => 'Nama Penduduk',
			'id_instansi' => 'Instansi Pemberi',
			'nama_instansi' => 'Instansi Pemberi',
			'jlh_zakat' => 'Jumlah Zakat',
			'tgl_diberikan' => 'Tanggal Diberikan',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$roleid = Yii::app()->user->getRoleId();
		$locaid = Yii::app()->user->getLocationId();

		$criteria=new CDbCriteria;
		$criteria->with='nik0';
		//$criteria->with = array('idInstansi' );
		$criteria->compare('id',$this->id);
		$criteria->compare('id_jenis_zakat', $this->id_jenis_zakat);
		$criteria->compare('nik',$this->nik);
		$criteria->compare('id_instansi', $this->id_instansi);
		$criteria->compare('jlh_zakat',$this->jlh_zakat);
		$criteria->compare('tgl_diberikan',$this->tgl_diberikan,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		
		if($roleid==2){
			$criteria->addCondition(
				'"nik0"."no_kab" = ' . $locaid
			);
		}
		if($roleid==3){
			$criteria->addCondition(
				'"nik0"."no_kec" = ' . $locaid
			);
		}
		if($roleid==4){
			$criteria->addCondition(
				'"nik0"."no_kel" = ' . $locaid
			);
		}
		
		$criteria->together = true;
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,			
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	/* nama penduduk */
	private $nama_penduduk;
	public function setNama_penduduk($name) {
	  $this->nama_penduduk = $name;
	}
	
	public function getNama_penduduk() {
		if($this->nik0 != null){
			$this->nama_penduduk = $this->nik0->nama_lgkp;
		}
		return $this->nama_penduduk;
	}
	
	/* nama instansi */
	private $nama_instansi;
	public function setNama_instansi($name) {
	  $this->nama_instansi = $name;
	}
	
	public function getNama_instansi() {
		if($this->idInstansi != null){
			$this->nama_instansi = $this->idInstansi->nama_instansi;
		}
		return $this->nama_instansi;
	}
	
	/* nama zakat */
	private $nama_zakat;
	public function setNama_zakat($name) {
	  $this->$nama_zakat = $name;
	}
	
	public function getNama_zakat() {
		$arrOptions = DlmActiveRecord::getJenisZakatOptions();
		$index = $this->id_jenis_zakat;
		if($index > 0){
			$this->nama_zakat = $arrOptions[$index];
		}
		return $this->nama_zakat;
	}
	
	public static function getNamaZakatById($idx) {
		
		$arrOptions = DlmActiveRecord::getJenisZakatOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
}