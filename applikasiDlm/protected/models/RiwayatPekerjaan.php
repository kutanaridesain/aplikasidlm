<?php

/**
 * This is the model class for table "RIWAYAT_PEKERJAAN".
 *
 * The followings are the available columns in table 'RIWAYAT_PEKERJAAN':
 * @property integer $id
 * @property integer $nik
 * @property integer $id_pek
 * @property string $tempat_bekerja
 * @property integer $rata_penghasilan
 * @property integer $tahun
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA[] $bIODATAs
 * @property BIODATA $nik0
 * @property PEKERJAAN $idPek
 * @property USER $user
 */
class RiwayatPekerjaan extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RiwayatPekerjaan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'RIWAYAT_PEKERJAAN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, nama_penduduk, no_kk, nama_pekerjaan, tempat_bekerja, rata_penghasilan, tahun', 'required'),
			array('nik, id_pek, rata_penghasilan, tahun', 'numerical', 'integerOnly'=>true),
			array('tempat_bekerja', 'length', 'max'=>200),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_penduduk, nama_pekerjaan, tempat_bekerja, rata_penghasilan, tahun', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bIODATAs' => array(self::HAS_MANY, 'BIODATA', 'kode_pkrj'),
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'idPek' => array(self::BELONGS_TO, 'PEKERJAAN', 'id_pek'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nik' => 'NIK',
			'nama_penduduk' => 'Nama',
			'no_kk' => 'No. KK',
			'id_pek' => 'Nama Pekerjaan',
			'nama_pekerjaan' => 'Nama Pekerjaan',
			'tempat_bekerja' => 'Tempat Bekerja',
			'rata_penghasilan' => 'Rata Penghasilan',
			'tahun' => 'Tahun',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nik',$this->nik);
		$criteria->compare('nama_penduduk',$this->nama_penduduk);
		$criteria->compare('no_kk',$this->no_kk);
		$criteria->compare('id_pek',$this->id_pek);
		$criteria->compare('tempat_bekerja',$this->tempat_bekerja,true);
		$criteria->compare('rata_penghasilan',$this->rata_penghasilan);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	/* pekerjaan */
	private $nama_pekerjaan;
	public function setNama_pekerjaan($name) {
	  $this->nama_pekerjaan = $name;
	}
	
	public function getNama_pekerjaan() {
		if($this->idPek != null){
			$this->nama_pekerjaan = $this->idPek->nama_pekerjaan;
		}
		return $this->nama_pekerjaan;
	}
	
	/* penduduk */
	private $nama_penduduk;
	public function setNama_penduduk($name) {
	  $this->nama_penduduk = $name;
	}

	public function getNama_penduduk(){
		if($this->nik0 != null){
			$this->nama_penduduk = $this->nik0->nama_lgkp;
		}
		return $this->nama_penduduk;
	}
	
	/* no kk */
	private $no_kk;
	public function setNo_kk($name) {
	  $this->no_kk = $name;
	}

	public function getNo_kk(){
		if($this->nik0 != null){
			$this->no_kk = $this->nik0->no_kk;
		}
		return $this->no_kk;
	}
}