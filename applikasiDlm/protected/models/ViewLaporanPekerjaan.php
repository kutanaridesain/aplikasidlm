<?php

/**
 * This is the model class for table "VIEW_LAPORAN_PEKERJAAN".
 *
 * The followings are the available columns in table 'VIEW_LAPORAN_PEKERJAAN':
 * @property string $nama_lgkp
 * @property string $nik
 * @property integer $id_pek
 * @property string $nama_pekerjaan
 * @property string $tempat_bekerja
 * @property integer $rata_penghasilan
 * @property integer $tahun
 * @property string $hub_keluarga
 * @property string $tgl_lhr
 * @property integer $agama
 * @property string $pendidikan
 * @property integer $stat_kwn
 * @property string $alamat
 */
class ViewLaporanPekerjaan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewLaporanPekerjaan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VIEW_LAPORAN_PEKERJAAN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_lgkp, nik, id_pek, nama_pekerjaan, tempat_bekerja, rata_penghasilan, tahun, tgl_lhr, agama, stat_kwn', 'required'),
			array('id_pek, rata_penghasilan, tahun, agama, stat_kwn', 'numerical', 'integerOnly'=>true),
			array('nama_lgkp, tempat_bekerja', 'length', 'max'=>200),
			array('nik', 'length', 'max'=>100),
			array('nama_pekerjaan', 'length', 'max'=>300),
			array('hub_keluarga, pendidikan', 'length', 'max'=>4000),
			array('alamat', 'length', 'max'=>620),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nama_lgkp, nik, id_pek, nama_pekerjaan, tempat_bekerja, rata_penghasilan, tahun, hub_keluarga, tgl_lhr, agama, pendidikan, stat_kwn, alamat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nama_lgkp' => 'Nama Lgkp',
			'nik' => 'Nik',
			'id_pek' => 'Id Pek',
			'nama_pekerjaan' => 'Nama Pekerjaan',
			'tempat_bekerja' => 'Tempat Bekerja',
			'rata_penghasilan' => 'Rata Penghasilan',
			'tahun' => 'Tahun',
			'hub_keluarga' => 'Hub Keluarga',
			'tgl_lhr' => 'Tgl Lhr',
			'agama' => 'Agama',
			'pendidikan' => 'Pendidikan',
			'stat_kwn' => 'Stat Kwn',
			'alamat' => 'Alamat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nama_lgkp',$this->nama_lgkp,true);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('id_pek',$this->id_pek);
		$criteria->compare('nama_pekerjaan',$this->nama_pekerjaan,true);
		$criteria->compare('tempat_bekerja',$this->tempat_bekerja,true);
		$criteria->compare('rata_penghasilan',$this->rata_penghasilan);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('hub_keluarga',$this->hub_keluarga,true);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('agama',$this->agama);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('stat_kwn',$this->stat_kwn);
		$criteria->compare('alamat',$this->alamat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* status keluarga */
	public static function getStatusKeluarga($hub_keluarga) {
		return DlmActiveRecord::getHubunganKeluargaById($hub_keluarga);
	}
	
	/* agama */
	public static function getAgama($agama) {
		return DlmActiveRecord::getAgamaById($agama);
	}
	
	/* status kawin */
	public static function getStatusKawin($status) {
		return DlmActiveRecord::getStatusKawinById($status);
	}
}