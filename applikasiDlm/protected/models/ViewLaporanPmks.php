<?php

/**
 * This is the model class for table "VIEW_LAPORAN_PMKS".
 *
 * The followings are the available columns in table 'VIEW_LAPORAN_PMKS':
 * @property string $nama
 * @property integer $hub_keluarga
 * @property string $tgl_lhr
 * @property string $nik_nama_ibu
 * @property string $nik_nama_ayah
 * @property string $nama_pmks
 * @property integer $id_jenis_pmks
 * @property integer $agama
 * @property integer $no_kab
 * @property string $nama_kabkot
 * @property integer $no_kec
 * @property string $nama_kec
 * @property integer $no_kel
 * @property string $nama_gampong
 * @property string $alamat
 * @property integer $dititip_di
 * @property integer $id_pantiasuhan
 * @property string $nama_pantiasuhan
 */
class ViewLaporanPmks extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewLaporanPmks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VIEW_LAPORAN_PMKS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tgl_lhr, nama_pmks, id_jenis_pmks, agama, no_kab, nama_kabkot, no_kec, nama_kec, no_kel, nama_gampong, dititip_di', 'required'),
			array('hub_keluarga, id_jenis_pmks, agama, no_kab, no_kec, no_kel, dititip_di, id_pantiasuhan', 'numerical', 'integerOnly'=>true),
			array('nama', 'length', 'max'=>420),
			array('nama_pmks', 'length', 'max'=>400),
			array('nama_kabkot, nama_kec, nama_gampong', 'length', 'max'=>200),
			array('alamat', 'length', 'max'=>620),
			array('nama_pantiasuhan', 'length', 'max'=>410),
			array('nik_nama_ibu, nik_nama_ayah', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nama, hub_keluarga, tgl_lhr, nik_nama_ibu, nik_nama_ayah, nama_pmks, id_jenis_pmks, agama, no_kab, nama_kabkot, no_kec, nama_kec, no_kel, nama_gampong, alamat, dititip_di, id_pantiasuhan, nama_pantiasuhan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nama' => 'NAMA <br/>NO. KK/NIK',
			'hub_keluarga' => 'STATUS KELUARGA <br/> TGL LAHIR',
			'tgl_lhr' => 'Tgl Lhr',
			'nik_nama_ibu' => 'NIK IBU <br/> NAMA IBU',
			'nik_nama_ayah' => 'NIK AYAH <br/> NAMA AYAH',
			'nama_pmks' => 'Nama Pmks',
			'id_jenis_pmks' => 'Id Jenis Pmks',
			'agama' => 'AGAMA',
			'no_kab' => 'No Kab',
			'nama_kabkot' => 'Nama Kabkot',
			'no_kec' => 'No Kec',
			'nama_kec' => 'Nama Kec',
			'no_kel' => 'No Kel',
			'nama_gampong' => 'Nama Gampong',
			'alamat' => 'ALAMAT <br/>(GP/KEC/KAB)',
			'dititip_di' => 'Dititip Di',
			'id_pantiasuhan' => 'Id Pantiasuhan',
			'nama_pantiasuhan' => 'NAMA PANTIASUHAN<br/>BANTUAN YANG PERNAH DITERIMA',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('hub_keluarga',$this->hub_keluarga);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('nik_nama_ibu',$this->nik_nama_ibu,true);
		$criteria->compare('nik_nama_ayah',$this->nik_nama_ayah,true);
		$criteria->compare('nama_pmks',$this->nama_pmks,true);
		$criteria->compare('id_jenis_pmks',$this->id_jenis_pmks);
		$criteria->compare('agama',$this->agama);
		$criteria->compare('no_kab',$this->no_kab);
		$criteria->compare('nama_kabkot',$this->nama_kabkot,true);
		$criteria->compare('no_kec',$this->no_kec);
		$criteria->compare('nama_kec',$this->nama_kec,true);
		$criteria->compare('no_kel',$this->no_kel);
		$criteria->compare('nama_gampong',$this->nama_gampong,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('dititip_di',$this->dititip_di);
		$criteria->compare('id_pantiasuhan',$this->id_pantiasuhan);
		$criteria->compare('nama_pantiasuhan',$this->nama_pantiasuhan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			//'sort'=>array('status'),
		));
	}
	
	
	/* filter Biodata */
	public function filterBiodataContext($filterChain)
	{ 
		$nik = null;
		if(isset($_GET['nik'])) {
			$nik = $_GET['nik'];
		}else{
			if(isset($_POST['nik'])) 
				$nik = $_POST['nik'];
		}
		
		$this->loadBiodata($nik); 
		
		$filterChain->run(); 
	}
	
	/* status keluarga */
	public static function getHubunganKeluarga($statusId, $agamaId) {
		return DlmActiveRecord::getHubunganKeluargaById($statusId) . "<br/>". DlmActiveRecord::getAgamaById($agamaId);
	}
}