<?php

/**
 * This is the model class for table "BANTUAN_SOSIAL".
 *
 * The followings are the available columns in table 'BANTUAN_SOSIAL':
 * @property integer $id
 * @property integer $nik
 * @property integer $no_kk
 * @property integer $status_bantuan
 * @property string $jenis_bantuan
 * @property integer $id_instansi
 * @property integer $tahun_diberikan
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA $nik0
 * @property INSTANSI $idInstansi
 * @property KEPALAKELUARGA $noKk
 * @property USER $user
 */
class BantuanSosial extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BantuanSosial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'BANTUAN_SOSIAL';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, no_kk, status_bantuan, id_instansi, tahun_diberikan', 'required'),
			array('nik, no_kk, status_bantuan, id_instansi, tahun_diberikan, userid', 'numerical', 'integerOnly'=>true),
			array('jenis_bantuan', 'length', 'max'=>1000),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nik, no_kk, status_bantuan, jenis_bantuan, id_instansi, tahun_diberikan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'idInstansi' => array(self::BELONGS_TO, 'INSTANSI', 'id_instansi'),
			'noKk' => array(self::BELONGS_TO, 'KEPALAKELUARGA', 'no_kk'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nik' => 'NIK',
			'nama_penduduk' => 'Nama Penduduk',
			'no_kk' => 'No KK',
			'status_bantuan' => 'Pernah Menerima Bantuan',
			'nama_status_bantuan' => 'Pernah Menerima Bantuan',
			'jenis_bantuan' => 'Jenis Bantuan',
			'id_instansi' => 'Instansi Pemberi',
			'nama_instansi' => 'Instansi Pemberi',
			'tahun_diberikan' => 'Tahun Diberikan',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nik',$this->nik);
		$criteria->compare('no_kk',$this->no_kk);
		$criteria->compare('status_bantuan',$this->status_bantuan);
		$criteria->compare('jenis_bantuan',$this->jenis_bantuan,true);
		$criteria->compare('id_instansi',$this->id_instansi);
		$criteria->compare('tahun_diberikan',$this->tahun_diberikan);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	/* nama penduduk */
	private $nama_penduduk;
	public function setNama_penduduk($name) {
	  $this->nama_penduduk = $name;
	}
	
	public function getNama_penduduk() {
		if($this->nik0 != null){
			$this->nama_penduduk = $this->nik0->nama_lgkp;
		}
		return $this->nama_penduduk;
	}
	
	/* nama status bantuan */
	private $nama_status_bantuan;
	public function setNama_status_bantuan($name) {
	  $this->$nama_status_bantuan = $name;
	}
	
	public function getNama_status_bantuan() {
		$arrOptions = $this->getStatusYaTidakOptions();
		$index = $this->status_bantuan;
		if($index > 0){
			$this->nama_status_bantuan = $arrOptions[$index];
		}
		return $this->nama_status_bantuan;
	}
	
	/* nama instansi */
	private $nama_instansi;
	public function setNama_instansi($name) {
	  $this->nama_instansi = $name;
	}
	
	public function getNama_instansi() {
		if($this->idInstansi != null){
			$this->nama_instansi = $this->idInstansi->nama_instansi;
		}
		return $this->nama_instansi;
	}
}