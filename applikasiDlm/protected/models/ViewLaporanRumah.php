<?php

/**
 * This is the model class for table "VIEW_LAPORAN_RUMAH".
 *
 * The followings are the available columns in table 'VIEW_LAPORAN_RUMAH':
 * @property integer $id
 * @property string $nik
 * @property string $no_kk
 * @property integer $status_rumah
 * @property integer $jenis_lantai
 * @property integer $jenis_dinding
 * @property integer $fasilitas_wc
 * @property integer $penerangan
 * @property integer $air_minum
 * @property integer $bahan_bakar_masak
 * @property integer $rumah_bantuan
 * @property string $pemberi_rumah_bantuan
 * @property integer $tahun_rumah_bantuan
 * @property string $koordinat_x
 * @property string $koordinat_y
 * @property string $photo
 * @property integer $jlh_makan
 * @property integer $lauk_pauk
 * @property integer $daging
 * @property integer $pakaian
 * @property double $panjang_lantai
 * @property double $lebar_lantai
 * @property string $nama
 * @property string $pekerjaan
 * @property string $penghasilan
 * @property integer $no_kab
 * @property integer $no_kec
 * @property integer $no_kel
 */
class ViewLaporanRumah extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewLaporanRumah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VIEW_LAPORAN_RUMAH';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, nik, no_kk, status_rumah, jenis_lantai, jenis_dinding, fasilitas_wc, penerangan, air_minum, bahan_bakar_masak, rumah_bantuan, koordinat_x, koordinat_y, photo, jlh_makan, lauk_pauk, daging, pakaian, no_kab, no_kec, no_kel', 'required'),
			array('id, status_rumah, jenis_lantai, jenis_dinding, fasilitas_wc, penerangan, air_minum, bahan_bakar_masak, rumah_bantuan, tahun_rumah_bantuan, jlh_makan, lauk_pauk, daging, pakaian, no_kab, no_kec, no_kel', 'numerical', 'integerOnly'=>true),
			array('panjang_lantai, lebar_lantai', 'numerical'),
			array('nik, no_kk', 'length', 'max'=>100),
			array('pemberi_rumah_bantuan, photo', 'length', 'max'=>200),
			array('koordinat_x, koordinat_y', 'length', 'max'=>1000),
			array('nama, pekerjaan, penghasilan', 'length', 'max'=>4000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nik, no_kk, status_rumah, jenis_lantai, jenis_dinding, fasilitas_wc, penerangan, air_minum, bahan_bakar_masak, rumah_bantuan, pemberi_rumah_bantuan, tahun_rumah_bantuan, koordinat_x, koordinat_y, photo, jlh_makan, lauk_pauk, daging, pakaian, panjang_lantai, lebar_lantai, nama, pekerjaan, penghasilan, no_kab, no_kec, no_kel', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nik' => 'Nik',
			'no_kk' => 'No Kk',
			'status_rumah' => 'Status Rumah',
			'jenis_lantai' => 'Jenis Lantai',
			'jenis_dinding' => 'Jenis Dinding',
			'fasilitas_wc' => 'Fasilitas Wc',
			'penerangan' => 'Penerangan',
			'air_minum' => 'Air Minum',
			'bahan_bakar_masak' => 'Bahan Bakar Masak',
			'rumah_bantuan' => 'Rumah Bantuan',
			'pemberi_rumah_bantuan' => 'Pemberi Rumah Bantuan',
			'tahun_rumah_bantuan' => 'Tahun Rumah Bantuan',
			'koordinat_x' => 'Koordinat X',
			'koordinat_y' => 'Koordinat Y',
			'photo' => 'Photo',
			'jlh_makan' => 'Jlh Makan',
			'lauk_pauk' => 'Lauk Pauk',
			'daging' => 'Daging',
			'pakaian' => 'Pakaian',
			'panjang_lantai' => 'Panjang Lantai',
			'lebar_lantai' => 'Lebar Lantai',
			'nama' => 'Nama',
			'pekerjaan' => 'Pekerjaan',
			'penghasilan' => 'Penghasilan',
			'no_kab' => 'No Kab',
			'no_kec' => 'No Kec',
			'no_kel' => 'No Kel',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('no_kk',$this->no_kk,true);
		$criteria->compare('status_rumah',$this->status_rumah);
		$criteria->compare('jenis_lantai',$this->jenis_lantai);
		$criteria->compare('jenis_dinding',$this->jenis_dinding);
		$criteria->compare('fasilitas_wc',$this->fasilitas_wc);
		$criteria->compare('penerangan',$this->penerangan);
		$criteria->compare('air_minum',$this->air_minum);
		$criteria->compare('bahan_bakar_masak',$this->bahan_bakar_masak);
		$criteria->compare('rumah_bantuan',$this->rumah_bantuan);
		$criteria->compare('pemberi_rumah_bantuan',$this->pemberi_rumah_bantuan,true);
		$criteria->compare('tahun_rumah_bantuan',$this->tahun_rumah_bantuan);
		$criteria->compare('koordinat_x',$this->koordinat_x,true);
		$criteria->compare('koordinat_y',$this->koordinat_y,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('jlh_makan',$this->jlh_makan);
		$criteria->compare('lauk_pauk',$this->lauk_pauk);
		$criteria->compare('daging',$this->daging);
		$criteria->compare('pakaian',$this->pakaian);
		$criteria->compare('panjang_lantai',$this->panjang_lantai);
		$criteria->compare('lebar_lantai',$this->lebar_lantai);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);
		$criteria->compare('penghasilan',$this->penghasilan,true);
		$criteria->compare('no_kab',$this->no_kab);
		$criteria->compare('no_kec',$this->no_kec);
		$criteria->compare('no_kel',$this->no_kel);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* nama nik no kk */
	public static function getNamaNikNoKk($nama, $nik, $no_kk) {
		return $nama."<br/>".$nik."<br/>".$no_kk;
	}
	
	/* status luas jenis lantai */
	public static function getStatusLuasJenisLantai($status, $panjang_lantai, $lebar_lantai, $jenis_lantai) {
		$l = $panjang_lantai ."m x ". $lebar_lantai."m";
		return DlmActiveRecord::getKepemilikanRumahById($status)."<br/>".$l."<br/>".DlmActiveRecord::getJenisLantaiById($jenis_lantai);
	}
	
	/* dinding wc sumber air minum */
	public static function getJenisDindingFasilitasWcSumberAirMinum($jenis_dinding, $fasilitas_wc, $air_minum) {
		return DlmActiveRecord::getJenisDindingById($jenis_dinding)."<br/>".DlmActiveRecord::getNamaStatusYaTidakById($fasilitas_wc)."<br/>".DlmActiveRecord::getSumberAirById($air_minum);
	}
	
	/* bahan bakar rumah bantuan pemberi */
	public static function getBahanBakarRumahBantuanPemberi($bahan_bakar_masak, $rumah_bantuan, $pemberi_rumah_bantuan) {
		return DlmActiveRecord::getBahanBakarMasakById($bahan_bakar_masak)."<br/>".
		DlmActiveRecord::getStatusRumahById($rumah_bantuan)."<br/>".
		$pemberi_rumah_bantuan;
	}
	
	/* nasi lauk daging susu */
	public static function getNasiLaukDagingSusu($jlh_makan, $lauk_pauk, $daging) {
		return DlmActiveRecord::getKonsumsiNasiById($jlh_makan)."<br/>".
		DlmActiveRecord::getKonsumsiLaukPaukDagingById($lauk_pauk)."<br/>".
		DlmActiveRecord::getKonsumsiLaukPaukDagingById($daging);
	}
	
	/* pakaian pekerjaan penghasilan */
	public static function getPakaianPekerjaanPenghasilan($pakaian, $pekerjaan, $penghasilan) {
		return DlmActiveRecord::getKonsumsiSandangById($pakaian)."<br/>".
		$pekerjaan."<br/>".
		$penghasilan;
	}
}