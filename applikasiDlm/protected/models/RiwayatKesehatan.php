<?php

/**
 * This is the model class for table "RIWAYAT_KESEHATAN".
 *
 * The followings are the available columns in table 'RIWAYAT_KESEHATAN':
 * @property integer $id
 * @property string $nik
 * @property string $no_kk
 * @property string $no_jka
 * @property string $no_jamkesmas
 * @property string $asuransilain1
 * @property string $no_asuransilain1
 * @property string $asuransilain2
 * @property string $no_asuransilain2
 * @property integer $id_penyakit
 * @property integer $tgl_sakit
 * @property string $id_puskesmas
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 * @property integer $id_penyakit1
 * @property integer $id_penyakit2
 *
 * The followings are the available model relations:
 * @property BIODATA $nik0
 * @property KEPALAKELUARGA $noKk
 * @property PENYAKIT $idPenyakit
 * @property PUSKESMAS $idPuskesmas
 * @property USER $user
 * @property PENYAKIT $idPenyakit1
 * @property PENYAKIT $idPenyakit2
 */
class RiwayatKesehatan extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RiwayatKesehatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'RIWAYAT_KESEHATAN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, no_kk, nama_penyakit, tgl_sakit, nama_puskesmas', 'required'),
			array('id_penyakit, tgl_sakit, userid, id_penyakit1, id_penyakit2', 'numerical', 'integerOnly'=>true),
			array('nik, no_kk, no_jka, no_jamkesmas, asuransilain1, no_asuransilain1, asuransilain2, no_asuransilain2', 'length', 'max'=>100),
			array('id_puskesmas', 'length', 'max'=>40),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nik, no_kk, no_jka, no_jamkesmas, asuransilain1, no_asuransilain1, asuransilain2, no_asuransilain2, id_penyakit, tgl_sakit, id_puskesmas, id_penyakit1, id_penyakit2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'noKk' => array(self::BELONGS_TO, 'KEPALAKELUARGA', 'no_kk'),
			'idPenyakit' => array(self::BELONGS_TO, 'PENYAKIT', 'id_penyakit'),
			'idPuskesmas' => array(self::BELONGS_TO, 'PUSKESMAS', 'id_puskesmas'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
			'idPenyakit1' => array(self::BELONGS_TO, 'PENYAKIT', 'id_penyakit1'),
			'idPenyakit2' => array(self::BELONGS_TO, 'PENYAKIT', 'id_penyakit2'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nik' => 'NIK',
			'no_kk' => 'No. KK',
			'no_jka' => 'No. JKA',
			'no_jamkesmas' => 'No. Jamkesmas',
			'asuransilain1' => 'Asuransi Lain 1',
			'no_asuransilain1' => 'No. Asuransi Lain 1',
			'asuransilain2' => 'Asuransi Lain 2',
			'no_asuransilain2' => 'No. Asuransi Lain 2',
			'id_penyakit' => 'Nama Penyakit',
			'nama_penyakit' => 'Nama Penyakit 1',
			'tgl_sakit' => 'Tahun',
			'id_puskesmas' => 'Nama Puskesmas',
			'nama_puskesmas' => 'Nama Puskesmas',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
			'id_penyakit1' => 'Nama Penyakit',
			'nama_penyakit1' => 'Nama Penyakit 2',
			'id_penyakit2' => 'Nama Penyakit',
			'nama_penyakit2' => 'Nama Penyakit 3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('no_kk',$this->no_kk,true);
		$criteria->compare('no_jka',$this->no_jka,true);
		$criteria->compare('no_jamkesmas',$this->no_jamkesmas,true);
		$criteria->compare('asuransilain1',$this->asuransilain1,true);
		$criteria->compare('no_asuransilain1',$this->no_asuransilain1,true);
		$criteria->compare('asuransilain2',$this->asuransilain2,true);
		$criteria->compare('no_asuransilain2',$this->no_asuransilain2,true);
		$criteria->compare('id_penyakit',$this->id_penyakit);
		$criteria->compare('tgl_sakit',$this->tgl_sakit);
		$criteria->compare('id_puskesmas',$this->id_puskesmas,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		$criteria->compare('id_penyakit1',$this->id_penyakit1);
		$criteria->compare('id_penyakit2',$this->id_penyakit2);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	/* penduduk */
	private $nama_penduduk;
	public function setNama_penduduk($name) {
	  $this->nama_penduduk = $name;
	}

	public function getNama_penduduk(){
		if($this->nik0 != null){
			$this->nama_penduduk = $this->nik0->nama_lgkp;
		}
		return $this->nama_penduduk;
	}
	
	/* no kk */
	public $no_kk;
	public function setNo_kk($name) {
	  $this->no_kk = $name;
	}

	public function getNo_kk(){
		if($this->nik0 != null){
			$this->no_kk = $this->nik0->no_kk;
		}
		return $this->no_kk;
	}
	
	/* nama penyakit */
	private $nama_penyakit;
	public function setNama_penyakit($name) {
	  $this->nama_penyakit = $name;
	}

	public function getNama_penyakit(){
		if($this->idPenyakit != null){
			$this->nama_penyakit = $this->idPenyakit->nama_penyakit;
		}
		return $this->nama_penyakit;
	}
	
	/* nama penyakit 1*/
	private $nama_penyakit1;
	public function setNama_penyakit1($name) {
	  $this->nama_penyakit1 = $name;
	}

	public function getNama_penyakit1(){
		if($this->idPenyakit1 != null){
			$this->nama_penyakit1 = $this->idPenyakit1->nama_penyakit;
		}
		return $this->nama_penyakit1;
	}
	
	/* nama penyakit 2*/
	private $nama_penyakit2;
	public function setNama_penyakit2($name) {
	  $this->nama_penyakit2 = $name;
	}

	public function getNama_penyakit2(){
		if($this->idPenyakit2 != null){
			$this->nama_penyakit2 = $this->idPenyakit2->nama_penyakit;
		}
		return $this->nama_penyakit2;
	}
	
	/* nama puskesmas */
	private $nama_puskesmas;
	public function setNama_puskesmas($name) {
	  $this->nama_puskesmas = $name;
	}

	public function getNama_puskesmas(){
		if($this->idPuskesmas != null){
			$this->nama_puskesmas = $this->idPuskesmas->nama_puskesmas;
		}
		return $this->nama_puskesmas;
	}
}