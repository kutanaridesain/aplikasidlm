<?php

/**
 * This is the model class for table "KELAS_BANTUAN_LAINNYA".
 *
 * The followings are the available columns in table 'KELAS_BANTUAN_LAINNYA':
 * @property integer $id
 * @property integer $id_bantuan_lainnya
 * @property integer $id_kelas_bantuan
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BANTUANLAINNYA $idBantuanLainnya
 * @property KELASBANTUAN $idKelasBantuan
 * @property USER $user
 */
class KelasBantuanLainnya extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KelasBantuanLainnya the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'KELAS_BANTUAN_LAINNYA';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_bantuan_lainnya, id_kelas_bantuan, userid, tglinput', 'required'),
			array('id_bantuan_lainnya, id_kelas_bantuan, userid', 'numerical', 'integerOnly'=>true),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_bantuan_lainnya, id_kelas_bantuan, userid, tglinput, tglupdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBantuanLainnya' => array(self::BELONGS_TO, 'BANTUANLAINNYA', 'id_bantuan_lainnya'),
			'idKelasBantuan' => array(self::BELONGS_TO, 'KELASBANTUAN', 'id_kelas_bantuan'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_bantuan_lainnya' => 'Id Bantuan Lainnya',
			'id_kelas_bantuan' => 'Id Kelas Bantuan',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_bantuan_lainnya',$this->id_bantuan_lainnya);
		$criteria->compare('id_kelas_bantuan',$this->id_kelas_bantuan);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
}