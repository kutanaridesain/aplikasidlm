<?php

/**
 * This is the model class for table "RUMAH".
 *
 * The followings are the available columns in table 'RUMAH':
 * @property integer $id
 * @property integer $nik
 * @property integer $no_kk
 * @property integer $status_rumah
 * @property double $panjang_lantai
 * @property double $lebar_lantai
 * @property integer $jenis_lantai
 * @property integer $jenis_dinding
 * @property integer $fasilitas_wc
 * @property integer $penerangan
 * @property integer $air_minum
 * @property integer $bahan_bakar_masak
 * @property integer $rumah_bantuan
 * @property string $pemberi_rumah_bantuan
 * @property integer $tahun_rumah_bantuan
 * @property string $koordinat_x
 * @property string $koordinat_y
 * @property string $photo
 * @property integer $jlh_makan
 * @property integer $lauk_pauk
 * @property integer $daging
 * @property integer $pakaian
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA $nik0
 * @property KEPALAKELUARGA $noKk
 * @property USER $user
 */
class Rumah extends DlmActiveRecord
{
	const PATH = "pictures/homes/";
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Rumah the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'RUMAH';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, no_kk, status_rumah, panjang_lantai, lebar_lantai, jenis_lantai, jenis_dinding, fasilitas_wc, penerangan, photo, air_minum, bahan_bakar_masak, rumah_bantuan, koordinat_x, koordinat_y, jlh_makan, lauk_pauk, daging, pakaian', 'required'),
			array('nik, no_kk, status_rumah, jenis_lantai, jenis_dinding, fasilitas_wc, penerangan, air_minum, bahan_bakar_masak, rumah_bantuan, tahun_rumah_bantuan, jlh_makan, lauk_pauk, daging, pakaian, userid', 'numerical', 'integerOnly'=>true),
			array('panjang_lantai, lebar_lantai', 'numerical'),
			array('pemberi_rumah_bantuan', 'length', 'max'=>200),
			array('koordinat_x, koordinat_y', 'length', 'max'=>1000),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nik, no_kk, status_rumah, panjang_lantai, lebar_lantai, jenis_lantai, jenis_dinding, fasilitas_wc, penerangan, air_minum, bahan_bakar_masak, rumah_bantuan, pemberi_rumah_bantuan, tahun_rumah_bantuan, koordinat_x, koordinat_y, photo, jlh_makan, lauk_pauk, daging, pakaian', 'safe', 'on'=>'search'),
			array('photo', 'file', 'types'=>'jpg, jpeg, gif, png','allowEmpty' => true, 'on'=>'update','maxSize'=>1024 * 1024 * 0.3, 'tooLarge'=>'File harus lebih kecil dari 300KB'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'noKk' => array(self::BELONGS_TO, 'KEPALAKELUARGA', 'no_kk'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nik' => 'NIK',
			'no_kk' => 'No KK',
			'status_rumah' => 'Kepemilikan Rumah',
			'nama_status_rumah' => 'Kepemilikan Rumah',
			'panjang_lantai' => 'Panjang Lantai',
			'lebar_lantai' => 'Luas Lantai',
			'jenis_lantai' => 'Jenis Lantai',
			'nama_jenis_lantai' => 'Jenis Lantai',
			'jenis_dinding' => 'Jenis Dinding',
			'nama_jenis_dinding' => 'Jenis Dinding',
			'fasilitas_wc' => 'Fasilitas Kebersihan/WC',
			'nama_fasilitas_wc' => 'Punya Fasilitas Kebersihan/WC',
			'penerangan' => 'Sumber Penerangan',
			'nama_penerangan' => 'Sumber Penerangan',
			'air_minum' => 'Sumber Air Minum',
			'nama_air_minum' => 'Sumber Air Minum',
			'bahan_bakar_masak' => 'Bahan Bakar Memasak',
			'nama_bahan_bakar_masak' => 'Bahan Bakar Memasak',
			'rumah_bantuan' => 'Status Rumah',
			'nama_rumah_bantuan' => 'Status Bantuan',
			'pemberi_rumah_bantuan' => 'Bantuan Dari',
			'tahun_rumah_bantuan' => 'Tahun Terima Bantuan',
			'koordinat_x' => 'Koordinat Longitude',
			'koordinat_y' => 'Koordinat Latitude',
			'photo' => 'Photo',
			'jlh_makan' => 'Makan Nasi 1 Hari',
			'nama_jlh_makan' => 'Makan Nasi 1 Hari',
			'lauk_pauk' => 'Konsumsi Lauk Pauk',
			'nama_lauk_pauk' => 'Konsumsi Lauk Pauk',
			'daging' => 'Konsumsi Daging',
			'nama_daging' => 'Konsumsi Daging',
			'pakaian' => 'Beli Pakaian Baru',
			'nama_pakaian' => 'Beli Pakaian Baru',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$roleid = Yii::app()->user->getRoleId();
		$locaid = Yii::app()->user->getLocationId();

		$criteria=new CDbCriteria;
		$criteria->with='nik0';

		$criteria->compare('id',$this->id);
		$criteria->compare('nik',$this->nik);
		$criteria->compare('no_kk',$this->no_kk);
		$criteria->compare('status_rumah',$this->status_rumah);
		$criteria->compare('panjang_lantai',$this->panjang_lantai);
		$criteria->compare('lebar_lantai',$this->lebar_lantai);
		$criteria->compare('jenis_lantai',$this->jenis_lantai);
		$criteria->compare('jenis_dinding',$this->jenis_dinding);
		$criteria->compare('fasilitas_wc',$this->fasilitas_wc);
		$criteria->compare('penerangan',$this->penerangan);
		$criteria->compare('air_minum',$this->air_minum);
		$criteria->compare('bahan_bakar_masak',$this->bahan_bakar_masak);
		$criteria->compare('rumah_bantuan',$this->rumah_bantuan);
		$criteria->compare('pemberi_rumah_bantuan',$this->pemberi_rumah_bantuan,true);
		$criteria->compare('tahun_rumah_bantuan',$this->tahun_rumah_bantuan);
		$criteria->compare('koordinat_x',$this->koordinat_x,true);
		$criteria->compare('koordinat_y',$this->koordinat_y,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('jlh_makan',$this->jlh_makan);
		$criteria->compare('lauk_pauk',$this->lauk_pauk);
		$criteria->compare('daging',$this->daging);
		$criteria->compare('pakaian',$this->pakaian);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		
		if($roleid==2){
			$criteria->addCondition(
				'"nik0"."no_kab" = ' . $locaid
			);
		}
		if($roleid==3){
			$criteria->addCondition(
				'"nik0"."no_kec" = ' . $locaid
			);
		}
		if($roleid==4){
			$criteria->addCondition(
				'"nik0"."no_kel" = ' . $locaid
			);
		}
		
		$criteria->together = true;
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	/* nama status rumah */
	private $nama_status_rumah;
	public function setNama_status_rumah($name) {
	  $this->$nama_status_rumah = $name;
	}
	
	public function getNama_status_rumah() {
		$arrOptions = DlmActiveRecord::getKepemilikanRumahOptions();
		$index = $this->status_rumah;
		if($index > 0){
			$this->nama_status_rumah = $arrOptions[$index];
		}
		return $this->nama_status_rumah;
	}
	
	public static function getNamaStatusRumahById($idx) {
		
		$arrOptions = DlmActiveRecord::getKepemilikanRumahOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/* nama jenis lantai */
	private $nama_jenis_lantai;
	public function setNama_jenis_lantai($name) {
	  $this->$nama_jenis_lantai = $name;
	}
	
	public function getNama_jenis_lantai() {
		$arrOptions = $this->getLantaiRumahOptions();
		$index = $this->jenis_lantai;
		if($index > 0){
			$this->nama_jenis_lantai = $arrOptions[$index];
		}
		return $this->nama_jenis_lantai;
	}
	
	/* nama jenis dinding */
	private $nama_jenis_dinding;
	public function setNama_jenis_dinding($name) {
	  $this->$nama_jenis_dinding = $name;
	}
	
	public function getNama_jenis_dinding() {
		$arrOptions = $this->getDindingRumahOptions();
		$index = $this->jenis_dinding;
		if($index > 0){
			$this->nama_jenis_dinding = $arrOptions[$index];
		}
		return $this->nama_jenis_dinding;
	}
	
	/* nama fasilitas wc */
	private $nama_fasilitas_wc;
	public function setNama_fasilitas_wc($name) {
	  $this->$nama_fasilitas_wc = $name;
	}
	
	public function getNama_fasilitas_wc() {
		$arrOptions = $this->getStatusYaTidakOptions();
		$index = $this->fasilitas_wc;
		if($index > 0){
			$this->nama_fasilitas_wc = $arrOptions[$index];
		}
		return $this->nama_fasilitas_wc;
	}
	
	/* nama sumber penerangan */
	private $nama_penerangan;
	public function setNama_penerangan($name) {
	  $this->$nama_penerangan = $name;
	}
	
	public function getNama_penerangan() {
		$arrOptions = $this->getSumberPeneranganOptions();
		$index = $this->penerangan;
		if($index > 0){
			$this->nama_penerangan = $arrOptions[$index];
		}
		return $this->nama_penerangan;
	}
	
	/* nama air minum */
	private $nama_air_minum;
	public function setNama_air_minum($name) {
	  $this->$nama_air_minum = $name;
	}
	
	public function getNama_air_minum() {
		$arrOptions = $this->getSumberAirOptions();
		$index = $this->air_minum;
		if($index > 0){
			$this->nama_air_minum = $arrOptions[$index];
		}
		return $this->nama_air_minum;
	}
	
	/* nama bahan bakar masak */
	private $nama_bahan_bakar_masak;
	public function setNama_bahan_bakar_masak($name) {
	  $this->$nama_bahan_bakar_masak = $name;
	}
	
	public function getNama_bahan_bakar_masak() {
		$arrOptions = $this->getBahanBakarMasakOptions();
		$index = $this->bahan_bakar_masak;
		if($index > 0){
			$this->nama_bahan_bakar_masak = $arrOptions[$index];
		}
		return $this->nama_bahan_bakar_masak;
	}
	
	/* nama rumah bantuan */
	private $nama_rumah_bantuan;
	public function setNama_rumah_bantuan($name) {
	  $this->$nama_rumah_bantuan = $name;
	}
	
	public function getNama_rumah_bantuan() {
		$arrOptions = $this->getStatusRumahOptions();
		$index = $this->rumah_bantuan;
		if($index > 0){
			$this->nama_rumah_bantuan = $arrOptions[$index];
		}
		return $this->nama_rumah_bantuan;
	}
	
	/* nama jumlah makan */
	private $nama_jlh_makan;
	public function setNama_jlh_makan($name) {
	  $this->$nama_jlh_makan = $name;
	}
	
	public function getNama_jlh_makan() {
		$arrOptions = $this->getKonsumsiNasiOptions();
		$index = $this->jlh_makan;
		if($index > 0){
			$this->nama_jlh_makan = $arrOptions[$index];
		}
		return $this->nama_jlh_makan;
	}
	
	/* nama lauk pauk */
	private $nama_lauk_pauk;
	public function setNama_lauk_pauk($name) {
	  $this->$nama_lauk_pauk = $name;
	}
	
	public function getNama_lauk_pauk() {
		$arrOptions = $this->getKonsumsiLaukPaukDagingOptions();
		$index = $this->lauk_pauk;
		if($index > 0){
			$this->nama_lauk_pauk = $arrOptions[$index];
		}
		return $this->nama_lauk_pauk;
	}
	
	/* nama daging */
	private $nama_daging;
	public function setNama_daging($name) {
	  $this->$nama_daging = $name;
	}
	
	public function getNama_daging() {
		$arrOptions = $this->getKonsumsiLaukPaukDagingOptions();
		$index = $this->daging;
		if($index > 0){
			$this->nama_daging = $arrOptions[$index];
		}
		return $this->nama_daging;
	}
	
	/* nama pakaian */
	private $nama_pakaian;
	public function setNama_pakaian($name) {
	  $this->$nama_pakaian = $name;
	}
	
	public function getNama_pakaian() {
		$arrOptions = $this->getKonsumsiSandangOptions();
		$index = $this->pakaian;
		if($index > 0){
			$this->nama_pakaian = $arrOptions[$index];
		}
		return $this->nama_pakaian;
	}
}