<?php

/**
 * This is the model class for table "PENDIDIKAN_AKHIR".
 *
 * The followings are the available columns in table 'PENDIDIKAN_AKHIR':
 * @property integer $id_pen
 * @property string $nama_pendidikan
 * @property string $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA[] $bIODATAs
 */
class PendidikanAkhir extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PendidikanAkhir the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PENDIDIKAN_AKHIR';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_pendidikan, userid, tglinput', 'required'),
			array('nama_pendidikan', 'length', 'max'=>200),
			array('userid', 'length', 'max'=>100),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_pen, nama_pendidikan, userid, tglinput, tglupdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bIODATAs' => array(self::HAS_MANY, 'BIODATA', 'pddk_akh'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pen' => 'Id Pen',
			'nama_pendidikan' => 'Nama Pendidikan',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pen',$this->id_pen);
		$criteria->compare('nama_pendidikan',$this->nama_pendidikan,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
}