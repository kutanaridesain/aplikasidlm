<?php

/**
 * This is the model class for table "KABUPATEN".
 *
 * The followings are the available columns in table 'KABUPATEN':
 * @property integer $id
 * @property string $nama_kabkot
 * @property string $ibukota
 * @property string $alamat
 * @property string $bupati
 * @property string $wakil
 * @property integer $luas_wilayah
 * @property string $lambang
 * @property string $visi
 * @property string $misi
 * @property string $koordinat_x
 * @property string $koordinat_y
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA[] $bIODATAs
 * @property USER $user
 * @property KECAMATAN[] $kECAMATANs
 * @property GAMPONG[] $gAMPONGs
 * @property INSTANSI[] $iNSTANSIs
 * @property KOPERASI[] $kOPERASIs
 * @property KELOMPOK[] $kELOMPOKs
 * @property DAYAH[] $dAYAHs
 */
class Kabupaten extends DlmActiveRecord
{
	const PATH = "pictures/kabupatens/";
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Kabupaten the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'KABUPATEN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kabkot, ibukota, alamat, bupati, wakil, luas_wilayah, visi, misi, koordinat_x, koordinat_y', 'required'),
			array('luas_wilayah, userid', 'numerical', 'integerOnly'=>true),
			array('nama_kabkot, ibukota, bupati, wakil', 'length', 'max'=>200),
			array('alamat', 'length', 'max'=>1000),
			array('visi, misi', 'length', 'max'=>4000),
			array('koordinat_x, koordinat_y', 'length', 'max'=>50),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_kabkot, ibukota, alamat, bupati, wakil, luas_wilayah, visi, misi, koordinat_x, koordinat_y', 'safe', 'on'=>'search'),
			array('lambang', 'file', 'types'=>'jpg, jpeg, gif, png','allowEmpty' => true, 'on'=>'update','maxSize'=>1024 * 1024 * 0.3, 'tooLarge'=>'File harus lebih kecil dari 300KB'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bIODATAs' => array(self::HAS_MANY, 'BIODATA', 'no_kab'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
			'kECAMATANs' => array(self::HAS_MANY, 'KECAMATAN', 'id_kab'),
			'gAMPONGs' => array(self::HAS_MANY, 'GAMPONG', 'id_kab'),
			'iNSTANSIs' => array(self::HAS_MANY, 'INSTANSI', 'id_kab'),
			'kOPERASIs' => array(self::HAS_MANY, 'KOPERASI', 'id_kab'),
			'kELOMPOKs' => array(self::HAS_MANY, 'KELOMPOK', 'id_kab'),
			'dAYAHs' => array(self::HAS_MANY, 'DAYAH', 'id_kab'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_kabkot' => 'Kabupaten/Kota',
			'ibukota' => 'Ibukota',
			'alamat' => 'Alamat',
			'bupati' => 'Nama Bupati',
			'wakil' => 'Nama Wakil',
			'luas_wilayah' => 'Luas Wilayah',
			'lambang' => 'Lambang',
			'visi' => 'Visi',
			'misi' => 'Misi',
			'koordinat_x' => 'Koordinat Longitude',
			'koordinat_y' => 'Koordinat Latitude',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_kabkot',$this->nama_kabkot,true);
		$criteria->compare('ibukota',$this->ibukota,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('bupati',$this->bupati,true);
		$criteria->compare('wakil',$this->wakil,true);
		$criteria->compare('luas_wilayah',$this->luas_wilayah);
		$criteria->compare('lambang',$this->lambang,true);
		$criteria->compare('visi',$this->visi,true);
		$criteria->compare('misi',$this->misi,true);
		$criteria->compare('koordinat_x',$this->koordinat_x,true);
		$criteria->compare('koordinat_y',$this->koordinat_y,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	/* get kabupaten for dropdown */
	public static function getOptionList()
	{
		$sql = 'SELECT "id" as "id", "nama_kabkot" as "name" 
				FROM kabupaten 
				ORDER BY "nama_kabkot" ';
		
		$roleid = Yii::app()->user->getRoleId();
		
		if($roleid > 1){
			$kabid = Yii::app()->user->getKabupatenId();
			$sql = 'SELECT "id" as "id", "nama_kabkot" as "name" 
				FROM kabupaten 
				WHERE "id" = '. $kabid. ' 
				ORDER BY "nama_kabkot" ';
		}
		
		$arrList = CHtml::listData( Yii::app()->db->createCommand($sql)->queryAll(), 'id' , 'name');
		return $arrList;
	}
	
	public static function getById($id)
	{
		$sql = 'SELECT "nama_kabkot" as "name"  
				FROM kabupaten 
				WHERE  "id" = '. $id. ' 
				ORDER BY "nama_kabkot" ';
		
		$name = "";
		$arrName = Yii::app()->db->createCommand($sql)->queryRow();
		if(count($arrName) == 1){
			$name = $arrName["name"];
		}
		return $name;
	}
}