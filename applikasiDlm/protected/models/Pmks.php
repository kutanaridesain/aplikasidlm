<?php

/**
 * This is the model class for table "PMKS".
 *
 * The followings are the available columns in table 'PMKS':
 * @property integer $id
 * @property integer $id_jenis_pmks
 * @property integer $nik
 * @property integer $no_kk
 * @property integer $jenis_cacat
 * @property integer $dititip_di
 * @property integer $id_pantiasuhan
 * @property string $bantuan_diterima
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property BIODATA $nik0
 * @property JENISPMKS $idJenisPmks
 * @property KEPALAKELUARGA $noKk
 * @property PANTIASUHAN $idPantiasuhan
 * @property USER $user
 */
class Pmks extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pmks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PMKS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_jenis_pmks, nik, nik_kk, no_kk, dititip_di, bantuan_diterima', 'required'),
			array('id_jenis_pmks, nik, no_kk, jenis_cacat, dititip_di, id_pantiasuhan', 'numerical', 'integerOnly'=>true),
			array('bantuan_diterima', 'length', 'max'=>200),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_jenis_pmks, nik, no_kk, jenis_cacat, dititip_di, id_pantiasuhan, bantuan_diterima', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'idJenisPmks' => array(self::BELONGS_TO, 'JENISPMKS', 'id_jenis_pmks'),
			'noKk' => array(self::BELONGS_TO, 'KEPALAKELUARGA', 'no_kk'),
			'idPantiasuhan' => array(self::BELONGS_TO, 'PANTIASUHAN', 'id_pantiasuhan'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_jenis_pmks' => 'Jenis Pmks',
			'nama_jenis_pmks' => 'Masalah Sosial',
			'nik' => 'NIK',
			'nik_kk' => "NIK Kepala Keluarga",
			'no_kk' => 'No. KK',
			'jenis_cacat' => 'Jenis Cacat',
			'nama_jenis_cacat' => 'Jenis Cacat',
			'dititip_di' => 'Dititip Di Pantiasuhan',
			'nama_dititip_di' => 'Penitipan',
			'id_pantiasuhan' => 'Panti Asuhan',
			'nama_pantiasuhan' => 'Panti Asuhan',
			'bantuan_diterima' => 'Bantuan yang sudah diterima',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_jenis_pmks',$this->id_jenis_pmks);
		$criteria->compare('nik',$this->nik);
		$criteria->compare('no_kk',$this->no_kk);
		$criteria->compare('jenis_cacat',$this->jenis_cacat);
		$criteria->compare('dititip_di',$this->dititip_di);
		$criteria->compare('id_pantiasuhan',$this->id_pantiasuhan);
		$criteria->compare('bantuan_diterima',$this->bantuan_diterima,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){ 
			if($this->dititip_di == 2){
				$this->id_pantiasuhan = null;
			}
			
			if($this->jenis_cacat == 0){
				$this->jenis_cacat = null;
			}
			
			return true;
	   }
		
		return false;
	}
	
	/* penduduk */
	private $nama_penduduk;
	public function setNama_penduduk($name) {
	  $this->nama_penduduk = $name;
	}

	public function getNama_penduduk(){
		if($this->nik0 != null){
			$this->nama_penduduk = $this->nik0->nama_lgkp;
		}
		return $this->nama_penduduk;
	}
	
	/* custom property */
	private $nik_kk;
	
	public function setNik_kk($nik) {
	  $this->nik_kk = $nik;
	}

	public function getNik_kk() {
		
		if($this->noKk != null){
			$this->nik_kk = $this->noKk->nik;
		}
		return $this->nik_kk;
	}
	
	public $nokkDataProvider;
	
	/* nama jenis pmks */
	private $nama_jenis_pmks;
	public function setNama_jenis_pmks($name) {
	  $this->nama_jenis_pmks = $name;
	}

	public function getNama_jenis_pmks() {
		if($this->idJenisPmks != null){
			$this->nama_jenis_pmks = $this->idJenisPmks->nama_pmks;
		}
		return $this->nama_jenis_pmks;
	}
	
	/* nama jenis cacat */
	private $nama_jenis_cacat;
	public function setNama_jenis_cacat($name) {
	  $this->$nama_jenis_cacat = $name;
	}
	
	public function getNama_jenis_cacat() {
		$arrJenis_cacats = $this->getJenisCacatOptions();
		$index = $this->jenis_cacat;

		if($index > 0){
			$this->nama_jenis_cacat = $arrJenis_cacats[$index];
		}
		return $this->nama_jenis_cacat;
	}
	
	/* nama penitipan */
	private $nama_dititip_di;
	public function setNama_dititip_di($name) {
	  $this->$nama_dititip_di = $name;
	}
	
	public function getNama_dititip_di() {
		$arrDititip_dis = $this->getDititipDiPantiAsuhanOptions();
		$index = $this->dititip_di;

		if($index > 0){
			$this->nama_dititip_di = $arrDititip_dis[$index];
		}
		return $this->nama_dititip_di;
	}
	
	/* nama pantiasuhan */
	private $nama_pantiasuhan;
	public function setNama_pantiasuhan($name) {
	  $this->nama_pantiasuhan = $name;
	}

	public function getNama_pantiasuhan() {
		if($this->idPantiasuhan != null){
			$this->nama_pantiasuhan = $this->idPantiasuhan->nama_pantiasuhan;
		}
		return $this->nama_pantiasuhan;
	}
}