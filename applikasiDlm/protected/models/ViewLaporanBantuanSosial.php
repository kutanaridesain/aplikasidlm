<?php

/**
 * This is the model class for table "VIEW_LAPORAN_BANTUAN_SOSIAL".
 *
 * The followings are the available columns in table 'VIEW_LAPORAN_BANTUAN_SOSIAL':
 * @property string $nama_lgkp
 * @property string $nik
 * @property string $no_kk
 * @property string $hub_keluarga
 * @property string $tgl_lhr
 * @property integer $agama
 * @property string $pendidikan
 * @property string $pekerjaan
 * @property integer $stat_kwn
 * @property integer $status_bantuan
 * @property string $jenis_bantuan
 * @property integer $id_instansi
 * @property string $nama_instansi
 * @property integer $tahun_diberikan
 * @property string $alamat
 */
class ViewLaporanBantuanSosial extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewLaporanBantuanSosial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VIEW_LAPORAN_BANTUAN_SOSIAL';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_lgkp, nik, no_kk, tgl_lhr, agama, stat_kwn, status_bantuan, id_instansi, nama_instansi, tahun_diberikan', 'required'),
			array('agama, stat_kwn, status_bantuan, id_instansi, tahun_diberikan', 'numerical', 'integerOnly'=>true),
			array('nama_lgkp', 'length', 'max'=>200),
			array('nik, no_kk', 'length', 'max'=>100),
			array('hub_keluarga, pendidikan, pekerjaan', 'length', 'max'=>4000),
			array('jenis_bantuan', 'length', 'max'=>1000),
			array('nama_instansi', 'length', 'max'=>300),
			array('alamat', 'length', 'max'=>620),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nama_lgkp, nik, no_kk, hub_keluarga, tgl_lhr, agama, pendidikan, pekerjaan, stat_kwn, status_bantuan, jenis_bantuan, id_instansi, nama_instansi, tahun_diberikan, alamat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nama_lgkp' => 'Nama Lgkp',
			'nik' => 'Nik',
			'no_kk' => 'No Kk',
			'hub_keluarga' => 'Hub Keluarga',
			'tgl_lhr' => 'Tgl Lhr',
			'agama' => 'Agama',
			'pendidikan' => 'Pendidikan',
			'pekerjaan' => 'Pekerjaan',
			'stat_kwn' => 'Stat Kwn',
			'status_bantuan' => 'Status Bantuan',
			'jenis_bantuan' => 'Jenis Bantuan',
			'id_instansi' => 'Id Instansi',
			'nama_instansi' => 'Nama Instansi',
			'tahun_diberikan' => 'Tahun Diberikan',
			'alamat' => 'Alamat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nama_lgkp',$this->nama_lgkp,true);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('no_kk',$this->no_kk,true);
		$criteria->compare('hub_keluarga',$this->hub_keluarga,true);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('agama',$this->agama);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);
		$criteria->compare('stat_kwn',$this->stat_kwn);
		$criteria->compare('status_bantuan',$this->status_bantuan);
		$criteria->compare('jenis_bantuan',$this->jenis_bantuan,true);
		$criteria->compare('id_instansi',$this->id_instansi);
		$criteria->compare('nama_instansi',$this->nama_instansi,true);
		$criteria->compare('tahun_diberikan',$this->tahun_diberikan);
		$criteria->compare('alamat',$this->alamat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* nama nik no_kk */
	public static function getNamaNoKkNik($nama, $no_kk, $nik) {
		return $nama."<br/>".$no_kk."<br/>".$nik;
	}
	
	/* status keluarga tgl_lhr */
	public static function getStatusKeluargaTanggalLahir($hub_keluarga, $tgl_lhr) {
		return DlmActiveRecord::getHubunganKeluargaById($hub_keluarga)."<br/>".$tgl_lhr;
	}
	
	/* agama pendidikan */
	public static function getAgamaPendidikan($agama, $pendidikan) {
		return DlmActiveRecord::getAgamaById($agama)."<br/>".$pendidikan;
	}
	
	/* pekerjaan status kawin */
	public static function getPekerjaanTerakhirStatusKawin($pekerjaan, $status) {
		return $pekerjaan."<br/>".DlmActiveRecord::getStatusKawinById($status);
	}
	
	/* instansi tahun */
	public static function getInstansiTahun($instansi, $tahun) {
		return $instansi."<br/>".$tahun;
	}
}