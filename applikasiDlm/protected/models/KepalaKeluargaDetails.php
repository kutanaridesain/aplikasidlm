<?php

/**
 * This is the model class for table "KEPALA_KELUARGA_DETAILS".
 *
 * The followings are the available columns in table 'KEPALA_KELUARGA_DETAILS':
 * @property integer $id
 * @property integer $no_kk
 * @property integer $nik
 * @property integer $hub_keluarga
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property KEPALAKELUARGA $noKk
 * @property BIODATA $nik0
 * @property BIODATA $user
 */
class KepalaKeluargaDetails extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KepalaKeluargaDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'KEPALA_KELUARGA_DETAILS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_kk, nik, hub_keluarga', 'required'),
			array('no_kk, nik, hub_keluarga, userid', 'numerical', 'integerOnly'=>true),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, no_kk, nik, nama_penduduk, hub_keluarga', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'noKk' => array(self::BELONGS_TO, 'KEPALAKELUARGA', 'no_kk'),
			'nik0' => array(self::BELONGS_TO, 'BIODATA', 'nik'),
			'user' => array(self::BELONGS_TO, 'BIODATA', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'no_kk' => 'No KK',
			'nik' => 'NIK',
			'nama_penduduk' => 'Nama',
			'hub_keluarga' => 'Hub. Keluarga',
			'nama_hubungan' => 'Hub. Keluarga',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('no_kk',$this->no_kk);
		$criteria->compare('nik',$this->nik);
		$criteria->compare('nama_penduduk',$this->nama_penduduk);
		$criteria->compare('hub_keluarga',$this->hub_keluarga);
		$criteria->compare('nama_hubungan',$this->nama_hubungan);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	
	/* nama penduduk */
	private $nama_penduduk;
	public function setNama_penduduk($name) {
	  $this->nama_penduduk = $name;
	}
	
	public function getNama_penduduk() {
		if($this->nik0 != null){
			$this->nama_penduduk = $this->nik0->nama_lgkp;
		}
		return $this->nama_penduduk;
	}
	
	/* nama hubungan */
	private $nama_hubungan;
	public function setNama_hubungan($name) {
		$this->nama_hubungan = $name;
	}

	public function getNama_hubungan() {
		$arrNamaHubungan = $this->getHubunganKeluargaOptions(null);
		if($this->hub_keluarga != null){
			$this->nama_hubungan = $arrNamaHubungan[$this->hub_keluarga];
		}
		
		return $this->nama_hubungan;
	}
}