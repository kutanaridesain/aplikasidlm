<?php

/**
 * This is the model class for table "VIEW_LAPORAN_BANTUAN_MODAL".
 *
 * The followings are the available columns in table 'VIEW_LAPORAN_BANTUAN_MODAL':
 * @property string $nama_lgkp
 * @property string $nik
 * @property string $no_kk
 * @property string $hub_keluarga
 * @property string $tgl_lhr
 * @property integer $agama
 * @property string $pendidikan
 * @property string $pekerjaan
 * @property integer $stat_kwn
 * @property integer $status_bantuan_modal
 * @property string $jenis_bantuan_modal
 * @property integer $kategory_bantuan_modal
 * @property integer $id_kelompok
 * @property string $nama_kelompok
 * @property integer $id_instansi
 * @property string $nama_instansi
 * @property integer $tahun_diberi
 * @property integer $sumber_bantuan_modal
 * @property integer $id_koperasi
 * @property string $nama_koperasi
 * @property string $alamat
 */
class ViewLaporanBantuanModal extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewLaporanBantuanModal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VIEW_LAPORAN_BANTUAN_MODAL';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_lgkp, nik, no_kk, tgl_lhr, agama, stat_kwn, status_bantuan_modal, kategory_bantuan_modal, nama_kelompok, id_instansi, nama_instansi, tahun_diberi, sumber_bantuan_modal, nama_koperasi', 'required'),
			array('agama, stat_kwn, status_bantuan_modal, kategory_bantuan_modal, id_kelompok, id_instansi, tahun_diberi, sumber_bantuan_modal, id_koperasi', 'numerical', 'integerOnly'=>true),
			array('nama_lgkp', 'length', 'max'=>200),
			array('nik, no_kk', 'length', 'max'=>100),
			array('hub_keluarga, pendidikan, pekerjaan', 'length', 'max'=>4000),
			array('jenis_bantuan_modal', 'length', 'max'=>400),
			array('nama_kelompok, nama_instansi, nama_koperasi', 'length', 'max'=>300),
			array('alamat', 'length', 'max'=>620),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nama_lgkp, nik, no_kk, hub_keluarga, tgl_lhr, agama, pendidikan, pekerjaan, stat_kwn, status_bantuan_modal, jenis_bantuan_modal, kategory_bantuan_modal, id_kelompok, nama_kelompok, id_instansi, nama_instansi, tahun_diberi, sumber_bantuan_modal, id_koperasi, nama_koperasi, alamat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nama_lgkp' => 'Nama Lgkp',
			'nik' => 'Nik',
			'no_kk' => 'No Kk',
			'hub_keluarga' => 'Hub Keluarga',
			'tgl_lhr' => 'Tgl Lhr',
			'agama' => 'Agama',
			'pendidikan' => 'Pendidikan',
			'pekerjaan' => 'Pekerjaan',
			'stat_kwn' => 'Stat Kwn',
			'status_bantuan_modal' => 'Status Bantuan Modal',
			'jenis_bantuan_modal' => 'Jenis Bantuan Modal',
			'kategory_bantuan_modal' => 'Kategory Bantuan Modal',
			'id_kelompok' => 'Id Kelompok',
			'nama_kelompok' => 'Nama Kelompok',
			'id_instansi' => 'Id Instansi',
			'nama_instansi' => 'Nama Instansi',
			'tahun_diberi' => 'Tahun Diberi',
			'sumber_bantuan_modal' => 'Sumber Bantuan Modal',
			'id_koperasi' => 'Id Koperasi',
			'nama_koperasi' => 'Nama Koperasi',
			'alamat' => 'Alamat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nama_lgkp',$this->nama_lgkp,true);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('no_kk',$this->no_kk,true);
		$criteria->compare('hub_keluarga',$this->hub_keluarga,true);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('agama',$this->agama);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);
		$criteria->compare('stat_kwn',$this->stat_kwn);
		$criteria->compare('status_bantuan_modal',$this->status_bantuan_modal);
		$criteria->compare('jenis_bantuan_modal',$this->jenis_bantuan_modal,true);
		$criteria->compare('kategory_bantuan_modal',$this->kategory_bantuan_modal);
		$criteria->compare('id_kelompok',$this->id_kelompok);
		$criteria->compare('nama_kelompok',$this->nama_kelompok,true);
		$criteria->compare('id_instansi',$this->id_instansi);
		$criteria->compare('nama_instansi',$this->nama_instansi,true);
		$criteria->compare('tahun_diberi',$this->tahun_diberi);
		$criteria->compare('sumber_bantuan_modal',$this->sumber_bantuan_modal);
		$criteria->compare('id_koperasi',$this->id_koperasi);
		$criteria->compare('nama_koperasi',$this->nama_koperasi,true);
		$criteria->compare('alamat',$this->alamat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* katageori kelompok */
	public static function getKategoriJenis($kategory_bantuan_modal, $jenis_bantuan_modal) {
		return $kategory_bantuan_modal."<br/>".$jenis_bantuan_modal;
	}
	
	/* instansi tahun */
	public static function getInstansiTahun($instansi, $tahun) {
		return $instansi."<br/>".$tahun;
	}
}