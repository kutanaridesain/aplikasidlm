<?php

/**
 * This is the model class for table "BIODATA".
 *
 * The followings are the available columns in table 'BIODATA':
 * @property string $nik
 * @property string $nama_lgkp
 * @property string $tmpt_lhr
 * @property string $tgl_lhr
 * @property integer $akta_lhr
 * @property string $no_akta_lhr
 * @property integer $gol_drh
 * @property integer $jenis_kelamin
 * @property integer $agama
 * @property integer $stat_kwn
 * @property integer $akta_kwn
 * @property string $no_akta_kwn
 * @property string $tgl_kwn
 * @property integer $akta_crai
 * @property string $no_akta_crai
 * @property string $tgl_crai
 * @property integer $pnydng_cct
 * @property integer $pddk_akh
 * @property integer $kode_pkrj
 * @property integer $stat_hidup
 * @property integer $no_kab
 * @property integer $no_kec
 * @property integer $no_kel
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property KEPALAKELUARGA[] $kEPALAKELUARGAs
 * @property KEPEMILIKANASSET[] $kEPEMILIKANASSETs
 * @property PMKS[] $pMKSs
 * @property RIWAYATKESEHATAN[] $rIWAYATKESEHATANs
 * @property RIWAYATPEKERJAAN[] $rIWAYATPEKERJAANs
 * @property RIWAYATPENDIDIKAN[] $rIWAYATPENDIDIKANs
 * @property RUMAH[] $rUMAHs
 * @property USER[] $uSERs
 * @property ZAKAT[] $Options
 * @property BANTUANLAINNYA[] $bANTUANLAINNYAs
 * @property BANTUANMODAL[] $bANTUANMODALs
 * @property BANTUANSOSIAL[] $bANTUANSOSIALs
 * @property GAMPONG $noKel
 * @property KABUPATEN $noKab
 * @property KECAMATAN $noKec
 * @property RIWAYATPEKERJAAN $kodePkrj
 * @property USER $user
 */
class Biodata extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BIODATA the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'BIODATA';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, nama_lgkp, tmpt_lhr, tgl_lhr, akta_lhr, gol_drh, jenis_kelamin, agama, stat_kwn, pnydng_cct, pddk_akh, stat_hidup, no_kab, nama_kab, no_kec, nama_kec, no_kel, nama_kel', 'required'),
			array('akta_lhr, gol_drh, jenis_kelamin, agama, stat_kwn, akta_kwn, akta_crai, pnydng_cct, pddk_akh, kode_pkrj, stat_hidup, no_kab, no_kec, no_kel, userid', 'numerical', 'integerOnly'=>true),
			array('nama_lgkp, tmpt_lhr', 'length', 'max'=>200),
			array('no_akta_lhr, no_akta_kwn', 'length', 'max'=>50),
			array('no_akta_crai', 'length', 'max'=>4000),
			array('tgl_kwn, tgl_crai, tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nik, nama_lgkp, tmpt_lhr, tgl_lhr, akta_lhr, no_akta_lhr, gol_drh, jenis_kelamin, nama_jenis_kelamin, agama, stat_kwn, akta_kwn, no_akta_kwn, tgl_kwn, akta_crai, no_akta_crai, tgl_crai, pnydng_cct, pddk_akh, kode_pkrj, stat_hidup, no_kab, no_kec, no_kel', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kEPALAKELUARGAs' => array(self::HAS_MANY, 'KEPALAKELUARGA', 'nik'),
			'kEPALAKELUARGADETAILs' => array(self::HAS_MANY, 'KEPALAKELUARGADETAILS', 'nik'),
			'kEPEMILIKANASSETs' => array(self::HAS_MANY, 'KEPEMILIKANASSET', 'nik'),
			'pMKSs' => array(self::HAS_MANY, 'PMKS', 'nik'),
			'rIWAYATKESEHATANs' => array(self::HAS_MANY, 'RIWAYATKESEHATAN', 'nik'),
			'rIWAYATPEKERJAANs' => array(self::HAS_MANY, 'RIWAYATPEKERJAAN', 'nik'),
			'rIWAYATPENDIDIKANs' => array(self::HAS_MANY, 'RIWAYATPENDIDIKAN', 'nik'),
			'rUMAHs' => array(self::HAS_MANY, 'RUMAH', 'nik'),
			'uSERs' => array(self::HAS_MANY, 'USER', 'nik'),
			'Options' => array(self::HAS_MANY, 'ZAKAT', 'nik'),
			'bANTUANLAINNYAs' => array(self::HAS_MANY, 'BANTUANLAINNYA', 'nik'),
			'bANTUANMODALs' => array(self::HAS_MANY, 'BANTUANMODAL', 'nik'),
			'bANTUANSOSIALs' => array(self::HAS_MANY, 'BANTUANSOSIAL', 'nik'),
			'noKel' => array(self::BELONGS_TO, 'GAMPONG', 'no_kel'),
			'noKab' => array(self::BELONGS_TO, 'KABUPATEN', 'no_kab'),
			'noKec' => array(self::BELONGS_TO, 'KECAMATAN', 'no_kec'),
			'kodePkrj' => array(self::BELONGS_TO, 'RIWAYATPEKERJAAN', 'kode_pkrj'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nik' => 'NIK',
			'nama_lgkp' => 'Nama Lengkap',
			'tmpt_lhr' => 'Tempat Lahir',
			'tgl_lhr' => 'Tanggal Lahir',
			'akta_lhr' => 'Status Akta Lahir',
			'nama_status_akta_lahir' => 'Punya Akta Lahir',
			'no_akta_lhr' => 'No. Akta Lahir',
			'gol_drh' => 'Golongan Darah',
			'nama_golongan_darah' => 'Golongan Darah',
			'jenis_kelamin' => 'Jenis Kelamin',
			'nama_jenis_kelamin' => 'Jenis Kelamin',
			'agama' => 'Agama',
			'nama_agama' => 'Agama',
			'stat_kwn' => 'Status Kawin',
			'nama_status_kawin' => 'Status Kawin',
			'akta_kwn' => 'Akta Kawin',
			'nama_akta_kawin' => 'Punya Akta Kawin',
			'no_akta_kwn' => 'No. Akta Kawin',
			'tgl_kwn' => 'Tanggal Kawin',
			'akta_crai' => 'Akta Cerai',
			'nama_akta_cerai' => 'Punya Akta Cerai',
			'no_akta_crai' => 'No. Akta Cerai',
			'tgl_crai' => 'Tanggal Cerai',
			'pnydng_cct' => 'Kelainan Cacat',
			'nama_cacat' => 'Punya Kelainan Cacat',
			'pddk_akh' => 'Pendidikan Terakhir',
			'nama_pendidikan_terakhir' => 'Pendidikan Terakhir',
			'kode_pkrj' => 'Pekerjaan',
			'stat_hidup' => 'Sudah Meninggal',
			'nama_status_hidup' => 'Sudah Meninggal',
			'tgl_meninggal' => 'Tanggal Meninggal',
			'no_kab' => 'Kabupaten',
			'nama_kab' => 'Nama Kabupaten',
			'no_kec' => 'Kecamatan',
			'nama_kec' => 'Nama Kecamatan',
			'no_kel' => 'Gampong',
			'nama_kel' => 'Nama Gampong',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$roleid = Yii::app()->user->getRoleId();
		$locaid = Yii::app()->user->getLocationId();
		
		$criteria=new CDbCriteria;
		$criteria->compare('nik',$this->nik);
		$criteria->compare('nama_lgkp',$this->nama_lgkp,true);
		$criteria->compare('tmpt_lhr',$this->tmpt_lhr,true);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('akta_lhr',$this->akta_lhr);
		$criteria->compare('no_akta_lhr',$this->no_akta_lhr,true);
		$criteria->compare('gol_drh',$this->gol_drh);
		$criteria->compare('jenis_kelamin', $this->jenis_kelamin);
		// $criteria->compare('nama_jenis_kelamin',$this->nama_jenis_kelamin);
		$criteria->compare('agama',$this->agama);
		$criteria->compare('stat_kwn',$this->stat_kwn);
		$criteria->compare('akta_kwn',$this->akta_kwn);
		$criteria->compare('no_akta_kwn',$this->no_akta_kwn,true);
		$criteria->compare('tgl_kwn',$this->tgl_kwn,true);
		$criteria->compare('akta_crai',$this->akta_crai);
		$criteria->compare('no_akta_crai',$this->no_akta_crai,true);
		$criteria->compare('tgl_crai',$this->tgl_crai,true);
		$criteria->compare('pnydng_cct',$this->pnydng_cct);
		$criteria->compare('pddk_akh',$this->pddk_akh);
		$criteria->compare('kode_pkrj',$this->kode_pkrj);
		$criteria->compare('stat_hidup',$this->stat_hidup);
		$criteria->compare('no_kab',$this->no_kab);
		$criteria->compare('no_kec',$this->no_kec);
		$criteria->compare('no_kel',$this->no_kel);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		if($roleid == 2){			
			$criteria->compare('no_kab',$locaid);
		}
		if($roleid==3){	
			$criteria->compare('no_kec',$locaid);
		}
		if($roleid== 4){	
			$criteria->compare('no_kel',$locaid);
		}
		$criteria->order = '"nama_lgkp" ASC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			if($this->stat_kwn == 1){
				$this->tgl_kwn = NULL;
				$this->akta_kwn = NULL;
				$this->no_akta_kwn = NULL;
				$this->tgl_crai = NULL;
				$this->akta_crai = NULL;
				$this->no_akta_crai = NULL;
			}
			
			if($this->stat_kwn == 2){
				$this->tgl_crai = NULL;
				$this->akta_crai = 2;
				$this->no_akta_crai = NULL;
			}
			
			if($this->stat_kwn >= 2){
				if($this->akta_kwn == 2){
					$this->no_akta_kwn = NULL;
				}
				
				if($this->akta_crai == 2){
					$this->no_akta_crai = NULL;
				}
			}
			
			if($this->stat_hidup == 1){
				$this->tgl_meninggal = NULL;
			}
			
			return true;
	   }
		
		return false;
	}
	
	protected function afterFind()
	{
		$roleid = Yii::app()->user->getRoleId();
		$locaid = Yii::app()->user->getLocationId();
		
		$act = Yii::app()->controller->action->id;
		
		if( $act != "index"){
			if(($roleid == 2 && $this->no_kab != $locaid) || ($roleid==3 && $this->no_kec != $locaid)||($roleid== 4 && $this->no_kec != $locaid)){	
				throw new CHttpException(401,'Tidak ada hak akses.');
			}
		}
	}
	
	/* gampong */
	private $nama_kel;
	public function setNama_kel($name) {
	  $this->nama_kel = $name;
	}
	
	public function getNama_kel() {
		if($this->noKel != null){
			$this->nama_kel = $this->noKel->nama_gampong;
		}
		return $this->nama_kel;
	}
	
	/* kecamatan */
	private $nama_kec;
	public function setNama_kec($name) {
	  $this->nama_kec = $name;
	}
	
	public function getNama_kec() {
		if($this->noKec != null){
			$this->nama_kec = $this->noKec->nama_kec;
		}
		return $this->nama_kec;
	}
	
	/* kabupaten */
	private $nama_kab;
	public function setNama_kab($name) {
	  $this->nama_kab = $name;
	}

	public function getNama_kab() {
		if($this->noKab != null){
			$this->nama_kab = $this->noKab->nama_kabkot;
		}
		return $this->nama_kab;
	}
	
	/* no kk */
	private $no_kk;
	public function setNo_kk($name) {
	  $this->no_kk = $name;
	}

	public function getNo_kk() {
		if($this->kEPALAKELUARGAs != null){
			$this->no_kk = $this->kEPALAKELUARGAs[0]->no_kk;
		}
		else if($this->kEPALAKELUARGADETAILs != null){
			$this->no_kk = $this->kEPALAKELUARGADETAILs[0]->no_kk;
		}
		return $this->no_kk;
	}
	
	/* nama jenis kelamin */
	private $nama_jenis_kelamin;
	public function setNama_jenis_kelamin($name) {
	  $this->$nama_jenis_kelamin = $name;
	}
	
	public function getNama_jenis_kelamin() {
		$arrJenis_kelamins = DlmActiveRecord::getJenisKelaminOptions();
		$index = $this->jenis_kelamin;

		if($index > 0){
			$this->nama_jenis_kelamin = $arrJenis_kelamins[$index];
		}
		return $this->nama_jenis_kelamin;
	}
	
	public static function getNamaJenisKelaminById($idx) {
		
		$arrOptions = DlmActiveRecord::getJenisKelaminOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
	
	/* nama status akta lahir */
	private $nama_status_akta_lahir;
	public function setNama_status_akta_lahir($name) {
	  $this->$nama_status_akta_lahir = $name;
	}
	
	public function getNama_status_akta_lahir() {
		$arrOptions = $this->getStatusYaTidakOptions();
		$index = $this->akta_lhr;
		if($index > 0){
			$this->nama_status_akta_lahir = $arrOptions[$index];
		}
		return $this->nama_status_akta_lahir;
	}

	/* nama golongan darah */
	private $nama_golongan_darah;
	public function setNama_golongan_darah($name) {
	  $this->$nama_golongan_darah = $name;
	}
	
	public function getNama_golongan_darah() {
		$arrOptions = $this->getGolonganDarahOptions();
		$index = $this->gol_drh;
		if($index > 0){
			$this->nama_golongan_darah = $arrOptions[$index];
		}
		return $this->nama_golongan_darah;
	}
	
	/* nama agama */
	private $nama_agama;
	public function setNama_agama($name) {
	  $this->$nama_agama = $name;
	}
	
	public function getNama_agama() {
		$arrOptions = $this->getAgamaOptions();
		$index = $this->agama;
		if($index > 0){
			$this->nama_agama = $arrOptions[$index];
		}
		return $this->nama_agama;
	}
	
	/* nama status kawin */
	private $nama_status_kawin;
	public function setNama_status_kawin($name) {
	  $this->$nama_status_kawin = $name;
	}
	
	public function getNama_status_kawin() {
		$arrOptions = $this->getStatusKawinOptions();
		$index = $this->stat_kwn;
		if($index > 0){
			$this->nama_status_kawin = $arrOptions[$index];
		}
		return $this->nama_status_kawin;
	}
	
	/* nama akta kawin */
	private $nama_akta_kawin;
	public function setNama_akta_kawin($name) {
	  $this->$nama_akta_kawin = $name;
	}
	
	public function getNama_akta_kawin() {
		$arrOptions = $this->getStatusYaTidakOptions();
		$index = $this->akta_kwn;
		if($index > 0){
			$this->nama_akta_kawin = $arrOptions[$index];
		}
		return $this->nama_akta_kawin;
	}
	
	/* nama akta cerai */
	private $nama_akta_cerai;
	public function setNama_akta_cerai($name) {
	  $this->$nama_akta_cerai = $name;
	}
	
	public function getNama_akta_cerai() {
		$arrOptions = $this->getStatusYaTidakOptions();
		$index = $this->akta_crai;
		if($index > 0){
			$this->nama_akta_cerai = $arrOptions[$index];
		}
		return $this->nama_akta_cerai;
	}
	
	/* nama cacat */
	private $nama_cacat;
	public function setNama_cacat($name) {
	  $this->$nama_cacat = $name;
	}
	
	public function getNama_cacat() {
		$arrOptions = $this->getStatusYaTidakOptions();
		$index = $this->pnydng_cct;
		if($index > 0){
			$this->nama_cacat = $arrOptions[$index];
		}
		return $this->nama_cacat;
	}
	
	/* nama pendidikan terakhir */
	private $nama_pendidikan_terakhir;
	public function setNama_pendidikan_terakhir($name) {
	  $this->$nama_pendidikan_terakhir = $name;
	}
	
	public function getNama_pendidikan_terakhir() {
		$arrOptions = $this->getPendidikanTerakhirOptions();
		$index = $this->pddk_akh;
		if($index > 0){
			$this->nama_pendidikan_terakhir = $arrOptions[$index];
		}
		return $this->nama_pendidikan_terakhir;
	}
	
	
	/* nama cacat */
	private $nama_status_hidup;
	public function setNama_status_hidup($name) {
	  $this->$nama_status_hidup = $name;
	}
	
	public function getNama_status_hidup() {
		$arrOptions = $this->getStatusYaTidakOptions();
		$index = $this->stat_hidup;
		if($index > 0){
			$this->nama_status_hidup = $arrOptions[$index];
		}
		return $this->nama_status_hidup;
	}
}