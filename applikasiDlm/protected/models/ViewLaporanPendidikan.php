<?php

/**
 * This is the model class for table "VIEW_LAPORAN_PENDIDIKAN".
 *
 * The followings are the available columns in table 'VIEW_LAPORAN_PENDIDIKAN':
 * @property string $no_kk
 * @property string $nik
 * @property string $nama_lgkp
 * @property string $tmpt_lhr
 * @property string $tgl_lhr
 * @property integer $akta_lhr
 * @property integer $jenis_kelamin
 * @property integer $agama
 * @property integer $pnydng_cct
 * @property integer $stat_hidup
 * @property integer $no_kab
 * @property string $nama_kabkot
 * @property integer $no_kec
 * @property string $nama_kec
 * @property integer $no_kel
 * @property string $nama_gampong
 * @property string $alamat
 * @property string $nik_nama_ibu
 * @property string $nik_nama_ayah
 * @property string $hub_keluarga
 * @property string $pendidikanid
 * @property string $pendidikan
 * @property string $pekerjaan
 * @property string $jenjang_sekolah
 * @property integer $kode_sekolah
 * @property string $kelas
 * @property integer $tahun
 * @property integer $dapat_beasiswa
 * @property double $jlh_beasiswa
 * @property string $pemberi_beasiswa
 * @property integer $thn_beasiswa
 * @property integer $status_pendidikan
 * @property string $mengapa_tdk_sekolah
 * @property integer $thn_putus_sekolah
 * @property string $nama_sekolah
 */
class ViewLaporanPendidikan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ViewLaporanPendidikan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VIEW_LAPORAN_PENDIDIKAN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik, nama_lgkp, tmpt_lhr, tgl_lhr, akta_lhr, jenis_kelamin, agama, pnydng_cct, stat_hidup, no_kab, nama_kabkot, no_kec, nama_kec, no_kel, nama_gampong, jenjang_sekolah, kelas, tahun, dapat_beasiswa, status_pendidikan, nama_sekolah', 'required'),
			array('akta_lhr, jenis_kelamin, agama, pnydng_cct, stat_hidup, no_kab, no_kec, no_kel, kode_sekolah, tahun, dapat_beasiswa, thn_beasiswa, status_pendidikan, thn_putus_sekolah', 'numerical', 'integerOnly'=>true),
			array('jlh_beasiswa', 'numerical'),
			array('no_kk, nik_nama_ibu, nik_nama_ayah, hub_keluarga, pendidikanid, pendidikan, pekerjaan', 'length', 'max'=>4000),
			array('nik, kelas', 'length', 'max'=>100),
			array('nama_lgkp, tmpt_lhr, nama_kabkot, nama_kec, nama_gampong, jenjang_sekolah, pemberi_beasiswa, nama_sekolah', 'length', 'max'=>200),
			array('alamat', 'length', 'max'=>620),
			array('mengapa_tdk_sekolah', 'length', 'max'=>400),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('no_kk, nik, nama_lgkp, tmpt_lhr, tgl_lhr, akta_lhr, jenis_kelamin, agama, pnydng_cct, stat_hidup, no_kab, nama_kabkot, no_kec, nama_kec, no_kel, nama_gampong, alamat, nik_nama_ibu, nik_nama_ayah, hub_keluarga, pendidikanid, pendidikan, pekerjaan, jenjang_sekolah, kode_sekolah, kelas, tahun, dapat_beasiswa, jlh_beasiswa, pemberi_beasiswa, thn_beasiswa, status_pendidikan, mengapa_tdk_sekolah, thn_putus_sekolah, nama_sekolah', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_kk' => 'No Kk',
			'nik' => 'Nik',
			'nama_lgkp' => 'Nama Lgkp',
			'tmpt_lhr' => 'Tmpt Lhr',
			'tgl_lhr' => 'Tgl Lhr',
			'akta_lhr' => 'Akta Lhr',
			'jenis_kelamin' => 'Jenis Kelamin',
			'agama' => 'Agama',
			'pnydng_cct' => 'Pnydng Cct',
			'stat_hidup' => 'Stat Hidup',
			'no_kab' => 'No Kab',
			'nama_kabkot' => 'Nama Kabkot',
			'no_kec' => 'No Kec',
			'nama_kec' => 'Nama Kec',
			'no_kel' => 'No Kel',
			'nama_gampong' => 'Nama Gampong',
			'alamat' => 'Alamat',
			'nik_nama_ibu' => 'Nik Nama Ibu',
			'nik_nama_ayah' => 'Nik Nama Ayah',
			'hub_keluarga' => 'Hub Keluarga',
			'pendidikanid' => 'Pendidikanid',
			'pendidikan' => 'Pendidikan',
			'pekerjaan' => 'Pekerjaan',
			'jenjang_sekolah' => 'Jenjang Sekolah',
			'kode_sekolah' => 'Kode Sekolah',
			'kelas' => 'Kelas',
			'tahun' => 'Tahun',
			'dapat_beasiswa' => 'Dapat Beasiswa',
			'jlh_beasiswa' => 'Jlh Beasiswa',
			'pemberi_beasiswa' => 'Pemberi Beasiswa',
			'thn_beasiswa' => 'Thn Beasiswa',
			'status_pendidikan' => 'Status Pendidikan',
			'mengapa_tdk_sekolah' => 'Mengapa Tdk Sekolah',
			'thn_putus_sekolah' => 'Thn Putus Sekolah',
			'nama_sekolah' => 'Nama Sekolah',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no_kk',$this->no_kk,true);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('nama_lgkp',$this->nama_lgkp,true);
		$criteria->compare('tmpt_lhr',$this->tmpt_lhr,true);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('akta_lhr',$this->akta_lhr);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin);
		$criteria->compare('agama',$this->agama);
		$criteria->compare('pnydng_cct',$this->pnydng_cct);
		$criteria->compare('stat_hidup',$this->stat_hidup);
		$criteria->compare('no_kab',$this->no_kab);
		$criteria->compare('nama_kabkot',$this->nama_kabkot,true);
		$criteria->compare('no_kec',$this->no_kec);
		$criteria->compare('nama_kec',$this->nama_kec,true);
		$criteria->compare('no_kel',$this->no_kel);
		$criteria->compare('nama_gampong',$this->nama_gampong,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('nik_nama_ibu',$this->nik_nama_ibu,true);
		$criteria->compare('nik_nama_ayah',$this->nik_nama_ayah,true);
		$criteria->compare('hub_keluarga',$this->hub_keluarga,true);
		$criteria->compare('pendidikanid',$this->pendidikanid,true);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);
		$criteria->compare('jenjang_sekolah',$this->jenjang_sekolah,true);
		$criteria->compare('kode_sekolah',$this->kode_sekolah);
		$criteria->compare('kelas',$this->kelas,true);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('dapat_beasiswa',$this->dapat_beasiswa);
		$criteria->compare('jlh_beasiswa',$this->jlh_beasiswa);
		$criteria->compare('pemberi_beasiswa',$this->pemberi_beasiswa,true);
		$criteria->compare('thn_beasiswa',$this->thn_beasiswa);
		$criteria->compare('status_pendidikan',$this->status_pendidikan);
		$criteria->compare('mengapa_tdk_sekolah',$this->mengapa_tdk_sekolah,true);
		$criteria->compare('thn_putus_sekolah',$this->thn_putus_sekolah);
		$criteria->compare('nama_sekolah',$this->nama_sekolah,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	
	/* nama nik no kk */
	public static function getNamaNikNoKk($nama, $nik, $no_kk) {
		return $nama."<br/>".$nik."<br/>".$no_kk;
	}
	
	/* status keluarga tanggal lahir */
	public static function getStatusKeluargaTanggalLahir($hub_keluarga, $tgl_lhr) {
		return DlmActiveRecord::getHubunganKeluargaById($hub_keluarga)."<br/>".$tgl_lhr;
	}
	
	/* agama jenjang pendidikan */
	public static function getAgamaJenjangPendidikan($agama, $jenjangPendidikan) {
		return DlmActiveRecord::getAgamaById($agama)."<br/>".$jenjangPendidikan;
	}
	
	/* sekolah kelas */
	public static function getSekolahKelas($sekolah, $kelas) {
		return $sekolah."<br/>".$kelas;
	}
	
	/* agama */
	public static function getAgama($agama) {
		return DlmActiveRecord::getAgamaById($agama);
	}
	
	/* jenjang pendidikan tahun putus sekolah*/
	public static function getJenjangPendidikanTahun($jenjangPendidikan, $thn) {
		return $jenjangPendidikan."<br/>".$thn;
	}
	
	/* beasiswa dari tahun terima */
	public static function getBeasiswaDariTahunTerima($pemberi_beasiswa, $thn) {
		return $pemberi_beasiswa."<br/>".$thn;
	}
}