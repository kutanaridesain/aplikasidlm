<?php

/**
 * This is the model class for table "PUSKESMAS".
 *
 * The followings are the available columns in table 'PUSKESMAS':
 * @property string $id
 * @property string $nama_puskesmas
 * @property integer $jenis_puskesmas
 * @property string $alamat_puskesmas
 * @property integer $jlh_dokter_umum
 * @property integer $jlh_dokter_gigi
 * @property integer $jlh_tenaga_lab
 * @property integer $jlh_tenaga_farmasi
 * @property integer $jlh_perawat
 * @property integer $jlh_ruang_inap
 * @property integer $jlh_ruang_layanan
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property USER $user
 * @property RIWAYATKESEHATAN[] $rIWAYATKESEHATANs
 */
class Puskesmas extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Puskesmas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PUSKESMAS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_puskesmas, jenis_puskesmas, alamat_puskesmas, jlh_dokter_umum, jlh_dokter_gigi, jlh_tenaga_lab, jlh_tenaga_farmasi, jlh_perawat, jlh_ruang_inap, jlh_ruang_layanan', 'required'),
			array('jenis_puskesmas, jlh_dokter_umum, jlh_dokter_gigi, jlh_tenaga_lab, jlh_tenaga_farmasi, jlh_perawat, jlh_ruang_inap, jlh_ruang_layanan, userid', 'numerical', 'integerOnly'=>true),
			array('nama_puskesmas', 'length', 'max'=>200),
			array('alamat_puskesmas', 'length', 'max'=>1000),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama_puskesmas, jenis_puskesmas, alamat_puskesmas, jlh_dokter_umum, jlh_dokter_gigi, jlh_tenaga_lab, jlh_tenaga_farmasi, jlh_perawat, jlh_ruang_inap, jlh_ruang_layanan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
			'rIWAYATKESEHATANs' => array(self::HAS_MANY, 'RIWAYATKESEHATAN', 'id_puskesmas'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_puskesmas' => 'Nama Puskesmas',
			'jenis_puskesmas' => 'Jenis Puskesmas',
			'nama_jenis_puskesmas' => 'Jenis Puskesmas',
			'alamat_puskesmas' => 'Alamat Puskesmas',
			'jlh_dokter_umum' => 'Jumlah Dokter Umum',
			'jlh_dokter_gigi' => 'Jumlah Dokter Gigi',
			'jlh_tenaga_lab' => 'Jumlah Tenaga Laboratorium',
			'jlh_tenaga_farmasi' => 'Jumlah Tenaga Farmasi',
			'jlh_perawat' => 'Jumlah Perawat',
			'jlh_ruang_inap' => 'Jumlah Ruang Inap',
			'jlh_ruang_layanan' => 'Jumlah Ruang Layanan',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nama_puskesmas',$this->nama_puskesmas,true);
		$criteria->compare('jenis_puskesmas',$this->jenis_puskesmas);
		$criteria->compare('alamat_puskesmas',$this->alamat_puskesmas,true);
		$criteria->compare('jlh_dokter_umum',$this->jlh_dokter_umum);
		$criteria->compare('jlh_dokter_gigi',$this->jlh_dokter_gigi);
		$criteria->compare('jlh_tenaga_lab',$this->jlh_tenaga_lab);
		$criteria->compare('jlh_tenaga_farmasi',$this->jlh_tenaga_farmasi);
		$criteria->compare('jlh_perawat',$this->jlh_perawat);
		$criteria->compare('jlh_ruang_inap',$this->jlh_ruang_inap);
		$criteria->compare('jlh_ruang_layanan',$this->jlh_ruang_layanan);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	/* nama zakat */
	private $nama_jenis_puskesmas;
	public function setNama_jenis_puskesmas($name) {
	  $this->$nama_jenis_puskesmas = $name;
	}
	
	public function getNama_jenis_puskesmas() {
		$arrOptions = DlmActiveRecord::getPuskesmasOptions();
		$index = $this->jenis_puskesmas;
		if($index > 0){
			$this->nama_jenis_puskesmas = $arrOptions[$index];
		}
		return $this->nama_jenis_puskesmas;
	}
	
	public static function getNamaPuskesmasById($idx) {
		
		$arrOptions = DlmActiveRecord::getPuskesmasOptions();

		if($idx > 0){
			return $arrOptions[$idx];
		}else{
			return "";
		}
	}
}