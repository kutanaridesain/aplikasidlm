<?php

/**
 * This is the model class for table "KOPERASI".
 *
 * The followings are the available columns in table 'KOPERASI':
 * @property integer $id
 * @property integer $id_kab
 * @property string $nama_koperasi
 * @property string $ketua_koperasi
 * @property string $bidang_usaha
 * @property string $no_telp
 * @property string $alamat
 * @property integer $userid
 * @property string $tglinput
 * @property string $tglupdate
 *
 * The followings are the available model relations:
 * @property KABUPATEN $idKab
 * @property USER $user
 * @property BANTUANMODAL[] $bANTUANMODALs
 * @property BANTUANLAINNYA[] $bANTUANLAINNYAs
 */
class Koperasi extends DlmActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Koperasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'KOPERASI';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kab, nama_koperasi, ketua_koperasi, bidang_usaha, no_telp, alamat', 'required'),
			array('id_kab', 'numerical', 'integerOnly'=>true),
			array('nama_koperasi, bidang_usaha', 'length', 'max'=>300),
			array('ketua_koperasi', 'length', 'max'=>200),
			array('no_telp', 'length', 'max'=>40),
			array('alamat', 'length', 'max'=>1000),
			array('tglupdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_kab, nama_koperasi, ketua_koperasi, bidang_usaha, no_telp, alamat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idKab' => array(self::BELONGS_TO, 'KABUPATEN', 'id_kab'),
			'user' => array(self::BELONGS_TO, 'USER', 'userid'),
			'bANTUANMODALs' => array(self::HAS_MANY, 'BANTUANMODAL', 'id_koperasi'),
			'bANTUANLAINNYAs' => array(self::HAS_MANY, 'BANTUANLAINNYA', 'id_koperasi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_kab' => 'Nama Kabupaten',
			'nama_kab' => 'Nama Kabupaten',
			'nama_koperasi' => 'Nama Koperasi',
			'ketua_koperasi' => 'Ketua Koperasi',
			'bidang_usaha' => 'Bidang Usaha',
			'no_telp' => 'No. Telepon',
			'alamat' => 'Alamat',
			'userid' => 'Userid',
			'tglinput' => 'Tglinput',
			'tglupdate' => 'Tglupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$roleid = Yii::app()->user->getRoleId();

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_kab',$this->id_kab);
		$criteria->compare('nama_koperasi',$this->nama_koperasi,true);
		$criteria->compare('ketua_koperasi',$this->ketua_koperasi,true);
		$criteria->compare('bidang_usaha',$this->bidang_usaha,true);
		$criteria->compare('no_telp',$this->no_telp,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('tglinput',$this->tglinput,true);
		$criteria->compare('tglupdate',$this->tglupdate,true);
		if($roleid>1){
			$kabid = Yii::app()->user->getKabupatenId();
			$criteria->addCondition(
				'"id_kab" = ' . $kabid
			);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/* beforeSave */
	protected function beforeSave()
	{
		if(parent::beforeSave()){   
			return true;
	   }
		
		return false;
	}
	
	protected function afterFind()
	{
		$roleid = Yii::app()->user->getRoleId();
		
		$act = Yii::app()->controller->action->id;
		
		if( $act != "index"){
			if($roleid>1){
				$kabid = Yii::app()->user->getKabupatenId();
				if($this->id_kab != $kabid){	
					throw new CHttpException(401,'Tidak ada hak akses.');
				}
			}
		}
	}
	
	/* kabupaten */
	private $nama_kab;
	public function setNama_kab($name) {
	  $this->nama_kab = $name;
	}

	public function getNama_kab() {
		if($this->idKab != null){
			$this->nama_kab = $this->idKab->nama_kabkot;
		}
		return $this->nama_kab;
	}
}