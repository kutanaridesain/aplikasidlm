<?php

class GaleryController extends Controller
{
	const GALERY_BASE_PATH = 'pictures/galery/';

	public function actionIndex()
	{
		
		$baseUrl = Yii::app()->theme->baseUrl; 
		$cs = Yii::app()->getClientScript();

		$cs->registerCssFile($baseUrl.'/css/reset.css');
		$cs->registerCssFile($baseUrl.'/css/colorbox.css');
		$cs->registerCssFile($baseUrl.'/css/main.css');

		$cs->registerScriptFile($baseUrl.'/js/jquery.imagesloaded.js');
		$cs->registerScriptFile($baseUrl.'/js/jquery.colorbox-min.js');
		$cs->registerScriptFile($baseUrl.'/js/jquery.wookmark.min.js');

		$galleries = $this->_getGalleries();
		if(empty($galleries))$galleries = array('Galery kosong!');
		$this->render('pictures', array('pictures'=>$galleries));
	}

	public function actionPictures()
	{
		$galleries = $this->_getGalleries(Yii::app()->request->getParam('id'));
		if(empty($galleries))$galleries = array('Galery kosong!');
		// header('Content-type: application/json');
		// echo CJSON::encode($galleries);
		$this->renderPartial('pictures', array('pictures'=>$galleries));
		Yii::app()->end();
	}

	protected function _getGalleries($albumId)
	{
		$result = array();
		if (!empty($albumId)) {
			$result = glob(self::GALERY_BASE_PATH . $albumId . '/*.jpg');
		}else{
			$result = glob(self::GALERY_BASE_PATH . '*.jpg');
		}

		return $result;
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}