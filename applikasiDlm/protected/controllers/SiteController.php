<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('is_visible', '1');
		$criteria->order = '"tglinput" DESC';
		$modelArtikel = new Artikel;
		$artikels = $modelArtikel->findAll($criteria);
		$this->render('index',array(
			'artikels'=>$artikels
		));
	}

	/**
	* view artikel detail on the client area
	*/
	public function actionArtikel()
	{
		//criteria for quering
		$criteria = new CDbCriteria;
		$criteria->compare('is_visible', '1');
		$criteria->order = '"tglinput" DESC';
		//make model artikel instan
		$modelArtikel = new Artikel;
		$artikels = $modelArtikel->findAll($criteria);
		$this->menu = array();

		foreach ($artikels as $artikel) {
			if(Yii::app()->request->getParam('id') == $artikel->id){
				$this->menu[] = array('label'=>$artikel->title, 'url'=>array('artikel', 'id'=>$artikel->id), 'active'=>true);	
			}else{
				$this->menu[] = array('label'=>$artikel->title, 'url'=>array('artikel', 'id'=>$artikel->id));
			}
		}

		$this->layout='//layouts/column2-artikel';
		
		$id = Yii::app()->request->getParam('id');
		$artikel = $modelArtikel->findbyPk($id);
		$this->render('artikel',array(
			'artikel'=>$artikel
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect($this->createUrl('/manajemendata'));
				//$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}