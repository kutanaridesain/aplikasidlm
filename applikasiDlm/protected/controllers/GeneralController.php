<?php

class GeneralController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			' ajaxOnly + getKabupaten', // we only allow deletion via POST request
			' ajaxOnly + getKecamatanByKabupaten', 
			' ajaxOnly + actionGetGampongByKecamatan', 
		);
	}

	/**
	 * Return Kabupaten
	 * @param string $term the part of kabupaten name
	 */
	function actionGetKabupaten()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_kabkot" as "name"
				FROM kabupaten 
				WHERE  upper("nama_kabkot") like  upper(\'%'. $term. '%\')
				ORDER BY "nama_kabkot" ';
		
		$kabupatens = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($kabupatens);
	}
	
	function actionGetKabupatenByRole()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_kabkot" as "name"
				FROM kabupaten 
				WHERE  upper("nama_kabkot") like  upper(\'%'. $term. '%\')
				ORDER BY "nama_kabkot" ';
				
		
		$roleid = Yii::app()->user->getRoleId();
		$locaid = Yii::app()->user->getLocationId();
		if($roleid == 2){
			$id_kab = $locaid;
			$sql = 'SELECT "id" as "id", "nama_kabkot" as "name"
				FROM kabupaten 
				WHERE  upper("nama_kabkot") like  upper(\'%'. $term. '%\') 
				AND "id" = '. $locaid . ' 
				ORDER BY "nama_kabkot" ';
		}
		if($roleid == 3){
			//get kabupaten
			$id_kab = 0;
			$kec = Kecamatan::model()->findByPk($locaid);
			if($kec != null){
				$id_kab = $kec->id_kab;
			}
			$sql = 'SELECT "id" as "id", "nama_kabkot" as "name"
				FROM kabupaten 
				WHERE  upper("nama_kabkot") like  upper(\'%'. $term. '%\') 
				AND "id" = '. $id_kab . ' 
				ORDER BY "nama_kabkot" ';
		}
		if($roleid == 4){
			$id_kab = 0;
			$gampong = Gampong::model()->findByPk($locaid);
			if($gampong != null){
				$id_kab = $gampong->id_kab;
			}
			$sql = 'SELECT "id" as "id", "nama_kabkot" as "name"
				FROM kabupaten 
				WHERE  upper("nama_kabkot") like  upper(\'%'. $term. '%\') 
				AND "id" = '. $id_kab . ' 
				ORDER BY "nama_kabkot" ';
		}
		
		$kabupatens = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($kabupatens);
	}
	
	/**
	 * Return Kecamatan
	 * @param string $term the part of kecamatan name
	 * @param string $id the id of kabupaten
	 */
	function actionGetKecamatanByKabupaten()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		if(isset($_POST['id']))
		{
			$id = $_POST['id'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_kec" as "name"
				FROM kecamatan 
				WHERE  upper("nama_kec") like  upper(\'%'. $term. '%\')
				AND "id_kab" = '. $id .'
				ORDER BY "nama_kec" ';
				
				
		
		$kecamatans = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($kecamatans);
	}
	
	function actionGetKecamatanByKabupatenAndRole()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		if(isset($_POST['id']))
		{
			$id = $_POST['id'];
		}
		
		$roleid = Yii::app()->user->getRoleId();
		$locaid = Yii::app()->user->getLocationId();
		
		$sql = 'SELECT "id" as "id", "nama_kec" as "name"
				FROM kecamatan 
				WHERE  upper("nama_kec") like  upper(\'%'. $term. '%\')
				AND "id_kab" = '. $id .'
				ORDER BY "nama_kec" ';
				
		if($roleid == 3){
			$id_kec = $locaid;
			$sql = 'SELECT "id" as "id", "nama_kec" as "name"
				FROM kecamatan 
				WHERE  upper("nama_kec") like  upper(\'%'. $term. '%\')
				AND "id_kab" = '. $id .'
				AND "id" = '. $id_kec. ' 
				ORDER BY "nama_kec" ';
		}
		if($roleid == 4){
			$id_kec = 0;
			$gampong = Gampong::model()->findByPk($locaid);
			if($gampong != null){
				$id_kec = $gampong->id_kec;
			}
			$sql = 'SELECT "id" as "id", "nama_kec" as "name"
				FROM kecamatan 
				WHERE  upper("nama_kec") like  upper(\'%'. $term. '%\')
				AND "id_kab" = '. $id .'
				AND "id" = '. $id_kec. '
				ORDER BY "nama_kec" ';
		}		
		
		$kecamatans = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($kecamatans);
	}
	
	/**
	 * Return Gampong
	 * @param string $term the part of gampong name
	 * @param string $id the id of kecamatan
	 */
	function actionGetGampongByKecamatan()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		if(isset($_POST['id']))
		{
			$id = $_POST['id'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_gampong" as "name"
				FROM gampong 
				WHERE  upper("nama_gampong") like  upper(\'%'. $term. '%\')
				AND "id_kec" = '. $id .'
				ORDER BY "nama_gampong" ';
		
		$gampongs = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($gampongs);
	}
	
	function actionGetGampongByKecamatanAndRole()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		if(isset($_POST['id']))
		{
			$id = $_POST['id'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_gampong" as "name"
				FROM gampong 
				WHERE  upper("nama_gampong") like  upper(\'%'. $term. '%\')
				AND "id_kec" = '. $id .'
				ORDER BY "nama_gampong" ';
				
		$roleid = Yii::app()->user->getRoleId();
		$locaid = Yii::app()->user->getLocationId();
		
		if($roleid == 4){
			$sql = 'SELECT "id" as "id", "nama_gampong" as "name"
				FROM gampong 
				WHERE  upper("nama_gampong") like  upper(\'%'. $term. '%\')
				AND "id_kec" = '. $id .'
				AND "id" = '.$locaid . ' 
				ORDER BY "nama_gampong" ';
		}
		
		$gampongs = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($gampongs);
	}
	
	
	/**
	 * Return no_kk
	 * @param string $term the part of no_kk
	 */
	function actionGetNoKk()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "no_kk" as "id", "no_kk" as "name"
				FROM kepala_keluarga 
				WHERE  upper("no_kk") like  upper(\'%'. $term. '%\')
				ORDER BY "no_kk" ';
		
		$nokks = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($nokks);
	}
	
	/**
	 * Return nik
	 * @param string $term the part of nik
	 */
	function actionGetNik()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "nama_lgkp" as "id", "nik" as "name" 
				FROM biodata 
				WHERE  upper("nik") like  upper(\'%'. $term. '%\')
				ORDER BY "nik" ';
		
		$niks = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($niks);
	}
	
	/**
	 * Return nik kepala keluarga
	 * @param string $term the part of nik
	 */
	function actionGetNikKepalaKeluarga()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "nama_lgkp" as "id", "nik" as "name" 
				FROM biodata 
				WHERE  upper("nik") like  upper(\'%'. $term. '%\') 
				AND "nik" NOT IN (SELECT "nik" FROM kepala_keluarga ) 
				AND "stat_kwn" > 1
				AND "jenis_kelamin" = 1 
				ORDER BY "nik" ';
		
		$niks = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($niks);
	}
	
	/**
	 * Return nik anggota keluarga
	 * @param string $term the part of nik
	 */
	function actionGetNikAnggotaKeluarga()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "nama_lgkp" as "id", "nik" as "name" 
				FROM biodata 
				WHERE  upper("nik") like  upper(\'%'. $term. '%\') 
				AND "nik" NOT IN (SELECT "nik" FROM kepala_keluarga ) 
				AND "nik" NOT IN (SELECT "nik" FROM kepala_keluarga_details ) 
				ORDER BY "nik" ';
		
		$niks = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($niks);
	}
	
	/**
	 * Return id
	 * @param string $term the part of nama_pekerjaan
	 */
	function actionGetPekerjaan()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_pekerjaan" as "name"
				FROM pekerjaan 
				WHERE  upper("nama_pekerjaan") like  upper(\'%'. $term. '%\')
				ORDER BY "nama_pekerjaan" ';
		
		$occupations = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($occupations);
	}
	
	/**
	 * Return id
	 * @param string $term the part of nama_sekolah
	 */
	function actionGetSekolah()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_sekolah" as "name"
				FROM sekolah 
				WHERE  upper("nama_sekolah") like  upper(\'%'. $term. '%\')
				ORDER BY "nama_sekolah" ';
		
		$schools = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($schools);
	}
	
	/**
	 * Return Penyakit
	 * @param string $term the part of penyakit name
	 */
	function actionGetPenyakit()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_penyakit" as "name"
				FROM penyakit 
				WHERE  upper("nama_penyakit") like  upper(\'%'. $term. '%\')
				ORDER BY "nama_penyakit" ';
		
		$penyakits = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($penyakits);
	}
	
	/**
	 * Return Puskesmas
	 * @param string $term the part of puskesmas name
	 */
	function actionGetPuskesmas()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_puskesmas" as "name"
				FROM puskesmas 
				WHERE  upper("nama_puskesmas") like  upper(\'%'. $term. '%\')
				ORDER BY "nama_puskesmas" ';
		
		$puskesmases = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($puskesmases);
	}
	
	/**
	 * Return id
	 * @param string $term the part of nik
	 */
	function actionGetKKByNIK()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = "";
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "no_kk" as "id", "nik" as "name"
				FROM kepala_keluarga 
				WHERE  upper("nik") like  upper(\'%'. $term. '%\')
				ORDER BY "nik" ';
		
		$kks = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($kks);
	}
	
	/**
	 * Return id
	 * @param string $term the part of kk
	 */
	function actionGetNIKByKK()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = "";
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "nik" as "id", "no_kk" as "name"
				FROM kepala_keluarga 
				WHERE  upper("no_kk") like  upper(\'%'. $term. '%\')
				ORDER BY "no_kk" ';
		
		$niks = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($niks);
	}
	
	/**
	 * Return nik
	 * @param string $term the part of nik
	 */
	function actionGetBiodataViewByNik()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "nama_lgkp" as "id", "nik" as "name", "no_kk" as "extra"
				FROM view_biodata 
				WHERE  upper("nik") like  upper(\'%'. $term. '%\')
				ORDER BY "nik" ';
		
		$niks = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($niks);
	}
	
	/**
	 * Return name
	 * @param string $term the part of name
	 */
	function actionGetBiodataViewByNama()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "nik" as "id", "nama_lgkp" as "name", "no_kk" as "extra"
				FROM view_biodata 
				WHERE  upper("nama_lgkp") like  upper(\'%'. $term. '%\')
				ORDER BY "nama_lgkp" ';
		
		$namas = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($namas);
	}
	
	/**
	 * Return nik
	 * @param string $term the part of nik
	 */
	function actionGetBiodataViewByNoKK()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "nama_lgkp" as "id", "no_kk" as "name", "nik" as "extra"
				FROM view_biodata 
				WHERE  upper("no_kk") like  upper(\'%'. $term. '%\')
				ORDER BY "no_kk" ';
		
		$nokks = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($nokks);
	}
	
	/**
	 * Return id
	 * @param string $term the part of nama_instansi
	 */
	function actionGetInstansi()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_instansi" as "name"
				FROM instansi 
				WHERE  upper("nama_instansi") like  upper(\'%'. $term. '%\')
				ORDER BY "nama_instansi" ';
		
		$instansis = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($instansis);
	}
	
	/**
	 * Return id
	 * @param string $term the part of nama_koperasi
	 */
	function actionGetKoperasi()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		$id = 0;
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$sql = 'SELECT "id" as "id", "nama_koperasi" as "name"
				FROM koperasi 
				WHERE  upper("nama_koperasi") like  upper(\'%'. $term. '%\')
				ORDER BY "nama_koperasi" ';
		
		$koperasis = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($koperasis);
	}
	
	/**
	 * Return Location
	 * @param string $term the part of location name
	 */
	function actionGetLocation()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(404);
			
		$term = "";
		if(isset($_POST['term']))
		{
			$term = $_POST['term'];
		}
		
		$role = "";
		if(isset($_POST['role']))
		{
			$role = $_POST['role'];
		}
		
		$sql = 'SELECT "id" as "id", "name" as "name"
				FROM view_user_location 
				WHERE  upper("name") like  upper(\'%'. $term. '%\') 
				ORDER BY "name" ';
		
		if($role > 1){
			$sql = 'SELECT "id" as "id", "name" as "name"
				FROM view_user_location 
				WHERE  upper("name") like  upper(\'%'. $term. '%\') 
				AND "role_id" = '.$role .' 
				ORDER BY "name" ';
		}
		
		$locations = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo CJSON::encode($locations);
	}
	
	/* filter biodata
	protected function loadBiodata($nik) {
		if($this->_biodata===null)
		{
			$this->_biodata=Biodata::model()->findbyPk($nik);
			if($this->_biodata===null)
			{
			throw new CHttpException(404,'Biodata tidak valid.'); 
			}
		}
		return $this->_biodata; 
	} 
	
	protected function filterBiodataContext($filterChain)
	{ 
		$nik = null;
		if(isset($_GET['nik'])) {
			$nik = $_GET['nik'];
		}else{
			if(isset($_POST['nik'])) 
				$nik = $_POST['nik'];
		}
		
		$this->loadBiodata($nik); 
		
		$filterChain->run(); 
	} */
}
