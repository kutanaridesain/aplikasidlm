<div id="main" role="main">
  <ul id="tiles">
  <?php foreach ($pictures as $pic): ?>
    <li>
      <a href="/<?php echo $pic; ?>" rel="lightbox">
        <img alt="<?php echo $pic; ?>" width="200" src="/<?php echo $pic; ?>">
      </a>
    </li>
  <?php endforeach;?>
  </ul>
</div>
<script type="text/javascript">
jQuery(function($) {
  // $("#tiles").imagesLoaded(function() {
        // Prepare layout options.
        var options = {
          autoResize: true, // This will auto-update the layout when the browser window is resized.
          container: $("#main"), // Optional, used for some extra CSS styling
          offset: 2, // Optional, the distance between grid items
          itemWidth: 210 // Optional, the width of a grid item
        };

        // Get a reference to your grid items.
        var handler = $("#tiles li");

        // Call the layout function.
        handler.wookmark(options);
        console.log(handler);
        // Init lightbox
        $("a", handler).colorbox({
          rel: "lightbox"
        });
      // });
});
</script>