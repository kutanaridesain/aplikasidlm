<div class="page-header">
	<h1>Login <small>ke akun anda</small></h1>
</div>
<div class="row-fluid">
    <div class="span4 offset4 boxlogin">
        <!-- <div class="navbar"> -->
            <!-- <div class="navbar-inner"> -->
                    <p>Silahkan masukkan data login anda:</p>
                <div class="form navbar-search">
                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'login-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                )); ?>
                
                    <p class="note"><?php echo Yii::t('dlm', 'required field'); ?></p>
                
                    <div class="row">
                        <?php echo $form->labelEx($model,'username'); ?>
                        <?php echo $form->textField($model,'username'); ?>
                        <?php echo $form->error($model,'username'); ?>
                    </div>
                
                    <div class="row">
                        <?php echo $form->labelEx($model,'password'); ?>
                        <?php echo $form->passwordField($model,'password'); ?>
                        <?php echo $form->error($model,'password'); ?>
                    </div>
                
                    <div class="row rememberMe">
                        <?php echo $form->checkBox($model,'rememberMe'); ?>
                        <?php echo $form->label($model,'rememberMe'); ?>
                        <?php echo $form->error($model,'rememberMe'); ?>
                    </div>
                
                    <div class="row buttons">
                        <?php echo CHtml::submitButton('Login',array('class'=>'btn btn btn-primary span3')); ?>
                    </div>
                
                <?php $this->endWidget(); ?>
                </div><!-- form -->
            <!-- </div> -->
        <!-- </div> -->
    </div>
</div>
<br/>