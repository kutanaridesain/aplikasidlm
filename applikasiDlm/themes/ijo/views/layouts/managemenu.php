<?php
/**
 * Created by Fery Putra Tarigan.
 * User: kutanaridesain
 * Email: kutanari@yahoo.com
 * Date: 7/19/13
 * Time: 8:03 AM
 */
 
$managemenu=array(
        array('label' => 'Galery', 'url'=>array('/manajemendata/galery')),
        array('label' => 'Artikel', 'url'=>array('/manajemendata/artikel')),
        array('label'=>'Biodata', 'url'=>array('/manajemendata/biodata')),
        array('label'=>'Kartu Keluarga', 'url'=>array('/manajemendata/kartukeluarga')),
        array('label'=>'Kepemilikan Aset', 'url'=>array('/manajemendata/kepemilikanasset')),
        array('label'=>'Rumah', 'url'=>array('/manajemendata/rumah')),
        array('label'=>'Zakat', 'url'=>array('/manajemendata/zakat')),
        array('label'=>'Dayah', 'url'=>array('/manajemendata/dayah')),
        array('label'=>'Instansi', 'url'=>array('/manajemendata/instansi')),
        array('label'=>'Kelompok', 'url'=>array('/manajemendata/kelompok')),
        array('label'=>'Koperasi', 'url'=>array('/manajemendata/koperasi')),
		
        array('label'=>'Gampong', 'url'=>array('/manajemendata/gampong')),
        array('label'=>'Kabupaten', 'url'=>array('/manajemendata/kabupaten')),
        array('label'=>'Kecamatan', 'url'=>array('/manajemendata/kecamatan')),
        array('label'=>'Panti Asuhan', 'url'=>array('/manajemendata/pantiasuhan')),
        array('label'=>'Pekerjaan', 'url'=>array('/manajemendata/pekerjaan')),
        array('label'=>'Penyakit', 'url'=>array('/manajemendata/penyakit')),
        array('label'=>'Puskesmas', 'url'=>array('/manajemendata/puskesmas')),
        array('label'=>'Sekolah', 'url'=>array('/manajemendata/sekolah')),
    );

$roleid = Yii::app()->user->getRoleId();

if($roleid > 1){
	$managemenu=array(
        array('label' => 'Artikel', 'url'=>array('/manajemendata/artikel')),
        array('label'=>'Biodata', 'url'=>array('/manajemendata/biodata')),
        array('label'=>'Kartu Keluarga', 'url'=>array('/manajemendata/kartukeluarga')),
        array('label'=>'Kepemilikan Aset', 'url'=>array('/manajemendata/kepemilikanasset')),
        array('label'=>'Rumah', 'url'=>array('/manajemendata/rumah')),
        array('label'=>'Zakat', 'url'=>array('/manajemendata/zakat')),
        array('label'=>'Dayah', 'url'=>array('/manajemendata/dayah')),
        array('label'=>'Instansi', 'url'=>array('/manajemendata/instansi')),
        array('label'=>'Kelompok', 'url'=>array('/manajemendata/kelompok')),
        array('label'=>'Koperasi', 'url'=>array('/manajemendata/koperasi')),
    );
}