<?php $this->beginContent('//layouts/main'); ?>

  <div class="row-fluid">
	<div class="span3">
		<div class="sidebar-nav sidebar-nav2">
		  <?php $this->widget('zii.widgets.CMenu', array(
			/*'type'=>'list',*/
            'id'=>'sideMenu',
			'encodeLabel'=>false,
			'items'=>array(
				array('label'=>'ARTIKEL LAINNYA','items'=>$this->menu, 'itemOptions'=>array('id'=>'operation')),
			),
			));?>
		</div>
    </div><!--/span-->
    <div class="span9">
    
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
			'homeLink'=>CHtml::link('Dashboard'),
			'htmlOptions'=>array('class'=>'breadcrumb')
        )); ?><!-- breadcrumbs -->
    <?php endif?>
    
    <!-- Include content pages -->
    <?php echo $content; ?>

	</div><!--/span-->
  </div><!--/row-->
<?php $this->endContent(); ?>