<?php $this->beginContent('//layouts/main'); ?>

  <div class="row-fluid">
	<div class="span3">
		<div class="sidebar-nav">
          <?php require_once('managemenu.php')?>
		  <?php $this->widget('zii.widgets.CMenu', array(
			/*'type'=>'list',*/
            'id'=>'sideMenu',
			'encodeLabel'=>false,
			'items'=>array(
//				// Include the operations menu
				array('label'=>'PROSES MANAJEMEN','items'=>$this->menu, 'itemOptions'=>array('id'=>'operation')),
                array('label'=>'MANAJEMEN DATA','items'=>$managemenu, 'itemOptions'=>array('id'=>'managementMenu')),
			),
			));?>
		</div>
    </div><!--/span-->
    <div class="span9">
    
    <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
			// 'homeLink'=>CHtml::link('Dashboard'),
			'htmlOptions'=>array('class'=>'breadcrumb')
        )); ?><!-- breadcrumbs -->
    <?php endif?>
    
    <!-- Include content pages -->
    <?php echo $content; ?>

	</div><!--/span-->
  </div><!--/row-->

<script type="text/javascript">
    jQuery(function($){
        $('#operation a:contains("List")').prepend('<i class="icon-list"></i>&nbsp;');
        $('#operation a:contains("Daftar")').prepend('<i class="icon-list"></i>&nbsp;');
        $('#operation a:contains("Create")').prepend('<i class="icon-plus"></i>&nbsp;');
        $('#operation a:contains("Tambah")').prepend('<i class="icon-plus"></i>&nbsp;');
        $('#operation a:contains("Ubah")').prepend('<i class="icon-pencil"></i>&nbsp;');
        $('#operation a:contains("Hapus")').prepend('<i class="icon-minus"></i>&nbsp;');
        $('#operation a:contains("Detil")').prepend('<i class="icon-list-alt"></i>&nbsp;');
        $('#operation a:contains("Kembali")').prepend('<i class="icon-chevron-left"></i>&nbsp;');
        
        //update is deprecated
        $('#operation a:contains("Update")').prepend('<i class="icon-pencil"></i>&nbsp;');
        $('#operation a:contains("Delete")').prepend('<i class="icon-minus"></i>&nbsp;');
        $('#managementMenu a').prepend('<i class="icon-tasks"></i>&nbsp;');
    });
</script>
<?php $this->endContent(); ?>