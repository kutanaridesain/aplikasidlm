
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Aplikasi Data Layanan Masyarakat Provinsi Aceh </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Free yii themes, free web application theme">
    <meta name="author" content="Webapplicationthemes.com">
	<!--<link href='http://fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>-->

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
    <!-- Fav and Touch and touch icons -->
    <link rel="shortcut icon" href="<?php echo $baseUrl;?>/img/icons/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $baseUrl;?>/img/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $baseUrl;?>/img/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $baseUrl;?>/img/icons/apple-touch-icon-57-precomposed.png">
	<?php  
	  $cs->registerCssFile($baseUrl.'/css/bootstrap.min.css');
	  $cs->registerCssFile($baseUrl.'/css/bootstrap-responsive.min.css');
	  // $cs->registerCssFile($baseUrl.'//js/bootstrap-datepicker/css/datepicker.css');
	  $cs->registerCssFile($baseUrl.'/css/sleeky.css');
      $cs->registerCssFile($baseUrl.'/css/ijo.css');
	  //$cs->registerCssFile($baseUrl.'/css/style-blue.css');
	  ?>
      <!-- styles for style switcher -->
	  <?php
	  // $cs->registerScriptFile($baseUrl.'/js/bootstrap.min.js');
	  // $cs->registerScriptFile($baseUrl.'/js/bootstrap-datepicker/js/bootstrap-datepicker.js');
	  $cs->registerScriptFile($baseUrl.'/js/plugins/jquery.sparkline.js');
	  $cs->registerScriptFile($baseUrl.'/js/plugins/jquery.flot.min.js');
	  $cs->registerScriptFile($baseUrl.'/js/plugins/jquery.flot.pie.min.js');
	  $cs->registerScriptFile($baseUrl.'/js/plugins/jquery.knob.js');
	  $cs->registerScriptFile($baseUrl.'/js/plugins/jquery.masonry.min.js');
	  $cs->registerScriptFile($baseUrl.'/js/main.js');
	?>
  </head>

<body>

<section id="navigation-main">   
<!-- Require the navigation -->
<?php require_once('header.php')?>
</section><!-- /#navigation-main -->
    
<section class="main-body">
    <?php
    if(preg_match('/manajemendata/',$this->uniqueid) || preg_match('/galery/',$this->uniqueid)):
    ?>
        <div class="container-fluid">
    <?php else: ?>
    <header class="header subhead">
        <h1>Database Layanan Masyarakat</h1>
        <p>Nangroe Aceh Darusalam</p>
    </header>
    <div class="container-fluid container-fluid-home">
    <?php endif; ?>
            <!-- Include content pages -->
            <?php echo $content; ?>
    </div>
</section>

<!-- Require the footer -->
<?php require_once('tpl_footer.php')?>
<script type="text/javascript">
    $(function(){
        //console.log('start');
//        var $window = $(window)
//            $('.menu-sidenav').affix({
//                offset: {
//                    top:  260,
//                    bottom: 200
//                }
//            })
        <?php
        $module = explode('/', $this->uniqueid);
        if($module[0] != 'site'):
        ?>
        $('#sideMenu a[href*="<?php echo $this->id; ?>"]').addClass('active');
        $('#topMenu a[href*="<?php echo $module[0]; ?>"]').parent().addClass('active');
        <?php endif; ?>
        //console.log('end');
    });

</script>
  </body>
</html>