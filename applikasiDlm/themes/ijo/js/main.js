TextFieldAutoComplete=function(url, textBoxId, hiddenFieldId, textraFieldId){
	var data;
		
	$('#'+textBoxId).typeahead({
		source: function (query, process) {
			var items = {};
			 map = {};
			jQuery.ajax({
				 'url':url,
				 'cache':false,
				 'type': "POST",
				 'data': parameter,
				 'beforeSend':function(xhr){
			 		$('#'+textBoxId).next().html('<img src="/themes/ijo/img/loader3.gif" alt="loader" />');
				 },
				 'success':function(data){
					items = [];
					map = [];
					$.each(jQuery.parseJSON(data), function (i, state) {
						map[state.name] = state;
						items.push(state.name);
					});
					$('#'+textBoxId).next().html('<i class="icon-search"></i>');
					process(items); 
				}
			});
		},
		updater: function (item) {
			selectedState = map[item].id;
			$("#"+hiddenFieldId).val(selectedState);
			if(textraFieldId != undefined){
				$("#"+textraFieldId).val(map[item].extra);
			}
			return item;
		},
		matcher: function (item) {
			 if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
				return true;
			}
		},
		sorter: function (items) {
			 return items.sort();
		},
		highlighter: function (item) {
		    var regex = new RegExp( '(' + this.query + ')', 'gi' );
			return item.replace( regex, "<strong>$1</strong>" );
		},
	});
}

var GeneratePiclist = function(elmContainer, data){
	var ul = '<ul class="thumbnails"></ul>';
	for (var i = data.length - 1; i >= 0; i--) {
		var img = '<li class="span3"><a href="#" class="thumbnail"><img src="/'+data[i]+'" alt="'+data[i]+'" width="100" height="100" /></a></li>'; 
		console.log(data[i]);
		$(ul).append(img);
	};

	$(elmContainer).html(ul);
} 